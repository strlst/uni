# GDS Eingangstest Numerik

## Versuch 1

### 1

$$ Festpunkt(x) = 111001001; n = 4 $$

$$ Festpunkt(x) = 111001001 = -(1100.1001)_2 $$
$$ -(1100.1001)_2 = -1 * (2^3 + 2^2 + 2^{-1} + 2^{-4}) $$
$$ -(1100.1001)_2 = -1 * (8 + 4 + 1/2 + 1/16) $$
$$ -(1100.1001)_2 = -8 - 4 - 8/16 - 1/16 $$
$$ -(1100.1001)_2 = -12 - 9/16 $$
$$ -(1100.1001)_2 = -(12.5625)_{10} $$

### 2

```IEEE 754-2008, 16 bit, round to nearest - round to even```

$$ f(x) = F(2, 11, -14, 15,true) $$

$$ f(1010001000110111) + f(0111000101101001) $$

die Aufgabenstellung:

```
  vz | e   | m          |g|r|s
  1   01000 1000 1101 11
+ 0   11100 0101 1010 01
```

##### Exzessberechnung

die Exzess enkodierten Zahlen $$ e(...)_2 $$ werden in Dezimalzahlen $$ _{dz}(x) $$ umgerechnet

$$ x + e = e(01000)_2 $$
$$ x = e(01000)_2 - e $$
$$ -x = e - e(01000)_2 $$

```
  01111
- 01000
= 00111
```

$$ x = -(00111)_2 $$
$$ x = (00111)_2 = -(7)_{10} $$

$$ y + e = e(11100)_2 $$
$$ y = e(11100)_2 - e $$

```
  11100
- 01111
= 00101
```

$$ y = (00101)_2 = (5)_{10} $$

##### Stellenunterschiedberechnung

der Stellenunterschied der zwei in der Aufgabenstellung gegebenen Exponenten

```
  11100
- 01000
= 10100
```

beträgt

$$ (10100)_2 = (20)_{10} $$

woraus sich folgende Rechnung ergibt:

```
  vz | e   | m          |g|r|s
  1   11100 0000 0000 00 0 0 0000 0000 0000 1101 11
 +0   11100 0101 1010 01
= 0   11100 0101 1010 01
 -0   11100 0000 0000 00 0 0 0000 0000 0000 1101 11
= 0   11100 0101 1010 01
```

nach jedem Rundungsschema

### 3

```IEEE 754-2008, 16 bit, round to nearest - round to even```

$$ f(x) = F(2, 11, -14, 15,true) $$

$$ f(1111010000010010) - f(1101000100001101) $$

die Aufgabenstellung:

```
  vz | e   | m          |g|r|s
  1   11101 0000 0100 10
 -1   10100 0100 0011 01
```

der Stellenunterschied der zwei in der Aufgabenstellung gegebenen Exponenten

```
  11101
- 10100
= 01001
```

beträgt

$$ (01001)_2 = (9)_{10} $$

woraus sich folgende Rechnung ergibt:

```
  vz | e   | m          |g|r|s
  1   11101 0000 0100 10
 -1   10100 0100 0011 01
= 1   11101 0000 0100 10
 -1   11101 0000 0000 00 1 0 0 0011 01
= 1   11101 0000 0100 10
 -1   11101 0000 0000 00 1 0 1
= 1   11101 0000 0100 10
 -1   11101 0000 0000 00 1 0 1
= 1   11101 0000 0100 10
 -1   11101 0000 0000 00
= 1   11101 0000 0011 11
```

das Ergebnis

$$ (1111010000010001)_2 $$

### 4

```IEEE 754-2008, 16 bit, round to nearest - round to even```

$$ f(x) = F(2, 11, -14, 15,true) $$

$$ f(1101001101111010) * f(0010100000100001) $$

die Aufgabenstellung:

```
  vz | e   | m          |g|r|s
  1   10100 1101 1110 10
 *0   01010 0000 1000 01
```

gemeinsamer Exponent wird berechnet:

$$ e(10100)_2 - e + e(01010)_2 $$

$$ e(10100)_2 - e = $$

```
  10100
 -01111
= 00101
```

$$ e(10100)_2 - e + e(01010)_2 = $$

```
  00101
 +01010
= 01111
```

Probe:

```
  11110
 -01111
  01111
```

Mantissenmultiplikation (ohne die implizite 1 zu vergessen!):

```
 (1) 1101 1110 10 * (1) 0000 1000 01
  1  1101 1110 1000 0000 0000
 +   0000 0000 0000 0000 0000
 +    000 0000 0000 0000 0000
 +     00 0000 0000 0000 0000
 +      0 0000 0000 0000 0000
 +        1110 1111 0100 0000
 +         000 0000 0000 0000
 +          00 0000 0000 0000
 +           0 0000 0000 0000
 +             0000 0000 0000
 +              111 0111 1010
=(1) 1110 0001 0000 1011 1010
```

das Vorzeichen wird geXORed, die Exponenten werden nach obigen Schema zusammengerechnet, und das Mantissenmultiplikationsergebnis abgeschrieben:

```
  vz | e   | m          |g|r|s
  1   10100 1101 1110 10
 *0   01010 0000 1000 01
= 1   01111 1101 1110 10
 *0   01111 0000 1000 01
= 1   01111 1110 0001 00 0 0 1 011 1010
= 1   01111 1110 0001 00 0 0 1
= 1   01111 1110 0001 00
```

das Ergebnis:

$$ (1011111110000100)_2 $$

### 5

$$ (10.011010)_2 $$

abrunden auf 3 Nachkommastellen (round min)

$$ (10.011)_2 $$

### 6

$$ (11.010001)_2 $$

optimal runden auf 3 Nachkommastellen (round to nearest - round away from zero)

$$ (11.010)_2 $$

### 7

$$ (10.101101)_2 $$

aufrunden auf 3 Nachkommastellen (round max)

$$ (10.110)_2 $$

### 7

$$ (10.011000)_2 $$

abschneiden ab 3 Nachkommastellen (truncate)

$$ (10.011)_2 $$
