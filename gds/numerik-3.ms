.TL
numerics entry test
.AU
strlst
.SH
attempt 3
.NH
.EQ
define repr `~sub { ~ $3 } ($1) sub { $2 ~ }`
define binary `repr($1, 2)`
define bin    `repr($1, 2)`
define is `~~=~~`
define corresponds `~{~= hat}~~`
delim $$
.EN
.PP
Given the following binary number $ bin(000010110) $, interpret it as $ repr(x, 2, fp) $ (where $ fp ... fixed point$) and calculate its decimal value. 
.EQ
bin(000010110) mark corresponds repr(000010110, 2, fp) is bin(00001.0110)
.EN
.EQ
lineup is 2 sup 1 + 2 sup -2 + 2 sup -3
.EN
.EQ
lineup is 2 + 1 smallover 4 + 1 smallover 8
.EN
.EQ
lineup is 2 + {2 + 1} smallover 8 is 2 + 3 smallover 8 is repr(2.375, 10)
.EN
.EQ
3 over 8 = 0.125 times 3 = 0.375
.EN
.NH
.PP
Add the following encoded binary numbers $ bin(1001110011001111), bin(0000011111010110) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   00111 0011001111
 +0   00001 1111010110
.EE
.ft

.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   00111 0011001111
  +0   00001 1111010110
 = 1   00111 0011001111 0 0 0 000
  +0   00001       1111 0 1 0 110
 = 1   00001 0011001111
  +0   00001 0000001111 0 1 0 110
 = 1   00001 0011001111
  +0   00001 0000001111 0 1 0 110
 = 1   00001 0011011110 0 1 0 110
 = 1   00001 0011011110 0 1 1
 = 1   00001 0011011110
.EE
.ft
.PP
The result is $ repr(1000010011011110, 2) $

.NH
.PP
Subtract the following encoded binary number $ bin(0110100111100110) $ from the following encoded binary number $ bin(1000101011100100) $, both numbers being represented using the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   00010 1011100100
 -0   11010 0111100110
.EE
.ft
.PP
Because our first number $ A $ is smaller than our second number $ B $, we can avoid overflow problems by reformulating the task:
.EQ
B > A ~: A - B is -~(B - A)
.EN
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   00010 1011100100
  -0   11010 0111100110
 = 1   11010 0111100110
  -1   00010 1011100100
  +1
.EE
.ft
.PP
Next we need to calculate the absolute exponent difference $ e sub { dist } = e sub B - e sub A ,~ e sub B > e sub A $
.LP
.ft CW
.EX
   11010
  -00010
 = 11000
.EE
.ft
.EQ
bin(11000) is 2 sup 3 + 2 sup 4 is 8 + 16 is repr(24, 10)
.EN
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   00010 1011100100
  -0   11010 0111100110
 = 1   11010 0111100110
  -1   00010 1011100100
  +1
 = 1   11010 0111100110
  -1   11010 0000000000 0 0 0 000000000001011100100
  +1
 = 1   11010 0111100110
.EE
.ft
.PP
The result is $ repr(1110100111100110, 2) $

.NH
.PP
Multiply the following encoded binary numbers $ bin(0100110011111010), bin(0100010100000001) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   10011 0011111010
 *0   10001 0100000001
.EE
.ft
.PP
We XOR the sign bit, add the exponents and multiply the mantissas. It should be noted, because adding the exponents would cause an overflow, we subtract the excess number first. 
.EQ
e sub { common } is e sub A - e + e sub B
.EN
.LP
.ft CW
.EX
   10011
  -01111
  +10001
 = 00100
  +10001
 = 10101
.ft
.EE
.PP
Thus our common exponent is $ bin(10101) $. Our sign bit remains $ 0 $.
.LP
.ft CW
.EX
 (1) 0011 1110 10 * (1) 0100 0000 01

= 1  0011 1110 10
 +   0000 0000 00 0
 +    100 1111 10 10
 +     00 0000 00 000
 +      0 0000 00 0000
 +        0000 00 0000 0
 +         000 00 0000 00
 +          00 00 0000 000
 +           0 00 0000 0000
 +             00 0000 0000 0
 +              1 0011 1110 10

= 1  1000 1110 01 1011 1110 10
.EE
.ft
.PP
Taking our results thus far, we can finalize our calculations:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   10011 0011111010
  *0   10001 0100000001
 = 0   10101 1000111001 1 0 1 1111010
 = 0   10101 1000111001 1 0 1
 = 0   10101 1000111010
.EE
.EE
.ft
.PP
The result is $ bin(0101011000111010) $
