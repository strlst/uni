.TL
numerics entry test
.AU
strlst
.SH
attempt 4
.NH
.EQ
define repr `~sub { ~ $3 } ($1) sub { $2 ~ }`
define binary `repr($1, 2)`
define bin    `repr($1, 2)`
define is `~~=~~`
define corresponds `~{~= hat}~~`
delim $$
.EN
.PP
Given the following binary number $ bin(100001010) $, interpret it as $ repr(x, 2, fp) $ (where $ fp ... fixed point$) and calculate its decimal value. 
.EQ
bin(100001010) mark corresponds repr(100001010, 2, fp) is bin(0.1010) times -1
.EN
.EQ
lineup is ( 2 sup -1 + 2 sup -3 ) times -1
.EN
.EQ
lineup is ( 1 smallover 2 + 1 smallover 8 ) times -1
.EN
.EQ
lineup is {4 + 1} smallover 8 times -1 is -1 times repr(0.625, 10)
.EN
.EQ
5 over 8 = 0.125 times 5 = 0.625
.EN
.NH
.PP
Add the following encoded binary numbers $ A = bin(1000101011100100), B = bin(0110100111100110) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   00010 1011100100
 +0   11010 0111100110
.EE
.ft
.PP
Because $ A < B $, we adjust our exponent of number A to that of number B. Because we add $ e sub B - e sub A = bin(11000) $ to $ e sub A $, we shift by $ bin(11000) = 24 $ digits:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s

   1   00010 1011100100
  +0   11010 0111100110
 = 1   11010 0000000000 0 0 0 0000000000 (1) 1011100100 
  +0   11010 0111100110
 = 0   11010 0111100110
  -0   11010 0000000000 0 0 0 0000000000 (1) 1011100100 
 = 0   11010 0111100110
  -0   11010 0000000000 0 0 1 
 = 0   11010 0111100110
  -0   11010 0000000000 
 = 0   11010 0111100110
.EE
.ft
.PP
The result is $ repr(0110100111100110, 2) $

.NH
.PP
Subtract the following encoded binary number $ bin(0100001111111100) $ from the following encoded binary number $ bin(1100001010110010) $, both numbers being represented using the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   10000 1010110010
 -0   10000 1111111100
.EE
.ft
.PP
Because we are subtracting the positive number $ B $ from the negative number $ A $, we can reformulate our task:
.EQ
(-A) - (B) is (-A) + (-B)
.EN
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   10000 1010110010
  -0   10000 1111111100
 = 1   10000 1010110010
  +1   10000 1111111100
 = 1   10001 1101010111 0
 = 1   10001 1101010111

 aux calculation:
   (1)   1010110010
  +(1)   1111111100
 = 11    1010101110
 = (1) 1 1010101110
 = (1)   1101010111 0 => overflow => exp. correction
.EE
.ft
.PP
The result is $ repr(1100011101010111, 2) $

.NH
.PP
Multiply the following encoded binary numbers $ bin(0010010001100101), bin(0011000000010010) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   01001 0001100101
 *0   01100 0000010010
.EE
.ft
.PP
We XOR the sign bit, add the exponents and multiply the mantissas. Adding the exponents would not cause an overflow, so we can primitively add the exponents. 
.EQ
e sub { common } is e sub A + e sub B
.EN
.LP
.ft CW
.EX
   01001 
  +01100
 = 10101
.ft
.EE
.PP
Thus our common exponent is $ bin(10101) $. Our sign bit remains $ 0 $.
.LP
.ft CW
.EX
 (1) 0001 1001 01 * (1) 0000 0100 10

= 1  0001 1001 01
 +   0000 0000 00 0
 +    000 0000 00 00
 +     00 0000 00 000
 +      0 0000 00 0000
 +        0000 00 0000 0
 +         100 01 1001 01
 +          00 00 0000 000
 +           0 00 0000 0000
 +             10 0011 0010 1
 +              0 0000 0000 00

= 1  0001 1110 00 1100 0110 10
.EE
.ft
.PP
Taking our results thus far, we can finalize our calculations:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   01001 0001100101
  *0   01100 0000010010
 = 0   10101 0001111000 1 1 0 0011010
 = 0   10101 0001111000 1 1 1
 = 0   10101 0001111001
.EE
.EE
.ft
.PP
The result is $ bin(0101010001111001) $
