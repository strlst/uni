.TL
numerics entry test
.AU
strlst
.SH
attempt 6
.NH
.EQ
define repr `~sub { ~ $3 } ($1) sub { $2 ~ }`
define binary `repr($1, 2)`
define bin    `repr($1, 2)`
define is `~~=~~`
define corresponds `~{~= hat}~~`
delim $$
.EN
.PP
Given the following binary number $ bin(010110001) $, interpret it as $ repr(x, 2, fp) $ (where $ fp ... fixed point$) and calculate its decimal value. 
.EQ
bin(010110001) mark corresponds repr(010110001, 2, fp) is bin(1011.0001)
.EN
.EQ
lineup is ( 8 + 2 + 1 + 2 sup -4 )
.EN
.EQ
lineup is ( 11 + 1 smallover 16 )
.EN
.EQ
lineup is ( 11 + 1 smallover 16 ) is repr(11.0625, 10)
.EN
.NH
.PP
Add the following encoded binary numbers $ A = bin(0010101101001011), B = bin(0100001101111011) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   01010 1101001011
 +0   10000 1101111011

    e
   10000
  -01010
 = 00110
.EE
.ft
.PP
Because $ A < B $, we adjust our exponent of number A to that of number B. As we add $ e sub B - e sub A = bin(00110) $ to $ e sub A $, we shift by $ bin(00110) = repr(6, 10) $ digits:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   01010 1101001011
  +0   10000 1101111011
 = 0   10000 0000011101 0 0 1011
  +0   10000 1101111011
 = 0   10000 0000011101
  +0   10000 1101111011
 = 0   10000 1110011000
.EE
.ft
.PP
The result is $ repr(0100001110011000, 2) $

.NH
.PP
Subtract the following encoded binary number $ bin(0010000110100111) $ from the following encoded binary number $ bin(1011011011101111) $, both numbers being represented using the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   01000 0110100111
 -1   01101 1011101111

    e
   01101
  -01000
 = 00101
.EE
.ft
.PP
Because we are subtracting the negative number $ B $ from the positive number $ A $, we can reformulate our task:
.EQ
(A) - (-B) is A + B
.EN
.PP
Additionally, like for addition, we inspect the absolute exponent distance. Because $ A > -B $, we shift $ -B $ by $ e sub a - e sub b = bin(00101) = repr(5, 10) $ .
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   01000 0110100111
  -1   01101 1011101111
 = 0   01000 0110100111
  +0   01101 1011101111
 = 0   01101 0000101101 0 0 1 11
  +0   01101 1011101111
 = 0   01101 0000101101
  +0   01101 1011101111
 = 0   01101 1100011100
.EE
.ft
.PP
The result is $ repr(0011011100011100, 2) $

.NH
.PP
Multiply the following encoded binary numbers $ bin(0011001011101110), bin(1100100010001000) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   01100 1011101110
 *1   10010 0010001000
.EE
.ft
.PP
We XOR the sign bit, add the exponents and multiply the mantissas. Adding the exponents would not cause an overflow, but we should subtract $ e $ regardless. 
.EQ
e sub { common } is e sub B  - e + e sub A
.EN
.LP
.ft CW
.EX
   10010
  -01111
  +01100 
 = 00011
  +01100 
 = 01111
.ft
.EE
.PP
Thus our common exponent is $ bin(01111) $. Our sign bit is $ 1 ~~hat~ 0 = 1 $.
.LP
.ft CW
.EX
 (1) 1011 1011 10 * (1) 0010 0010 00

= 1  1011 1011 10
 +   0000 0000 00 0
 +    000 0000 00 00
 +     11 0111 01 110
 +      0 0000 00 0000
 +        0000 00 0000 0
 +         000 00 0000 00
 +          11 01 1101 110
 +           0 00 0000 0000
 +             00 0000 0000 0
 +              0 0000 0000 00

= 1  1111 0110 01 1001 1100 00
.EE
.ft
.PP
Taking our results thus far, we can finalize our calculations:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   01100 1011101110
  *1   10010 0010001000
 = 1   01111 1111011001 1 0 0 1110000
 = 1   01111 1111011001 1 0 1
 = 1   01111 1111011010
.EE
.EE
.ft
.PP
The result is $ bin(1011111111011010) $
