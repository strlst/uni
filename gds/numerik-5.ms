.TL
numerics entry test
.AU
strlst
.SH
attempt 5
.NH
.EQ
define repr `~sub { ~ $3 } ($1) sub { $2 ~ }`
define binary `repr($1, 2)`
define bin    `repr($1, 2)`
define is `~~=~~`
define corresponds `~{~= hat}~~`
delim $$
.EN
.PP
Given the following binary number $ bin(100011001) $, interpret it as $ repr(x, 2, fp) $ (where $ fp ... fixed point$) and calculate its decimal value. 
.EQ
bin(100011001) mark corresponds repr(100011001, 2, fp) is bin(1.1001) times -1
.EN
.EQ
lineup is ( 1 + 2 sup -1 + 2 sup -4 ) times -1
.EN
.EQ
lineup is ( 1 + 1 smallover 2 + 1 smallover 16 ) times -1
.EN
.EQ
lineup is ( 1 + {8 + 1} smallover 16 ) times -1 is -1 times repr(1.5625, 10)
.EN
.EQ
9 over 16 = 0.0625 times 9 = 1 smallover 2 + 0.0625 = 0.5625
.EN
.NH
.PP
Add the following encoded binary numbers $ A = bin(0010010001100101), B = bin(1100011101011011) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   01001 0001100101
 +1   10001 1101011011

    e
   10001
  -01001
 = 01000
.EE
.ft
.PP
Because $ A > B $, we adjust our exponent of number B to that of number A. As we add $ e sub B - e sub A = bin(01000) $ to $ e sub A $, we shift by $ bin(01000) = repr(8, 10) $ digits:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s

   0   01001 0001100101
  +1   10001 1101011011
 = 0   01001 0001100101 0 0 0 00000
  -0   01001        111 0 1 0 11011
 = 0   01001 0001100101
  -0   01001        111 0 1 1
 = 0   01001 0001011101 1 0 1
 = 0   01001 0001011110
.EE
.ft
.PP
The result is $ repr(0010010001011110, 2) $

.NH
.PP
Subtract the following encoded binary number $ bin(1011101000111001) $ from the following encoded binary number $ bin(0100101010111000) $, both numbers being represented using the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   10010 1010111000
 -1   01110 1000111001

    e
   10010
  -01110
 = 00100
.EE
.ft
.PP
Because we are subtracting the negative number $ B $ from the positive number $ A $, we can reformulate our task:
.EQ
(A) - (-B) is A + B
.EN
.PP
Additionally, like for addition, we inspect the absolute exponent distance. Because $ A > -B $, we shift $ -B $ by $ e sub a - e sub b = bin(00100) = repr(4, 10) $ .
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   10010 1010111000
  -1   01110 1000111001
 = 0   10010 1010111000
  +0   01110 1000111001
 = 0   10010 1010111000
  +0   10010 0001100011 1 0 0 1
 = 0   10010 1010111000
  +0   10010 0001100011 1 0 1
 = 0   10010 1100011011 1 0 1
 = 0   10010 1100011100
.EE
.ft
.PP
The result is $ repr(0100101100011100, 2) $

.NH
.PP
Multiply the following encoded binary numbers $ bin(1010100010110111), bin(1101001000001000) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   01010 0010110111
 *1   10100 1000001000
.EE
.ft
.PP
We XOR the sign bit, add the exponents and multiply the mantissas. Adding the exponents would not cause an overflow, but we should subtract $ e $ regardless. 
.EQ
e sub { common } is e sub B  - e + e sub A
.EN
.LP
.ft CW
.EX
   10100
  -01111
  +01010 
 = 00101
  +01010 
 = 01111
.ft
.EE
.PP
Thus our common exponent is $ bin(01111) $. Our sign bit is $ 1 ~~hat~ 1 = 0 $.
.LP
.ft CW
.EX
 (1) 0010 1101 11 * (1) 1000 0010 00

= 1  0010 1101 11
 +   1001 0110 11 1
 +    000 0000 00 00
 +     00 0000 00 000
 +      0 0000 00 0000
 +        0000 00 0000 0
 +         000 00 0000 00
 +          10 01 0110 111
 +           0 00 0000 0000
 +             00 0000 0000 0
 +              0 0000 0000 00

= 1  1100 0110 11 1110 1110 00
.EE
.ft
.PP
Taking our results thus far, we can finalize our calculations:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   01010 0010110111
  *1   10100 1000001000
 = 0   01111 1100011011 1 1 1 0111000
 = 0   01111 1100011011 1 1 1
 = 0   01111 1100011100
.EE
.EE
.ft
.PP
The result is $ bin(0011111100011100) $
