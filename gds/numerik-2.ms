.TL
numerics entry test
.AU
strlst
.SH
attempt 2
.NH
.EQ
define repr `~sub { ~ $3 } ($1) sub { $2 ~ }`
define binary `repr($1, 2)`
define bin    `repr($1, 2)`
define is `~~=~~`
define corresponds `~{~= hat}~~`
delim $$
.EN
.PP
Given the following binary number $ bin(101001010) $, interpret it as $ repr(x, 2, fp) $ (where $ fp ... fixed point$) and calculate its decimal value. 
.EQ
bin(101001011) mark corresponds repr(101001011, 2, fp) is bin(10100.1011)
.EN
.EQ
lineup is (2 sup 2 + 2 sup -1 + 2 sup -3 + 2 sup -4 ) times -1
.EN
.EQ
lineup is (4 + 1 smallover 2 + 1 smallover 8 + 1 smallover 16 ) times -1
.EN
.EQ
lineup is -4 + {8 + 2 + 1} smallover 16 is -4 + 11 smallover 16 is repr(-4.6875, 10)
.EN
.NH
.PP
Add the following encoded binary numbers $ bin(1011100011000101), bin(1110000010101100) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   01110 0011000101
 +1   11000 0010101100
.EE
.ft
.PP
We calculate the exponent distance:
.LP
.ft CW
.EX
  11000   trial: 01010
 -01110         +01110

= 01010          11000
.EE
.ft
.PP
We can interpret this absolute difference between our two exponents as a decimal "distance" between the two mantissa parts of our encoded binary numbers.
.EQ
bin(01010) is 2 sup 1 + 2 sup 3 is 2 + 8 is repr(10, 10)
.EN
.PP
As the absolute decimal difference of the exponent is $ 10 $, we can rewrite our encoded binary numbers to have the same exponent and shift the mantissa accordingly:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   01110 0011000101
  +1   11000 0010101100
 = 1   11000 0000000001 0 0 11000101
  +1   11000 0010101100
 = 1   11000 0000000001 0 0 1
  +1   11000 0010101100
 = 1   11000 0000000001
  +1   11000 0010101100
 = 1   11000 0010101101
.EE
.ft
.PP
The result is $ repr(1110000010101101, 2) $

.NH
.PP
Subtract the following encoded binary number $ bin(1010111100001111) $ from the following encoded binary number $ bin(1010111101110100) $, both numbers being represented using the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  1   01011 1101110100
 -1   01011 1100001111
.EE
.ft
.PP
As the absolute decimal difference of the exponent is $ 0 $, we can begin calculating:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   1   01011 1101110100
  -1   01011 1100001111
 = 1   01011 1101110100
  +0   01011 1100001111
 = 0   01011 1101110100
  -0   01011 1100001111
  +1
 = 1   01011 0001100101
 = 1   00111 1001010000
.EE
.ft
.PP
The result is $ repr(1010 1100 0110 0101, 2) $

.NH
.PP
Multiply the following encoded binary numbers $ bin(0111000010100011), bin(0011000100000010) $ represented by the system $ F(2,11,-14,15,true) $ (IEEE 754-2008 with half precision) using $ (round to nearest - round to even ) $ as a rounding scheme. The resulting number should be encoded in the same format. 
.PP
The task at hand can be represented as follows:
.LP
.ft CW
.EX
  vz | e   | m        |g|r|s
  0   11100 0010100011
 *0   01100 0100000010
.EE
.ft
.PP
We XOR the sign bit, add the exponents and multiply the mantissas. It should be noted, because adding the exponents would cause an overflow, we subtract the excess number first. 
.EQ
e sub { common } is e sub 1 - e + e sub 2
.EN
.LP
.ft CW
.EX
   11100
  -01111
  +01100
 = 01101
  +01100
 = 11001
.ft
.EE
.PP
Thus our common exponent is $ bin(11001) $. Our sign bit remains $ 0 $.
.LP
.ft CW
.EX
 (1) 0010 1000 11 * (1) 0100 0000 10

= 1  0010 1000 11
 +   0000 0000 00 0
 +    100 1010 00 11
 +     00 0000 00 000
 +      0 0000 00 0000
 +        0000 00 0000 0
 +         000 00 0000 00
 +          00 00 0000 000
 +           0 00 0000 0000
 +             10 0101 0001 1
 +              0 0000 0000 00

= 1  0111 0011 10 0001 0001 10
.EE
.ft
.PP
Taking our results thus far, we can finalize our calculations:
.LP
.ft CW
.EX
   vz | e   | m        |g|r|s
   0   11100 0010100011
  *0   01100 0100000010
 = 0   11001 0111001110 0 0 0 1000110
 = 0   11001 0111001110 0 0 1
 = 0   11001 0111001110
.EE
.ft
.PP
The result is $ bin(0110010111001110) $
