# DAVO Ausarbeitung (20.2.2022)

# gesammelte Prüfungsfragen

## Folien Teil 1 - Prozesse und Automatisierung

### Wie ist ein Prozess definiert

Ein Prozess ist die Gesamtheit von aufeinander wirkenden Vorgängen in einem System, durch die Materie, Energie oder Information umgeformt, transportiert oder gespeichert wird.

Ein technischer Prozess ist ein Prozess, dessen physikalische Größen mit technischen Mitteln erfasst und beeinflusst werden.

### Die drei Aufgaben eines Automatisierungssystems

1. messen, regeln, steuern
2. überwachen (anzeigen), melden (alarmieren)
3. optimieren

### Die drei Aufgaben eines Menschen bei Automatisierung

1. Bedienen
2. Leiten
3. Führen

### Nennen Sie mindestens 4 Fertigungsverfahren

Zu den Fertigungsverfahren gehören:

- umformen
- trennen
- fügen
- beschichten
- ändern von Stoffeigenschaften

### Welchte Arten von Prozesskopplungen gibt es?

- Handbedienter Prozess: anzeige zum Menschen und Steuerung durch Menschen
- indirekte Prozesskopplung
  - offline: wie handbedienter Prozess, aber mit Protokollierung durch einen Rechner abseits des technischen Prozesses
  - inline: wieder von Menschen kontrolliert, mit Erfassung der Zustandsdaten durch einen Menschen, Steuerung durch einen Rechner, aber endgültige Entscheidung beim Menschen
- direkte Prozesskopplung
  - Zustandserfassung: Rechner erfasst den Zustand, Zustandsabbild wird dem Menschen angezeigt, der Mensch steuert anschließend
  - Prozessbeeinflussung: Mensch lenkt, Rechner protokolliert und steuert
  - closed loop: Rechner misst und steuert, Mensch sieht Protokolle ein und gibt gegebenenfalls Anweisungen

### Welche zwei Arten der indirekten Prozesskopplung gibt es?

- offline
- inline

### Welche drei Arten der direkten Prozesskopplung gibt es?

- Zustandserfassung
- Prozessbeeinflussung
- closed loop

### Wie lautet die Formel für die Verfügbarkeit? In welcher Einheit wird diese gemessen?

$V = \frac{{MTBF}}{MTBF + MTTR}$

Verfügbarkeit ist ein Verhältnis (zwischen durchschnittlichen Zeiten, mean time between failure und mean time to repair)!

### Was ist eine heiße Reserve?

Bei einer heißen Reserve existieren mehrere Prozessrechner welche alle aktiv sind, die selben Prozessabbildungen erhalten und durch einen Voter verifiziert werden. Die heiße Reserve ist also eine Form der statischen Redundanz.

### Was ist eine kalte Reserve?

Bei einer kalten Reserve existieren mehrere Prozessrechner, aber nur einer davon ist aktiv (1 führender Rechner, n mitwirkende Rechner in stand-by). Bei einem Ausfall wird durch Fehlererkennung umgeschaltet. Die kalte Reserve ist also eine Form der dynamischen Redundanz

### Feldgeräte 3 <irgendwas> aufzählen (ungefährer Wortlaut)

- analoge Übertragung zur SPS
- analoge Übertragung mit Vorverarbeitung (Skalierung in Mikrocontroller, Anzeiger, DAC) zur SPS
- digitale Übertragung mit Kommunikationscontroller zwischen Feldgerät und SPS

### Bei seriellen Bussystemen wird oft asynchroner Zeitmultiplex betrieben. In welche beiden Verfahren kann dabei grob eingeteilt werden?

- deterministischer/geordneter/kontrollierter Buszugriff 
  - zentral: master/slave
  - dezentral: tokenring, TDMA
- nicht deterministischer/ungeordneter/zufälliger Buszugriff
  - CSMA (carrier sense multiple access, Trägerzustandserkennung mit mehrfachen Teilnehmern)
    - CD: collision detection
    - CA: collision avoidance

## Folien Teil 2 - SPS

### Aufzählen der charakteristischen Zeiten einer SPS und schätzen des Worst Case

Eingangsverzögerungszeit $t_E$, Ausgangsverzögerungszeit $t_A$, interne Bearbeitungszeit $t_I$, Zykluszeit $t_Z = \sum t_I$, Reaktionszeit $t_R = 2 \cdot (t_Z + t_E + t_A)$

### Ein Öffner wird betätigt, Wie lauten die Ergebnisse der Abfrage in einem SPS des Schließerkontakts und des Öffnerkontakts?

Ist der Schließer betätigt (logisch 1), oder Öffner (invertierter Schließer) nicht betätigt (logisch 0), kann das das Signal propagieren. Ist der Schließer nicht betätigt (logisch 0), oder Öffner betätigt (logisch 1), kann das Signal nicht propagieren.

### Zeichnene Sie einen KOP für folgende Bedingung: A0.0 == 1 if E0.0 == E0.1 und A0.1 == 1 if E0.0 != E0.1

`und` -> seriell, `oder` -> parallel, unterschiedliche Ausgänge -> unterschiedliche Netzwerke

```
Netzwerk 1:
    E0.0   E0.1      A0.0
|---|  |---|  |--+---(  )
|   E0.0   E0.1  |
|---|/ |---|/ |--+

Netzwerk 2:
    E0.0   E0.1      A0.1
|---|  |---|/ |--+---(  )
|   E0.0   E0.1  |
|---|/ |---|  |--+
```

### Unterschied Funktion/Funktionsblock IEC 61131

Ein Funktionsblock hat einen internen Variablenspeicher, während eine Funktion nur Eingänge und Ausgänge behandelt. Funktionsblock agiert als eine Art Objekt.

### Welche 5 Programmiersprachen sind in IEC 61131-3 definiert?

- Kontaktplan (KOP)
- Funktionsplan (FUP)
- Strukturierter Text (ST)
- Anweisungsliste (AWL)
- Ablaufsprache (AS)

### Wie ist der Funktionsblock vom IEC 61499 aufgebaut?

Der Funktionsblock aus IEC 61499 hat Event Eingänge, Event Ausgänge, Eingangsvariablen, Ausgangsvariablen, **Interne Variablen**, **einem oder mehrere Algorithmen** und einem **Execution Control Chart (ECC)**. Dabei werden Eingangsdaten mit Event Eingängen und Ausgangsdaten mit Event Ausgängen assoziiert. Die Algorithmen können in einer beliebigen Sprache programmiert sein. Das ECC ist eine Art state machine/Zustandsautomat.

Der Standard ist offen (portabel, interoperabel, konfigurierbar).

### Welche Aufgabe haben die ECC in Function Blocks nach IEC 61499 und wie sind sie aufgebaut?

Die ECCs sind Zustandsautomaten welche die Ausführung der Algorithmen verwalten. Dabei wird die Ausführung durch die Events gesteuert.

### Aus simple Function Blocks eine XOR Schaltung erstellen

- zwei Eingänge `x`, `y` vernetzt mit Event `req`
- ein Ausgang `z` vernetzt mit Event `CNF`
- `z = ((not x) and y) or ((not y) and x)` (compound function block)

### Welche zwei Service Primitives bei IEC 61499 gibt es?

- application initiated transactions
  - service wird gestartet, während Aufrechterhaltung der Verbindung werden requests serviciert, service wird wieder beendet
  - Applikation initiiert Schreibzugriffe auf Ressource, Ressource antwortet mit gelesenen Inputs
- resource initiated transactions: 
  - Ressource initiiert Lesezugriffe und leitet gelesene Inputs and die Applikation, Applikation initiiert Schreibzugriffe

### Ein IEC 61499 Funktionsblockdiagramm war gegeben, die Funktion sollte festgestellt werden

## Folien Teil 3 - ASi

ASi (AS-interface, Actuator Sensor interface)

### Wieviele E/A bei ASi?

max. 31 slaves pro Strang, bis zu 4 Sensoren/Aktoren pro Slave, also max. 124 E/A-Geräte.

### Erklären sie das ASi Protokoll

Es handelt sich um ein single master Bussystem mit zyklischem Polling das ungeschirmte Zweidrahtleitungen (für Daten aber auch Energie) benutzt. Eine beliebige Topologie kann gewählt werden, das Kabel muss aber nach max. 100m durch Repeater verlängert werden. Dabei wird in einem Zyklus jeder individuelle Slave hintereinander angesprochen, die Zyklusdauer ergibt sich daher (automatisch) aus der Anzahl angeschlossener Slaves.

### Nennen Sie vier Punkte zur ASi-Datensicherung

Alternation, start/end, information, länge, parity

### Was bedeutet gerade Parität?
#### Bezogen auf USART
#### Allgemein
### Was ist ein ASi-Koppelmodul?

nicht ASi kompatible E/A-Geräte

### Welche drei Phasen könne bei der ASi-Ablaufkontrollschicht unterschieden werden?

Erkennungsphase (konstruieren der Liste erkannter Slaves (LES)), Aktivierungsphase (konstruieren der Liste aktivierter Slaves (LAS)), Datenaustauschphase (polling der Slaves), Managementphase (azyklische Nachrichten, genau ein Masteraufruf für unterschiedliche Konfigurationszwecke)

### Beschreiben von ASi safety at work
## Folien Teil 4 - Profibus
### Welche drei Profibus-Varianten gibt es und wo werden sie eingesetzt?
### Was ist RS485

Es handelt sich um einen Übertragungsstandard bei dem das Signal durch eine Spannungsdifferenz indiziiert wird. Dabei ist eine RS485 Leitung terminiert (R1 über Leitung A zu VCC, Leitung B zu GND, R2 über Leitung 1 zu Leitung 2).

### Welche Daten werden bei UART übertragen?
### Was versteht man unter einer schlupffreien Übertragung?
### Wie funktioniert der Buszugriff bei Profibus? Welche Formel gilt für den Tokenumlauf?
### Wie funktioniert das FDL Protokoll bei Profibus?

Fieldbus Data Link (FDL) ist ein Protokoll welches Token passing innerhalb der Master (in einem logischen Ring) definiert.

Nachdem Geräte aktiviert sind gehen sie in den passive idle Zustand, bis der Token bei ihnen ankommt (explizites Token Verfahren) oder genug Zeit verstrichen ist um das Token anzufordern (claim Token Zustand). Wurde das Token erhalten geht es in den active idle Zustand. Dort wird dann das Token verwendet, also kommt man in den use token Zustand. Dort wird mit den slaves kommuniziert und dann überprüft, ob die Zeit verstrichen ist. Ist genug Zeit verstrichen wird das Token weitergegeben.

### Nennen Sie die drei Komponenten zu Profibus DP?

- DP Master Klasse 1 (DPM1): zentrale Steuerung (SPS)
- DP Master Klasse 2 (DPM2): Projektierung, Überwachung, Engineering, auch zur Inbetriebnahme von Slaves
- DP Slave: Peripherigerät mit direkter Schnittstelle zu den E/A-Geräten

### Was bedeutet Freeze?
### Was bedeutet Sync?
### Welche Aufgaben führt ein DP-Master im Zustand CLEAR durch?
### Was sind die 2 Aufgaben des DTM und von wem werden sie zur Verfügung gestellt?
### Nennen Sie die drei Komponenten zu Profinet IO?
### Was ist CIP?
## Folien Teil 5 - Sensor/Aktor Systeme in Gebäuden
### Nennen Sie drei typische Anwendungen für die Gebäudeautomation?
### Was versteht man unter Graceful Degradation?
### Nennen von 3 Herausforderungen bei der Gebäudeautomation
### Aufgaben der Managementebene bei Gebäudeautomation
### Was versteht man unter Offenen Systemen? Nennen Sie Vor- und Nachteile dazu?
### Nenne 2 gewerkeübergreifende offene Systeme zur Gebäudeautomation
### Nennen Sie die 3 Komponenten von LonWorks
### Nennen Sie die 3 typische Netzwerktechniken für BACnet?
## Folien Teil 6 - Sensor/Aktor Systeme in Automobilen
### Verschiedene Nutzungen von Netzwerken in Autos
### Was versteht man unter Bitstuffing? Welche Schicht nach dem OSI-Modell ist dafür verantwortlich? Nennen Sie ein Feldbus-Protokoll, das sich dieser Methode bedient.
### CAN Kommunikationszyklus, welche 4 Segmente gibt es?
### Wie wird ein CAN Fehlertelegramm von Datennachricht unterschieden?
### Wie wird bei CAN verhindert, dass ein fehlerhafter Knoten das Netz lahmlegt?
### Ein Zeitdiagramm mit 3 Timeslots zum einem LIN Cluster (1 Master + 2 Slaves) bei Event Triggered Frames. Einzuzeichnen die Antworten der Slaves
### Was steht im LIN Descriptor file (LDF)
#### Node Capability File (NCF)
### Wie ist ein Flexray-Knoten aufgebaut? Skizze und Erklärung der Punkte
### Bus Guardian: was macht der? Wo wird der eingesetzt?
### Nennen Sie die vier aufeinanderfolgenden Segmente bei einem Kommunikationszyklus bei FlexRay
## Folien Teil 7 - LU / Sensoren & Aktuatoren
### Mit welchen Sensoren lässt sich Entfernung messen?
### Wie funktioniert ein induktiver Näherungsschalter? Welche Voraussetzung müssen die Gegenstände dafür erfüllen?
### Was ist ein kapazitiver Näherungsschalter
### Beschreiben eines Reed-Schalters mit einfacher Skizze.
### Wie funktioniert ein Drehwinkelgeber? Skizze?
## Building Automation
### Was ist DDC?
## nur im Skriptum
### 4 Arten der Redundanz aufzählen!
#### Heiße/Kalte Redundanz
### Erkläre Reflexions-Lichtschranke

# ungesammelte Prüfungsfragen

## 19.3.2012

### Bei seriellen Bussystemen wird oft asynchroner Zeitmultiplex betrieben. In welche beiden Verfahren kann dabei grob eingeteilt werden?
### CAN Welches Device gewinnt die Arbitrierung
### Wie ist der Funktionsblock vom IEC 61499 aufgebaut?
### Wie kann man die 3 Phasen von technischen Systemen einteilen? Welche Kurve beschreibt dieses Verhalten?
### Aufgaben der Managementebene bei Gebäudeautomation
### Was versteht man unter Einweglichtschranke? Was ist eine Dunkelschaltung?
### Welche drei Phasen könne bei der ASi-Ablaufkontrollschicht unterschieden werden?
### Erklären sie Bitstuffing
### Was heisst "kalte Reserve"?
### Nennen Sie die 3 Komponenten von LonWorks
### FlexRay minislotting einzeichnen
### Erklären sie ASi Safety at Work
### ProfiNet Protokollstufen
### Anforderungen an Safety-Critical systems im Automotive Bereich? Kann man LIN dafür verwenden?
### Was ist IEC 61508
### Welche Paradigmen (Client-Server, Publisher-Subscriber, Producer-Consumer) werden bei Engineering und Prozessdatenaustausch verwendet? Begründen sie warum
### Unterschied zw. Tunnel Router und Gateway? Wo werden diese verwendet
### Zugriffverfahren bei ControlNet

## 27.1.2015

### Wie ist der Funktionsblock vom IEC 61499 aufgebaut? Inkl. Skizze. (6 Punkte)
### Erklären sie das ASi Protokoll (Übertragungstechnik, Protokoll; 6 Punkte)
### Wie wird ein CAN Fehlertelegramm von Datennachricht unterschieden? (6 Punkte)
### Wie funktioniert ein Induktiver Näherungsschalter? (Oder war es kapazitiver? Ich weiß es nicht mehr so genau.... ;))
### Welche wichtigen Zeitparameter gibt es bei Profibus und wie stehen sie zueinander? (Antwort: TTH = TTR TRR)
### Geben sie drei Herausforderungen zu Safety & Critical Systems in Automobilen an? Sollte man dafür LIN verwenden?
### Nennen Sie drei typische Netzwerktechniken für BACnet!
### Erklären sie FlexRay!
### Welche Aufgabe hat der Network Layer (Schicht 3 im Schichtenmodell)? Warum wird die Schicht in manchen Automationsnetzen nicht implementiert?
### Was bedeutet "Industrie 4.0"?
### Erklären sie ControlNet!
### Erklären sie den Data Link Layer von WirelessHART! (oder so ähnlich)
### Erklären sie die Herausforderungen von Funknetzen! (oder so ähnlich)
### Dann war noch irgendeine Frage zu CIP, ich glaube so in der Arte "Nennen Sie drei typische Netzwerktechnologien bei CIP"
### Nennen sie drei Fertigungsverfahren

## 27.1.2009

### Welche drei Profibus-Varianten gibt es und wo werden sie eingesetzt?
### Was ist ein ASi-Koppelmodul?
### Beschreiben eines Reed-Schalters mit einfacher Skizze.
### Nennen von 3 Herausforderungen bei der Gebäudeautomation
### Bei seriellen Bussystemen wird oft asynchroner Zeitmultiplex betrieben. In welche beiden Verfahren kann dabei grob eingeteilt werden?
### Beschreiben von ASi safety at work
### Bus Guardian: was macht der? Wo wird der eingesetzt?
### Ein einfacher KOP war gegeben mit Signallaufplan, wo der Ausgang eingezeichnet werden soll (Zeichnung dazu)
### ein IEC 61499 Funktionsblockdiagramm war gegeben, die Funktion sollte festgestellt werden
### welche zwei Service Primitives bei IEC 61499 gibt es?
### wie funktioniert das FDL Protokoll bei Profibus?
### drei Aufgaben eines Automatisierungssystems
### irgendwas zu event triggered frames bei LIN (8 Punkte)
### Aufzählen der charakteristischen Zeiten einer SPS und schätzen des Worst Case (8 Punkte)

## 4.1.2008
## 27.1.2009
## 27.1.2009
## 27.1.2009
## 27.1.2009
## 27.1.2009