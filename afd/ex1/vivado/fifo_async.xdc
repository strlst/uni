# clock constraint
create_clock -name rd_clk -period 10 [get_ports {rd_clk}];
create_clock -name wr_clk -period 5 [get_ports {wr_clk}];

# onboard clock port
set_property PACKAGE_PIN Y9 [get_ports {rd_clk}];  # "GCLK"

# PMOD A port
set_property PACKAGE_PIN Y11  [get_ports {wr_en}];  # "JA1"
set_property PACKAGE_PIN AA11 [get_ports {full}];  # "JA2"
set_property PACKAGE_PIN AA9  [get_ports {wr_clk}];  # "JA4"

# PMOD B port
set_property PACKAGE_PIN W12 [get_ports {wr_data[0]}];  # "JB1"
set_property PACKAGE_PIN W11 [get_ports {wr_data[1]}];  # "JB2"
set_property PACKAGE_PIN V10 [get_ports {wr_data[2]}];  # "JB3"
set_property PACKAGE_PIN W8 [get_ports {wr_data[3]}];  # "JB4"
set_property PACKAGE_PIN V12 [get_ports {wr_data[4]}];  # "JB7"
set_property PACKAGE_PIN W10 [get_ports {wr_data[5]}];  # "JB8"
set_property PACKAGE_PIN V9 [get_ports {wr_data[6]}];  # "JB9"
set_property PACKAGE_PIN V8 [get_ports {wr_data[7]}];  # "JB10"
# PMOD C port
set_property PACKAGE_PIN AB6 [get_ports {wr_data[8]}];  # "JC1_N"
set_property PACKAGE_PIN AB7 [get_ports {wr_data[9]}];  # "JC1_P"
set_property PACKAGE_PIN AA4 [get_ports {wr_data[10]}];  # "JC2_N"
set_property PACKAGE_PIN Y4  [get_ports {wr_data[11]}];  # "JC2_P"
set_property PACKAGE_PIN T6  [get_ports {wr_data[12]}];  # "JC3_N"
set_property PACKAGE_PIN R6  [get_ports {wr_data[13]}];  # "JC3_P"
set_property PACKAGE_PIN U4  [get_ports {wr_data[14]}];  # "JC4_N"
set_property PACKAGE_PIN T4  [get_ports {wr_data[15]}];  # "JC4_P"

# LEDs
set_property PACKAGE_PIN T22 [get_ports {led[0]}];  # "LD0"
set_property PACKAGE_PIN T21 [get_ports {led[1]}];  # "LD1"
set_property PACKAGE_PIN U22 [get_ports {led[2]}];  # "LD2"
set_property PACKAGE_PIN U21 [get_ports {led[3]}];  # "LD3"
set_property PACKAGE_PIN V22 [get_ports {led[4]}];  # "LD4"

# push buttons
set_property PACKAGE_PIN P16 [get_ports {reset}];  # "BTNC"

set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 33]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 34]];
# bank 35 not used
#set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 13]];

set_max_delay 5 -datapath_only -from [get_clocks wr_clk] -to [get_clocks rd_clk]
set_input_delay 2 -clock [get_clocks wr_clk] [get_ports wr_en]
set_input_delay 2 -clock [get_clocks wr_clk] [get_ports wr_data]

set_false_path -from [get_ports reset]
set_false_path -to [get_ports full]
set_false_path -to [get_ports led]
set_false_path -from [get_clocks rd_clk] -to [get_clocks wr_clk]
set_false_path -from [get_clocks wr_clk] -to [get_clocks rd_clk]
