module gen_wr_ptr #(parameter ADDR_WIDTH = 4)
(
	input reset_n,
	input wr_clk,
	input wr_en,
	input [ADDR_WIDTH - 1 : 0] rd_ptr_gray_sync,
	output [ADDR_WIDTH - 1 : 0] wr_ptr_bin,
	output [ADDR_WIDTH - 1 : 0] wr_ptr_gray,
	output full
);

reg [ADDR_WIDTH - 1 : 0] binary;

assign wr_ptr_bin = binary;
assign wr_ptr_gray = (binary >> 1) ^ binary;

assign full = (wr_ptr_gray[ADDR_WIDTH - 1] != rd_ptr_gray_sync[ADDR_WIDTH - 1]) & (wr_ptr_gray[ADDR_WIDTH - 2] != rd_ptr_gray_sync[ADDR_WIDTH - 2]) & (wr_ptr_gray[ADDR_WIDTH - 3 : 0] == rd_ptr_gray_sync[ADDR_WIDTH - 3 : 0]);

always @(posedge wr_clk) begin
	if (reset_n == 1'b0) begin
		binary <= 0;
	end else if (wr_en == 1'b1 & full == 1'b0) begin
		binary <= binary + 1;
	end
end

endmodule