module gen_rd_ptr #(parameter ADDR_WIDTH = 4)
(
	input reset_n,
	input rd_clk,
	input rd_en,
	input [ADDR_WIDTH - 1 : 0] wr_ptr_gray_sync,
	output [ADDR_WIDTH - 1 : 0] rd_ptr_bin,
	output [ADDR_WIDTH - 1 : 0] rd_ptr_gray,
	output empty
);

reg [ADDR_WIDTH - 1 : 0] binary;

assign rd_ptr_bin = binary;
assign rd_ptr_gray = (binary >> 1) ^ binary;

assign empty = rd_ptr_gray == wr_ptr_gray_sync;

always @(posedge rd_clk) begin
	if (reset_n == 1'b0) begin
		binary <= 0;
	end else if (rd_en == 1'b1 & empty == 1'b0) begin
		binary <= binary + 1;
	end
end

endmodule