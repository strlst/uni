module sync #(parameter WIDTH = 4)
(
	input reset_n,
	input clk,
	input [WIDTH - 1 : 0] in,
	output [WIDTH - 1 : 0] out
);

reg [WIDTH - 1 : 0] data_gray [1 : 0];

assign out = data_gray[1];

always @(posedge clk) begin
	if (reset_n == 1'b0) begin
		data_gray[0] <= 0;
		data_gray[1] <= 0;
	end else begin
		data_gray[1] <= data_gray[0];
		data_gray[0] <= in;
	end
end

endmodule