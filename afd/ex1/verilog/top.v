`include "../verilog/fifo_async.v"

module top (
	input reset,
	input rd_clk,
	input wr_clk,
	input wr_en,
	input [15 : 0] wr_data,
	output full,
	output [4 : 0] led
);

reg [2 : 0] sync_reset_n;

wire fifo_full;
wire fifo_empty;
wire [15 : 0] rd_data;

assign full = fifo_full;
assign led[4] = fifo_full;
assign led[3 : 0] = {^rd_data[15 : 12], ^rd_data[11 : 8], ^rd_data[7 : 4], ^rd_data[3 : 0]};

fifo_async #(
	.DATA_WIDTH (16),
	.ADDR_WIDTH (4)
) fifo_async (
	.reset_n (sync_reset_n[2]),

	.wr_clk(wr_clk),
	.wr_en(!fifo_full & wr_en),
	.full(fifo_full),
	.wr_data(wr_data),

	.rd_clk(rd_clk),
	.rd_en(!fifo_empty),
	.empty(fifo_empty),
	.rd_data(rd_data)
);

always @(posedge wr_clk) begin
	sync_reset_n[2] <= sync_reset_n[1];
	sync_reset_n[1] <= sync_reset_n[0];
	sync_reset_n[0] <= !reset;
end
	
endmodule
