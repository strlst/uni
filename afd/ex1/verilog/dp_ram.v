module dp_ram #(parameter DATA_WIDTH = 16, ADDR_WIDTH = 4, DEPTH = 2**ADDR_WIDTH)
( 
     input wire wr_clk,
     input wire wr_en,
     input wire [DATA_WIDTH - 1 : 0] wr_data,
     input wire [ADDR_WIDTH - 1 : 0] wr_addr,
     
     input wire rd_clk,
     output wire [DATA_WIDTH - 1 : 0] rd_data,
     input wire [ADDR_WIDTH - 1 : 0] rd_addr
);

(* ram_style = "block" *) reg [DATA_WIDTH - 1 : 0] mem [DEPTH - 1 : 0];
reg [DATA_WIDTH - 1 : 0] reg_rd_data;
// for visualisation only:
reg [ADDR_WIDTH - 1 : 0] reg_rd_addr;

assign rd_data = reg_rd_data;

always @(posedge wr_clk)
begin
     if (wr_en == 1'b1)
          mem[wr_addr] <= wr_data;
end

always @(posedge rd_clk)
begin
     reg_rd_data = mem[rd_addr];
     reg_rd_addr = rd_addr;
end

endmodule