(* black_box *) module tmr (
	input clk,
	input reset_n,
	input inject,
	output select,
	output fault
);
endmodule
