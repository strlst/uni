module counter #(parameter CYCLES_SEC = 100_000_000) (
	input clk,
	input reset_n,
	output reg toggled
);

parameter COUNT_BITS = $clog2(CYCLES_SEC - 1);

reg [COUNT_BITS - 1 : 0] count;
wire overflow = (count == CYCLES_SEC - 1);

always @(posedge clk) begin
	if (reset_n == 1'b0) begin
		count <= 0;
		toggled <= 1'b0;
	end else begin
		count <= overflow ? 0 : count + 1;
		toggled <= overflow ? ~toggled : toggled;
	end
end
	
endmodule