module sync #(parameter DATA_WIDTH = 3) (
        input clk,
        input reset_n,
        input data_in,
        output data_out
);

assign data_out = sync[2];

reg [DATA_WIDTH - 1 : 0] sync [2 : 0];

always @(posedge clk) begin
	if (reset_n == 1'b0) begin
		sync[0] <= 0;
		sync[1] <= 0;
		sync[2] <= 0;
	end else begin
		sync[2] <= sync[1];
		sync[1] <= sync[0];
		sync[0] <= data_in;
	end
end

endmodule
