module tmr #(parameter CYCLES_SEC = 100_000_000) (
	input clk,
	input reset_n,
	input inject,
	output select,
	output fault
);

// save toggled state for each counter
(* keep="true" *) wire [2 : 0] toggled;
(* keep="true" *) wire [2 : 0] tampered;

// triple modular redundancy: at least two out of three bits set
assign select = (tampered[1] & tampered[0]) | (tampered[2] & tampered[1]) | (tampered[2] & tampered[0]);

injector injector_inst (
	.clk(clk),
	.reset_n(reset_n),
	.inject(inject),
	.original(toggled),
	.tampered(tampered),
	.fault(fault)
);

counter #(.CYCLES_SEC (CYCLES_SEC)) counter_inst_0 (
	.clk(clk),
	.reset_n(reset_n),
	.toggled(toggled[0])
);

counter #(.CYCLES_SEC (CYCLES_SEC)) counter_inst_1 (
	.clk(clk),
	.reset_n(reset_n),
	.toggled(toggled[1])
);

counter #(.CYCLES_SEC (CYCLES_SEC)) counter_inst_2 (
	.clk(clk),
	.reset_n(reset_n),
	.toggled(toggled[2])
);
	
endmodule
