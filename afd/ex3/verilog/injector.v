module injector (
	input clk,
	input reset_n,
	input inject,
	input [2 : 0] original,
	output [2 : 0] tampered,
	output fault
);

// inject = 0 simulates stuck at 0 fault
reg reg_inject;
reg reg_fault;

assign tampered = {original[2], original[1], original[0] & reg_inject};
assign fault = reg_fault;

always @(posedge clk) begin
	if (reset_n == 1'b0) begin
		reg_inject <= 3'b111;
		reg_fault <= 1'b0;
	end else begin
		if (inject == 1'b1) begin
			reg_inject <= 1'b0;
			reg_fault <= 1'b1;
		end
	end
end

endmodule
