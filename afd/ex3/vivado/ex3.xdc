#set_property PACKAGE_PIN Y9 [get_ports clk]

# define clk associated pin as a clock source with 100 MHz
#create_clock -period 10.000 -name clk [get_ports clk]

# User LEDs - Bank 33
set_property PACKAGE_PIN T22 [get_ports {led[0]}];
set_property PACKAGE_PIN T21 [get_ports {led[1]}];
set_property PACKAGE_PIN U22 [get_ports {led[2]}];
#set_property PACKAGE_PIN U21 [get_ports {led[3]}];  # "LD3"
#set_property PACKAGE_PIN V22 [get_ports {led[4]}];  # "LD4"
#set_property PACKAGE_PIN W22 [get_ports {led[5]}];  # "LD5"
#set_property PACKAGE_PIN U19 [get_ports {led[6]}];  # "LD6"
#set_property PACKAGE_PIN U14 [get_ports {led[7]}];  # "LD7"

# User Push Buttons - Bank 34
set_property PACKAGE_PIN P16 [get_ports {inject}]; "BTNC"
#set_property PACKAGE_PIN R16 [get_ports {BTND}];  # "BTND"
#set_property PACKAGE_PIN N15 [get_ports {inject[0]}]; "BTNL"
#set_property PACKAGE_PIN R18 [get_ports {inject[2]}]; "BTNR"
set_property PACKAGE_PIN T18 [get_ports reset]; # "BTNU"

# User DIP Switches - Bank 35
#set_property PACKAGE_PIN H18 [get_ports {inject[2]}];  # "SW5"
#set_property PACKAGE_PIN H17 [get_ports {inject[1]}];  # "SW6"
#set_property PACKAGE_PIN M15 [get_ports {inject[0]}];  # "SW7"

set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 33]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 34]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 13]];

# sets: dont_touch, exclude_placement, contain_routing
set_property HD.RECONFIGURABLE true [get_cells tmr_inst]

# define a new pblock
create_pblock pblock_tmr_inst
add_cells_to_pblock [get_pblocks pblock_tmr_inst] [get_cells -quiet [list tmr_inst]]
resize_pblock [get_pblocks pblock_tmr_inst] -add {SLICE_X106Y50:SLICE_X113Y99}
resize_pblock [get_pblocks pblock_tmr_inst] -add {RAMB18_X5Y20:RAMB18_X5Y39}
resize_pblock [get_pblocks pblock_tmr_inst] -add {RAMB36_X5Y10:RAMB36_X5Y19}
set_property RESET_AFTER_RECONFIG true [get_pblocks pblock_tmr_inst]
set_property SNAPPING_MODE ROUTING [get_pblocks pblock_tmr_inst]
