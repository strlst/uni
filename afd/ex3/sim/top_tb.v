`timescale 1ns/1ns
`include "../verilog/sync.v"
`include "../verilog/counter.v"
`include "../verilog/inject.v"
`include "../verilog/tmr.v"
`include "../verilog/tmr_base.v"
`include "../verilog/top.v"

module top_tb;

reg clk;
reg reset;
reg [2:0] inject;
wire [1:0] led;
reg [1:0] old;

integer file;
integer toggles;

initial begin
	toggles <= 0;
	file = $fopen("../toggles.txt", "w");

	$dumpfile("top.vcd");
	$dumpvars(0, top);

	// initial values
	reset <= 1'b1;
	old <= 2'b10;
	inject <= 3'b000;

	// after some time release reset
	#200
	reset <= 1'b0;

	// begin
	@(posedge clk);
	while (toggles < 100) begin
		// wait for clk
		@(posedge clk)

		if (led != old) begin
			toggles <= toggles + 1;
			$display("@%2d: toggle to %1d\n", $time, led);
			$fwrite(file, "@%2d: toggle to %1d\n", $time, led);
		end
		old <= led;

		// inject faults
		if (toggles == 23) begin
			// inject stuck at 0 fault
			inject[0] <= 1'b1;
		end else if (toggles == 26) begin
			// inject stuck at 0 fault
			inject[1] <= 1'b1;
		end else if (toggles == 27) begin
			// inject stuck at 0 fault
			inject[2] <= 1'b1;
		// remove faults
		end else if (toggles == 30) begin
			inject <= 3'b000;
		end
	end

	$fclose(file);  
	$display("test complete");
	$finish;
end

initial begin
	clk <= 1'b0;
	// wait 10ns for freq of 100 MHz
	forever #10 clk <= ~clk;
end

top #(.CYCLES_SEC (10)) top_inst (
	.clk(clk),
	.reset(reset),
	.inject(inject),
	.led(led)
);

endmodule
