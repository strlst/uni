#!/bin/bash

# copy bin files
mkdir -p /lib/firmware
cp /run/media/mmcblk0p2/home/root/*.bin /lib/firmware

# set up gpio
echo 1011 > /sys/class/gpio/export

# program top design
echo top.bin > /sys/class/fpga_manager/fpga0/firmware

# prepare for PR calls
echo 1 > /sys/class/fpga_manager/fpga0/flags
