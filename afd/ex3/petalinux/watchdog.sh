#!/bin/bash
while true; do
        value=$(cat /sys/class/gpio/gpio1011/value)
        if [ $value -eq 1 ]; then
                echo -n "fault detected, reconfiguring device in... "
                for i in 3 2 1; do
                        echo -n "$i... "
                        sleep 1
                done
                echo
                echo tmr_inst_tmr_partial.bin > /sys/class/fpga_manager/fpga0/firmware && echo "success" || echo "failure"
        fi
done
