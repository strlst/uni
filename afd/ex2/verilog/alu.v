module alu #(DATA_WIDTH = 8) (
	input opcode,
	input [DATA_WIDTH-1 : 0] a,
	input [DATA_WIDTH-1 : 0] b,
	output [DATA_WIDTH-1 : 0] add_r,
	output [(2*DATA_WIDTH-1) : 0] mul_r,
	output reg [DATA_WIDTH-1 : 0] r
);

assign add_r = a + b;
assign mul_r = a * b;

always @(*) begin
	case (opcode)
		1'b0:
			r <= add_r;
		1'b1:
			r <= mul_r[DATA_WIDTH-1 : 0];
	endcase
end
	
endmodule