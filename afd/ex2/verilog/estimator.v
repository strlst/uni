module estimator #(parameter INT_SIZE = 8, RESULT_SIZE = 35) (
	input clk,
	input [INT_SIZE - 1 : 0] add_alpha,
	input add_alpha_valid,
	input [INT_SIZE - 1 : 0] mul_alpha,
	input mul_alpha_valid,
	output [RESULT_SIZE - 1 : 0] estimate,
	output estimate_valid
);

wire [INT_SIZE : 0] alpha;
wire [RESULT_SIZE - 1 : 0] alpha_f;

assign alpha = add_alpha + mul_alpha;

assign estimate = alpha_f >> INT_SIZE;
assign estimate_valid = add_alpha_valid & mul_alpha_valid;

mul_const_f mul_const_f_inst (
	.CLK(clk),  // input wire CLK
	.A(alpha),  // input wire [7 : 0] A
	.P(alpha_f) // output wire [34 : 0] P
);

endmodule