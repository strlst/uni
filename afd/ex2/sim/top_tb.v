`timescale 1ns/1ns
//`include "../verilog/alu.v"
//`include "../verilog/counter.v"
//`include "../verilog/estimator.v"
//`include "../verilog/top.v"
//`include "../vivado/ex2.gen/sources_1/ip/mul_const_f/mul_const_f_sim_netlist.v"

module top_tb;

reg clk;
reg reset;
wire [1:0] led;

integer f;
integer estimates;

initial begin
	estimates <= 0;
	f = $fopen("estimates.txt", "w");

	$dumpfile("top.vcd");
	$dumpvars(0, top);

	reset <= 1'b1;
	#200

	reset <= 1'b0;
	@(posedge clk);
	while (estimates < 1000) begin
		@(posedge clk)
		if (top.estimate_valid == 1'b1) begin
			estimates <= estimates + 1;
			$display("@%2d: estimate is %1d\n", $time, top.estimate);
			$fwrite(f, "@%2d: estimate is %1d\n", $time, top.estimate);
		end
	end

	$fclose(f);  
	$display("test complete");
	$finish;
end

initial begin
	clk <= 1'b0;
	// wait 10ns for freq of 100 MHz
	forever #10 clk <= ~clk;
end

top top_inst (
	.clk(clk),
	.reset(reset),
	.led(led)
);
endmodule
