#!/usr/bin/env python3

values = [int(line.strip().split(' ')[-1]) for line in open('estimates.txt', 'r')]
average = sum(values) / len(values)
print(f'average is {average}')
