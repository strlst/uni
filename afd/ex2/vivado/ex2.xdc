set_property PACKAGE_PIN Y9 [get_ports {clk}];  # "GCLK"

# define clk associated pin as a clock source with 100 MHz
create_clock -name clk -period 10 [get_ports {clk}];

# User LEDs - Bank 33
set_property PACKAGE_PIN T22 [get_ports {led[0]}];  # "LD0"
set_property PACKAGE_PIN T21 [get_ports {led[1]}];  # "LD1"
#set_property PACKAGE_PIN U22 [get_ports {led[2]}];  # "LD2"
#set_property PACKAGE_PIN U21 [get_ports {led[3]}];  # "LD3"
#set_property PACKAGE_PIN V22 [get_ports {led[4]}];  # "LD4"
#set_property PACKAGE_PIN W22 [get_ports {led[5]}];  # "LD5"
#set_property PACKAGE_PIN U19 [get_ports {led[6]}];  # "LD6"
#set_property PACKAGE_PIN U14 [get_ports {led[7]}];  # "LD7"

# User Push Buttons - Bank 34
set_property PACKAGE_PIN P16 [get_ports {reset}];  # "BTNC"
#set_property PACKAGE_PIN R16 [get_ports {BTND}];  # "BTND"
#set_property PACKAGE_PIN N15 [get_ports {BTNL}];  # "BTNL"
#set_property PACKAGE_PIN R18 [get_ports {BTNR}];  # "BTNR"
#set_property PACKAGE_PIN T18 [get_ports {BTNU}];  # "BTNU"

set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 33]];
#set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 34]];
#set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 13]];