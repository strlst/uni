.AM
.TL
Analysis UE1
.AU
strlst
.NH
.EQ
gsize -1
define so `smallover`
define is `~=~`
define limntoinf `lim from { n -> inf }`
define implies `~~=>~~`
define AND `size 24 { ~ sub ~ hat ~ }`
delim $$
.EN
.PP
Man finde ein Bildungsgesetz fu\*[:]r die unendlichen Folgen:
.EQ
roman { (a) } ~ 0.3;~0.09;~0.024;~...
roman { (b) } ~ 1 so 2 ;~ 4 so 3 ;~ 9 so 4 ;~...
roman { (b) } ~ 1 so 2 ;~ 2 so 4 ;~ 3 so 8 ;~...
.EN
.PP
Wie gro\*[8] ist dann jeweils das zwo\*[:]lfte Folgenglied?
.SH
Lo\*[:]sung:
.EQ
(a) is left [
    pile { 0.3 above 0.09 above 0.027 above 0.0081 above ... above 5.31441 times 10 sup -7 }
right ]
~
pile { times 0.3 above times 0.3 above times 0.3 above times 0.3 sup 8 above ~ }
~~~~~~~
(b) is 
~
pile { +2 above +2 }
~
pile { +3 above +5 above +7 }
~
left [
    pile { 1 ~~/~~ 2 above 4 ~~/~~ 3 above 9 ~~/~~ 4 above 16 ~~/~~ 5 }
right ]
~
pile { +1 above +1 above +1 }
~~~~~~~
(c) is 
pile { +1 above +1 above +1 }
~
left [
    pile { 1 ~~/~~ 2~ above 2 ~~/~~ 4~ above 3 ~~/~~ 8~ above 4 ~~/~~ 16 }
right ]
~
pile { times 2 above times 2 above times 2 }
.EN
.EQ
b(n) mark is { (1 + n) sup 2 } over { 2 + n } ~~~ b(12) is { (1 + 12) sup 2 } over { 12 + 2 } is 13 sup 2 over 14 is 169 over 14
.EN
.EQ
c(n) lineup is n+1 over 2 sup n+1 ~~~~~~~ c(12) is 12+1 over 2 sup 12+1 is 13 over 8192
.EN

.NH
.PP
Man untersuche nachstehende Folgen in Hinblick auf Monotonie, Beschraenktheit und moegliche Grenzwerte. Ferner veranschauliche man die Folgen auf der reellen Zahlengeraden:
.EQ
mark (a) ~~~~
(a sub n ) is 1, 1 so 2 , 3, 1 so 4 , 5, 1 so 6 , ..., n, 1 so n+1 , ...
.EN
.EQ
lineup (b) ~~~~
(b sub n ) is n+5 over n-1 ,~~~ n >= 2
.EN
.EQ
lineup (c) ~~~~
(b sub n ) is (-1) sup n n+2 over n ,~~~ n >= 1
.EN
.SH
Lo\*[:]sung:
.NH 2
.EQ
(a sub n ) is left {
    rpile { 1 so n above ~ above n }
    ~~lpile { roman {~~~falls~ n~ gerade} above ~ above roman {~~~falls~ n~ ungerade} }
~~~~~~~~~~~~~~~
pile { { limntoinf ~ a(n sub gerade ) is 0 } above { limntoinf ~ a(n sub ungerade ) is inf } }
~~~
implies roman { Haeufungswerte~bei~" { " } 0, inf roman " } "
.EN
.PP
$ (a sub n ) $ ist divergent!
.EQ
pile {
a sub { n sub { gerade + 2}} < a sub { n sub {gerade}}
above
a sub { n sub { ungerade + 2}} > a sub { n sub {ungerade}}
}
~~~
implies (a sub n ) size 8 { ~nicht~monoton~steigend~oder~fallend }
~~~~~~~~~~~~~~~
pile { { "sup" ~ a sub n mark is inf } above { "inf" ~ a sub n lineup is 0 } }
.EN
.PP
$ (a sub n ) $ ist nicht monoton!
.PP
$ (a sub n ) $ hat ein Supremum und Inferium!
.PP
 
.PP
 
.NH 2
.EQ
(b sub n ) is n+5 over n-1 ,~~~ n >= 2
.EN
.EQ
n+5 over n-1 > n+1+5 over n+1-1 implies (n + 5) ~n > (n - 1)(n + 6) implies n sup 2 + 5n mark > n sup 2 - n + 6n - 6
.EN
.EQ
n sup 2 + 5n lineup > n sup 2 - 5n - 6
.EN
.PP
$ (b sub n ) $ streng monoton fallend!
.PP
$ (b sub 2 ) $ obere Schranke
.EQ
lim bar ~ (b sub n ) is b sub 2 is 2+5 over 2-1 is 7
~~~~~~~~~~~~~~~~
lim under is limntoinf ~ (b sub n )
.EN
.EQ
limntoinf ~ (b sub n ) is limntoinf n+5 over n-1 is limntoinf { n~({1 + 5 so n }) } over { n~({1 - 1 so n }) } is {1 + 5 so inf } over {1 - 1 so inf } is 1
.EN
.EQ
1 <= (b sub n ) <= 7
.EN
.PP
$ (b sub n ) $ ist konvergent!
.PP
 
.PP
 
.NH 2
.EQ
(c sub n ) is (-1) sup n n+2 over n ,~~~ n >= 1
~~~~~~~~~~~~~~~~~~~~~~~~
(c sub n ) is left {
    rpile { +{ n+2 so n } above ~ above -{ n+2 so n } }
    ~~~~~~lpile { ~~~~~~ 2 ~|~ n above ~ above not~ 2 ~|~ n }
.EN
.EQ
n+2 over n > (n+1)+2 over (n+1) implies (n+2)~(n+1) mark > n~(n+3)
.EN
.EQ
n sup 2 + n + 2n + 2 lineup > n sup 2 + 3n
.EN
.EQ
n sup 2 + 3n + 2 lineup > n sup 2 + 3n implies (c sub {n~pos} ) ~~ size 8 roman {streng~monoton~fallend}
.EN
.EQ
- n+2 over n > - (n+1)+2 over (n+1) implies -1~(n+2)~(n+1) lineup > -n~(n+3)
.EN
.EQ
n sup 2 - 3n - 2 lineup > n sup 2 - 3n implies (c sub {n~neg} ) ~~ size 8 roman {streng~monoton~steigend}
.EN
.EQ
(c sub {n~pos} ) ~~ size 8 roman {streng~monoton~fallend} AND (c sub {n~neg} ) ~~ size 8 roman {streng~monoton~steigend} implies (c sub n ) ~~ size 8 roman {nicht~monoton}
.EN
.EQ
lim ~ ( c sub {n~pos} ) is limntoinf ~ n+2 over n is limntoinf { n~(1 + 2 so n ) } over n is limntoinf ~ 1 + 2 over n is 1 + 2 over inf mark is 1
.EN
.EQ
lim ~ ( c sub {n~neg} ) is limntoinf ~ - n+2 over n is ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
lineup is 1
.EN
.PP
$ roman "{" -1,~1 roman "}" $ sind Haeufungswerte $ implies (c sub n ) ~~ size 8 roman {divergent} ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -1 < (c sub n ) < 1 $
.PP
 
.PP
 
.NH
.PP
Gegeben sei die rekursiv definiterte Folge $ (a sub n ) $ mit $ a sub 0 is 1 $ und
.EQ
a sub n+1 is 1 over 2 ~ (a sub n + 5 so a sub n ) ,~~~ n >= 0
.EN
.PP
Man berechne die Folgenglieder $ a sub n $ fuer $ n is 0,~...,~10 $, untersuche die Folge in Bezug auf Monotonie, Beschraenktheit sowie Konvergenz und berechne - wenn moeglich - den Grenzwert.
.EQ
define generic3         `1 over 2 ~ left ( { a sub $1 + 5 over a sub $1 } right )`
define generic3inserted `1 over 2 ~ left ( {       $1 + 5 over       $1 } right )`
define generic3fraction `1 over 2 ~ left ( $2 so $1 + 5 over { $2 so $1 } right )`
define generic3expanded `1 over 2 ~ left ({ $2 sup 2 + 5 times $1 sup 2 } over { $1 times $2 } right )`
a sub 1 mark is generic3(0) is generic3inserted(1) is 3
~~~~~~~~~~~~~~~
a sub 2 is generic3(1) is generic3inserted(3) is 7 over 3
.EN
.EQ
a sub 3 lineup is generic3fraction(3, 7) is generic3expanded(3, 7) is 1 over 2 ~ left ({49+45} over 21 right ) is 1 over 2 ~ left ( 94 over 21 right ) is 47 over 21
.EN
.EQ
a sub 4 lineup is generic3fraction(47, 21) is generic3expanded(47, 21) approx ~ 2.23607
.EN
.EQ
a sub 5 lineup is generic3inserted(2.23607) approx ~ 2.23607
.EN
.EQ NUMERIK
.EN
