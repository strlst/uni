#!/usr/bin/env python3
import sys
from math import gcd

def length(LS, n):
    return LS[n - 1]

def getLS(A, n):
    LS = list()

    # loop all i until n
    for i in range(n):
        lengthiest = 1
        # loop all j until i
        for j in range(i):
            # check for validity between a_i and a_j
            if A[j] > A[i] and gcd(A[i], A[j]) == 1:
                localLengthiest = LS[j] + 1
                # update if new max was found
                if localLengthiest > lengthiest:
                    lengthiest = localLengthiest
        LS.append(lengthiest)
    return LS

def backtrackLS(A, LS, n):
    LSseq = list()
    l = length(LS, n)
    for i in reversed(range(1, n)):

        # backtracking parameters
        islengthier = l >= LS[i]
        isfirst = len(LSseq) == 0
        if not isfirst:
            isbiggerthanlast = LSseq[-1] < A[i]
            iscoprime = gcd(LSseq[-1], A[i]) == 1

        # decide whether A[i] was part of initial solution or not
        if (islengthier and isfirst) or (islengthier and isbiggerthanlast and iscoprime):
            LSseq.append(A[i])
            l -= 1
    # reverse result
    return LSseq[::-1]

def main(args):
    # parsing
    A = list(map(int, args.split(' ')))
    n = len(A)
    print("(a_i):   ", A)

    # compute values of interest
    LS = getLS(A, n)
    print("LS:      ", LS)

    l = length(LS, n)
    print("length:  ", l)

    LSseq = backtrackLS(A, LS, n)
    print("sequence:", LSseq)

if __name__ == '__main__':
    lines = sys.stdin.read()
    main(lines)
