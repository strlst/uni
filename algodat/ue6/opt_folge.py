#!/usr/bin/env python3

def opt_folge(n):
  f = list()
  for i in range(n + 1):
    f.append(0)
    if i <= 2:
      continue
    if i == 3:
      f[i] = 1
      continue
    f[i] = f[i - 1] + 2 * f[i - 2] + 3 * f[i - 3]
  return f[n]

print([opt_folge(n) for n in range(30)])
