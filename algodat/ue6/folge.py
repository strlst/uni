#!/usr/bin/env python3

def folge(n):
  if n <= 2:
    return 0
  if n == 3:
    return 1
  else:
    return folge(n-1) + 2 * folge(n-2) + 3 * folge(n-3)

print([folge(n) for n in range(30)])
