% AlgoDat ue7
% 10 June 2020

# Aufgabe 1

## (a)

This problem can be likened to *3-Color*. Each group gets assigned a color, thus we have 3 colors. We construct a graph in which every student is represented by a vertex, two vertices are connected by an edge, if the distance between the two students represented by the vertices is less than a meter. 3-Colorability then corresponds to being able to hand out the exam in such a way, that no two students separated by less than a meter are part of the same group. Because we have to decide, whether such a configuration is possible, we have a decision problem. Hence, the problem is NP-complete.

## (b)

This problem can be likened to the *knapsack problem*. A factory decommission's cost savings can be defined as its *value* and production capacity as its *weight* (in terms of the *knapsack problem*). The problem is still a maximization problem, using a slight transformation: we define the maximum capacity (weight) of the knapsack as $G = (1 - 0.3) \cdot \sum p_i$, where $p_i$ denotes a factory's production capacity. If we rephrase this definition, we are allowed to pack at most as many factories as are required to reach $G$ production capacity, where $G$ represents $70%$ of the initial total production capacity. Like with the *knapsack problem*, we maximize value. The result we get is a set of factories to decommission for *maximal cost savings* for a $70%$ productivity drop *at most*. The result we seek is then given by $F_K = F_T \\ F_S$, where $F_K$ denotes the set of factories to keep, $F_T$ the set of all factories and $F_S$ the set of factories supplied by a solution to the *knapsack problem*. In other words, the *complement* of $F_S$. We know this optimization problem to be NP-hard, although we can reduce complexity using Branch-And-Bound.

## (c)

This problem can be likened to *Vertex Cover* (VC). As we are given a list of possible build locations, as well as their reach to individual cities ($population \geq 10^3$), we can construct a graph, in which either a build location or a city is represented by a vertex. Let $V$ be the vertices for our cities and $W$ the vertices for our build locations, $V \cap W = \emptyset$. Furthermore, let our edges be $E$. Then $(v, w) \in E, v \in V, w \in W$ only iff city $v$ is covered by a tower built at location $w$. We now have a graph $G = (V \cup W, E)$ that can be used for VC. An additional restriction has to be placed, while our graph has vertices $V \cup W$, a valid vertex cover can only include vertices from $W$ (because cities can't cover other cities). If a city happens to be a valid build location for a tower, then we loosen our restriction $V \cap W = \emptyset$ to $V \cap W \not = \emptyset$. By applying an algorithm for VC, we solve our initial problem. Because we care for an actual solution, we are faced with an optimization problem, which we know to be NP-hard, although we can reduce complexity using Branch-And-Bound.

## (d)

This problem can be likened to finding a *Topological Ordering*. We can model this problem using a *directed acyclic graph* (DAG) $G = (V, E)$. Let $V$ be the set of programs and $E = \{ (v, w) | v, w \in V \}$ be the set of edges that directly connects a program to another. Two programs are only connected, iff $w$ is a dependency of $v$. We then construct a topological ordering in which program $P \in V$ is the source. The order in which all programs $P_1', ..., P_n'$ need to be installed is then given by reading the topological from right to left, saving each vertex into an ordered list.

# Aufgabe 2

Refer to scanned page at the end!

# Aufgabe 3

We assign each vertex of $V$ an ID such that our vertices are labelled $1, ..., n$ and order our graph by the IDs. The resulting graph is a topological ordering with nodes arranged by ID, such that the leftmost vertex is $1$ and the rightmost vertex $n$. Furthermore, we make a distinction between two types of edges: an edge is a *forward edge*, if $(v, w) \in E, v < w$, analogously, an edge is a *backward edge*, iuf $(v, w) \in E, v > w$. A valid topological ordering would not have *forward edges* and *forward edges* simultaneously, so we still have some work to do. In our topological ordering, we can arrange for all *forward edges* to be displayed above the vertices and for all *backward edges* below the vertices. This step is for our intuition, but can be considered optional. Let $E_1$ denote *forward edges* and $E_2$ *backward edges*. In order to get a valid acyclic graph, we make a comparison between $|E_1|$ and $|E_2|$. If $|E_1| \geq |E_2|$, then $G' = (V, E_1)$, else $G' = (V, E_2)$. It follows that $m = max \{ |E_1|, |E_2| \}$, where $m = |E'|$.

Correctness follows directly from having a valid topological ordering, as $G'$ cannot be cyclic if it can be represented by a valid topological ordering. As we assign IDs $1, ..., n$ arbitrarily, this step only takes linear time. For the rest of the algorithm, we have to count/assign *forward* and *backward edges*, which is trivially achieved by iterating all edges and thus also possible in linear time. Our actual task then becomes proving $\frac{1}{2}$ approximability.

Assuming multi-edges (multiple edges with identical endpoints) are allowed, a guarantee of $E'$ being at least half of the optimal solution is trivial. The difficult case arises, when our graph is maximally connected. This means, each vertex has a directed edge to each other vertex. Even in this example, $E$ is split into $E_1$ and $E_2$ such that $|E_1| = \frac{1}{2} \cdot |E|, |E_2| = \frac{1}{2} \cdot |E|$, we can therefore guarantee $\frac{1}{2}$-approximability even in this "worst-case" scenario. Assuming no multi-edges are allowed and our graph is not maximally connected, then $|E'| > \frac{1}{2}$. This leaves no scenario in which we cannot guarantee $\frac{1}{2}$-approximability. Thus, our algorithm guarantees an approximation at least half as good as the optimal solution.

# Aufgabe 4

Similarly to `Approx-Vertex-Cover`, we can make use of stable matches. We don't need to construct a graph to apply this algorithm, but it helps with intuition. Consider an undirected graph $G = (V, E)$, where $V = \mathcal{K} \cup \mathcal{I}$ and $E = \{ (v, w) \mid v \in \mathcal{K}, w \in \mathcal{I} \}$. Both vaccines as well as diseases are vertices, while edges can only be connections between a vaccine vertex and a disease vertex. For an edge $(v, w)$, we call $w$ a cure for disease $v$. Our problem is now very similar to *Vertex Cover*, only that we don't want to cover all edges, but diseases.

In our algorithm, we take a random edge $(v, w) \in E$ and save any $x \in \mathcal{K}, y \in \mathcal{I}$ such that $x$ is adjacent to $w$ and $y$ is adjacent to $v$. We remove remove all such diseases $x$ combined with $v$ from $V$ and save all such vaccines $y$ combined with $w$ in our solution set $V_s$. There might be immunities we overlook, so temporarily we also save all such $y \in Y \subseteq \mathcal{I}$. In the next step, we search diseases $z \in Z \subseteq \mathcal{K}$ such that $z$ is adjacent to any $y \in Y$. If $|Z| = 0$, we can continue the algortihm by arbitrarily choosing another edge. If $|Z| \not = 0$, then we repeat this process for an edge $(z, y)$ for all $z \in Z$. The algorithm terminates once all diseases have been removed, e.g. $\mathcal{K} \cap V = \emptyset$.

Using this approach, we always remove a disease $v$ for at least one vaccine $w$. This vaccine $w$ may lead to the removal of arbitrarily many other diseases. But we simultaneously automatically take all vaccines that cover this disease $v$. Because the amount of vaccines available per disease is bound to be at most 5, there is no way to take more than 4 additional vaccines $y$ at once, such that $|\{w\} \cup Y| \leq 5$. Much more unintuitive is the step that follows, we need to trace all (thus far 'unconsidered') vaccines $z$ that connect to remaining diseases (connected to our initial vaccine $w$ as well as all $y$). If at this stage $|Z| = 0$, we check remaining edges. If there are none left, we have all diseases covered, so the algorithm terminates. If there are, we repeat this process. Because $V$ can only decrease in size, $|Z| = 0$ eventually has to occur. By similar reasoning, $|V \cap \mathcal{K} = 0$ also has to occur at some point, proving that our proposed algorithm always terminates. The algorithm is correct, because program termination implies having a solution set $V_s$ that covers all diseases.

As for runtime complexity, choosing an arbitrary edge can be done in constant time $O(1)$. Looking up all vaccines for a disease takes at most linear time $O(|\mathcal{I}|)$ and is at best constant. Recursing for remaining diseases connected to vaccines $Y$ takes $5 \cdot O(|\mathcal{K}|)$ and can thus be done in linear time $O(|\mathcal{K}|)$. For the remaining diseases, the process is identical, giving us identical runtime complexity constraints. Because there is at least $1$ vaccine for every disease, this process is repeated at most $|\mathcal{I}| - 1$ times, giving us a total complexity of $(O(|\mathcal{I}|) + O(|\mathcal{K}|)) \cdot O(|\mathcal{I}|) = O(|\mathcal{I}|^2) + O(|\mathcal{K}| \cdot |\mathcal{I}|)$

We want to show that our algorithm is at most $5$-approximable. That is to say, a solution $V_s \subseteq \mathcal{I}$ given by the algorithm is at most 5 times as big as an optimal solution: $\frac{|V_s|}{|V_{opt}|} \leq 5$, $E_{opt} \subseteq \mathcal{I}$, $V_{opt}$ ... optimal solution.

As can be easily inferred, our algorithm naively incorporates any vaccine $w \in \mathcal{I}$ into the solution set $V_s \subset V$. Under normal circumstances, such a naive approach would have very high potential to return considerably suboptimal results. But in this case, we know the amount of vaccines per disease we can naively include to be bound by $5$. This yields an upper boundary for $V_s$, namely $5 \cdot |\mathcal{K}|$. In order for this problem to be $5$-approximable by our algorithm, we need to prove that any optimal solution is at least a fifth of our solution set.

We illustrate plausibility by providing an extreme example in Fig.1.

![Extreme Example](./res/4-extreme-example.png)

In this example, the optimal solution would be to take the upper rightmost vaccine that covers all diseases, while all the rest cover exactly 1 disease. We can start at an arbitrary vaccine, in this case the lower rightmost (the same argument applies for all other starting positions by symmetry). We delete the corresponding disease and lookup all other associated cures, which *forces* us to find the upper rightmost vaccine. We now remove all corresponding diseases, leaving us with $V \cap \mathcal{K} = \emptyset$, or put another way, $V_s = \mathcal{K}$. At this point the algorithm terminates.

This suggests that our algorithm is constructed to include at least a subset of an actual optimal solution. Indeed, because vaccines of an optimal solution are bound to have a connection to at least one disease, and because we cover all diseases, our solution must include optimal vaccines. As for additional, technically unneeded vaccines we naively take alongside the optimal vaccines, the upper boundary of $5$ vaccines per disease guarantees that in total we will never have more than $5$ times the amount of vaccines of an optimal solution in a solution of our algorithm, therefore proving $\frac{|V_s|}{|V_{opt}|} \leq 5$. We conclude that this problem is $5$-approximable by this algorithm.

# Aufgabe 5

To solve this problem, we assume having solved the problem *optimally* already for $t + 1$. We then define a recurrence relation resembling Bellman's equations:

$$
OPT(\vec{S}, t) =
\begin{cases}
\mathcal{R}(\vec{S}), & if\ t = T - 1 \\
\max_{i, j \in \{-1, 0, 1\}, i \not = j, i \not = -j} \left\{ \mathcal{R}\left(
\begin{bmatrix}
S_x \\
S_y
\end{bmatrix}
\right) + OPT\left(
\begin{bmatrix}
S_x + i \\
S_y + j
\end{bmatrix}
, t + 1\right) \right\}, & else
\end{cases}
$$

This effectively yields us individual cost benefit values $G_t$ for individual timesteps. To see how this plays out for the grid this example supplies us with for $t = 0, T = 3$, refer to Tab.1.

||
:-:|:-:|:-:|:-:
(0, 0, 4)|(0, 4, 16)|(4, 16, 16)|(0, 4, 16)
(0, 0, 4)|(0, 4, 16)|(12, 16, 16)|(0, 12, 16)
(0, 9, 9)|(0, 0, 12)|(0, 12, 16)|(0, 0, 12)
(9, 9, 9)|(0, 9, 9)|(0, 0, 12)|(0, 0, 0)
Table: $OPT$ values for $t \in {2, 1, 0}$

But we can do even better, what particularly interests us is the *direction* our robot would have to take. For this, we supply the following equation:

$$
OPT_{direction}(\vec{S}, t) =
\begin{cases}
up, & if\ OPT(\vec{S}, t) = \mathcal{R}\left(
\begin{bmatrix}
S_x \\
S_y
\end{bmatrix}
\right) + OPT\left(
\begin{bmatrix}
S_x \\
S_y + 1
\end{bmatrix}
, t + 1\right) \\
\\
left, & if\ OPT(\vec{S}, t) = \mathcal{R}\left(
\begin{bmatrix}
S_x \\
S_y
\end{bmatrix}
\right) + OPT\left(
\begin{bmatrix}
S_x - 1\\
S_y
\end{bmatrix}
, t + 1\right) \\
\\
right, & if\ OPT(\vec{S}, t) = \mathcal{R}\left(
\begin{bmatrix}
S_x \\
S_y
\end{bmatrix}
\right) + OPT\left(
\begin{bmatrix}
S_x + 1\\
S_y
\end{bmatrix}
, t + 1\right) \\
\\
down, & if\ OPT(\vec{S}, t) = \mathcal{R}\left(
\begin{bmatrix}
S_x \\
S_y
\end{bmatrix}
\right) + OPT\left(
\begin{bmatrix}
S_x \\
S_y - 1
\end{bmatrix}
, t + 1\right)
\end{cases}
$$

Putting this together, we display the results of applying $OPT_{direction}$ to the given example for $t = 1$ and $t = 0$ in Tab.2 and Tab.3 respectively.

||
:-:|:-:|:-:|:-:
$\cdot$|$\rightarrow$|$\downarrow$|$\leftarrow$
$\cdot$|$\rightarrow$|$\uparrow$|$\leftarrow$
$\downarrow$|$\cdot$|$\uparrow$|$\cdot$
$\cdot$|$\leftarrow$|$\cdot$|$\cdot$
Table: $OPT_{direction}$ values for $t = 1$

||
:-:|:-:|:-:|:-:
$\rightarrow$|$\rightarrow$|$\downarrow$|$\leftarrow$
$\rightarrow$|$\rightarrow$|$\uparrow$|$\leftarrow$
$\downarrow$|$\rightarrow$|$\uparrow$|$\leftarrow$
$\cdot$|$\leftarrow$|$\uparrow$|$\cdot$
Table: $OPT_{direction}$ values for $t = 0$

In Tab.2 and Tab.3, $\cdot$ was used as a filler for unmeaningful directions so as to not clutter the grid.

We are now ready to express our ideas algorithmically:

```
01: robot_opt(n, T, R[][]):
02:     initialize G[][][] with 0
03:     initialize G[T - 1] with R[][]
04:     for t = T - 2, ..., 0
05:         for x = 1, ..., n
06:             for y = 1, ..., n
07:                 max = 0
08:                 for i = -1, 1
09:                     for j = -1, 1
10:                         if i == j or i == -j:
11:                             continue
12:                         if R[x + i][y + j] > max:
13:                             max = R[x + i][y + j]
14:                 G[t][x][y] = G[t + 1][x][y] + max
15:     return G[0]
```

# Aufgabe 6

Refer to scanned page at the end!
