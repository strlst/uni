package main.java.exercise;

import main.java.framework.InstanceSet;

import java.io.BufferedReader;
import java.nio.file.Path;


public class InstanceSetImplementation extends InstanceSet<InstanceImplementation, StudentSolutionImplementation, ResultImplementation, VerifierImplementation, Object> {

    public InstanceSetImplementation(Path instanceSetPath, Path outputPath) {
        super(instanceSetPath, outputPath, ResultImplementation.class);
    }

    @Override
    protected InstanceImplementation instanceFromCsv(String line) {
        String[] splitLine = line.split(",", 5);
        String[] valuePerLength = splitLine[4].split("\\|");
        int ropeLength = Integer.parseInt(splitLine[2]);
        int[] parsedPricePerLength = new int[ropeLength + 1];
        for (int i = 1; i <= ropeLength; i++) {
            parsedPricePerLength[i] = Integer.parseInt(valuePerLength[i - 1]);
        }
        return new InstanceImplementation(splitLine[1], Integer.parseInt(splitLine[0]), ropeLength, parsedPricePerLength, Integer.parseInt(splitLine[3]));
    }

    @Override
    protected StudentSolutionImplementation provideStudentSolution() {
        return new StudentSolutionImplementation();
    }

    @Override
    protected VerifierImplementation provideVerifier() {
        return new VerifierImplementation();
    }

    @Override
    protected Object parseAdditionalInput(BufferedReader reader) {
        return null;
    }
}
