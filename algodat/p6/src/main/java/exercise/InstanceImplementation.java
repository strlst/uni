package main.java.exercise;

import main.java.framework.Instance;

public class InstanceImplementation implements Instance {

    private String groupName;

    private int number;

    private int ropeLength;

    private int[] valuePerLength;

    private int highestPossibleValue;

    public InstanceImplementation(String groupName, int number, int ropeLength, int[] valuePerLength, int highestPossibleValue) {
        this.groupName = groupName;
        this.number = number;
        this.ropeLength = ropeLength;
        this.valuePerLength = valuePerLength;
        this.highestPossibleValue = highestPossibleValue;
    }

    @Override
    public String getGroupName() {
        return this.groupName;
    }

    @Override
    public int getNumber() {
        return this.number;
    }

    public int getRopeLength() {
        return ropeLength;
    }

    public int[] getValuePerLength() {
        return valuePerLength;
    }

    public int getHighestPossibleValue() {
        return highestPossibleValue;
    }
}
