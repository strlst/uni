package main.java.exercise;

import main.java.framework.PersistAs;
import main.java.framework.Result;

public class ResultImplementation implements Result {

    private int[] ropeFragmentationNaive;
    private int[] ropeFragmentationDynProg;

    @PersistAs("duration")
    private long duration;

    @PersistAs("ropeLength")
    private long ropeLength;

    @PersistAs("valuePerLength")
    private String valuePerLengthString;

    @PersistAs("ropeFragmentationNaive")
    private String ropeFragmentationNaiveString;

    @PersistAs("ropeFragmentationDynProg")
    private String ropeFragmentationDynProgString;

    @PersistAs("elapsedNaiveTime")
    long elapsedNaiveTime;

    @PersistAs("elapsedDynProgTime")
    long elapsedDynProgTime;

    public ResultImplementation(long duration, int ropeLength, int[] valuePerLength, int[] ropeFragmentationNaive, int[] ropeFragmentationDynProg, long elapsedNaiveTime, long elapsedDynProgTime) {
        this.duration = duration;
        this.ropeLength = ropeLength;
        this.ropeFragmentationNaive = ropeFragmentationNaive;
        this.ropeFragmentationDynProg = ropeFragmentationDynProg;
        this.elapsedNaiveTime = elapsedNaiveTime;
        this.elapsedDynProgTime = elapsedDynProgTime;

        String valuePerLengthString = "";
        for (int i = 1; i < valuePerLength.length; i++) {
            if (i != 1) {
                valuePerLengthString += "|";
            }
            valuePerLengthString += valuePerLength[i];
        }
        this.valuePerLengthString = valuePerLengthString;

        String ropeFragmentationNaiveString = "";
        for (int i = 1; i < ropeFragmentationNaive.length; i++) {
            if (i != 1) {
                ropeFragmentationNaiveString += "|";
            }
            ropeFragmentationNaiveString += ropeFragmentationNaive[i];
        }
        this.ropeFragmentationNaiveString = ropeFragmentationNaiveString;

        String ropeFragmentationDynProgString = "";
        for (int i = 1; i < ropeFragmentationDynProg.length; i++) {
            if (i != 1) {
                ropeFragmentationDynProgString += "|";
            }
            ropeFragmentationDynProgString += ropeFragmentationDynProg[i];
        }
        this.ropeFragmentationDynProgString = ropeFragmentationDynProgString;
    }

    public int[] getRopeFragmentationNaive() {
        return ropeFragmentationNaive;
    }

    public int[] getRopeFragmentationDynProg() {
        return ropeFragmentationDynProg;
    }
}
