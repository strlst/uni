#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void invariant(int x, int y) {
    int z = 1;
    if (x > y) {
        int t = x;
        x = y;
        y = t;
    }
    while (y > x) {
        printf("loop start x %i y %i z %i\n", x, y, z);
        x = x + 1;
        y = y - 1;
        z = z + (y - x);
        printf("loop end   x %i y %i z %i\n", x, y, z);
        assert((y - x) + 2 != 0);
    }
}

int main(void) {
    invariant(1, -1);
    for (int i = -100; i < 100; i++) {
        for (int j = -100; j < 100; j++) {
            invariant(i, j);
        }
    }
    return 0;
}
