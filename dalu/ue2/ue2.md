% LU Dezentrale Automation Protokoll Übung 2 (183.169)
% Norbert Tremurici (11907087)
% 28. Oktober 2021

\break

# 1. Visualisierung (3 Punkte)

## 1.1. HMI Menüs

Zur Steuerung aber auch zur Visualisierung der unterschiedlichen Funktionen und des Prozessablaufes werden unterschiedliche Fenster angeboten. Dabei wurde darauf geachtet, dass die Informationen pro Menü nicht überwältigend sind, Notaus Funktionen angeboten werden und auch entsprechend präsentiert und zugänglich sind und auch sinnvoll prozessrelevante Trends einsehbar sind. Die Erklärung zur Visualisierung im Sinne dieser Übung und der Hintergrund dazu wird in den folgenden Unterkapiteln jeweils pro Menü geführt.

Nähere Ausführungen zur Bedienung und Hintergründe der einzelnen Designs sind im Abschnitt 2 (Programmbeschreibung) zu finden.

### 1.1.1. Startbildschirm

Die Hauptoberfläche ist der Startbildschirm auf den man automatisch bei Inbetriebnahme landet. Dabei ist in Fig. 1 abgebildet, wie dieser Startbildschirm aussieht. Annotiert wurden die einzelnen Komponenten des Menüs mit Zahlen um die Komponenten des Menüs in Folge aufzuschlüsseln.

![Screenshot Startbildschirm aus TIA Portal](./res/hmi-0-annotated.jpg)

1. bis 5. Buttons: um einfach auf die einzelnen Menüs zuzugreifen (auch über die Dialogbox möglich)
6. Notaus: ein Notaus muss zugänglich gestaltet sein, als solcher ist er auch auf dem Startbildschirm bedienbar (wie beim Monitormenü)

### 1.1.2. Monitormenü

In Fig. 2 dargestellt findet man einen Screenshot zum Design für das Monitormenü. 

![Screenshot Monitormenü aus TIA Portal](./res/hmi-1-annotated.jpg)

1. Ampel: die Ampel wie sie beim Galvanisierungsautomaten als Signalleuchte vorkommen würde ist hier mit einem daran angelehnten Design wieder zu finden; es werden je nach Status wie bereits in Übung 1 spezifiziert eine der drei Ampelfarben Rot, Gelb, Grün ausgegeben
2. Prozessfortschritt: da es auftreten kann, dass das HMI Interface in einer Stelle sich befindet, aus welcher der Automat nicht direkt beobachtet werden kann, macht es Sinn anzuzeigen wie weit der Schlitten bereits ausgefahren ist; unterstützend dazu sind die Positionen der Stationen intuitiv überlagert
3. Notaus: da das Monitormenü als passive Überwachungsoberfläche dient, kann es durchaus sein, dass das HMI Interface auch für beachtliche Dauer in diesem Menü verweilen wird; als solches ist es sinnvoll eine Notaus Funktion anzubieten, welche auch entsprechend bemerkbar und groß gemacht wurde

### 1.1.3. Kontrollmenü

Als nächstes folgt in Fig. 3 das Kontrollmenü. Leider sind im Screenshot die genauen Animationen nicht deutlich, daher wird auch die Visualisierungssemantik in Folge aufgeklärt.

![Screenshot Kontrollmenü aus TIA Portal](./res/hmi-2-annotated.jpg)

1. Programmanzeige: visuell abgebildet sind die Texte `Programm [1-3]`, sowie eine Box welche entweder rot oder grün leuchtet; im Betrieb kann immer nur eine davon Grün aufleuchten und signalisiert damit, ob gerade ein bestimmtes Programm aktiv ist
2. Warnschild: das Warnschild ist überlagert mit der Programmanzeige und wird aber erst sichtbar, sobald der reguläre Prozessablauf unterbrochen wurde; sie signalisiert einen irregulären Zustand
3. Stationsanzeige: ähnlich wie bei der Programmanzeige gibt es hier rote/grüne Kreise, die jeweils signalisieren welche Station belegt ist
4. Zylinderanzeige: in diesem Abschnitt gibt es eine Grafik die einen Zylinder repräsentiert, welche prozessgestuert animiert ist; der Text `WIRD BESCHICHTET` scheint nur auf wenn der Zylinder ausgefahren ist
5. Notaus: wie bereits beim Monitormenü gibt es hier eine Notaus Funktion; die Elemente 6 und 7 bzw. der Text `MANUELLER HANDSTEUERUNGSMODUS AN` erscheinen erst, sobald der manuelle Handsteuerungsmodus aktiviert wurde, welcher durch die Notausfunktion automatisch aktiviert wird
6. Schlittensteuerung: diese Schaltfläche wird erst im manuellen Handsteuerungsmodus aktiv bzw. sichtbar und dient zur Steuerung des Schlittens
7. Zylindersteuerung: diese Schaltfläche wird erst im manuellen Handsteuerungsmodus aktiv bzw. sichtbar und dient zur Steuerung des Zylinders

### 1.1.4. Programmauswahlmenü

Weiters folgt in Fig. 4 das Programmauswahlmenü.

![Screenshot Programmauswahlmenü aus TIA Portal](./res/hmi-3-annotated.jpg)

1. Miniampel: die rechteckige Fläche repräsentiert eine einfachere Version der Ampel, die jeweils gelb oder grün leuchtet
2. Textausgabe: unterschiedliche Texte werden ausgegeben, abhängig davon in welchem Zustand der Prozess mit Respekt zur Programmausführung befindet (`manueller Arbeitsschritt`, `gestoppt`, `Fehler`, `in Bearbeitung`)
3. Fehleranzeige bzw. manuelle Bestätigung: im Hintergrund ist eine rechteckige Fläche wie bereits bei 1., welche nur sichtbar ist und deutlich rot aufleuchtet, falls ein Fehler unterlaufen ist oder der man. Handsteuerungsmodus ausgelöst worden ist; darüber befindet sich ein Knopf `Ok`, welcher nur verfügbar ist, wenn der Prozess gerade einen manuellen Arbeitsschritt erfordert
4. bis 6. Programmauswahlknöpfe: dienen zur unkomplizierten Auswahl des Programmes für den Galvanisierungsprozess

### 1.1.5. Prozesstrendmenü

In Fig. 5 abgebildet ist das Prozesstrendmenü.

![Screenshot Prozesstrendmenü aus TIA Portal](./res/hmi-4-annotated.jpg)

1. Trendansicht: auf einem Platz kann die Historie von 5 Prozessrelevanten Zählwerten eingesehen werden; dabei gibt es eigene Zähler für Prozessunterbrechungen (rot), vollendete Galvanisierungsprozesse (grün), sowie eine Aufteilung auf die individuellen Programme 1 bis 3
2. Navigation Trendansicht: die Ansicht bietet auch gleich Navigationselemente um das Trendansichtfenster zu steuern

### 1.1.6. Nachrichtenmenü

Zu guter letzt gibt es ein Nachrichtenmenü wie in Fig. 6 abgebildet. Hier erscheinen in Folge Informationen/Warnungen/Fehler. Diese Alarme und Events werden näher in Abschnitt 3 (Alarme & Events) zu finden.

![Screenshot Nachrichtenmenü aus TIA Portal](./res/hmi-5.jpg)

# 2. Programmbeschreibung (4 Punkte)

Die Programmbeschreibung baut auf die Erklärung der individuellen Visualisierungselemente aus Abschnitt 1 (Visualisierung) auf und ergänzt jeweils die Funktionsbeschreibungen und Arbeitsweise.

### 2.1. Änderungen vom Programm aus Übung 1

In Übung 1 wurde bereits umfassend ein Galvanisierungsautomat entwickelt. Die Programmstruktur hat sich nicht verändert, bis auf kleine Anpassungen und natürlich alle Erweiterungen für die HMI Steuerung. Zentral sind immer noch ein Funktionsbaustein für die Schrittkette und in einem anderen Funktionsblock Zuweisungen welche für die Schrittkette notwendig sind.

Neu ist ein Funktionsbaustein `Logging`, welcher sich um die Erhebung von Trenddaten und Alarmwerten befasst. Dabei gibt es eine klare Trennung der Netzwerke, zum einen findet man Zähler für die Trenderhebung, Zuweisungen für das Alarmwort und ein HMI Hilfsvariablen Netzwerk. Logisch dazu gehören zwei neue globale Datenbausteine, `HMIDB` und `LogDB`. Das erste ist alleine für die Speicherung von HMI Variablen, auf welche die HMI Tags symbolisch zugreifen, zuständig. Das zweite speichert die Zähler sowie das Alarmwort und ist allgemein für jegliche Form der Datenerfassung zuständig.

### 2.2. Bedienungsanleitung HMI Interface

Die Namen der Menüs sollten bereits darauf deuten, wie sie zu verwenden sind. In Kürze wird der Zweck und die Bedienung der einzelnen Menüs behandelt

#### 2.2.1. Startbildschirm

Der Startbildschirm hat Knöpfe zur Aktivierung der unterschiedlichen Fenster, aber auch die Notaus Funktion. Diese ist semantisch gleich zur Notaus Funktion im Kontrollmenü (2.2.2.) und führt einen auch gleich in besagtes Menü.

An dieser Stelle war es wichtig Wert darauf zu legen, die Notausmenü leicht, groß und zugänglich auch mehrere Menüs zu verteilen, und zwar jene die einen Großteil der Zeit auch passiv laufen werden, wenn jetzt nicht jemand das HMI Interface bedient.

#### 2.2.2. Kontrollmenü

Das Kontrollmenü soll eine Übersicht liefern die vorallem praktisch ist beim beheben von Problemen. Ist nämlich der manuelle Handsteuerungsmodus aktiviert, so lassen sich Schlitten und Zylinder von hier aus steuern. Aktiviert kann dieser gleich werden, indem die Notaus Funktion (`STOP` Button) verwendet wird. Diesen Modus verlässt man auch wieder, indem die Notaus Funktion erneut betätigt wird. (Toggle Semantik)

#### 2.2.3. Monitormenü

Der Monitor hat nur ein Bedienelement, und das ist die Notaus Funktion. Dieser bringt einen gleich ins Kontrollmenü und verhält sich gleich, wie im Kontrollmenü. Ansonsten können hier Schlittenposition sowie die Ampel eingesehen werden und ist somit gedacht, den Prozess beobachtbar zu machen, auch wenn der Galvanisierungsautomat nicht notwendigerweise gesehen werden kann.

#### 2.2.4. Programmauswahlmenü

Im Programmauswahlmenü kann der Prozess teilweise gesteuert werden. Sollte der Prozess ungestört bleiben, kann hier laufend das Programm ausgewählt werden sowie der manuelle Arbeitsschritt bestätigt werden. Im Falle eines Fehlers ist das Kontrollmenü aufzusuchen.

#### 2.2.5. Prozesstrendmenü

Das Prozesstrendmenü hat nur die Bedienelemente die mit der Trendansicht von Haus aus gekoppelt sind. Unter anderem kann die Zeitleiste vergrößert oder verkleinert werden. Näheres wird in Abschnitt 4 (Trends & Historische Daten) erklärt.

#### 2.2.6. Nachrichtenmenü

Das Nachrichtenmenü bietet eine Liste an Meldungen. Näheres in Abschnitt 4 (Trends & Historische Daten).

### 2.3. Annahmen

Es gelten die selben Annahmen wie bereits bei Übung 1, da das wirkliche Kernstück der Lösung sich nicht verändert hat.

Eine wichtige Annahme war, dass die Verteilung der Funktionen auf mehrere HMI Fenster zulässig ist. Abhängig von den Anforderungen könnte das ein Problem darstellen, aber andererseits funktioniert die gelieferte Implementierung gut sogar auf kleinen Bildschirmen. Dabei wurden so sinnvoll wie möglich alle Funktionen und Informationen auf so wenige Bildschirme wie nötig verteilt.

In Folge lässt sich die Frage stellen wie das HMI Interface genau zum Einsatz kommen soll. Es wurde hier nicht angenommen, dass das Interface sich gleich bei dem Automaten befindet, da man in einem solchen Fall gleich die Steuerung des Automaten verwenden kann. Es ist somit möglich den Automaten sinnvoll über das HMI Interface zu betreiben, auch wenn man keine Sicht auf den Automaten hat.

# 3. Alarme & Events (2 Punkte)

Eine Liste an Alarmen sowie Events und deren Semantik sowie Meildungstext wird in folgender Tabelle bereitgestellt:

|Alarm|Semantik|Text|
|:-:|:-:|:-:|
|Reset (HMI)|Error|(HMI) man. Handst.modus|
|Reset (PLC)|Error|(PLC) man. Handst.modus|
|Programm 1 Start|No Acknowledgement|Programm 1 gestartet|
|Programm 2 Start|No Acknowledgement|Programm 2 gestartet|
|Programm 3 Start|No Acknowledgement|Programm 3 gestartet|
|Sonar|Warning|Objekt zu nah an Automat|
|EinLS|Warning|Schlitten blockiert|
|Switch0|Warning|Notschalter betätigt|

Dabei wurde global überschrieben, wie Alarme und Events aufscheinen, um sie besser in das allgemeine Konzept zu integrieren. Meldungen erscheinen alle in einer kleinen einzeiligen Leiste ganz oben am Bildschirm, wo sie den Betrieb nicht stören aber auch nicht zu leicht ignoriert werden können.

Meldungen die eine Quittierung benötigen kommen in einem Popup Fenster, welches die untere Hälfte des Bildschirms einnimmt. Es sollte immer noch möglich sein, die Notaus Funktionen zu benutzen. Dabei wurde auch versucht, die Quittierung im Programmablauf miteinzubeziehen: sollte der manuelle Handsteuerungsmodus durch die Notaus Funktion aktiviert werden, so scheint eine Meldung auf, welche quittiert werden muss um diesen wieder zu verlassen. Dabei kann der HMI ausgelöste manuelle Handsteuerungsmodus nur beendet werden, wenn die Quittierung bereits stattgefunden hat. Bei PLC ausgelösten manuellen Handsteuerungsmodi wird das nicht erfordert.

# 4. Trends & Historische Daten (1 Punkt)

Im Prozesstrendmenü kann eine Trend über ein laufendes Zeitfenster mit dem Umfang einer Stunde beobachtet werden. Dabei gibt es fünf Trendkurven die von Interesse sind, Prozessunterbrechungen (rot), vollendete Galvanisierungsprozesse (grün) und genau welche Programme wann erfolgreich beendet wurden (orange, blau und violet). Vorgesehen ist hier, dass diese Daten für die Dauer eines Tages erfasst werden, damit am Ende des Tages eine brauchbare Statistik erfasst werden kann.

Auch interessant wären an dieser Stelle genauere Erhebungen zum Galvanisierungsprozess. Die Dauer die ein Werkstück in einer Station verweilt wäre von Interesse, um den Prozess qualitatitv zu erhöhen. Dabei könnte man pro Station auftragen, wie lange unterschiedliche Werkstücke in der Station verweilt sind, sowie ein Mittelwert. Womöglich nur sehr schwer umsetzbar wäre ein Boxplot für jede Station.

Ursprünglich geplant, aber aufgrund von Schwierigkeiten nicht durchgeführt, war eine x/y Trendanzeige. Dabei wären die Prozessunterbrechungen auf der y-Achse und die vollendeten Galvanisierungsprozesse auf der x-Achse aufgetragen gewesen, so dass eine Kurve entsteht aus der man intuitiv die Erfolgsrate ablesen kann. Eine Flache Kurve würde auf ein erfolgreicher Prozess deuten, während eine hoch steigende Kurve viele Fehler signalisiert. So eine Ansicht ließe sich bestimmt implementieren.

# 5. Notizen zür Übung (Feedback)

- eine angenehmere Übung als Übung 1, weil man sich hier schon ein bisschen in die Arbeitsweise mit TIA eingefunden hat und die Designarbeit nicht so technisch ausfällt, wie die letzte Übung war; definitiv gerechtfertigt im Ausmaß
- Simulation wäre interessant gewesen, aber war dann doch kein so großer Punkt und kann vermutlich auch gar nicht so gut funktionieren, da die Applikation bereits eng an das Target und seine Ein-/Ausgänge gekoppelt ist
