% LU Dezentrale Automation Protokoll Übung 1 (course number)
% name lastname (matriculation number)
% 26. Oktober 2021

\break

# 1. Sensor/Aktor Zuordnung (3 Punkte)

Die einzelnen Stationen sind aufgeteilt auf die 4 schön verteilten Sensoren entlang der Schlitteneinrichtung. Um die Stationen anzusteueren wird wird mit dem Schlitten und dem Encoder gearbeitet.

Arbeitsbereiche welche erfordern, dass der Automat nicht läuft (etwa weil Arbeit in diesen Bereichen während der Operation des Automaten gefährlich wäre) sind mit Sensoren ausgestattet um Unfällen vorzubeugen. Abgebildet wurde das mithilfe der Sicherungslichtschranke und dem Ultraschall Mehrbereichs-Näherungsschalter.

Die LEDs zeigen dabei bei jeder Beschichtung an, ob gerade eine Station belegt ist und gegebenenfalls auch welche. Die vier Lampen halten sich an die Semantik der Ampel aus der Angabe. Die Taster zu den Lampen bieten in Situationen in welchen eine manuelle Eingabe gefragt ist die Bestätigung des Arbeiters. Die Schalter dienen auch einigen Funktionen, zum einen repräsentieren sie ein handgesteuertes Notaus mit dem gleichzeitig der Automat in den manuellen Handsteuerungsmodus versetzt wird, zum anderen dienen sie auch im jeweiligen Schritt zur Programmauswahl oder der manuellen Handsteuerung des Zylinders. Zu guter Letzt kann im manuellen Handsteuerungsmodus mithilfe des Joysticks die Schlittenposition gesteuert werden.

Sensor/Aktor|Beschreibung
:--|:--
Schlitteneinheit|Galvanisierungsautomat Transportwagen
` `|und Stationen
Drehwinkelgeber|Transportwagen Positionsmessgerät
Motor|Galvanisierungsautomat Transportwagenmotor
Zylinder|Transportwagen Absenkung/Heraushebung
Induktiver Näherungsschalter|Station 1
Kapazitiver Näherungsschalter|Station 2
Kapazitiver Näherungsschalter|Station 3
(PosElM)|` `
Optischer Näherungsschalter|Station 4
Lamper[3-0]|Ampelelemente
LED[7-4]|Stationbelegungsanzeige
Joystick|manuelle Transportwagenhandsteuerung
Taster[3-0]|manuelle Bedienelemente
Schalter[7-0]|manuelle Bedienelemente
Sonar|Notaussensor
Sicherungslichtschranke|Notaussensor

# 2. Programmbeschreibung (4 Punkte)

### 2.1. allgemeine Beschreibung

Das Programm ist strukturiert in der Form einer Schrittabfolge in welcher die Zustände in einem Funktionsblock "Galvanisierung" behandelt werden, während die Zuweisungen die aus dem Zustand folgen in "GalvanisierungZuweisungen" zu finden sind. Dabei wird ein Datenbankblock "GalvanisierungDB" verwendet in dem gewisse Informationen sowie der Zustand abgespeichert werden. Anhand des Funktionsblocks für die Zustände ist leicht ersichtlich wofür die einzelnen Zustände zuständig sind und welche Bedingungen erfüllt sein müssen, damit in den nächsten Zustand gewechselt werden kann. Ein grober Ablauf des Programms und die Bedeutung der Zustände ist in folgendem *flowchart* (Figur 1) ersichtlich.

![Ablaufdiagramm Galvanisierung](./res/galvanisierung.png)

Was nicht auf dem *flowchart* zu erkennen ist die genaue Notfalls- und Fehlersemantik. Diese wird in den nachfolgenden Protokollpunkten behandelt. Auch zu beachten ist, dass Zustände für die jeweiligen Beschichtungen die identische Aufgaben erledigen trotzdem nicht konsolidiert aufzufinden sind. Der Hintergrund dafür ist um gegen mögliche Komplikationen bei der Erweiterung des Prozesses durch die resultierende Unlesbarkeit und Unanpassbarkeit vorzubeugen.

### 2.2. Bedienungsanleitung

Die Bedienung erfolgt grob erklärt auf folgende Art und Weise:

1. Der Prozess wird gestartet, der Automat begibt sich in Ausgangsposition, damit das Werkstück eingelegt werden kann
2. Wurde das Werkstück eingelegt, muss der gelbe Taster gedrückt werden um dies zu bestätigen. Dabei blinkt die gelbe Lampe in diesem Schritt bis durch den gelben Taster der manuelle Arbeitsschritt bestätigt wurde.
3. Das Programm muss ausgewählt werden indem ein dem Programm entsprechender Schalter geschalten wird und durch drücken des grünen Tasters die Auswahl bestätigt wird. Dabei darf nur ein Schalter geschalten sein, sonst wird die Bestätitung nicht durchgeführt. Die Programme für Beschichtung A, Beschichtung B und Beschichtung C entsprechen den Schaltern 7, 6 und 5. In diesem Schritt leuchtet die grüne Lampe.
4. Abhängig von der Programmauswahl wird das Programm für eine der drei Beschichtungen durchgeführt. Für den Rest des Prozesses ist keine Bedienung mehr notwendig (außer in einer Sicherheits- oder Fehlerbedingung). Kann das Programm problemlos ausgeführt werden, so erledigt der Automat seine Arbeit und begibt sich wieder in Startposition damit das fertige Werkstück entnommen und ein neues eingelegt werden kann. Der Prozess beginnt somit wieder von Schritt 1.
5. Das Programm kann in jedem Schritt unterbrochen und zurückgesetzt werden indem Schalter 0 aktiviert wird und während Schalter 0 aktiv ist der rote Taster gedrückt wird.

\break

### 2.3. Annahmen

In dem Schritt für die Programmauswahl wurde angenommen, dass das Programm nicht in einen Fehlerzustand geraten soll wenn mehrere Programme ausgewählt werden. Die Programmauswahl wurde ja über eine Kombination von Schaltern und dem grünen Taster implementiert, wodurch dieser Fall überhaupt erst entsteht. Das wurde gemacht, weil nicht genügend Taster zur Verfügung stehen, es verwirrend geworden wäre weil die Bedeutung der Taster überladen wäre und sonst keine neuen Programme hinzugefügt werden können ohne neue Taster zu installieren. Dabei verweilt das Programm in dem Programmauswahlzustand bis eine valide Eingabe getätigt wurde.

Die Positionserkennung der einzelnen Stationen hängt vom Encoder Drehwinkelgeber ab. Das wurde gemacht weil der Distanzsensor zu große Diskretisierungsfehler hat und ungenau ist. Die Sensoren wurden dafür auch nicht verwendet, weil sie unterschiedlich früh oder spät schalten, abhängig von dem Material des Werkstücks. Eine inhärente Annahme ist also, dass eine Positionserkennung über dem Drehwinkelgeber sinnvoll ist. Das wird damit begründet, dass in einem industriellen Kontext der Automat einmal konstruiert und kalibriert wird, aber abgesehen davon sich nicht weiter verändert.

Die manuelle Bedienung funktioniert nur im Fehlerzustand. Diese Entscheidung wurde getroffen, einerseits weil bei Fehlern in Folge eine manuelle Bedienung notwendig ist, aber andererseits das die eleganteste Lösung darstellt. Eine automatische Fehlerbehandlung findet nicht statt, weil dies sehr schnell zu kompliziert wäre zu implementieren. Die automatische Fehlerbehandlung wäre nämlich selbst wieder anfällig für Fehler. Wird also eine Fehlerbedingung ausgelöst, so befindet sich der Automat im manuellen Handsteuerungsmodus und keine weiteren automatischen Schritte erfolgen, selbst wenn dieser beendet wird. Es muss ein $Reset$ über entsprechende Bedienelemente stattfinden.

# 3. Sicherheitsbedingungen (2 Punkte)

### 3.1. automatischer Notaus

*Folgender Aufzählungspunkt übernommen aus Abschnitt 1:* Arbeitsbereiche welche erfordern, dass der Automat nicht läuft (etwa weil Arbeit in diesen Bereichen während der Operation des Automaten gefährlich wäre) sind mit Sensoren ausgestattet um Unfällen vorzubeugen. Abgebildet wurde das mithilfe der Sicherungslichtschranke ($EinLS$) und dem Ultraschall Mehrbereichs-Näherungsschalter ($Sonar$). Dabei gibt es einen allgemeinen Fehlerzustand in dem eine manuelle Bedienung möglich ist, und dieser wird einerseits durch den Schalter 0 oder die Sensoren aktiviert.

$S$ bezeichne hier die Menge aller Zustände, und die Funktion folgende Signatur: $State(x) = \text{Zustand x} \in S \text{ ist aktiv}$. Der Zustand $ManualStep \in S$ bezeichne den manuellen Handsteuerungsmodus. Zusätzlich definieren wir die logischen Aggregatvariablen

$$Sonar = SonarWeit \land SonarMittel \land SonarNah$$

für den Automaten gilt

$$State(x) \land (Switch0 \lor EinLS \lor Sonar) \implies State(ManualStep)$$
$$State(ManualStep) \equiv Reset, x \in S$$

Der Reset aller Zustände ist also äquivalent mit der Aktivität des manuellen Handsteuerungsmodus, welcher gleichzeitig den Notauszustand repräsentiert. Eine Begründung für diese Entscheidung lässt sich in Abschnitten 2.3 und 4 finden. Betrachtet man noch die Zuweisungen z.B. für die Bewegung des Schlittens oder die Steuerung des Zylinders, so ist ersichtlich dass in dem manuellen Handsteuerungsmodus diese sich nicht von selbst (sprich automatisch) bewegen können.

$$State(ManualStep) \implies McGo = (JoystickIsUp \lor JoystickIsDown)$$

Somit kann im manuellen Handsteuerungsmodus nur der Joystick den Schlitten beeinflussen und die Alnage ist somit sicher, außer ein Mitarbeiter sieht vor einen anderen zu verletzen. Formell wird das für den Zylinder in dem nächsten Abschnitt gezeigt.

### 3.2.

Der pneumatisch angetriebene Zylinder ist ein kräftiges und somit für vorallem die manuelle Bedienung gefährlicher Aktor. Aus diesem Grund ist die manuelle Bedienung so gestaltet, dass die Wahrscheinlichkeit den Zylinder unabsichtlich zu bedienen sehr klein ist. Damit der Zylinder gesteuert werden kann, muss zuerst in den manuellen Handsteuerungsmodus geschalten werden ($Switch0$), zunächst die Zylinderbedienung eingeschalten werden ($Switch1$) und erst dann kann mit dem Zylinderschalter ($Switch2$) der Zylinder binär gesteuert werden.

$$State(ManualStep) \implies ZylUp = (Switch0 \land Switch1 \land \neg Switch2)$$
$$State(ManualStep) \implies ZylDown = (Switch0 \land Switch1 \land Switch2)$$

Somit ist die Steuerung des Zylinders im manuellen Handsteuerungsmodus einzig und allein von manuellen Bedienelementen entschieden und der Automat daher sicher vor Unfällen die durch Automation geschehen können.

# 4. Fehlerbedingungen (1 Punkt)

Der Drehwinkelgeber bestimmt die Position für alle Stationen, somit ist eine fehlerhafte Kalibrierung oder sonstiger Fehlerzustand des Encoders ausschlaggebend für die Operation des Automaten. Eine mögliche, aber zeitmangels nicht eingebaute Fehlerbedingung, könnte sein dass in dem komplett zurückgefahrenen Zustand des Schlittens (wenn der magnetische Endschalter $MagE_D$ an ist) die Drehwinkelgeberposition gegen einen gespeicherten Wert verglichen wird. Auch möglich wäre es gleich die Kalibrierung jede Runde in demselben Zustand automatisch durchführen zu lassen.

Gerade passiert die Galvanisierung naiv, das bedeutet, der Automat weiß nicht ob ein Arbeiter tatsächlich ein Werkstück eingelegt hat. Eine Erkennung wäre abhängig von dem Material des Werkstücks über entsprechende Sensoren erkennbar. Leider nicht über die Höhe des Zylinders in seiner betätigten Position, da diese keine auslesbare Eigenschaft ist. Wurde aufgrund von Komplexität und Zeitmangel nicht implementiert.

Zu guter Letzt gibt es den manuellen Handsteuerungsmodus, welcher selbst als Fehlerbedingung angesehen werden kann, weil in diesem Zustand der Automat nicht automatisch operieren darf.

Fehlerbedingung|Beschreibung
:--|:--
$Switch0$|manueller Handsteuerungsmodus, alle automatischen Prozesse müssen eingestellt werden
$MagE_D \land (EncoPos \neq p)$|Drehwinkelgeber fehlkalibriert (Startposition $p$ entspricht nicht tatsächlicher Position)
$State(Station1) \implies \neg IndNS$|kein für die Vorbereitung valides Werkstück vorhanden
$State(Station2) \implies \neg KapNS$|kein für die Nickelelektrolytbehandlung valides Werkstück vorhanden
$State(Station3) \implies \neg PosElM$|kein für die Rhodiumelektrolytbehandlung valides Werkstück vorhanden
$State(Station4) \implies \neg OptNS$|kein für die Goldelektrolytbehandlung valides Werkstück vorhanden

# 5. Notizen zür Übung (Feedback)

- es war nicht ganz klar wie das Protokoll genau aufgebaut hätte sein sollen, daher wäre es toll wenn auf mögliche Probleme mit diesem Protokoll aufmerksam gemacht werden
- auch wenn nicht genug Zeit dafür da gewesen wäre, ich hatte schon Ideen zum Beispiel den Buzzer in gewissen (vorallem sicherheitsbedingten) Situationen einzuschalten oder ähnliches, aber es ist nicht klar wie diese über die Angabe hinausgehende Arbeit aufgenommen wird, das kann vielleicht mit in die Angabe übernommen werden
- die Sensoren selbst wurden nicht verwendet um zu erkennen in welcher Station der Transportwagen gerade sein soll weil die Sensoren zu ungenau sind und das das Beispiel erheblich verkompliziert
- es ist auf jeden Fall toll mit so einem Target zu arbeiten und vorallem die Unterschiede der Sensoren und Werkstoffe erleben zu dürfen
- als Anregung würde ich gerne nennen, dass aus meiner Perspektive die Aufgabe sehr stressig ist, zum einen wird man ins kalte Wasser geworfen, zum anderen sind es essentiell 3 Tage Zeit die man im Labor hat
- nach erheblichen Installationsproblemen und später auch Lizenzproblemen habe ich wenig Hoffnung sinnvoll an dem Beispiel von zu Hause aus zu arbeiten, daher kann ich mir nur wünschen mehr Zeit im Labor zu bekommen; das Problem geht so weit, dass ich gerade beim Schreiben dieses Protokolls kein Zugang auf meine eigene Lösung habe; wenn es Ressourcen gibt mit denen diese Probleme vermieden können werden fände ich es sinnvoll wenn diese im Rahmen der LVA angegeben werden
