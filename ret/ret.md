# Schlüsselfrage

RISC-V kann eine Zukunft schaffen mit Prozessoren die mehr Energie sparen, offener zugänglich für Nutzer, Entwickler und Hersteller sind und weniger für alle kosten.

# Elevator Pitch

### Linux

Betriebssyteme sind ein wichtiger Bestandteil eines jeden Computersystems und Smartphone heute.
Ohne Betriebssytem kann ein Computer nicht für alltägliche Zwecke verwendet werden und ist für den normalen Benutzter unbedienbar.
In der Vergangenheit haben proprietäre Lösungen wie Microsoft Windows den Markt dominiert, aber in den letzten Jahren wurde ein interessanter Trend verzeichnet.
Ein Betriebssystem namens Linux, welches komplett frei entwickelt wurde und ohne Kosten verwendet und weiter modifiziert werden darf betreibt heutzutage 85% aller Smartphones, über 96% aller Webserver und 100% aller TOP500 Supercomputer auf der ganzen Welt. 

Zuvor war es undenkbar dass Linux eingesetzt werden würde für Desktop PCs.
Ein Bundesland in Deutschland hat beschlossen, seine Regierungsinfrastruktur auf Linux zu migrieren.
Der Vertreiber der weltweit größten Gaming Plattform hat mit der Steam Deck eine Handheld Konsole gebaut die Linux verwendet.
Zunehmend wird Linux seine bekannten Konkurrenten mit Windows und Mac OS verdrängen.

Linux ist das Betriebssystem der Zukunft.


#### [1]
https://www.top500.org/statistics/details/osfam/1/

#### [2]
https://www.enterpriseappstoday.com/stats/linux-statistics.html

\newpage

# Pecha Kucha

#### Slide 1

El Capitan.
Das ist der Name des Berges der sich über den Yosemite National Park aus Kalifornien erstreckt.
Dieser Berg ist 2308m hoch und besitzt eine prominente Felswand, die 900m hoch ist.
Die Unrauheit dieser Felswand lässt einen fast denken, dass sei kein normaler Berg.

Was wenn ich euch erzählen würde, dass es jemanden gibt der über diese Felswand nach oben klettern will.
Und zwar klettern, ganz ohne Hilfsmittel.
Kein Seil, kein Sicherungsgerät, kein Fallschirm, nichts.

#### Slide 2

Diese Person gibt es wirklich, sein name ist Alex Honnold.
Alex Honnold ist ein Amerikaner, geboren in 1985 in Kalifornien.
El Capitan ist nicht der erste Berg den er ohne Sicherheitsmaßnahmen sich vorgenommen hat.
Aber sein ultimativer Traum war es immer, El Capitan zu bezwingen.

Imposant ist die Felswand vor ihm gestanden, unbeweglich wie jeder andere Berg.
Aber der Tag ist gekommen, an dem er ihn bezwungen hat.

#### Slide 3

Seine Reise ist dokumentiert in dem Film Free Solo, wie auch sein unbeholfener Akt des Kletterns allgemein genannt wird.
Die spezifische Route die er geklettert ist hat einen Schwierigkeitsgrad von 7c+, was ganze 17 aus insgesamt 30 Schwierigkeitsgraden, nach dem französischen System, von der einfachstmöglichen Route entfernt ist.
Keineswegs einfach.

#### Slide 4

Vorbereitet hat er sich auf diesen Tag eine sehr lange Zeit.
Aus seiner Sicht, sein ganzes Leben.
Er dachte öfters, dieser Tag wird vermutlich nicht kommen, und dennoch hat er menschliche Höhen erreicht, die zuvor unvorstellbar gewesen sind.
Von Start bis Ende hat sein Versuch ganze 3 Stunden und 56 Minuten gedauert.
Die körperliche Anstrengung dabei ist schwer zu begreifen.

#### Slide 5

Etwas nach der Mitte gibt es eine Stelle, wo es keinen leichten Weg gibt voranzuschreiten.
Hier ist man an einer Ecke des Berges und gezwungen, seinen Fuß auf die linke Seitenwand zu kicken, ohne das er dann abrutscht.
An dieser entscheidenden Stelle, auch genannt das "Boulder Problem", war die Spannung so hoch, dass das Kamerateam welches alles verfilmt hat, nicht hinschauen konnte.

Ich bin unendlich begeistert davon, dass wenn man sich wirklich, wirklich, wirklich etwas vornimmt, man sogar eine wortwörtliche Felswand erklimmen kann.
Danke für eure Aufmerksamkeit.
