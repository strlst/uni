## Chapter 1

- In the lecture we used a hardware description language (VHDL) to design our circuits. What benefit do we get from using HDLs? For what purposes, besides implementation, can HDLs be used for?
  - it also serves as documentation of a specific design, as VHDL describes the structure and behavior we expect out of it
  - communication, the design can be shared
  - abstraction, helps producivity gap
- Explain the verification/productivity gap and argue how design languages like VHDL and Verilog help to tackle the problem!
  - if we model how many transistors we could fit together and produce in a process as a function $x(t)$, then the amount of transistors we can actually put together in a design ($y(t)$) is bounded by this function, the amount of transistors in a design that can be verified to be correct ($z(t)$) is smaller yet still and bounded by $y(t)$
- Add the missing parts of the table comparing SW and HW!
- Draw the hardware implementation of the following code piece (e.g. those from the slides). What are the biggest differences compared to an execution in software?
  - parallelism of execution, non-sequentialism, no real loops, real time usually modelled by counters
- Name at least three properties that can be tuned in a custom hardware design.
  - safety, security, power consumption, size, speed
- Split the arguments in a given task description into two classes: Those suggesting to use a hardware implementation (mark with HW) and those suggesting a software approach (mark with SW). Explain your classifications!
  - sw: turn based game
  - hw: highly concurrent

\newpage

## Chapter 2

- Explain the initial purpose of VHDL.
  - documentation language
  - model behavior and structure of hardware using a programming language
- Name the purpose of the entity/architecture in VHDL.
  - entity provides the definition of a structural component while architecture provides a concrete implementation of that structural component
- Underline in the shown entity all port modes! Note for each the respective read- and write-rights from within the entity.
  - IN, OUT, INOUT, BUFFER
- draw the temporal evolution of the shown concurrent code piece
- implement the shown concurrent code piece using the sequential design style
  - sequential design style == processes
  - ```vhdl
[ label ] : process [ (sensitivity_list) ]
begin
    [ statements ]
end process [ label ];
```
- draw an equivalent circuit implementation of the shown code piece
- Mark in the shown code piece instance and component name! Is positional or named mapping used?
- Mark configuration, component declaration and component instantiation! Map them to the example of mounting sockets on a PCB presented in the lecture!
  - `component COMPONENT is generic () port () end component;` within architecture header, `INST : entity ENTITY(ARCHITECTURE) generic map () port map ();` within architecture body
- Provide the entity of a testbench an explain its shape.
  - the testbench provides the entire universe for the unit under test! there should be not outgoing signals
  - ```vhdl
entity top is
end entity;
```
- Apply the given delay statements to the shown input trajectory! Expand the shown abbreviated statement to the explicit long form.
  - some combination of `transport`, `reject` and `inertial`
- Analyze the execution behavior of the shown process (once, infinitely often, never) and use the correct wait statement such that it is run once, at a change on signal A, every 20 ns, ...
- Explicitly state the signals that are included with the all keyword in the sensitivity list. Convert the sensitivity list to an equivalent wait statement.
  - `wait on sensitivity_list;`
- Draw the equivalent circuit implementation of an if / case / loop.
  - if corresponds to a mux chain?
  - case corresponds to one big mux?
  - loop corresponds to gate chain?
- Analyze multiple processes that differ in their usage of variables and signals. How often is the process executed following a signal change and what is the state of the signals / variables at the end of each iteration.
- Describe in one to two sentences the main properties of concurrent, sequential and structural design style.
  - while in structural design style we define the how structural components are wired together, in sequential design style we instead specify their behaviour, while finally in the concurrent design style we specify the what happens at the gate level
- Analyze the shown implementation. Which style is used? Transform it to style X.
- Which memory element is shown here (Latch or Flip Flop)? Transform it to the other one.
  - `rising_edge` (or `falling_edge`) => flip flop, else latch
  - to get a latch, leave some cases in process unspecified (inferred latch)
- What is wrong with the shown implementation? Adapt it, such that an asynchronous reset is achieved.
- Explain when finite state machines have to be used.
  - when there are a known amount of cases each with specific behavior, in general when we know that certain combinatorial logic has different meanings depending on the state of the circuit to generete certain output combinatorial logic
- Name the main components of a state machine and their purpose.
  - next-state logic: determines the next state to be stored as a function of ingoing signals and current state
  - state (flip-flops): stores current state
  - output logic: generates outgoing signals as a function of ingoing signals and current state
- Identify an inferred latch in small code piece (missing assignment (trainingAssignment.pdf)) and correct the error.
- Explain and compare the 1, 2 and 3-process method. Are they equally powerful?
  - 1 process method: one synchronous process
    - less expressive or powerfull than other two
  - 2 process method: one synchronous, one asynchronous process
  - 3 process method: one synchronous, two asynchronous processes
    - as expressive or powerful as 2 process method
- Provide a small code piece that generates a clock signal with a period of CLOCK_PERIOD in a testbench.
  - assumes we have a time `CLOCK_PERIOD` and `stop_clock` already specified
  - ```vhdl
generate_clk : process
begin
    while not(stop_clock) loop
        clk <= '1';
        wait for CLOCK_PERIOD / 2;
        clk <= '0';
        wait for CLOCK_PERIOD / 2;
    end loop;
end process;
```
- Explain purpose and usage of report on a small example.
  - report is used for (unsynthesizable) debugging statements useful in simulations and can be used akin to `report <string>;`
  - optional specifiers for severity `report <string> severity (note|warning|error|failure);`
- Explain which of the data types std_logic, unsigned and integer can be used for computation.
  - std_logic represents the states an actual piece of wire can assume, so probably not std_logic
  - integer definitely can be used for computation
  - not sure with unsigned
- Convert an integer to a std_logic_vector and vice versa.
  - int to std_logic_vector: `std_logic_vector(to_unsigned(int_val, width))`
  - std_logic_vector to int: `to_integer(unsigned(int_val))`
- Mark the signal attribute in the shown architecture. What is the advantage of using attributes?
  - attributes can make designs more readable and can also be powerfully expressive
