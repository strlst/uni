//=============================================================================
// Project  : Industrial HW Verification
//
// File Name: avmm_readback_seq.sv
//
//
// Version:   1.0
//
// Code created by Easier UVM Code Generator version 2016-04-18-EP on Thu Apr 28 06:08:21 2022
//=============================================================================
// Description: Password sequence for agent avmm
//=============================================================================

`ifndef AVMM_READBACK_SEQ_SV
`define AVMM_READBACK_SEQ_SV

class avmm_readback_seq extends uvm_sequence #(avmm_tr);

  `uvm_object_utils(avmm_readback_seq)

  int MAX_U8_VALUE = 255;
  int MAX_U32_VALUE = 4294967295;
  
  extern function new(string name = "");
  extern task body();

endclass : avmm_readback_seq


function avmm_readback_seq::new(string name = "");
  super.new(name);
endfunction : new


task avmm_readback_seq::body();
  int i;
  int address[5] = {0, 1, 127, 128, 255};

  `uvm_info(get_type_name(), "readback sequence starting", UVM_HIGH)

  i = $urandom_range(0, 4);
  `uvm_do_with(req, {read == 1; write == 0; address == address[i];})
  `uvm_do_with(req, {read == 1; write == 0; address == address[i];})

  i = $urandom_range(0, 4);
  `uvm_do_with(req, {read == 0; write == 1; address == address[i];})
  `uvm_do_with(req, {read == 1; write == 0; address == address[i];})

  i = $urandom_range(0, 4);
  `uvm_do_with(req, {read == 1; write == 0; address == address[i];})
  `uvm_do_with(req, {read == 0; write == 1; address == address[i];})

  i = $urandom_range(0, 4);
  `uvm_do_with(req, {read == 0; write == 1; address == address[i];})
  `uvm_do_with(req, {read == 0; write == 1; address == address[i];})

  `uvm_info(get_type_name(), "readback sequence completed", UVM_HIGH)
endtask : body


`endif // AVMM_READBACK_SEQ_SV

