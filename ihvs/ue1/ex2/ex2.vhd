library common_lib;
context common_lib.common_context;
  
entity ex2 is
end entity;

architecture behav of ex2 is

  type StateT is (Idle, Invert, NotAffected, Keep);
  signal state: StateT := Idle;
  signal clock, clock_n, output: std_logic := '0';
  signal eventCounter: integer := -1;
  signal transCounter: integer := -1;

  -- here to visualize attributes of output signal
  type OutputAttributesT is record
    AEvent : boolean;
    ATransaction : bit;
    AActive : boolean;
    ALastEvent : time;
    ALastActive : time;
    ALastValue : std_logic;
  end record;
  signal outputAttributes : OutputAttributesT;

  /* Exercise 2 Task 1 answers:
  - Process `event_count_p` is triggered on `output`, as it should only be updated if there is an
       actual event
     - Process `trans_count_p` is instead triggered on its attribute, `output'transaction`, as it
       should only be updated on transitions
     - In state Idle, `output` is continually assigned '0', which causes no events, but a transaction
       in every clock cycle.
     - In state NotAffected, `output` is continually assigned `unaffected`, which causes *no*
       updates to its `event` or `transaction` attribute
     - In state Keep, `output` is continually assigned itself (latch behavior), which doesn't cause an
       event but does cause transactions still, note that this is similar to the Idle state
   */

   /* Exercise 2 Task 2 answers:
     - A process can be made sensitive on the `output'transcation` attribute, but we could also make
       this visible as a signal by storing it (like it was done) in the `outputAttributes` signal,
       which was originally created for debug purposes to view various attributes of the `output`
       signal. Since the attribute on a signal is just another signal, one can also assign it to
       a signal.
     - The process `state_p` toggles on rising edges just like a normal synchronous process would.
       This is just a normal description of registers, which capture the values that have previously
       (hopefully) become stable within the clock period. After the registers assume their new
       values, the logic will after some time (hopefully) within the clock period assume their next
       stable value. Since we want to compare with the stable values, we check the outputs on the
       falling edge! In simulations transitions occur instantly, so all values will have become stable
       after half a clock cycle.
    */
  
begin

  CreateClock(clock, 10 ns);
  clock_n <= not clock;

  outputAttributes <= (
    AEvent => output'event,
    ATransaction => output'transaction,
    AActive => output'active,
    ALastEvent => output'last_event,
    ALastActive => output'last_active,
    ALastValue => output'last_value
  );

  state_p: process (clock) is
  begin
    if rising_edge(clock) then
      case state is
        when Idle =>
          output <= '0';
        when Invert =>
          output <= not output;
        when NotAffected =>
          output <= unaffected;
        when Keep =>
          output <= output;
      end case;
    end if;
  end process;

  event_count_p: process (output) is
  begin
    eventCounter <= eventCounter + 1;
  end process;

  trans_count_p: process (output'transaction) is
  begin
    transCounter <= transCounter + 1;
  end process;

  stimuli_p: process is
  begin
    -- During initialization, the process is evaluated until the first wait statement
    wait for 0 ns;
    state <= Idle;
    WaitForClock(clock_n, 20);

    -- only the transaction counter should have incremented by 20
    AffirmIfEqual(eventCounter, 0, "event counter does not have the expected value");
    AffirmIfEqual(transCounter, 20, "transition counter does not have the expected value");

    state <= Invert;
    WaitForClock(clock_n, 20);

    -- both should have incremented by 20
    AffirmIfEqual(eventCounter, 20, "event counter does not have the expected value");
    AffirmIfEqual(transCounter, 40, "transition counter does not have the expected value");

    state <= NotAffected;
    WaitForClock(clock_n, 20);

    -- nothing should have changed
    AffirmIfEqual(eventCounter, 20, "event counter does not have the expected value");
    AffirmIfEqual(transCounter, 40, "transition counter does not have the expected value");

    state <= Keep;
    WaitForClock(clock_n, 20);
    
    -- only the transaction counter should have incremented by 20
    AffirmIfEqual(eventCounter, 20, "event counter does not have the expected value");
    AffirmIfEqual(transCounter, 60, "transition counter does not have the expected value");

    -- report actual counts
    report "total recorded events on signal output total       " & integer'image(eventCounter) severity note;
    report "total recorded transactions on signal output total " & integer'image(transCounter) severity note;

    Log("**********************************");
    ReportAlerts;
    std.env.stop;
    wait; 
  end process;

end architecture;
