library common_lib;
context common_lib.common_context;
library ieee;
  use ieee.math_complex.all;
  use ieee.math_real.all;

entity ex7 is
end entity;

architecture behav of ex7 is

  constant ITERATIONS : integer := 25000;

  procedure GetTimeC(hour, minute, second : out integer);
  attribute foreign of GetTimeC : procedure is "GetTimeC shared.so";

  procedure OneStepC(zr, zi, cr, ci : real; outr, outi : out real);
  attribute foreign of OneStepC : procedure is "OneStepC shared.so";

  function IterateC(x, y : real) return integer;
  attribute foreign of IterateC : function is "IterateC shared.so";

  procedure GetTimeC(hour, minute, second : out integer) is
  begin
    report "error: foreign subprogram GetTimeC was not executed correctly";
  end procedure;

  procedure OneStepC(zr, zi, cr, ci : real; outr, outi : out real) is
  begin
    report "error: foreign subprogram OneStepC was not executed correctly";
  end procedure;

  function IterateC(x, y : real) return integer is
  begin
    report "error: foreign subprogram IterateC was not executed correctly";
    return -1;
  end function;

  pure function ToAscii(iter: integer) return character is
  begin
    if iter < 50 then
      return '#';
    elsif iter < 100 then
      return '=';
    elsif iter < 150 then
      return ':';
    elsif iter < 200 then
      return '.';
    end if;
    return ' ';
  end function;

  pure function OneStep(z, c: Complex) return complex is
  begin
    return z*z + c;
  end function;

  pure function Iterate(c: Complex; useOneStepC, useIterateC : boolean := false) return integer is
    variable z : Complex := (re => 0.0, im => 0.0);
    variable o : Complex;
  begin
    if useOneStepC and useIterateC then
      return IterateC(c.re, c.im);
    elsif useOneStepC then
      for i in 1 to ITERATIONS loop
        -- C variant
        OneStepC(z.re, z.im, c.re, c.im, o.re, o.im);
        z := o;
        if sqrt(z.re * z.re + z.im * z.im) > 2.0 then
          return i;
        end if;
      end loop;
      return ITERATIONS;
    else
      for i in 1 to ITERATIONS loop
        -- non C variant
        z := OneStep(z, c);
        if sqrt(z.re * z.re + z.im * z.im) > 2.0 then
          return i;
        end if;
      end loop;
      return ITERATIONS;
    end if;
  end function;

  procedure Image (useOneStepC, useIterateC : boolean := false) is
    constant delta: real := 0.01;
    variable text: string(1 to integer(3.0 / delta)) := (others => ' ');
    variable x, y: real;
    variable mandel, idx: integer;
  begin
    y := -1.0;
    while y < 1.0 loop
      x := -2.0;
      idx := 1;
      text := (others => ' ');
      while x < 1.0 loop
        mandel := Iterate(Complex'(re => x, im => y), useOneStepC, useIterateC);
        text(idx) := ToAscii(mandel);
        x := x + delta;
        idx := idx + 1;
      end loop;
      Log(text);
      y := y + delta;
    end loop;
  end procedure;

  pure function TimeToString(hour, minute, second : integer) return string is
  begin
    return integer'image(hour) & "h:" & integer'image(minute) & "m:" & integer'image(second) & "s";
  end function;

  type integerArrayT is array (integer range <>) of integer;
  
begin

stimuli_p: process is
  variable hour, minute, second : integerArrayT(0 to 5);
  begin
    GetTimeC(hour(0), minute(0), second(0));
    Image;
    GetTimeC(hour(1), minute(1), second(1));

    GetTimeC(hour(2), minute(2), second(2));
    Image(true, false);
    GetTimeC(hour(3), minute(3), second(3));

    GetTimeC(hour(4), minute(4), second(4));
    Image(true, true);
    GetTimeC(hour(5), minute(5), second(5));

    Log("all vhdl start  time is " & TimeToString(hour(0), minute(0), second(0)));
    Log("all vhdl finish time is " & TimeToString(hour(1), minute(1), second(1)));
    Log("mixed    start  time is " & TimeToString(hour(2), minute(2), second(2)));
    Log("mixed    finish time is " & TimeToString(hour(3), minute(3), second(3)));
    Log("all c    start  time is " & TimeToString(hour(4), minute(4), second(4)));
    Log("all c    finish time is " & TimeToString(hour(5), minute(5), second(5)));
    wait;
  end process;

end architecture;
