//#include <mti.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define ITERATIONS 25000

void GetTimeC(int *hour, int *minute, int *second) {
	time_t result = time(NULL);
	struct tm *time_structure = localtime(&result);
	*hour = time_structure->tm_hour;
	*minute = time_structure->tm_min;
	*second = time_structure->tm_sec;
}

void OneStepC(double *zr, double *zi, double *cr, double *ci, double *or, double *oi) {
	// we implement o = z^2 + c by splitting calculation into real and imaginary parts
	// (a, b) * (c, d) = (ac - bd, ad + bc)
	*or = (*zr) * (*zr) - (*zi)* (*zi) + (*cr);
	*oi = 2 * ((*zr) * (*zi)) + (*ci);
}

int IterateC(double *x, double *y) {
	double zr, zi, cr, ci, nr, ni;
	zr = zi = 0;
	cr = *x;
	ci = *y;
	for (int i = 0; i < ITERATIONS; i++) {
		OneStepC(&zr, &zi, &cr, &ci, &nr, &ni);
		zr = nr;
		zi = ni;
		if (sqrt(zr * zr + zi * zi) > 2.) {
			return i;
		}
	}
	return ITERATIONS;
}
