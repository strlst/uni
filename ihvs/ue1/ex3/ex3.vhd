library common_lib;
context common_lib.common_context;

entity ex3 is
end entity;

architecture behav of ex3 is

  signal clock : std_logic;
  --signal clock_16_6_MHz : std_logic;

  signal tx_electrical, rx_electrical : std_logic;
  signal tx_electrical_charge : std_logic;
  signal tx_electrical_discharge : std_logic;

  signal tx_optical, rx_optical : std_logic;
  signal transit_optical : std_logic;

  -- transmission line general delay
  constant electrical_delay : time := 23 ns;
  -- tau = RC
  -- calculate time constant for charge curve
  -- (50 + 10^3) Ohm * 50 * 10^-9 F = 52.5 * 10^-6
  constant electrical_tau_charge : time := 52.5 us;
  -- 50 Ohm * 50 * 10^-9 F = 2.5 * 10^-6
  constant electrical_tau_discharge : time := 2.5 us;

  -- 200000 km/s for fiber length of 120m
  -- 120m / 2*10^8 m/s = 600 ns
  constant optical_delay : time := 600 ns;
  -- laser propagation time is 32 ns symmetrically
  constant laser_prop_time : time := 23 ns;
  -- rejection time on glitches
  constant rejection_time : time := 20 ns;

begin

  CreateClock(clock, 600 us);
  --CreateClock(clock_16_6_MHz, 600 ns);
  tx_electrical <= clock;
  tx_optical <= clock;

  stimuli_p: process is
  begin
    wait for 10 ms;
    Log("**********************************");
    std.env.stop;
    wait ; 
  end process;

  -- electrical signals
  tx_electrical_charge <= inertial tx_electrical after electrical_tau_charge;
  tx_electrical_discharge <= inertial tx_electrical after electrical_tau_discharge;
  rx_electrical <= transport tx_electrical_charge when tx_electrical = '1' else tx_electrical_discharge after electrical_delay;

  -- optical signals
  transit_optical <= reject rejection_time inertial tx_optical after laser_prop_time;
  rx_optical <= transport transit_optical after optical_delay;
  
end architecture;
