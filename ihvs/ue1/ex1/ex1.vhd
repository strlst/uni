library common_lib;
context common_lib.common_context;
  
entity ex1 is
end entity;

architecture behav of ex1 is

  constant ALERT_ID_EX1 : integer := 1;

  type ColorT is (Red, Green, Blue, Yellow);
  type AccessString is access string;

  pure function ColorToString(pos: integer) return string is
  begin
    -- handle error case
    if pos < ColorT'pos(ColorT'low) or pos > ColorT'pos(ColorT'high) then
      Alert(ALERT_ID_EX1, "could not convert position to color string, position does not index valid color enumeration element", ERROR);
      return "OutOfRange";
    end if;

    return ColorT'image(ColorT'val(pos));
  end function;

  pure function ColorsToList return string is
    variable message : AccessString := new string'("");
  begin
    -- loop all but last element
    for i in ColorT'pos(ColorT'low) to ColorT'pos(ColorT'high) - 1 loop
      message := new string'(message.all & ColorT'image(ColorT'val(i)) & string'(", "));
    end loop;

    -- handle last element in a special way
    message := new string'(message.all & ColorT'image(ColorT'val(ColorT'pos(ColorT'high))));

    -- return final string
    return message.all;
  end function;

begin

  stimuli_p: process is
  begin
    -- During initialization, the process is evaluated until the first wait statement
    wait for 0 ns;
    for i in ColorT'pos(ColorT'low) - 1 to ColorT'pos(ColorT'high) + 1 loop
      report ColorToString(i) severity note;
    end loop;
    report "colors to list output: " & ColorsToList severity note;
    Log("**********************************");
    ReportAlerts;
    std.env.stop;
    wait; 
  end process;

end architecture;
