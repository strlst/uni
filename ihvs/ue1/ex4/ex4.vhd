library common_lib;
context common_lib.common_context;

-- Implement the generic package head here. 
package linked_list is
  generic (
    type ElementT;
    function ToStringF(item : ElementT) return string
  );

  -- provide incomplete declaration to be filled later
  type NodeT;

  -- create access type for list node
  type NodePtrT is access NodeT;

  -- create record for list node
  type NodeT is record
    Element : ElementT;
    NextNode : NodePtrT;
  end record;

  -- declare type linked list type
  type LinkedListT is protected
    -- declare all functions and procedures
    impure function Count return integer;
    impure function GetAt(index : integer) return ElementT;
    impure function Dump return string;
    procedure AddFirst(item : ElementT);
    procedure RemoveAt(index : integer);
  end protected;

  -- define access string type for package
  type StringPtr is access string;

end package;

package body linked_list is
  -- define linked list type
  type LinkedListT is protected body
    -- store root of list
    variable root : NodePtrT;

    -- Count function returns the number of elements in the linked list
    impure function Count return integer is
      variable count : integer;
      variable node : NodePtrT;
    begin
      -- handle empty list
      if root = null then
        return 0;
      end if;

      count := 0;
      node := root;
      loop
        -- count each element
        count := count + 1;
        -- exit count loop when there is no next element
        exit when node.NextNode = null;
        -- otherwise continue walking the list
        node := node.NextNode;
      end loop;

      -- finally return the count
      return count;
    end function;

    -- GetAt function returns the i-th item of the list (with root having index 0)
    impure function GetAt(index : integer) return ElementT is
      variable node : NodePtrT;
      constant len : integer := Count;
    begin
      -- handle list that is too small for index
      if index >= len then
        -- alert user
        Alert("index " & integer'image(index) & " cannot be greater or equal to element count " & integer'image(len), ERROR);
	-- cannot return null at this instance, we would need some sort of ElementT neutral value which is not available here!
	-- just return root.Element, although that will fail for empty lists
	return root.Element;
      end if;

      -- iterate nodes
      node := root;
      for i in 0 to index - 1 loop
        node := node.NextNode;
      end loop;

      -- finally return element
      return node.Element;
    end function;

    -- Dump function returns a comma separated list of string representations of all items in the list in stored order
    impure function Dump return string is
      variable node : NodePtrT;
      variable message : StringPtr;
    begin
      -- handle empty list gracefully
      if root = null then
        return "";
      end if;

      -- iterate list to build string
      node := root;
      message := new string'(ToStringF(node.Element));
      loop
        -- exit when we' re at the last element
        exit when node.NextNode = null;
        -- iterate
        node := node.NextNode;
        -- append node to string
        message := new string'(message.all & ", " & ToStringF(node.Element));
      end loop;

      -- return the finished string
      return message.all;
    end function;

    -- AddFirst procedure adds an element to the front of the list
    procedure AddFirst(item : ElementT) is
      variable newNode : NodePtrT;
    begin
      -- allocate memory for new node
      newNode := new NodeT;

      -- populate new node
      newNode.Element := item;
      newNode.NextNode := root;

      -- finally set new node to root
      root := newNode;
    end procedure;

    -- RemoveAt procedure removes the i-th item from the list (with root having index 0)
    procedure RemoveAt(index : integer) is
      variable node : NodePtrT;
      variable nextNode : NodePtrT;
      constant len : integer := Count;
    begin
      if index >= len then
        -- alert user
        Alert("index " & integer'image(index) & " cannot be greater or equal to element count " & integer'image(len), ERROR);
        return;
      end if;

      -- handle 0 element case
      if index = 0 then
        nextNode := root.NextNode;
        deallocate(root);
        root := nextNode;
        return;
      end if;

      -- instantiate helper variables
      node := root;
      nextNode := null;
      
      -- go to appropriate position for removal
      for i in 0 to index - 2 loop
        node := node.NextNode;
      end loop;

      -- only access pointer if it is not null
      -- NOTE: apparently this case can not happen
      --if node.NextNode /= null then
        nextNode := node.NextNode.NextNode;
      --end if;

      -- finally deallocate and set next node accordingly
      deallocate(node.NextNode);
      node.NextNode := nextNode;
    end procedure;
  end protected body;
end package body;

library common_lib;
context common_lib.common_context;

entity ex4 is
end entity;

architecture behav of ex4 is

  type PrimeRecT is record
    Number: integer;
    IsPrime: boolean;
  end record;

  function ToString(item: PrimeRecT) return string is
  begin
    return "PrimeRec(" & integer'image(item.Number) & ", prime=" & boolean'image(item.IsPrime) & ")";
  end function;

  -- instantiate and use linked list package
  package linkedListPkg is new work.linked_list generic map (PrimeRecT, ToString);
  use linkedListPkg.all;

  type PrimeRecArrayT is array (integer range <>) of PrimeRecT;

  shared variable list : LinkedListT;
begin

  stimuli_p: process is
    constant primes : PrimeRecArrayT(0 to 15) := (
      (0, false),
      (1, true),
      (2, true),
      (3, true),
      (4, false),
      (5, true),
      (6, false),
      (7, true),
      (8, false),
      (9, false),
      (10, false),
      (11, true),
      (12, false),
      (13, true),
      (14, false),
      (15, false)
    );
  begin
    wait for 0 ns;
    Log("*********** BUILD LIST ***********");
    -- print empty list
    report "len(list)=" & integer'image(list.Count) & ", list={ " & list.Dump & " }";

    -- verify counts, add elements and print list
    for i in 0 to primes'length - 1 loop
      AlertIf(list.Count /= i, "list count is not correct");
      list.AddFirst(primes(i));
      report "len(list)=" & integer'image(list.Count) & ", list={ " & list.Dump & " }";
    end loop;

    Log("************ READ LIST ***********");
    for i in 0 to primes'length - 1 loop
      -- check if list elements correspond to primes
      AlertIf(list.GetAt(i) /= primes(primes'length - 1 - i), "element mismatch at index " & integer'image(i));
    end loop;

    Log("********* DESTROY LIST ***********");
    -- various removal tests
    list.RemoveAt(primes'length + 10);

    list.RemoveAt(primes'length - 1);
    report "len(list)=" & integer'image(list.Count) & ", list={ " & list.Dump & " }";

    list.RemoveAt(primes'length / 2);
    report "len(list)=" & integer'image(list.Count) & ", list={ " & list.Dump & " }";

    -- remove rest of list and print
    for i in 0 to primes'length - 3 loop
      list.RemoveAt(0);
      report "len(list)=" & integer'image(list.Count) & ", list={ " & list.Dump & " }";
    end loop;

    -- add and remove single member
    list.AddFirst(primes(0));
    list.AddFirst(primes(1));
    list.AddFirst(primes(2));
    -- access out of bounds member
    report ToString(list.GetAt(42));
    list.RemoveAt(2);
    list.RemoveAt(1);
    list.RemoveAt(0);

    Log("**********************************");
    std.env.stop;
    wait;
  end process;

end architecture;
