onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /ex5/clk_i
add wave -radix hexadecimal -noupdate /ex5/trans_io
add wave -radix hexadecimal -noupdate /ex5/pins_io
add wave -radix hexadecimal -noupdate /ex5/ram
add wave -radix hexadecimal -noupdate /ex5/reg_read_countdown
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1000
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
