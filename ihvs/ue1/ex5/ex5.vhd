library common_lib;
context common_lib.common_context;
use work.avmm_pkg.all;

-- need an additional dependency
library osvvm;
context osvvm.OsvvmContext;
use osvvm.ScoreBoardPkg_slv.all;

entity ex5 is
end entity;

architecture behav of ex5 is

  signal SB : ScoreBoardIDType;

  signal clk_i : std_logic;
  signal trans_io : AvmmRecType;
  signal pins_io : AvmmPinoutType;

  type RamT is array (0 to 2**pins_io.address'length - 1) of std_logic_vector(pins_io.writedata'length - 1 downto 0);
  signal ram : RamT := (others => (others => '0'));

  signal reg_read_countdown : integer := -1;

begin

  CreateClock(clk_i, 10 ns);

  avmm_vu_inst: entity work.avmm_vu
  port map (
    trans_io => trans_io,
    clk_i    => clk_i,
    pins_io  => pins_io
  );

  avmm_p : process (clk_i) is 
    variable oldData : std_logic_vector(pins_io.writedata'range);
    variable mask : std_logic_vector(pins_io.writedata'range);
  begin
    -- high active
    if rising_edge(clk_i) then
      -- todo: implement byteena
      if pins_io.write = '1' then
        oldData := ram(to_integer(unsigned(pins_io.address)));
        mask := ByteenaToMask(pins_io.byteenable);
        ram(to_integer(unsigned(pins_io.address))) <= (pins_io.writedata and mask) or (oldData and not mask);
      elsif reg_read_countdown >= 0 then
        reg_read_countdown <= reg_read_countdown - 1;
        if reg_read_countdown = 1 then
          pins_io.readdata <= ram(to_integer(unsigned(pins_io.address))) and ByteenaToMask(pins_io.byteenable);
        end if;
      elsif reg_read_countdown < 0 and pins_io.read = '1' then
          reg_read_countdown <= 1;
      end if;
    end if;
  end process;

  stimuli_p: process is
    variable read_data : std_logic_vector(AvmmDataRange);
    variable data : std_logic_vector(AvmmDataRange);
    variable address : std_logic_vector(AvmmAddrRange);
  begin
    -- initialize scoreboard
    SB <= NewID("AVMM_SB");

    -- begin testing

    -- write with different byteenas and read back values
    AvmmWrite(trans_io, "00000", x"DEAD", "00");
    Push(SB, x"0000");
    AvmmRead(trans_io, "00000", "11", read_data);
    Check(SB, read_data);

    AvmmWrite(trans_io, "00001", x"DEAD", "01");
    Push(SB, x"00AD");
    AvmmRead(trans_io, "00001", "11", read_data);
    Check(SB, read_data);

    AvmmWrite(trans_io, "00010", x"DEAD", "10");
    Push(SB, x"DE00");
    AvmmRead(trans_io, "00010", "11", read_data);
    Check(SB, read_data);

    AvmmWrite(trans_io, "00011", x"DEAD", "11");
    Push(SB, x"DEAD");
    AvmmRead(trans_io, "00011", "11", read_data);
    Check(SB, read_data);

    -- push before, write just once and read with different byteenas
    Push(SB, x"0000");
    Push(SB, x"BE00");
    Push(SB, x"00EF");
    Push(SB, x"BEEF");
    AvmmWrite(trans_io, "00001", x"BEEF", "11");
    AvmmRead(trans_io, "00001", "00", read_data);
    Check(SB, read_data);
    AvmmRead(trans_io, "00001", "10", read_data);
    Check(SB, read_data);
    AvmmRead(trans_io, "00001", "01", read_data);
    Check(SB, read_data);
    AvmmRead(trans_io, "00001", "11", read_data);
    Check(SB, read_data);

    for i in 0 to 2**address'length - 1 loop
      -- calculate various data
      data := std_logic_vector(to_unsigned(to_integer(unsigned'(x"FF00")) + i, pins_io.writedata'length));
      address := std_logic_vector(to_unsigned(i, pins_io.address'length));
      -- push readback result
      Push(SB, data);
      -- write to bus
      AvmmWrite(trans_io, address, data, "11");
      -- read from bus
      AvmmRead(trans_io, address, "11", read_data);
      -- check result
      Check(SB, read_data);
    end loop;

    for i in 0 to 2**address'length - 1 loop
      -- calculate various data
      data := std_logic_vector(to_unsigned(to_integer(unsigned'(x"8800")) + i, pins_io.writedata'length));
      address := std_logic_vector(to_unsigned(i, pins_io.address'length));
      -- push readback result
      Push(SB, data);
      -- write to bus
      AvmmWrite(trans_io, address, x"8888", "11");
      AvmmReadModifyWrite(trans_io, address, data, x"00FF");
      -- read from bus
      AvmmRead(trans_io, address, "11", read_data);
      -- check result
      Check(SB, read_data);
    end loop;
    Log("**********************************");
    std.env.stop;
    wait ; 
  end process;
  
end architecture;