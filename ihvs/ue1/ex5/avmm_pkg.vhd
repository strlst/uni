library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library osvvm;
  context osvvm.OsvvmContext;

library osvvm_common ;
  context osvvm_common.OsvvmCommonContext ; 

package avmm_pkg is

  constant ADDR_WIDTH : integer := 5;
  constant DATA_WIDTH : integer := 16;
  constant BYTEENA_WIDTH : integer := 2;

  type AvmmPinoutT is record
    address            : std_logic_vector;
    writedata          : std_logic_vector;
    readdata           : std_logic_vector;
    byteenable         : std_logic_vector;
    read               : std_logic;
    write              : std_logic;
  end record;

  subtype AvmmAddrRange is natural range ADDR_WIDTH - 1 downto 0;
  subtype AvmmDataRange is natural range DATA_WIDTH - 1 downto 0;
  subtype AvmmByteenaRange is natural range BYTEENA_WIDTH - 1 downto 0;

  subtype AvmmRecType is AddressBusRecType(
    Address(AvmmAddrRange),
    DataToModel(AvmmDataRange),
    DataFromModel(AvmmDataRange)
  );

  subtype AvmmPinoutType is AvmmPinoutT(
    address(AvmmAddrRange),
    writedata(AvmmDataRange),
    readdata(AvmmDataRange),
    byteenable(AvmmByteenaRange)
  );

  function ByteenaToMask(byteena : std_logic_vector) return std_logic_vector;
  function MaskToByteena(mask : std_logic_vector) return std_logic_vector;

  procedure AvmmWrite(signal trans : inout AddressBusRecType; addr, data, byte_enable : std_logic_vector);
  procedure AvmmRead(signal trans : inout AddressBusRecType; addr, byte_enable: std_logic_vector; variable read_data : out std_logic_vector);
  procedure AvmmReadModifyWrite(signal trans : inout AddressBusRecType; addr, data, write_mask : std_logic_vector);

  constant BYTEENA_OPTION_ID : integer := 1;

end package;

package body avmm_pkg is

  function ByteenaToMask(byteena : std_logic_vector) return std_logic_vector is
    constant dataWidth : integer := byteena'length * 8;
    variable dataMask : std_logic_vector(dataWidth - 1 downto 0) := (others => '0');
  begin
    -- loop byteena bits to generate mask
    for i in 0 to byteena'length - 1 loop
      dataMask((i + 1) * 8 - 1 downto i * 8) := x"FF" when byteena(i) else x"00";
    end loop;
    --report to_hex_string(dataMask) & " from " & to_string(byteena);

    return dataMask;
  end function;

  function MaskToByteena(mask : std_logic_vector) return std_logic_vector is
    constant byteenaWidth : integer := mask'length / 8;
    variable byteena : std_logic_vector(byteenaWidth - 1 downto 0) := (others => ('0'));
    variable currentByte : std_logic_vector(7 downto 0);
    variable dataMask : std_logic_vector(mask'length - 1 downto 0);
  begin
    -- necessary for some reason (otherwise an odd error occurs when operating on mask directly)
    dataMask := mask;
    -- loop byteena bits to generate byteena
    for i in 0 to byteenaWidth - 1 loop
      currentByte := dataMask((i + 1) * 8 - 1 downto i * 8);
      byteena(i) := '1' when or(currentByte) = '1' else '0';
    end loop;
    --report to_string(byteena) & " from mask " & to_hex_string(dataMask) & " (" & to_hex_string(mask) & ")";

    return byteena;
  end function;

  procedure AvmmWrite(signal trans: inout AddressBusRecType; addr, data, byte_enable: std_logic_vector) is
  begin
    -- set additional byteena option
    SetModelOptions(trans, BYTEENA_OPTION_ID, byte_enable);
    -- call write function
    Write(trans, addr, data);
  end procedure;

  procedure AvmmRead(signal trans: inout AddressBusRecType; addr, byte_enable: std_logic_vector; variable read_data: out std_logic_vector) is
  begin
    -- set additional model options
    SetModelOptions(trans, BYTEENA_OPTION_ID, byte_enable);
    -- perform read
    Read(trans, addr, read_data);
  end procedure;

  procedure AvmmReadModifyWrite(signal trans: inout AddressBusRecType; addr, data, write_mask: std_logic_vector) is
    variable read_data : std_logic_vector(data'range);
  begin
    -- perform read
    Read(trans, addr, read_data);
    -- set additional model options
    SetModelOptions(trans, BYTEENA_OPTION_ID, MaskToByteena(write_mask));
    -- perform write
    Write(trans, addr, data);
  end procedure;
  
end package body;