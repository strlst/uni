library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use work.avmm_pkg.all;

library osvvm;
context osvvm.OsvvmContext;

library osvvm_common;
context osvvm_common.OsvvmCommonContext;

entity avmm_vu is
  port (
    trans_io: inout AddressBusRecType;
    clk_i   : in    std_logic;
    pins_io : inout AvmmPinoutT
  );
end entity;

architecture behav of avmm_vu is
begin

  sequencer_p : process is
    --variable wait_cycles_v : integer;
    variable byte_enable : std_logic_vector(AvmmByteenaRange) := (others => '0');
  begin
    wait for 0 ns;

    pins_io.address(AvmmAddrRange) <= (others => '0');
    pins_io.byteenable(AvmmByteenaRange) <= (others => '0');
    pins_io.writedata(AvmmDataRange) <= (others => '0');
    pins_io.read <= '0';
    pins_io.write <= '0';
    wait until rising_edge(clk_i);

    dispatcher_loop: loop
      WaitForTransaction(clk => clk_i, Rdy => trans_io.Rdy, Ack => trans_io.Ack);

      pins_io.address(AvmmAddrRange) <= (others => '0');
      pins_io.byteenable(AvmmByteenaRange) <= (others => '0');
      pins_io.writedata(AvmmDataRange) <= (others => '0');
      pins_io.read <= '0';
      pins_io.write <= '0';

      case trans_io.Operation is
        when SET_MODEL_OPTIONS =>
          byte_enable := std_logic_vector(to_unsigned(trans_io.IntToModel, byte_enable'length));

        when WRITE_OP =>
          pins_io.address(AvmmAddrRange) <= SafeResize(trans_io.Address, ADDR_WIDTH);
          pins_io.byteenable(AvmmByteenaRange) <= SafeResize(byte_enable, pins_io.byteenable'length);
          pins_io.writedata(AvmmDataRange) <= SafeResize(trans_io.DataToModel, DATA_WIDTH);
          pins_io.write <= '1';
          wait until rising_edge(clk_i);
          pins_io.write <= '0';

        when READ_OP =>
          pins_io.address(AvmmAddrRange) <= SafeResize(trans_io.Address, pins_io.address'length);
          pins_io.byteenable(AvmmByteenaRange) <= SafeResize(byte_enable, pins_io.byteenable'length);
          pins_io.read <= '1';
          wait until rising_edge(clk_i);
          wait until rising_edge(clk_i);
          wait until rising_edge(clk_i);
          pins_io.read <= '0';
          trans_io.DataFromModel <= SafeResize(pins_io.readdata, pins_io.readdata'length);

        when others =>
          null;
      end case;
    end loop;
  end process;

end architecture;
