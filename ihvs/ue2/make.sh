#!/bin/bash

source ../settings.make

if [[ "$1" == "clean" ]]; then
  rm -rf work transcript vsim.wlf
  exit
elif [[ "$1" == "remote" ]]; then
  rm -rf ../*/work ../*/transcript ../*/vsim.wlf
  scp -r -o ProxyJump=ntremurici@ssh.ecslab.tuwien.ac.at "${PWD%/*}" ntremurici@lab6:~
  # just run it externally since this somehow does not work
  #ssh -o ProxyJump=ntremurici@ssh.ecslab.tuwien.ac.at ntremurici@lab6 "cd $(basename ${PWD%/*}); $MODELSIM_PATH -do do run.do"
  exit
elif [[ "$1" == "gui" ]]; then
  "$MODELSIM_PATH" -do "do run.do"
  exit
fi

"$MODELSIM_PATH" -batch -do "do run.do; quit"

