library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use work.common_pkg.all;

entity ex3 is
end entity;

architecture beh of ex3 is

  signal clk, req, ack, busy, done: std_logic;
  signal read_req, gnt, cancel, data_start, data, data_end : std_logic;
  
begin

  Clock(clk, 30);

  -- task 1
  Waveform(clk, req, "010000010000000");
  Waveform(clk, ack, "001000001000000");
  Waveform(clk, busy, "000111000111100");
  Waveform(clk, done, "000000100000010");

  -- task 2
  Waveform(clk, read_req, "0010000000000010000000");
  Waveform(clk, gnt, "0000010000000000001000");
  Waveform(clk, cancel, "0000000000000000010000");
  Waveform(clk, data_start, "0000001000000000000000");
  Waveform(clk, data, "0000000111000000000000");
  Waveform(clk, data_end, "0000000000100000000000");

end architecture;
