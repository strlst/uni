#!/usr/bin/env python3

def print_trace(trace):
    print(''.join(['H' if v else 'L' for v in trace]))

def c_gate(a, b, state):
    return not state if (a == (not state) and b == (not state)) else state

def c_gate2(a, b, state):
    return (a and b) or (state and (a or b))

def muller():
    x0 = [False, True, True, False, False, False, True, True, True, True, False, False, False, False, False, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True]
    x5 = [False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, False, False, False, False, False, True, True, True, True, False, False, False, True, True, True, True, True, True]

    elements = len(x0)
    x1 = [False] * elements
    x2 = [False] * elements
    x2n = [True] * elements
    x3 = [False] * elements
    x3n = [True] * elements
    x4 = [False] * elements
    x4n = [True] * elements
    x5n = [True] * elements

    for i in range(len(x0) - 1):
        x2n[i + 1] = not x2[i]
        x3n[i + 1] = not x3[i]
        x4n[i + 1] = not x4[i]
        x5n[i + 1] = not x5[i]
        x1[i + 1] = c_gate(x0[i], x2n[i], x1[i])
        x2[i + 1] = c_gate(x1[i], x3n[i], x2[i])
        x3[i + 1] = c_gate(x2[i], x4n[i], x3[i])
        x4[i + 1] = c_gate(x3[i], x5n[i], x4[i])

    for trace in [x0, x1, x2, x2n, x3, x3n, x4, x4n, x5, x5n]:
        print_trace(trace)

if __name__ == '__main__':
    muller()
