\documentclass[10pt]{article}
\usepackage{protocol}
\title{Homework III}
\author{Norbert Tremurici - 11907086}
\begin{document}
\maketitle

\FloatBarrier % Leave the FloatBarriers in place.
\section{STG Synthesis (35 points)}

\subsection{Subtask (a)}

For this first subtask, we can explore the space of reachable states by using STG transition rules and trying all valid transitions from all states we have already reached.
If we denote our state vector as \texttt{(R, S, A, B)}, then our initial state $s_0$ is \texttt{0011}, as our initial tokens are placed right after the \texttt{A+} and \texttt{B+} transitions.
If we do this, we can enumerate quite a large space of possible states and we find that some states have the same state vectors (which indicates CSC conflicts).

This task was explored using pen and paper, but the result was also verified using Workcraft.
Figure~\ref{fig:a-state-graph} shows the graph produced by Workcraft.
Because Workcraft is a useful tool in verifying one's results and produces readable graphics, all graphics for this task will be generated using Workcraft after solving the exercise.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth, height=\textheight, keepaspectratio]{graphics/task1-a-state-graph.pdf}
	\caption{\label{fig:a-state-graph} State Graph generated using Workcraft}
\end{figure}

\FloatBarrier

\subsection{Subtask (b)}

Most states vectors assigned to their respective states are unique, but two state vectors are assigned to two conflicting states each.
If we denote these by their bit vectors as well as their possible predecessors in the form of $S_k \leftarrow \{S_i, S_j, ...\}$, we can enumerate the bit vectors as follows:

\begin{itemize}
	\item CSC \texttt{1011}
	\begin{itemize}
		\item $S_1=\texttt{1011} \leftarrow \{S_0=\texttt{0011}\}$
		\item $S_3=\texttt{1011} \leftarrow \{S_6=\texttt{1010}\}$
	\end{itemize}
	\item CSC \texttt{1111}
	\begin{itemize}
		\item $S_2=\texttt{1111} \leftarrow \{S_1=\texttt{1011}, S_4=\texttt{0111}\}$
		\item $S_5=\texttt{1111} \leftarrow \{S_3=\texttt{1011}\}$
	\end{itemize}
\end{itemize}

It is interesting to note that the first CSC \texttt{1011} always has a successor CSC \texttt{1111}.

\FloatBarrier

\subsection{Subtask (c)}

To resolve the conflicts, we need to be able to differentiate the conflicting states by giving them each unique state vectors.
This is possible if we add a new bit that denotes whether we are coming from the earlier branch at $S_0=\texttt{0011}$, or the later branch at $S_6=\texttt{1010}$.

After adding a new internal state csc to our graph, our new state vector is \texttt{(R, S, A, B, csc)}.

We can now resolve the conflict by inserting the \texttt{csc+} and \texttt{csc-} transitions right before the problematic transitions to $S_1=\texttt{1011}$ and $S_3=\texttt{1011}$ we identified in Subtask (b).
This is exemplified in Figure~\ref{fig:c-stg}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=0.5\textheight, keepaspectratio]{graphics/task1-c-stg.pdf}
	\caption{\label{fig:c-stg} Modified STG generated using Workcraft with CSC states manually resolved}
\end{figure}

If we consider the state graph, the problematic, conflicting transitions now have a suffix \texttt{0} or \texttt{1} in their state vector, depending on where in the STG we are.
Figure~\ref{fig:c-state-graph} shows the new state graph resulting from our modified STG.
Do note that there are no more conflicting states, we solved two conflicts using one additional state variable!

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{graphics/task1-c-state-graph.pdf}
	\caption{\label{fig:c-state-graph} Modified State Graph generated using Workcraft}
\end{figure}

\FloatBarrier

\subsection{Subtask (d)}

Another way we could resolve the conflict is by introducing synchrony between the two conflicting states.
This works, as we are not allowing the two identical states to be reached independently by two different asynchronous events, but by instead letting them both happen at the same time.
This restricts the space of possible transitions (as a result we should expect our state graph to be smaller respectively) but in this way it also restricts the concurrency of the system.

In concrete terms, we can do this by introducing an edge between the branches with the conflicting state.
The chosen approach was to introduce an edge from \texttt{R-} to \texttt{S-}.

Figure~\ref{fig:d-stg} shows the resulting STG after applying the modification, with the added edge colored in red.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=0.5\textheight, keepaspectratio]{graphics/task1-d-stg.pdf}
	\caption{\label{fig:d-stg} Modified STG generated using Workcraft with CSC states manually resolved}
\end{figure}

Figure~\ref{fig:d-state-graph} shows the resulting state graph after applying the modification.
Notably, the graph has become much smaller.
Although we are cutting away many states, since we are already introducing a timing assumption by requiring a transition to happen before another, this particular modification was chosen since we are constraining two inputs instead of making inputs depend on each other, or an output dependent on an input, or vice versa.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth, height=\textheight, keepaspectratio]{graphics/task1-d-state-graph.pdf}
	\caption{\label{fig:d-state-graph} Modified State Graph generated using Workcraft}
\end{figure}

\FloatBarrier

\subsection{Subtask (e)}

Here we are tasked to first construct the KV maps.
To do this, we use the modified state graph to figure out where the excitation regions are, as well as the steady states, separately for the outputs A and B.
Figure~\ref{fig:1-e-kvs} show the KV maps for outputs A and B.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth, height=\textheight, keepaspectratio]{graphics/1-e-kv-a.png}
	\includegraphics[width=0.4\textwidth, height=\textheight, keepaspectratio]{graphics/1-e-kv-b.png}
	\caption{\label{fig:1-e-kvs} KV Maps for outputs A and B}
\end{figure}

Some states are unused.
The KV maps are also consistent in the sense that there is always a R or F boundary between 1's and 0's.

If we apply the rules for an atomic complex gate implementation for A and the rules for a state-holding element implementation for B, we end up with the KV maps as in Figure~\ref{fig:1-e-impl}
For A, we group only all stable 1's and rising states.
For B, we have one group (blue) for $f_{set}$ and one group (purple) for $f_{reset}$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth, height=\textheight, keepaspectratio]{graphics/1-e-kv-a-atomic.png}
	\includegraphics[width=0.4\textwidth, height=\textheight, keepaspectratio]{graphics/1-e-kv-b-state.png}
	\caption{\label{fig:1-e-impl} KV Maps for implementations of A and B}
\end{figure}

Thus for A, we have $A \equiv B'$.
For B we have:

$$
\begin{aligned}
B &\equiv f_{set}(R, B) \lor (B \land \neg f_{reset}(R, S)) \\
&\equiv \neg S \lor (B' \land \neg (R \land S)) \\
\end{aligned}
$$

Figure~\ref{fig:1-e-sr} shows the SR latch implementation of outputs A and B.
Even though we should use atomic complex gates for output A, because we simply have $A \equiv B$, we included output A in the SR latch implementation of B.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth, height=\textheight, keepaspectratio]{graphics/task1-e-sr.pdf}
	\caption{\label{fig:1-e-sr} SR latch implementation of outputs A and B}
\end{figure}

\newpage

\FloatBarrier % Leave the FloatBarriers in place.
\section{Static Data-Flow Structures - Montgomery Ladder (25 points)}

\begin{figure}[htb]
	\centering
	\input{tikz/gcd_circuit.tex}
	\caption{\label{fig_gcd_circuit} GCD Dataflow circuit}
\end{figure}

\subsection{Subtask (a)}

In this example, $R_1$ initially holds a data token of value 0, which indicates that a previous operation has completed, whereas $R_2$ initially holds an empty token, which is required to come after a data token for a valid 4-phase protocol.
All the other buffers have no unconsumed values.

If we simply remove $R_2$ without reassigning any values, then there will be a problem, as there would be no buffer that contains an unconsumed empty token in the reset state.
This would violate the protocol and break the underlying pipeline, of which this dataflow is the abstraction of.

Now we could consider removing $R_2$ but giving $R_3$ an unconsumed empty token in the reset state instead.
But in this case, there is a circular dataflow between $R_1$ and $R_3$, which constitutes a problem because there are no buffers in-between that hold bubbles.
We learned in the lecture that every ring must contain at least one data token, one empty token and a bubble, otherwise the system is in a deadlock.

So the answer is no, removing $R_2$ would be problematic either way.

\subsection{Subtask (b)}

Table~\ref{table:transitions} shows the first 6 rows of the computation, assuming a new buffer $R_{12}$ (with the only possible initial value being a consumed value token) is introduced.
The table was constructed by executing all asynchronous steps which do not interfere with each other in parallel.

\begin{table}[ht]
	\centering
	\begin{tabular}{c c||c c c c c c||c }
		\textbf{Step} & \textbf{A,B} & $\bf{R_1}$ & $\bf{R_{12}}$ & $\bf{R_2}$ & $\bf{R_3}$ & $\bf{R_4}$ & $\bf{R_5}$ & \textbf{Output} \\
\hline
\hline
		  1 & (8,6) & (0) & 0 & (E) & E & E & E & E \\
\hline
		  2 & 8,6 & 0 & (E) & E & (8,6) & E & E & E \\
\hline
		  3 & (E) & (E) & E & (1) & 8,6 & E & (8,6) & E \\
\hline
		  4 & E & E & (1) & 1 & (E) & (2,6) & 8,6 & E \\
\hline
		  5 & E & (1) & 1 & (E) & E & (2,6) & (E) & E \\
\hline
		  6 & E & 1 & (E) & E & (2,6) & 2,6 & (E) & E \\
\hline
    7 & $\hdots$ & $\hdots$ & $\hdots$ & $\hdots$ & $\hdots$ & $\hdots$ & $\hdots$ & $\hdots$\\
  \end{tabular}
  \caption{Value transitions of the GCD circuit}
  \label{table:transitions}
\end{table}

To answer the question, yes, the circuit does still work.
The extra buffer acts as an intermediary holder of the passing data and empty tokens.
In fact, the extra buffer seems to have sped up our execution, in terms of steps of our table.
This is because we computed the table with all asynchronous steps that can occur at the same time in parallel.
More precisely, because buffer $R_2$ remained free after passing on its value to $R_{12}$, it was ready at a sooner step (step 3 instead of step 4) to accept the incoming empty token.

\subsection{Subtask (c)}

Let's first consider whether we can remove any buffer (although buffers aren't flow control components).
Our dataflow is composed of two separate dataflow with a ring structure.
Each ring contains three buffers and one of the buffers between each ring is shared.

Thus we would require that each ring has at least three buffers, such that it can hold at least one data token, at least one empty token and at least one bubble, because otherwise the circuit is in a deadlock.
This requirement is fulfilled for our circuit (as well as in (b) but violated in (a)).

If we remove $R_3$, the shared buffer, then both rings have this property violated and so this is no solution.
If we remove any of the buffers $R_1$ or $R_2$, then the upper (control) ring violates this property.
If we remove any of the buffers $R_3$ or $R_4$, then the lower (compute) ring violates this property.

So we cannot optimize this circuit just by removing a buffer.

Now for the flow control components.

We have four forks, two multiplexer and two demultiplexer in this circuit.
If we simply remove the input multiplexer, then we will have to use a join instead, but that will change the semantics of the circuit as the data and null phase of both rings in the circuit will need to occur simultaneously, whereas previously the compute ring only had a data phase when the control ring had a value of 1.
The next problem is that the circuit can only transition in each step of computation when the input has a new token available, although the input is currently applied only once when giving the input values of A and B.
So this is no option.

If we remove the demultiplexer at the output of the circuit, then any asynchronous consumer at the output of the circuit will have trouble recognizing when the output of the gcd circuit is valid, as the circuit would output not only output data tokens, but also the input data and intermediate results.
So this is no option either.

For the compute ring, we cannot remove the multiplexer without the demultiplexer or vice versa.
If we removed the multiplexer by using a fork instead, then both compute branches would receive data tokens but at the demultiplexer only one would get consumed.
Whereas if we removed the demultiplexer (and used a join instead), we would still need to select the appropriate result at a later point which reuqires us to use another multiplexer, so we wouldn't gain anything.
But if we juse a merge instead, we could merge the signals of both compute branches as only one of them will have tokens.
This yields the correct result because both A and B are passed down both branches in either case.

So we cannot remove any components, except the demultiplexer in the compute ring, by simply merging the two compute branches of the compute ring.

\subsection{Subtask (d)}

For this task, we should create a circuit similar in spirit to the GCD dataflow circuit that implements modular exponentiation.
As a base for this implementation, the alternative Montgomery ladder as described in the following paper has been used:

\begin{quote}
Joye, M., Yen, SM. (2003). The Montgomery Powering Ladder. In: Kaliski, B.S., Koç, ç.K., Paar, C. (eds) Cryptographic Hardware and Embedded Systems - CHES 2002. CHES 2002. Lecture Notes in Computer Science, vol 2523. Springer, Berlin, Heidelberg. https://doi.org/10.1007/3-540-36400-5\_22
\end{quote}

This version employs the same algorithm, just expressed in a slightly different (and for our purposes more usable) form.
Figure~\ref{fig:montgomery-algorithm} shows the version that is employed in our implementation.
In this version, instead of starting from $R_0 = A$ and $R_1 = A^2$, we start from $R_0 = 1$ and $R_1 = A$ instead and the loop begins from $b - 1$ instead of $b - 2$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.4\textwidth, height=\textheight, keepaspectratio]{graphics/montgomery-algorithm.png}
	\caption{\label{fig:montgomery-algorithm} The Montgomery ladder (quoted from the paper)}
\end{figure}

Figure~\ref{fig:montgomery} shows the implementation that employs modular exponentiation by squaring.
It is strikingly similar to the GCD dataflow circuit, although there is more bookkeeping involved.
The double bars represent join operations, like before we also have buffers and forks.
Where appropriate the signals have been annotated.
The buffers are supposed to have descriptive names, \texttt{RI} is the input buffer, \texttt{RB} is just the buffer for the input buffer, \texttt{RV} is the value buffer, \texttt{RC} is the compute buffer and \texttt{RO} is the output buffer.
The variable $R_0$ (depicted as \texttt{R0}) is an auxiliary variable, as is $i$, which keeps track of the current index in $B$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{graphics/montgomery.pdf}
	\caption{\label{fig:montgomery} Montgomery dataflow circuit}
\end{figure}

The algorithm terminates as soon as all bits of B have been reduced to 0, but it can take $b - 1$ steps even if just the LSB is set to 1, because the implementation begins at index $b - 1$.
The implementation also references individual bits of B, with the i-th bit being referenced as \texttt{B.i}.

\begin{table}[ht]
	\centering
	\begin{tabular}{c c||c c c c c c||c }
		\textbf{Step} & \textbf{A,B,N} & \bf{RV} & \bf{RB} & \bf{RI} & \bf{RC} & \bf{RO} & \textbf{Output} \\
\hline
\hline
		  1 & (13,22,127) & E & (E) & (0) & E & E & E \\
\hline
		  2 & 13,22,127 & (13,22,127,1,4) & (E) & 0 & E & E & E \\
\hline
		  3 & (E) & (13,22,127,1,4) & E & (E) & E & E & E \\
\hline
		  4 & (E) & 13,22,127,1,4 & (1) & (E) & (13,22,127,1,4) & E & E \\
\hline
		  5 & E & (E) & (1) & E & 13,22,127,1,4 & (42,6,127,13,3) & E \\
\hline
		  6 & E & (E) & E & (1) & 13,22,127,1,4 & (42,6,127,13,3) & E \\
\hline
		  7 & E & E & (E) & (1) & (E) & (42,6,127,13,3) & E \\
\hline
		  8 & E & (42,6,127,13,3) & (E) & 1 & (E) & 42,6,127,13,3 & E \\
\hline
		  9 & E & (42,6,127,13,3) & E & (E) & E & (E) & E \\
\hline
		  10 & E & 42,6,127,13,3 & (1) & (E) & (42,6,127,13,3) & (E) & E \\
\hline
		  11 & E & (E) & (1) & E & (42,6,127,13,3) & E & E \\
\hline
		  12 & E & (E) & 1 & (1) & 42,6,127,13,3 & (38,6,127,42,2) & E \\
\hline
		  13 & E & E & (E) & (1) & (E) & (38,6,127,42,2) & E \\
\hline
		  14 & E & (38,6,127,42,2) & (E) & 1 & (E) & 38,6,127,42,2 & E \\
\hline
		  15 & E & (38,6,127,42,2) & E & (E) & E & (E) & E \\
\hline
		  16 & E & 38,6,127,42,2 & (1) & (E) & (38,6,127,42,2) & (E) & E \\
\hline
		  17 & E & (E) & (1) & E & (38,6,127,42,2) & E & E \\
\hline
		  18 & E & (E) & 1 & (1) & 38,6,127,42,2 & (47,2,127,72,1) & E \\
\hline
		  19 & E & E & (E) & (1) & (E) & (47,2,127,72,1) & E \\
\hline
		  20 & E & (47,2,127,72,1) & (E) & 1 & (E) & 47,2,127,72,1 & E \\
\hline
		  21 & E & (47,2,127,72,1) & E & (E) & E & (E) & E \\
\hline
		  22 & E & 47,2,127,72,1 & (1) & (E) & (47,2,127,72,1) & (E) & E \\
\hline
		  23 & E & (E) & (1) & E & (47,2,127,72,1) & E & E \\
\hline
		  24 & E & (E) & 1 & (1) & 47,2,127,72,1 & (50,0,127,82,0) & E \\
\hline
		  25 & E & E & (E) & (1) & (E) & (50,0,127,82,0) & E \\
\hline
		  26 & E & (50,0,127,82,0) & (E) & 1 & (E) & 50,0,127,82,0 & E \\
\hline
		  27 & E & (50,0,127,82,0) & E & (E) & E & (E) & E \\
\hline
		  28 & E & 50,0,127,82,0 & (1) & (E) & (50,0,127,82,0) & (E) & E \\
\hline
		  29 & E & (E) & (1) & E & (50,0,127,82,0) & E & E \\
\hline
		  30 & E & (E) & 1 & (1) & 50,0,127,82,0 & (36,0,127,120,-1) & E \\
\hline
		  31 & E & E & (E) & (1) & (E) & (36,0,127,120,-1) & E \\
\hline
		  32 & E & (36,0,127,120,-1) & (E) & 1 & (E) & 36,0,127,120,-1 & E \\
\hline
		  33 & E & (36,0,127,120,-1) & E & (E) & E & (E) & E \\
\hline
		  34 & E & 36,0,127,120,-1 & (0) & (E) & E & (E) & (120) \\
  \end{tabular}
  \caption{Computation steps of $13^{22} \mod 127$ using the circuit from above, yielding the correct result $120$}
  \label{table:montgomery-computation}
\end{table}

Table~\ref{table:montgomery-computation} shows the individual computational steps in calculating the desired value $13^{22} \mod 127 = 120$.
It's a long computation, but the steps are cyclic as can be seen.
We might have been able to use fewer steps if we introduced a buffer like in (b), but we chose to use less resources.

\newpage

\FloatBarrier % Leave the FloatBarriers in place.
\section{Dual-Rail Function Block Design (40 points)}

\subsection{Subtask (a)}

First we list our gate library implementation, which can be seen in Figure~\ref{fig:wchb-a-gate-library}.
The implementations were inspired from the slides and these elements we will insert into our design as blackboxes.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{graphics/wchb-a-gate-library.pdf}
	\caption{\label{fig:wchb-a-gate-library} Gate Library with DIMS XOR gate and NCLX AND gate}
\end{figure}

Now for our pipeline, we are tasked to insert the logic between two WCHB pipeline stages.
A standard WCHB pipeline is used, which also includes input detection.
Do note that input detection is not necessarily necessary here, the DIMS XOR implementation already has input detection.
For this reason we used the outputs of the second DIMS XOR gate to pass it on as $ack_{out}$.
Of course, this might lead to $ack_{out}$ being generated later than it would otherwise be possible, because the path is longer, but we want to save on redundant gates here.

We don't need any output detection for the XOR implementation, because all outputs are mutually exclusive.

For the NCLX AND gates, we check each $done$ signal individually, so as to avoid having orphans in our circuit.
This is then used together with the input detection of the next stage to determine when it is legal to change the inputs.

The last NCLX AND gate is used here as OR gate with inverted inputs and output.
Because inversion is free in dual rail logic, we are simply rewiring the true and false rails to implement the OR gate as an NCLX AND gate withough any additional resources using our gate library.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{graphics/wchb-a-pipeline.pdf}
	\caption{\label{fig:wchb-a-pipeline} Full Adder circuit inserted in WCHB pipeline, with input, output and datapath}
\end{figure}

Figure~\ref{fig:wchb-a-pipeline} shows the full adder circuit, with a WCHB pipeline input and output included.
The WCHB pipeline input, output and datapath are neatly separated.

\begin{table}[ht]
	\centering
	\begin{tabular}{c||c c c c||c c}
		& \bf {AND} & \bf{OR} & \bf{C-gate} & \bf{inverter} & \textbf{DIMS XOR} & \textbf{NCLX AND} \\
\hline
\hline
		DIMS XOR             & 0 & 2 & 4 & 0 & 0 & 0 \\
\hline
		NCLX AND             & 1 & 3 & 1 & 0 & 0 & 0 \\
\hline
		WCHB pipeline input  & 0 & 0 & 7 & 1 & 0 & 0 \\
\hline
		WCHB pipeline output & 0 & 2 & 5 & 1 & 0 & 0 \\
\hline
		datapath             & 0 & 1 & 2 & 0 & 2 & 3 \\
\hline
\hline
		\textbf{$\Sigma$ total}       & 0 & 3 & 15 & 2 & 2 & 3 \\
\hline
		$\Sigma$ expanded             & 3 & 16 & 26 & 2 & 0 & 0 \\
	\end{tabular}
	\caption{Gate Counts for the design}
	\label{table:gate-count}
\end{table}

Table~\ref{table:gate-count} shows the total counts of all elements, with extra rows just for the gate library included.
The \textit{$\Sigma$ total} column counts the gate library components, whereas the \textit{$\Sigma$ expanded} column expands the primitive gate counts for the gate library components.
Again, if we are allowed to optimize the WCHB pipeline input stage, then we can have three less OR gates and three less C-gates!

Finally we turn to the question of whether the resulting circuit is strongly or weakly indicating.
We could have weakly indicating results, because we have weakly indicating components in our circuit, but practically, because the DIMS XOR result is used in the second NCLX AND gate, our result is strongly indicating.
This is because the second NCLX AND won't have results from the DIMS XOR gate until all inputs of the DIMS XOR gate have been provided.
Because the DIMS gates are strongly indicating, our entire design is strongly indicating, even though parts of the circuit are weakly indicating.

\subsection{Subtask (b)}

For this subtask we are to replace the DIMS XOR with an NCLX XOR implementation and the NCLX AND with a DIMS AND implementation.
Figure~\ref{fig:wchb-b-gate-library} shows the new gate library with the new implementations.
Do note that the NCLX XOR gate looks quite similar to the DIMS XOR gate, but we have a ladder of C-gates for input detection.

Like before we don't need any output detection for XOR because all outputs are mutually exclusive.
But this time we do need input detection, so the NCLX XOR gate has a C-gate for input detection.
The gate also has a C-gate for completion detection, as we need to make sure not to unwittingly introduce orphans!

But do note that there is a potential gate orphan for our DIMS AND gate which we need to consider.
Because this is a DIMS gate, the one-hot coding eliminates any potential for multiple states being active at the same time, so there are no unobserved transitions and we don't need any output detection for the OR gate ladder, so there is no problem.

Like before, we generate the $ack_{out}$ signal using the redundant input completion parts of the datapath to save extra input detection resources.
The inverter at the input C-gates use the done signals for the two NCLX XOR gates.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{graphics/wchb-b-gate-library.pdf}
	\caption{\label{fig:wchb-b-gate-library} Modified Gate Library with NCLX XOR gate and DIMS AND gate}
\end{figure}

Figure~\ref{fig:wchb-b-pipeline} shows the full adder circuit, with a WCHB pipeline input and output included.
The WCHB pipeline input, output and datapath are neatly separated.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{graphics/wchb-b-pipeline.pdf}
	\caption{\label{fig:wchb-b-pipeline} Modified Full Adder circuit inserted in WCHB pipeline, with input, output and datapath}
\end{figure}

\begin{table}[ht]
	\centering
	\begin{tabular}{c||c c c c||c c}
		& \bf {AND} & \bf{OR} & \bf{C-gate} & \bf{inverter} & \textbf{NCLX XOR} & \textbf{DIMS AND} \\
\hline
\hline
		NCLX XOR             & 4 & 5 & 2 & 0 & 0 & 0 \\
\hline
		DIMS AND             & 0 & 2 & 4 & 0 & 0 & 0 \\
\hline
		WCHB pipeline input  & 0 & 0 & 7 & 1 & 0 & 0 \\
\hline
		WCHB pipeline output & 0 & 2 & 5 & 1 & 0 & 0 \\
\hline
		datapath             & 0 & 2 & 1 & 0 & 2 & 3 \\
\hline
\hline
		\textbf{$\Sigma$ total}       & 0 & 4 & 15 & 2 & 2 & 3 \\
\hline
		$\Sigma$ expanded             & 8 & 20 & 29 & 2 & 0 & 0 \\
	\end{tabular}
	\caption{Modified Gate Counts for the design}
	\label{table:modified-gate-count}
\end{table}

Again, Table~\ref{table:modified-gate-count} shows the total counts of all elements, with extra rows just for the gate library included.
The \textit{$\Sigma$ total} column counts the gate library components, whereas the \textit{$\Sigma$ expanded} column expands the primitive gate counts for the gate library components.

The resource usage seems to be higher in this design than the one in (a).

Again as before, the entire resulting circuit is strongly indicating because we use the strongly indicating DIMS AND component from our gate library.

\subsection{Subtask (c)}

There is an improvement to be made if we have $n$-input gates available.

The first consideration applies to both designs from (a) and (b).
If we had $n$-input C-gates available, we could replace our C-gate ladders with a single $n$-input C-gate, simplifying the design.
This is under the assumption that the C-gate will actually wait until all inputs have made the correct transition for the currently expected phase.

\subsection{Subtask (d)}

The NCL full adder based on threshhold gates switches on the output as soon as the output signals switch.
The output signals switch as soon as the respective threshhold gates that generate them have collected enough high signals.
For this reason, the outputs could switch even before all inputs have been applied, if such an execution exists.

In fact, we can construct such an execution easily: one way would be to let $a.T$ and $b.T$ go high, which would produce a correct result on $c_{out}.T$ with only part of the inputs.
Other executions also exist.

Thus the NCL full adder circuit is \textit{weakly indicating}.

\subsection{Subtask (e)}

In Figure~\ref{timing:and} an example has been constructed that shows erroneous behavior of the DIMS AND gate.
In this example, we assume that the fork of signal $b.T$ is not isochronic.
We split this signal into two paths, one of them being $b.T \to x.F$ and the other $b.T \to x.T$.
More specifically, our assumption is that there is a considerable delay on the path $b.T \to x.T$.

In our example, the circuit upholds the protocol: after application of $a.F$ and $b.T$, an output is produced and then a NULL phase is initiated.
Both inputs $a.F$ and $b.T$ are brought to NULL and the output $x.F$ goes to NULL too.
Now we apply $a.T$ and $b.F$ instead, but the $b.T \to x.T$ path has considerable delay and remains high.
Correctly, $x.F$ goes high again, but because the path $b.T \to x.T$ is active in addition to $a.T$, the output $x.T$ goes high as well.
Now we are in the region of undefined behavior, which we would call at least undesired behavior.

\begin{figure}[htb]
	\centering
	\begin{tikztimingtable} [timing/e/background/.style={fill=gray}]
		$a.F$ & LHHHLLLLLLLLL \\
		$a.T$ & LLLLLLLHHHLLL \\
		$b.F$ & LLLLLLLHHHLLL \\
		$b.T \to x.F$ & LHHHLLLLLLLLL \\
		$b.T \to x.T$ & LHHHHHHHHHHHH \\
		$x.F$ & LLHHHLLLHHHLL \\
		$x.T$ & LLLLLLLLHHHHH \\
		\extracode
		\begin{pgfonlayer}{background}
			\begin{scope}[semitransparent ,semithick]
				\vertlines[gray,opacity=0.3]{1,2,...,12}
			\end{scope}
		\end{pgfonlayer}
	\end{tikztimingtable}
	\caption{\label{timing:and} Example execution of DIMS AND gate with considerable delay on the $b.T \to x.T$ path leading up to the downmost C-gate causing an erroneous transition on signal $x.T$}
\end{figure}


\FloatBarrier
\phantomsection
\addcontentsline{toc}{section}{References}
\bibliographystyle{plain}
\bibliography{refs.bib}

\end{document}
