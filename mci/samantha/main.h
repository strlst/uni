#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f30x.h"

// function definitions
/**
  * @brief  Configures all periphery of the STM32.
  *         RCC, GPIO, TIM, SPI, ADC
  * @param None
  * @retval None
 */
void STM32Config(void);

/**
  * @brief  Translates a full RX buffer of activations after a successful SPI
  *         transmission to actual collisions with game objects and removes
  *         them if needed.
  * @param None
  * @retval None
 */
void rxToCollision(void);

// SPI definitions
// general definitions
#define SPIx                             SPI1
#define SPIx_CLK                         RCC_APB2Periph_SPI1
#define SPIx_IRQn                        SPI1_IRQn

// SCK pin
#define SPIx_SCK_PIN                     GPIO_Pin_3
#define SPIx_SCK_GPIO_PORT               GPIOB
#define SPIx_SCK_GPIO_CLK                RCC_AHBPeriph_GPIOB
#define SPIx_SCK_SOURCE                  GPIO_PinSource3
#define SPIx_SCK_AF                      GPIO_AF_5

// MISO pin
#define SPIx_MISO_PIN                    GPIO_Pin_5
#define SPIx_MISO_GPIO_PORT              GPIOB
#define SPIx_MISO_GPIO_CLK               RCC_AHBPeriph_GPIOB
#define SPIx_MISO_SOURCE                 GPIO_PinSource5
#define SPIx_MISO_AF                     GPIO_AF_5

// MOSI pin
#define SPIx_MOSI_PIN                    GPIO_Pin_4
#define SPIx_MOSI_GPIO_PORT              GPIOB
#define SPIx_MOSI_GPIO_CLK               RCC_AHBPeriph_GPIOB
#define SPIx_MOSI_SOURCE                 GPIO_PinSource4
#define SPIx_MOSI_AF                     GPIO_AF_5

/*
#define SPIx_NSS_PIN                     GPIO_Pin_4
#define SPIx_NSS_GPIO_PORT               GPIOB
#define SPIx_NSS_GPIO_CLK                RCC_AHBPeriph_GPIOB
#define SPIx_NSS_SOURCE                  GPIO_PinSource4
#define SPIx_NSS_AF                      GPIO_AF_5
*/

#define SPI_DATASIZE                     SPI_DataSize_8b

#endif /* __MAIN_H */
