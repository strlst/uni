% Optimizing Compilers
% Exercise 5
% Hamza Muric, Norbert Tremurici, Lorenz Winkler

# task 1

We define the Gen/Kill equations acting on program labels for our dominator analysis as follows.

$$
\begin{aligned}
\text{gen}_{\text{DA}}(\ell) &= \{ \ell \} \\
\text{kill}_{\text{DA}}(\ell) &= \emptyset \\
\end{aligned}
$$

That is, information is never explicitly killed and only implicitly reduced when meeting program paths.

$$
\begin{aligned}
DA_{\text{entry}}(\ell) &= 
\begin{cases}
\emptyset & \text{if } \ell = \text{init}(S_\star) \\
\bigcap \{ DA_{\text{exit}}(\ell') \mid (\ell', \ell) \in \text{flow}(S_\star) \} & \text{otherwise} \\
\end{cases} \\
DA_{\text{exit}}(\ell) &= (DA_\text{entry}(\ell) \setminus \text{kill}_{\text{DA}}(\ell)) \cup \text{gen}_{\text{DA}}(\ell) \\
\end{aligned}
$$

# task 2

Our analysis result for the given C program can be viewed in Figure 1.

![Figure 1: Analysis result visualized using `aisee` for `dominator_analysis.optla`](result.png)
