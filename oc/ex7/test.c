int main() {
    int a, b, c, d, x, y, z;
    a = 1;
    b = a;
    x = ((1 << 3) >> 1);
    y = (4 & x) | 1;
    z = (1 + y * 8 / 2) % 3;
    b--;
    if (a <= b)
        c = 2;
    else
        c = 1;
    // we never enter this loop
    while (a > 1) {
        a = (a << 1) + ((b * b) << 2) + ((c * c * c) << 3);
    }
    // this update should be reflected
    a = a + 2;
    // but we enter this one
    while (a < 4) {
        a = (a << 1) + ((b * b) << 2) + ((c * c * c) << 3);
    }
    // this one should not
    a = a + 4;
    while (a % 2 == 1) {
        a = (a << 1) + ((b * b) << 2) + ((c * c * c) << 3);
    }
    a = a + 8;
    while (d > 0) {
        d--;
    }
    a = a + 8;
    return a;
}
