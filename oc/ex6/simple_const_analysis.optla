// Optimierende Uebersetzer WS 24
// Aufgabe 1: Dominator-Analyse
// H. Moritsch

SET

DOMAIN
    ValueLattice = flat(snum)
    ConstantDFunc = str -> ValueLattice
ENDDOMAIN

PROBLEM sc
    direction:  forward
    carrier:    ConstantDFunc
    init_start: [-> bot]
    init:       top

    equal:      eq
    combine:    comb
    retfunc:    comb
    widening:   wide

TRANSFER

// dont care about these
DeclareStmt(_, _), _ : @;
UndeclareStmt(_), _ : @;
FunctionEntry(_), _ : @;
FunctionExit(_, _), _ : @;
IfJoin(),_:@;
WhileJoin(),_:@;

ExprStatement(AssignOp(lhs, expr)), _ : 
    let value = expr_val(expr, @);
	variable = varid_str(varref_varid(lhs));
    in println("updating ", variable, " to ", value)
       @\[variable->value];

ScopeStatement(IfStmt(ExprStatement(expr))),_ :
    let value = expr_val(expr,@);
    in println("If with value: ", value) @;

// rest of node types is irrelevant
statement, _ :
    println("warning: cannot handle statement of the form:", stmt_asttext(statement))
    @;

SUPPORT

const_add :: ValueLattice, ValueLattice -> ValueLattice;
const_add(a, bot) = bot;
const_add(bot, a) = bot;
const_add(a, top) = top;
const_add(top, a) = top;
const_add(a, b) = lift(drop(a) + drop(b));

const_sub :: ValueLattice, ValueLattice -> ValueLattice;
const_sub(a, bot) = bot;
const_sub(bot, a) = bot;
const_sub(a, top) = top;
const_sub(top, a) = top;
const_sub(a, b) = lift(drop(a) - drop(b));

const_mul :: ValueLattice, ValueLattice -> ValueLattice;
const_mul(lift(0), a) = lift(0);
const_mul(a, lift(0)) = lift(0);
const_mul(a, bot) = bot;
const_mul(bot, a) = bot;
const_mul(a, top) = top;
const_mul(top, a) = top;
const_mul(a, b) = lift(drop(a) * drop(b));

const_div :: ValueLattice, ValueLattice -> ValueLattice;
const_div(a, bot) = bot;
const_div(bot, a) = bot;
const_div(a, top) = top;
const_div(top, a) = top;
const_div(a, b) = lift(drop(a) / drop(b));

const_less :: ValueLattice, ValueLattice -> ValueLattice;
const_less(a, bot) = bot;
const_less(bot, a) = bot;
const_less(a, top) = top;
const_less(top, a) = top;
const_less(a, b) = lift(if drop(a) < drop(b) then 1 else 0);

const_lesseq :: ValueLattice, ValueLattice -> ValueLattice;
const_lesseq(a, bot) = bot;
const_lesseq(bot, a) = bot;
const_lesseq(a, top) = top;
const_lesseq(top, a) = top;
const_lesseq(a, b) = lift(if drop(a) <= drop(b) then 1 else 0);

const_eq :: ValueLattice, ValueLattice -> ValueLattice;
const_eq(a, bot) = bot;
const_eq(bot, a) = bot;
const_eq(a, top) = top;
const_eq(top, a) = top;
const_eq(a, b) = lift(if drop(a) = drop(b) then 1 else 0);

expr_val :: Expression, ConstantDFunc -> ValueLattice;
expr_val(expr, constants) =
    case expr of
        IntVal(value) => lift(val-astint(value));
        VarRefExp(var) => constants{!val-aststring(var)!};
        AssignOp(VarRefExp(_), rhs) => expr_val(rhs, constants);
        AddOp(lhs, rhs) => const_add(expr_val(lhs, constants), expr_val(rhs, constants));
        SubtractOp(lhs, rhs) => const_sub(expr_val(lhs, constants), expr_val(rhs, constants));
        LessThanOp(lhs, rhs) => const_less(expr_val(lhs, constants), expr_val(rhs, constants));
	EqualityOp(lhs, rhs) => const_lesseq(expr_val(lhs, constants), expr_val(rhs, constants));
	LessOrEqualOp(lhs, rhs) => const_eq(expr_val(lhs, constants), expr_val(rhs, constants));
	_ => println("warning: expr_val does not support: ", expr_asttext(expr))
             top;
    endcase;

eq :: ConstantDFunc, ConstantDFunc -> bool;
    eq(c1, c2) = (c1 = c2);

combine_func :: ValueLattice, ValueLattice -> ValueLattice;
    combine_func(a,b) = (a glb b);

comb :: ConstantDFunc, ConstantDFunc -> ConstantDFunc;
    // This is key for combining If and While. We cake the results of c1 and c2
    comb(c1, c2) = crunch(c1, c2, combine_func);

wide(c1, c2) = c2; // no widening used
