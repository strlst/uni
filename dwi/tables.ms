.TL
Computational Thinking Sortiert Tabellen
.AU
strlst
.SH
Liste
.TS
center, box, tab ($) ;
l     c c c c c c.
Liste$7$6$6$3$2$1
.TE
.SH
Count Table
.TS
center, box, tab ($) ;
l      c c c c c c c.
Count
Index$1$2$3$4$5$6$7
Anzahl$1$1$1$0$0$2$1
.TE
.
.NT C "test"
Count Table summiert
.TS
center, box, tab ($) ;
l      c c c c c c c.
Count
Index$1$2$3$4$5$6$7
Anzahl$1$2$3$3$3$5$6
.TE
