% streckenübertragungsfunktion
s = tf('s');
% stabil (hurwitzpolynom, 2 grad, selbes vorzeichen und reell kleiner 0)
zG = (2*(1+s/4));
nG = (s*(1+1/2e4*s+1/16e4*s^2));
G = zG/nG;
%bode(G)

% anforderungen
% doppelintegratorvorgabe (keine bleib. regelabweichung auf rampenf.
% referenzsignal) e_inf|d(t)=t = 0
% multipliziert mit s da einfach integrator bereits vorhanden
di = 1/s^2*s;
tr = 0.7; % anstiegszeit
ue = 0.3; % erlaubtes überschwingen

% durchtrittsfrequenz 20*log(abs(L(I*omega))) = 0 => abs(L(I*omega)) = 1
% und wc*tr approx 1.5 => 1.5/tr = wc
wc = 1.5/tr;
% phasenreserve phi [grad] + ue [%] approx 70 => phi [grad] = 70 - ue [%]
% = 70 - 100 * ue
% phi [rad] = phi [grad] / 180 * pi
phi = 70-100*ue;
phir = phig/180*pi;

R1 = (nG/s)/s;
L1 = R1*G;

% arg(L1(I*wc))
[mag, phase] = bode(L1, wc);