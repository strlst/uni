% Wissenschaftliches Arbeiten
% name lastname (11907086)
% 11. Oktober 2021

# 1.

In my humble opinion, the two most important considerations of the essay "The Spirit of Research" (by Kate L. Turabian) are as follows:

- the role and influence of authority
- the role and influence of beliefs

Two considerations that are similar but subtly different.

# 2.

As is beautifully outlined in the essay, historically, authority has dictated accepted truth. This stands in obvious conflict to the way the scientific community has collectively decided to pursue truth. While scientific ideas are meant to transcend our individual experience, these ideas can sometimes challenge our personal beliefs. Sometimes they can also challenge personal beliefs of authoritative figures, creating said conflict.

While the essay outlines those two important considerations as separate points, they appear to be intertwined, as a threat to authority is only present when scientific ideas challenge the accepted narrative (a narrative that is the result of the beliefs of authoritative figures). It is important however to emphasize both considerations separately, as the essay does, because it allows us to contemplate authority as well as ourselves separately. In a system designed to pursue scientific truth there is likely no room for the negative influences of authority and only with awareness can this point be adressed. On the other hand, there is no pursuing of scientific truth without individuals who are actively working to establish those scientific truths and this pursuit begins with the values of research presented in the essay. A cornerstone of the author's argumentation is how those values can elicit difficult emotions, for example, scientific inquiries that are seen as personal attacks.

In my view, both figures of authority as well as ourselves have to reconcile with those values of research if the goal is to further our scientific body of knowledge. As presented in the essay, in words better than I could have chosen them: "But in the world of research, both academic and professional, good evidence and sound reasoning trump belief every time, or at least they should." At the very least, that is why I regard those considerations as important.
