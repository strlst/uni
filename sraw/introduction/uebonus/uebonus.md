% Wissenschaftliches Arbeiten
% name lastname (11907086)
% 12. Oktober 2021

# Ersatzaufgabe

## Was ist Wissenschaft? Was heißt wissenschaftlich arbeiten? Was bringt ein wissenschaftliches Studium? Ein Brief (von Wolf-Dieter Narr)

### 1.

Das in "Die Technik des wissenschaftlichen Arbeitens" (16. Auflage) erschienene Vorwort ist ein Brief (der persönlichen Art) von Wolf-Dieter Narr. In diesem Brief adressiert er Studenten und Studentinnen, vorallem jene die soeben mit dem Studium begonnen haben. Die äußerst zentralen, im Studium relevanten Fragen, was sei Wissenschaft, was hieße es wissenschaftlich zu arbeiten und was bringe ein wissenschaftliches Studium werden in detailierter Ausführung dekonstruiert und behandelt.

Dabei dient der Text als ein Orientierungsleitfaden, ein Fremdenführer für eine Welt die anfänglich verwirrend ist (und vielleicht sogar so verbleiben mag). Hier wird betont, dass die Funktionsweise einer Universität, eines Studiums, sogar unterschiedlicher Lehrveranstaltungen zum einen schwer zu durchschauende Hürden darstellt, aber zum anderen die passenden Einrichtungen zur Weiterbildung sind. Es ist allgemein ein zentrales Thema, dass der Leser sich eine Balance an Bescheidenheit und Selbstbestimmung (Flexibilität und Sturheit wenn man so will) aneignen muss um erfolgreich in der Wissenschaft zu funktionieren. Dabei ausschlaggebend ist, dass man zu jedem Zeitpunkt ein klares Bild darüber hat, was es zu tun gibt, wie die Dinge funktionieren und was zu erwarten ist. Gleichzeitig aber muss man jederzeit aufnahmefähig genug sein um seinen alten Weg der Dinge durch etwas Vorteilhafteres zu ersetzen, unter anderem auch, anzupassen wie man lernt und erfasst.

Im Herzen des Textes steht immer die Wissenschaft. Keine Überraschung, wenn der Text das Vorwort zu einem solchen Buch darstellt. Dem Leser werden dabei die wichtigsten Aspekte sowie eine praktikable Sicht des wissenschaftlichen Arbeitens und des wissenschaftlichen Studierens beigebracht. All dies auch mit einer sekundären Motivation, wie im Text genau erläutert wird, dem Leser genug Selbstbewusstsein (oder Know-How über die Akquise von Selbstbewusstsein) zu geben, um die modernen Probleme der Wissenschaft behandeln zu können.

### 2.

Der Erwerb von Urteilsfähigkeit ist stark durch das ständige Fragen geprägt. In dem Brief wird beispielsweise der kuriose Fall der folgenden Frage erwähnt: "Was ist Wissenschaft?" Im Erarbeiten einer persönlichen Antwort, wie vom Autor gefordert, stößt man auf die vielen unterschiedlichen Bereiche und Methoden der Wissenschaft. Es ist durchaus ein wissenschaftlicher Aspekt des Studiums sich in diesen Bereichen und Methoden zurechtzufinden. Dafür benötigt man aber zuerst die Urteilsfähigkeit um diese auseinanderzuhalten, eine Urteilsfähigkeit die wie bereits erwähnt durch das ständige Fragen erworben werden kann.

Konkret wird Urteilsfähigkeit auch als eine bestimmte Fertigkeit näher erläutert: "Sie können sich überall irgendwie, ohne sich zu verlieren, zurechtfinden." Um aus dem Studium zu bekommen was erwartet wird müsse man sich die zusammengehörigen Teile aus dem Studium holen. Hier gibt es auch einen Zusammenhang zum wissenschaftlichen Charakter des Studiums, es ist ja ähnlich neugieriges Verhalten für das wissenschaftliche Arbeiten notwendig.

### 3. a.

#### Ein Problem bearbeitbar machen: Klare Fragestellungen formulieren

Nicht nur schränkt eine klare Fragestellung die Arbeit auf einen realistischen Ausmaß ein, sondern kommt an dieser Stelle auch eine perfekte Gelegenheit auf, festzulegen "was Sie vom Problem damit auf welche Weise herauszufinden gedenken."

#### Vorraussetzungen prüfen, Vorraussetzungen offenlegen

Abstraktionen sind oftmals wegen ihrer Ausdruckskraft notwendig, aber mit der Einführung von Abstraktionen kommen Vorraussetzungen, die vielleicht oder vielleicht auch nicht aufgedeckt sind. Wie erklärt wird muss hier mit dem Transparenz- und Explikationspostulat gearbeitet werden, wo unter anderem Vorraussetzungen näher betrachtet werden.

#### Zum "Stand der Literatur": Kein Imponiergehabe

Die gelesene Literatur formt eine Grundlage des wissenschaftlichen Arbeitens, wie vorallem betont wird, heutzutage mehr denn je.

#### Abstand gewinnen

An dieser Stelle wird einleitend erklärt dass, sobald die Hausarbeit fertig gediehen sei, man einige Tage ruhen solle. Dabei wird das selbst Geschriebene teilweise so fremd, dass man wieder aufbauend darauf seine Arbeit verbessern kann.

#### Interdisziplinarität

Auch wenn diese Empfehlung auf den ersten Blick nicht so relevant erscheint, es wird schlüssig argumentiert wieso diese so ausschlaggebend sein kann: "Sie werden das eigene Fach in seinen Zusammenhängen oder auch Zusammenhangslosigkeiten und Widersprüchen besser begreifen."

### 3. b.

#### Ein Problem bearbeitbar machen: Klare Fragestellungen formulieren

Ohne eine Fragestellung greift man schlicht und einfach im dunkeln. Eine klare Problemformulierung und eine klare Fragestellung sind ausschlaggebend dafür, dass die eigene Arbeit überhaupt wissenschaftlich angegangen werden kann.

#### Vorraussetzungen prüfen, Vorraussetzungen offenlegen

Vorallem in unserem Bereich der Informatik schleichen Abstraktionen wohin auch nur geschaut wird, da darf einfach auf Voraussetzungen nicht vergessen werden.

#### Zum "Stand der Literatur": Kein Imponiergehabe

Die eigenen Interessen und die eigene Suche nach Forschung, Problemen und Antworten ist in signifikanter Art beeinflusst durch die konsumierte Literatur. Dabei ist diese feine Linie zwischen dem neuesten Stand der Dinge und den alten Schultern der Giganten ein Grundbaustein einer guten Arbeit. Außerdem soll die Fantasie nicht verloren gehen!

#### Abstand gewinnen

Um effektiv wissenschaftlich zu arbeiten muss erst effektiv gearbeitet werden. Dazu zählt für mich persönlich einfach dieses Konzept des Abstandes.

#### Interdisziplinarität

Mir ist an dieser Stelle bewusst, dass streng genommen dieser Aspekt keine Empfehlung aus dem letzten Abschnitt ist. Ich halte ihn aber durchaus für wichtig, da wie in dem Brief erwähnt wird, die daraus resultierende Motivation nicht zu unterschätzen ist. Diese Motivation kann den Unterschied machen ob eine großartige Arbeit überhaupt entsteht. Persönlich motiviert mich jedenfalls die Interdisziplinarität Technische Informatik zu studieren.
