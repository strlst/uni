# Wissenschaft

- Abgespalten von der Philosophie im 18. Jh.
- baut auf Evidenz, Begründung und rational geführter Argumentation (rationaler Dialog)
- später auch von Geschichte abgespalten (Naturgeschichte und Naturkunde)
- seit 18. Jh. neue Gesellschaftsanalyse
- Einteilung nach Methode (verstehend versus erklärend), Begründungsart (apriorische versus empirische)
- Wissenschaft kann aus (technischer) Kunst resultieren
- komplexe Dynamik zwischen Wissenschaft und Technik

### lecture 1

- Kennzeichen:
  - methodisch
  - objektiv
  - konsistent
  - stringent
  - relevant
