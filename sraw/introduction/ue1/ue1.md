% Wissenschaftliches Arbeiten
% 4. Oktober 2021

### name lastname (11907086), name lastname (11816865)
### name lastname (11929150), name lastname (51841002)
### name lastname (11809914), name lastname (11905164)
### name lastname (01529018), name lastname (01529233)

# Beschreiben und erläutern Sie Ihre bisherigen Erfahrungen mit Wissenschaft in Ihrem Informatik-Studium?

Die für jeden vermutlich erste Konfrontation mit wissenschaftlichem Arbeiten passiert in der LVA Denkweisen der Informatik, in dem man wissenschaftliche Texte lesen und analysieren muss. Außerdem wird einem das wichtige Konzept von Peer Review vorgestellt. Auf Zitieren wird ebenso geachtet, wie in der LVA Informatikrecht oder in der LVA IT-Strategie. Neben Denkweisen musste man in Fächern wie Algebra auf die korrekte Beweisführung achten, sowie eine präzise Konkretisierung der Definitionen. Zu guter Letzt finden sich viele Lehrveranstaltungen in denen die Professoren begeistert ihre eigene Forschung vorstellen.

Eine Person aus der Gruppe hat an der LVA Grundlagen des wissenschaftlichen Arbeitens teilgenommen, in dem in einem wissenschaftlichen Stil 5 bis 10 Seiten die sich mit einem Fachbereich aus der Informatik befassen produziert werden musste. Zusätzlich wurde Google Scholar verwendet und es wurde beigebracht wie gründlich und korrekt zitiert wird. Zuletzt war jeder von uns schon einmal konfrontiert mit der Thematik von Korrelation versus Kausation, die für die Wissenschaft essentiell ist.
