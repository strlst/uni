# This data file was generated by the Spreadsheet Calculator Improvised (sc-im)
# You almost certainly shouldn't edit it.

newsheet "Sheet1"
movetosheet "Sheet1"
offscr_sc_cols 0
offscr_sc_rows 0
nb_frozen_rows 0
nb_frozen_cols 0
nb_frozen_screenrows 0
nb_frozen_screencols 0
format A 12 2 0
format B 39 2 0
format J 13 2 0
format K 16 2 0
format L 12 2 0
label A0 = "semester"
rightstring B0 = "course"
rightstring C0 = "type"
rightstring D0 = "ECTS"
rightstring E0 = "done"
rightstring F0 = "achieved"
rightstring G0 = "todo"
label A1 = "1"
rightstring B1 = "fundamentals of digital systems"
rightstring C1 = "VU"
let D1 = 3
let E1 = 1
let F1 = @if(E1>0,D1,0)
rightstring G1 = @if(E1<1,"!","")
rightstring B2 = "ways of thinking in informatics"
rightstring C2 = "VU"
let D2 = 5.5
let E2 = 1
let F2 = @if(E2>0,D2,0)
rightstring G2 = @if(E2<1,"!","")
rightstring B3 = "orientation informatics"
rightstring C3 = "VU"
let D3 = 1
let E3 = 1
let F3 = @if(E3>0,D3,0)
rightstring G3 = @if(E3<1,"!","")
rightstring B4 = "algebra and discrete mathematics"
rightstring C4 = "VO"
let D4 = 4
let E4 = 1
let F4 = @if(E4>0,D4,0)
rightstring G4 = @if(E4<1,"!","")
rightstring B5 = "algebra and discrete mathematics"
rightstring C5 = "UE"
let D5 = 5
let E5 = 1
let F5 = @if(E5>0,D5,0)
rightstring G5 = @if(E5<1,"!","")
rightstring B6 = "analysis for computer science"
rightstring C6 = "VO"
let D6 = 2
let E6 = 1
let F6 = @if(E6>0,D6,0)
rightstring G6 = @if(E6<1,"!","")
rightstring B7 = "analysis for computer science"
rightstring C7 = "UE"
let D7 = 4
let E7 = 1
let F7 = @if(E7>0,D7,0)
rightstring G7 = @if(E7<1,"!","")
rightstring H7 = "tally"
rightstring I7 = "achieved"
rightstring B8 = "introduction to programming"
rightstring C8 = "VU"
let D8 = 5.5
let E8 = 1
let F8 = @if(E8>0,D8,0)
rightstring G8 = @if(E8<1,"!","")
let H8 = @sum(D1:D8)
let I8 = @sum(F1:F8)
label A9 = "2"
rightstring B9 = "formal modelling"
rightstring C9 = "VU"
let D9 = 3
let E9 = 1
let F9 = @if(E9>0,D9,0)
rightstring G9 = @if(E9<1,"!","")
rightstring B10 = "algorithms and data structures"
rightstring C10 = "VU"
let D10 = 8
let E10 = 1
let F10 = @if(E10>0,D10,0)
rightstring G10 = @if(E10<1,"!","")
rightstring B11 = "electrical engineering"
rightstring C11 = "VU"
let D11 = 4
let E11 = 1
let F11 = @if(E11>0,D11,0)
rightstring G11 = @if(E11<1,"!","")
rightstring B12 = "electrical engineering"
rightstring C12 = "LU"
let D12 = 3.5
let E12 = 1
let F12 = @if(E12>0,D12,0)
rightstring G12 = @if(E12<1,"!","")
rightstring B13 = "analysis 2"
rightstring C13 = "VO"
let D13 = 3
let E13 = 1
let F13 = @if(E13>0,D13,0)
rightstring G13 = @if(E13<1,"!","")
rightstring B14 = "analysis 2"
rightstring C14 = "UE"
let D14 = 4.5
let E14 = 1
let F14 = @if(E14>0,D14,0)
rightstring G14 = @if(E14<1,"!","")
rightstring H14 = "tally"
rightstring I14 = "achieved"
rightstring B15 = "introduction to programming 2"
rightstring C15 = "VU"
let D15 = 4
let E15 = 1
let F15 = @if(E15>0,D15,0)
rightstring G15 = @if(E15<1,"!","")
let H15 = @sum(D9:D15)
let I15 = @sum(F9:F15)
label A16 = "3"
rightstring B16 = "theoretical computer science and logics"
rightstring C16 = "VU"
let D16 = 6
let E16 = 1
let F16 = @if(E16>0,D16,0)
rightstring G16 = @if(E16<1,"!","")
rightstring B17 = "digital design"
rightstring C17 = "VO"
let D17 = 3
let E17 = 1
let F17 = @if(E17>0,D17,0)
rightstring G17 = @if(E17<1,"!","")
rightstring B18 = "probability and stochastic processes"
rightstring C18 = "VO"
let D18 = 4
let E18 = 1
let F18 = @if(E18>0,D18,0)
rightstring G18 = @if(E18<1,"!","")
rightstring B19 = "probability and stochastic processes"
rightstring C19 = "UE"
let D19 = 3.5
let E19 = 1
let F19 = @if(E19>0,D19,0)
rightstring G19 = @if(E19<1,"!","")
rightstring B20 = "operating systems"
rightstring C20 = "VO"
let D20 = 2
let E20 = 1
let F20 = @if(E20>0,D20,0)
rightstring G20 = @if(E20<1,"!","")
rightstring B21 = "operating systems"
rightstring C21 = "UE"
let D21 = 4
let E21 = 1
let F21 = @if(E21>0,D21,0)
rightstring G21 = @if(E21<1,"!","")
rightstring B22 = "computer networks"
rightstring C22 = "VU"
let D22 = 3
let E22 = 1
let F22 = @if(E22>0,D22,0)
rightstring G22 = @if(E22<1,"!","")
rightstring H22 = "tally"
rightstring I22 = "achieved"
rightstring B23 = "signals and systems 1"
rightstring C23 = "VU"
let D23 = 4.5
let E23 = 1
let F23 = @if(E23>0,D23,0)
rightstring G23 = @if(E23<1,"!","")
let H23 = @sum(D16:D23)
let I23 = @sum(F16:F23)
label A24 = "4"
rightstring B24 = "hardware modeling"
rightstring C24 = "VO"
let D24 = 1.5
let E24 = 1
let F24 = @if(E24>0,D24,0)
rightstring G24 = @if(E24<1,"!","")
rightstring B25 = "computer organization and design"
rightstring C25 = "VO"
let D25 = 3
let E25 = 1
let F25 = @if(E25>0,D25,0)
rightstring G25 = @if(E25<1,"!","")
rightstring B26 = "digital design and architecture"
rightstring C26 = "LU"
let D26 = 7.5
let E26 = 1
let F26 = @if(E26>0,D26,0)
rightstring G26 = @if(E26<1,"!","")
rightstring B27 = "signals and systems 2"
rightstring C27 = "VU"
let D27 = 4
let E27 = 1
let F27 = @if(E27>0,D27,0)
rightstring G27 = @if(E27<1,"!","")
rightstring B28 = "mathematical modelling"
rightstring C28 = "VU"
let D28 = 3
let E28 = 1
let F28 = @if(E28>0,D28,0)
rightstring G28 = @if(E28<1,"!","")
rightstring B29 = "program and system verification"
rightstring C29 = "VU"
let D29 = 6
let E29 = 1
let F29 = @if(E29>0,D29,0)
rightstring G29 = @if(E29<1,"!","")
rightstring B30 = "real-time systems"
rightstring C30 = "VO"
let D30 = 2
let E30 = 1
let F30 = @if(E30>0,D30,0)
rightstring G30 = @if(E30<1,"!","")
rightstring H30 = "tally"
rightstring I30 = "achieved"
rightstring B31 = "dependable systems"
rightstring C31 = "VU"
let D31 = 3
let E31 = 1
let F31 = @if(E31>0,D31,0)
rightstring G31 = @if(E31<1,"!","")
let H31 = @sum(D24:D31)
let I31 = @sum(F24:F31)
label A32 = "5"
rightstring B32 = "microcomputers"
rightstring C32 = "VU"
let D32 = 1
let E32 = 1
let F32 = @if(E32>0,D32,0)
rightstring G32 = @if(E32<1,"!","")
rightstring B33 = "automation"
rightstring C33 = "VU"
let D33 = 4.5
let E33 = 1
let F33 = @if(E33>0,D33,0)
rightstring G33 = @if(E33<1,"!","")
rightstring B34 = "control engineering lab"
rightstring C34 = "LU"
let D34 = 2.5
let E34 = 1
let F34 = @if(E34>0,D34,0)
rightstring G34 = @if(E34<1,"!","")
rightstring B35 = "distributed automation systems"
rightstring C35 = "VO"
let D35 = 2
let E35 = 1
let F35 = @if(E35>0,D35,0)
rightstring G35 = @if(E35<1,"!","")
rightstring B36 = "distributed automation systems"
rightstring C36 = "LU"
let D36 = 4
let E36 = 1
let F36 = @if(E36>0,D36,0)
rightstring G36 = @if(E36<1,"!","")
rightstring H36 = "tally"
rightstring I36 = "achieved"
rightstring B37 = "scientific research and writing"
rightstring C37 = "SE"
let D37 = 3
let E37 = 1
let F37 = @if(E37>0,D37,0)
rightstring G37 = @if(E37<1,"!","")
let H37 = @sum(D32:D37)
let I37 = @sum(F32:F37)
label A38 = "6"
rightstring B38 = "microcomputers"
rightstring C38 = "LU"
let D38 = 2
let E38 = 1
let F38 = @if(E38>0,D38,0)
rightstring G38 = @if(E38<1,"!","")
rightstring B39 = "bachelor thesis"
rightstring C39 = "PR"
let D39 = 10
let E39 = 1
let F39 = @if(E39>0,D39,0)
rightstring G39 = @if(E39<1,"!","")
rightstring H39 = "tally"
rightstring I39 = "achieved"
rightstring B40 = "introduction to security"
rightstring C40 = "VU"
let D40 = 3
let E40 = 1
let F40 = @if(E40>0,D40,0)
rightstring G40 = @if(E40<1,"!","")
let H40 = @sum(D38:D40)
let I40 = @sum(F38:F40)
rightstring A41 = "without rec"
rightstring B41 = "distributed systems"
rightstring C41 = "VO"
let D41 = 3
let E41 = 1
let F41 = @if(E41>0,D41,0)
rightstring G41 = @if(E41<1,"!","")
rightstring H41 = "needed"
rightstring B42 = "distributed systems"
rightstring C42 = "UE"
let D42 = 3
let E42 = 1
let F42 = @if(E42>0,D42,0)
rightstring G42 = @if(E42<1,"!","")
let H42 = 180-H8-H15-H23-H31-H37-H40-18
rightstring B43 = "parallel computing"
rightstring C43 = "VU"
let D43 = 6
let E43 = 1
let F43 = @if(E43>0,D43,0)
rightstring G43 = @if(E43<1,"!","")
rightstring H43 = "tally"
rightstring I43 = "achieved"
rightstring B44 = "xx"
rightstring C44 = "XX"
let D44 = 0
let E44 = 1
let F44 = @if(E44>0,D44,0)
rightstring G44 = @if(E44<1,"!","")
let H44 = @sum(D41:D44)
let I44 = @sum(F41:F44)
label A45 = "free"
rightstring B45 = "electrical engineering revision course"
rightstring C45 = "RE"
let D45 = 2
let E45 = 1
let F45 = @if(E45>0,D45,0)
rightstring G45 = @if(E45<1,"!","")
rightstring H45 = "needed"
label A46 = "transferable"
rightstring B46 = "it projects for teens"
rightstring C46 = "PR"
let D46 = 4.5
let E46 = 1
let F46 = @if(E46>0,D46,0)
rightstring G46 = @if(E46<1,"!","")
let H46 = 180-H8-H15-H23-H31-H37-H40-H42
rightstring B47 = "quantum informatics 1"
rightstring C47 = "UE"
let D47 = 6
let E47 = 1
let F47 = @if(E47>0,D47,0)
rightstring G47 = @if(E47<1,"!","")
rightstring H47 = "tally"
rightstring I47 = "achieved"
rightstring J47 = "  total tally"
rightstring K47 = "  total achieved"
rightstring L47 = "  total left"
rightstring B48 = "good scientific practice"
rightstring C48 = "VO"
let D48 = 2
let E48 = 1
let F48 = @if(E48>0,D48,0)
rightstring G48 = @if(E48<1,"!","")
let H48 = @sum(D45:D49)
let I48 = @sum(F45:F49)
let J48 = H8+H15+H23+H31+H37+H40+H44+H48
let K48 = I8+I15+I23+I31+I37+I40+I44+I48
let L48 = J48-K48
rightstring B49 = "current trends in computer science"
rightstring C49 = "VU"
let D49 = 1.5
let E49 = 1
let F49 = @if(E49>0,D49,0)
rightstring G49 = @if(E49<1,"!","")
goto E46
movetosheet "Sheet1"
mark v "Sheet1" A1 B2
