# Protokoll Messtechnik

## 2. Simulationen

### 2.0. Parameter der Simulationen

Aufgabe|Parameter|Wert||runden auf|gerundet|
:--|:--|--:|:--|--:|--:|:--
2.1|R              | 32.57   | k$\Omega$ | E24          | 33   | k$\Omega$
2.1|C              | 9.16    | nF        | E6           | 10   | nF       
2.2|R              | 6.55    | k$\Omega$ | E24          | 6.8  | k$\Omega$
2.2|L              | 65.70   | mH        | E6           | 68   | mH       
2.3|C              | 287.81  | nF        | E6           | 270  | nF       
2.3|L              | 17.48   | mH        | E6           | 18   | mH       
2.3|$R_s$          | 1.60    | $\Omega$  | nicht runden | 1.60 | $\Omega$ 
2.3|$R_{klein}$    | 118.49  | $\Omega$  | E24          | 120  | $\Omega$ 
2.3|$R_{kritisch}$ | 438     | $\Omega$  | nicht runden | 438  | $\Omega$ 
2.3|$R_{gross}$    | 2621.63 | $\Omega$  | E24          | 2700 | $\Omega$ 

### 2.1. Simulation des Verhaltens eines RC-Filters 1. Ordnung

#### 2.1.1. RC-Tiefpass

Filter 1. Ordnung

Charakteristische Werte des Filters sind die Zeitkonstante $\tau$ und die Grenzfrequenz $\omega_0$:

$$ \tau = RC = 33 k \Omega \cdot 10 nF = 330 \mu \frac{m^2 kg}{s^3 A^2} \cdot \frac{s^4 A^2}{m^2 kg} = 330 \mu s $$

$$ \omega_0 = \frac{1}{RC} = \frac{1}{\tau} = 3030.303 Hz $$

##### Schaltkreis

des RC-Filters 1. Ordnung

![RC-Filter 1. Ordnung](./res/2-1-circuit.png)

##### Simulation

der Sprungantwort des abgebildeten RC-Filters 1. Ordnung, von Zeitpunkt $t_0 = 0$, bis $5 \tau = 1650 \mu s$.

![Sprungantwort](./res/2-1-jump-response.png)

##### Simulation

der Sprungantwort zweier periodischen Rechteckssignale, wobei die Signale jeweils 5 Zyklen haben und in $1 pS$ umschalten. In Fig.3 hat das Signal eine Periode $T_{period}$ von $10 \cdot \tau$ und eine on-time $T_{on}$ von $5 \cdot \tau$. In Fig.4 hingegen hat das Signal eine Periode $T_{period}$ von $\tau$ und eine on-time $T_{on}$ von $0.5 \cdot \tau$.

![Rechteckssignal 1](./res/2-1-square-1.png)

![Rechteckssignal 2](./res/2-1-square-2.png)

Eine mögliche Erklärung könnte sein, dass bei Fig.4 das Signal sich in kürzer getakteten Abständen als bei Fig.3 ändert, was dazu führt, dass die höherfrequentigen Anteile des Eingangsignals in Fig.4 eine größere Amplitude haben, als in Fig.3. Ein Tiefpassfilter lässt diese höherfrequentigen Anteile weniger gut durch. Während bei Fig.3 die Zeit ausreicht, um das Ausgangssignal von der Amplitude an das Eingangssignal anzupassen, ist das bei Fig.4 nicht mehr der Fall. Das Ausgangssignal fluktuiert in Folge bei Fig.4 nicht annähernd so stark.

##### Ein Bode Diagramm

eines angelegten sinusförmigen Eingangssignals mit einer Spannung $1 V_{PP}$ findet sich in Fig.5. Eingezeichnet ist auch die 3dB-Grenzfrequenz an welcher der Phasengang 45° beträgt.

![Bode Diagramm](./res/2-1-bode-sine.png)

Für die Erstellung dieses Bode Diagramms wurde die AC Analysis Simulationsfunktion von LTSpice verwendet, um die vielen Frequenzen zu testen. Interessanterweise liegt hier die Grenzfrequenz bei 484 Hz, im Vergleich zu den berechneten 3030.303 Hz. Die gewählte Einstellung der AC Analyse war aber "Octave", was vermutlich einen Effekt gehabt haben wird.

##### Ergebnisse

der Simulationen

Die Simulationsergebnisse lassen darauf schließem, dass durch die langsame Änderungsrate des Ausgangssignals im Verhältnis zum Eingangsignal ein Tiefpassfilter vorliegt. Ein Blick auf das Bode Diagramm offenbart, dass es sich tatsächlich um einen Tiefpassfilter handeln muss, da nur die hohen Frequenzanteile abfallen.

#### 2.1.2. RC-Hochpass

Filter 1. Ordnung

Analog zum Tiefpass werden in derselben Reihenfolge dieselben Simulationen durchgeführt.

##### Schaltkreis

des RC-Filters 1. Ordnung

![RC-Filter 1. Ordnung](./res/2-1-hp-circuit.png)

##### Simulation

der Sprungantwort des abgebildeten RC-Filters 1. Ordnung, von Zeitpunkt $t_0 = 0$, bis $5 \tau = 1650 \mu s$. Ablesbar in Fig.7

![Sprungantwort](./res/2-1-hp-jump-response.png)

##### Simulation

der Sprungantwort zweier periodischen Rechteckssignale. Die Parameter sind identisch zu den Tiefpasssimulationen.

![Rechteckssignal 1](./res/2-1-hp-square-1.png)

![Rechteckssignal 2](./res/2-1-hp-square-2.png)

Spannend ist, dass bei höheren tieffrequentigen Signalanteilen das Ausgangssignal zu 0V abklingt. So ist in Fig.8 zu sehen, dass bevor das Eingangssignal umtakten kann, das Ausgangssignal bereits abgeklungen ist. Anders ist es bei Fig.9, wo das Signal schnell genug taktet, um das Signal gar nicht abklingen zu lassen.

\pagebreak

##### Ein Bode Diagramm

eines angelegten sinusförmigen Eingangssignals mit einer Spannung $1 V_{PP}$ findet sich in Fig.10. Eingezeichnet ist auch die 3dB-Grenzfrequenz an welcher der Phasengang 45° beträgt. Auch hier wurde analog zur Simulation des Tiefpassfilters vorgegangen.

![Bode Diagramm](./res/2-1-hp-bode-sine.png)

Der Phasengang ist identisch zu dem Tiefpassgegenstück. Lediglich die "Richtung" des Amplitudengangs ist verkehrt, sodass niedrigere Frequenzen vernachlässigt werden, aber nicht höhere.

##### Ergebnisse

der Simulationen

Es handelt sich bei dieser zweiten Ausführung um einen Hochpassfilter.

### 2.2. Simulation des Verhaltens eines RL-Filters 1. Ordnung

#### 2.2.1. RL-Hochpass

Filter 1. Ordnung

Charakteristische Werte des Filters sind die Zeitkonstante $\tau$ und die Grenzfrequenz $\omega_0$:

$$ \omega_0 = RL = 6.8 k \Omega \cdot 68 mH = 462.4 \frac{m^2 kg}{s^3 A^2} \cdot \frac{s^2 A^2}{m^2 kg} = 462.2 Hz $$

$$ \tau = \frac{1}{RL} = \frac{1}{\omega_0} = 2.163 ms $$

##### Schaltkreis

des RL-Filters 1. Ordnung, abgebildet in Fig.11

![RL-Hochpassfilter 1. Ordnung](./res/2-2-hp-circuit.png)

##### Simulation

der Sprungantwort des abgebildeten RL-Filters 1. Ordnung, von Zeitpunkt $t_0 = 0$, bis $5 \tau = 10.815 ms$, abgebildet in Fig.12.

![Sprungantwort](./res/2-2-hp-jump-response.png)

##### Simulation

der Sprungantwort zweier periodischen Rechteckssignale, wobei die Signale jeweils 5 Zyklen haben und in $1 pS$ umschalten. In Fig.13 hat das Signal eine Periode $T_{period}$ von $10 \cdot \tau$ und eine on-time $T_{on}$ von $5 \cdot \tau$. In Fig.14 hingegen hat das Signal eine Periode $T_{period}$ von $\tau$ und eine on-time $T_{on}$ von $0.5 \cdot \tau$.

![Rechteckssignal 1](./res/2-2-hp-square-1.png)

![Rechteckssignal 2](./res/2-2-hp-square-2.png)

Der markante Unterschied ist, dass das Ausgangssignal aus Fig.13 so schnell abklingt, dass sich das kaum beobachten lässt, während das Ausgangssignal aus Fig.14 eine gewisse (auch wenn kurze) Zeit zum abklingen braucht. Der Unterschied könnte erklärt werden dadurch, dass bei dem Fall der sich bei Fig.13 ergibt eine Periode länger dauert, und somit durch die fehlenden tieffrequentigen Anteile beim Amplitudengang dafür sorgen, dass das Ausgangssignal fast zur Gänze 0V bleibt. Beim Fall der sich bei Fig.14 ergibt hingegen, ist eine Periode kürzer, womit die hochfrequentigen Anteile signifikanter werden und das Ausgangssignal weniger schnell abklingt.

##### Ein Bode Diagramm

eines angelegten sinusförmigen Eingangssignals mit einer Spannung $1 V_{PP}$ findet sich in Fig.15. Eingezeichnet ist auch die 3dB-Grenzfrequenz an welcher der Phasengang 45° beträgt, welche sich bei ungefähr 16 kHz befindet. Auch hier wurde wie bisher vorgegangen.

![Bode Diagramm](./res/2-2-hp-bode-sine.png)

##### Simulation

mit Rücksichtnahme eines parasitären Innenwiderstandes der Spule. In Fig.16 werden die beiden Sprungantworten gegenüber gestellt, während in Fig.17 die resultierenden Bode Diagramme gegenüber gestellt werden. Bei Fig.16 und Fig.17 stellt das Diagramm links die Simulation mit idealer Spule dar, während das Diagramm rechts eine Spule mit einem parasitären Innenwiderstand $R_{L_{par}}$ in Höhe von $8.5 \Omega$ simuliert.

![Vergleich Sprungantwort, $R_{L_{par}} = 0$ links, $R_{L_{par}} = 8.5 \Omega$ rechts](./res/2-2-hp-jump-response-parasitic.png)

![Vergleich Bode Diagramm, $R_{L_{par}} = 0$ links, $R_{L_{par}} = 8.5 \Omega$ rechts](./res/2-2-hp-bode-comparison.png)

Die dargestellte Sprungantwort ist das Ausgangssignal $V(n002)$ für das Eingangssignal $V(n001)$, welches einen einmaligen Sprung von 0V zu 1V darstellt. Bei der Sprungantwort einen Unterschied zu erkennen ist für die gewählte Simulationsdauer für diese Parameter nahezu unmöglich, glücklicherweise veranschaulichen die Bode Diagramme die Unterschiede. Zum einen verhält sich der Filter, welcher eigentlich tiefe Frequenzanteile abschwächen soll, nicht ganz linear in einem Bereich der niedrigen Frequenz $f = 10 Hz$ für die Schaltung mit parasitärem Innenwiderstand in Höhe von $8.5 \Omega$ (rechts). Somit flacht die Kurve ab in der Umgebung dieser Frequenz. Das ist definitiv nicht erwünscht, weil ganz niedrige Frequenzen zu einem gewissen Anteil dennoch durchgelassen werden. Bei der Schaltung mit idealer Spule (links), tritt dieser Effekt nicht auf.

Ein sehr offensichtlicher Unterschied ist auch, das der Phasengang bei der realen Schaltung (rechts) zu Beginn steigt bis es ein Maximum erreicht, bevor der Phasengang zu 0° tendiert. Bei der idealen Schaltung (links) lässt sich das nicht observieren.

Interessant ist auch, dass die (mit Cursor eingezeichnete) 3dB-Grenzfrequenz in beiden Fällen ungefähr 16 kHz beträgt.

#### 2.2.2. RL-Tiefpass

Filter 1. Ordnung

Analog zum Hochpass werden in derselben Reihenfolge dieselben Simulationen durchgeführt.

##### Schaltkreis

des RC-Filters 1. Ordnung, abgebildet in Fig.18

![RL-Tiefpassfilter 1. Ordnung](./res/2-2-tp-circuit.png)

##### Simulation

der Sprungantwort des abgebildeten RL-Filters 1. Ordnung, von Zeitpunkt $t_0 = 0$, bis $5 \tau = 10.815 ms$, abgebildet in Fig.19.

![Sprungantwort](./res/2-2-tp-jump-response.png)

##### Simulation

der Sprungantwort zweier periodischen Rechteckssignale, wobei die Signale jeweils 5 Zyklen haben und in $1 pS$ umschalten. In Fig.20 hat das Signal eine Periode $T_{period}$ von $10 \cdot \tau$ und eine on-time $T_{on}$ von $5 \cdot \tau$. In Fig.21 hingegen hat das Signal eine Periode $T_{period}$ von $\tau$ und eine on-time $T_{on}$ von $0.5 \cdot \tau$.

![Rechteckssignal 1](./res/2-2-tp-square-1.png)

![Rechteckssignal 2](./res/2-2-tp-square-2.png)

Die Erklärung kann hier analog zum Tiefpass aus 2.1.1. geführt werden. Der Effekt ist zwar schwer wahrnehmbar, weil die Werte unpassend für eine Veranschaulichung sind, aber das Ausgangssignal braucht eine Weile um die Amplitude des Eingangssignals zu erreichen. Dass das Ausganssignal nicht so spontan umschalten kann, deutet darauf, dass die Amplitudenanteile der hohen Frequenzen geschwächt werden, während die Amplitudenanteile der tiefen Frequenzen normal durchkommen. Das ist ein Indiz für einen Tiefpassfilter.

##### Ein Bode Diagramm

eines angelegten sinusförmigen Eingangssignals mit einer Spannung $1 V_{PP}$ findet sich in Fig.22. Eingezeichnet ist auch die 3dB-Grenzfrequenz an welcher der Phasengang 45° beträgt, welche sich bei 15.902 kHz befindet. Auch hier wurde wie bisher vorgegangen.

![Bode Diagramm](./res/2-2-tp-bode-sine.png)

### 2.3. Simulation eines dynamischen Systems 2. Ordnung

Wir berechnen zu Beginn die *stop time* als $t = 20 \cdot \sqrt{L \cdot C}$:

$$ t = 20 \cdot \sqrt{L \cdot C} = 20 \cdot \sqrt{18 H \cdot 270 nF} = 20 \cdot \sqrt{4860 n \cdot \frac{kg \cdot m^2}{s^2 A^2} \frac{s^4 A^2}{kg \cdot m^2}} = 20 \cdot \sqrt{4860 ns^2} $$

$$ t = 44.091 ms $$

Als nächstes simulieren wir unseren Schaltkreis, wobei wir zwischen drei möglichen Werten für den Widerstand $R$ unterscheiden:

Widerstand|Wert|
:--|--:|:--
$R_{klein}$    | 120  | $\Omega$
$R_{kritisch}$ | 438  | $\Omega$
$R_{gross}$    | 2700 | $\Omega$

##### Schaltkreis

des RLC-Systems 2. Ordnung, abgebildet in Fig.23.

![Schaltkreis](./res/2-3-circuit.png)

##### Simulation

des RLC-Systems 2. Ordnung, abgebildet in Fig.24. Die drei abgebildeten Spannungsverläufe repräsentieren das RLC-System mit jeweiligem Widerstandswert. Die Korrespondenz ist dabei:

Widerstand|$V(x)$
:--|:-:
$R_{klein}$    |$V(n003)$
$R_{kritisch}$ |$V(n006)$
$R_{gross}$    |$V(n009)$

![Sprungantwort für RLC-System mit $R_i$](./res/2-3-combined.png)

Die Sprungantwort ist die Antwort des RLC-Systems eines angelegten Eingangssignal welches zu $t = 0$ von 0V auf 1V springt. Um die Sprungantwort anschaulicher zu machen, finden sich in Fig.25 dieselben Spannungsverläufe abgebildet, nur ist das Eingangssignal zusätzlich abgebildet.

![Sprungantwort für RLC-System mit $R_i$ inkl. Eingangssignale](./res/2-3-combined-with-v.png)

Es kommt im Zeitbereich zu einem Überschwingen, bei einem Versuch das Ausgangssignal an eine Eingangsspannung von 1V anzugleichen, dann zu einem Unterschwingen, usw. ad infinitum. Dabei ist jede konsekutive Schwingung gedämpft, sodass es sich für das Ausgangssignal insgesamt um eine gedämpfte Schwingung handelt. Bei der Systemfunktion $H(s)$ liegen also 2 konjugiert komplexe Pole vor. Die Vermutung, dass das System sich nach ausreichender Zeit fast zur Gänze eingeschwungen hat, liegt daher besonders nahe. In Fig.26 wird dasselbe, was schon in Fig.25 zu sehen war abgebildet, aber mit dem Unterschied, dass die Simulation dieses Mal 1s lang ist.

![Sprungantwort für RLC-System mit $R_i$ inkl. Eingangssignale, $t = 1s$](./res/2-3-combined-descent.png)

Tatsächlich gilt für alle $R_i$, dass spätestens nach 1s auch das am schwächsten gedämpfte System fast zur Gänze eingeschwungen ist. Wie stark ein System dabei gedämpft wird ließe sich auch intuitiv bereits richtig beschreiben: größere Widerstandswerte $R_i$ führen zu stärkerer Dämpfung.

##### Bode Diagramme

des RLC-Systems 2. Ordnung, abgebildet in Fig.27.

![Bodediagramme für RLC-System mit $R_i$](./res/2-3-bode-combined.png)

Die Konstellation der Systeme mit jeweiligen $R_i$ ist gleich wie bei der letzte Simulation. Markiert ist die Stelle, an der die Amplitudengänge jeweils 3dB betragen, daher auch die 3dB-Grenzfrequenz. Interessant ist es hier, die "Buckel" der Amplitudengänge zu vergleichen. Es fällt auf, dass das System, welches im Zeitbereich als um schwächsten gedämpfte System aufgefallen ist, den größten "Buckel" aufweist.

Ganz elementar ist auch die Beobachtung, dass hohe Frequenzen attenuiert werden, während niedrige Frequenzanteile unblockiert dass System passieren. Mit der Ausnahme der Frequenzen in der Umgebung des "Buckels", die sogar beachtlich verstärkt werden. Es handelt sich um einen Tiefpassfilter. 

Der Buckel ist nicht wünschenswert, weil dadurch Frequenzen die wir womöglich bereits blockieren wollen sogar verstärkt werden. Auch die Phasengänge sind abgesehen von ihrer Steilheit ident. Es gibt also keinen Grund, die Systeme mit niedrigen $R_i$ gegenüber dem System mit $R_{gross}$ zu bevorzugen.
