# Protokoll Messtechnik

### 3.1 Norm-Reihe

Für die personenbezogenen Widerstandswerte sollen im Rahmen dieser Übung entsprechend der E24 Norm-Reihe normierte Werte verwendet werden. Der folgenden Tabelle können die normierten Werte der E24 Reihe übernommen werden. Wichtig zu bemerken ist, dass alle Werte dieser Reihe eine 5% Toleranz aufweisen. Dadurch ergibt sich außerdem, dass alle Widerstände, welche im weiteren Verlauf zu verwenden sind, in ihrem Farbcode immer mit Gold enden werden.

Die Wertefolgen der jeweiligen Norm-Reihen sind von folgender Quelle bezogen worden: *[https://en.wikipedia.org/wiki/E_series_of_preferred_numbers (link)](https://en.wikipedia.org/wiki/E_series_of_preferred_numbers)*

|||||||||||||
|---|---|---|---|---|---|---|---|---|---|---|---|
|1.0|1.1|1.2|1.3|1.5|1.6|1.8|2.0|2.2|2.4|2.7|3.0|
|3.3|3.6|3.9|4.3|4.7|5.1|5.6|6.2|6.8|7.5|8.2|9.1|
Table: E24 Norm-Reihe Wertefolge

$R_i$ ... personenbezogener Widerstandswert mit Index i

$R_E$ ... personenbezogener Widerstandswert mit Index i entsprechend E24 normiert

|Aufgabe|$i$|$R_i$|$R_{iE}$|Einheit|Farbcode|
|:----|:--|------:|:------|:-----------|---|
| 3.3 | 1 | 18.79 | 18    | $\Omega$   | Braun, Grau, Schwarz, Gold |
| 3.3 | 2 | 28.81 | 27    | k$\Omega$  | Rot, Violett, Orange, Gold |
| 3.3 | 3 | 1.98  | 2     | M$\Omega$  | Rot, Schwarz, Grün, Gold |
| 3.4 | 4 | 48.35 | 47    | k$\Omega$  | Gelb, Violett, Orange, Gold |
| 3.4 | 5 | 11.18 | 11    | k$\Omega$  | Braun, Braun, Orange, Gold |
| 3.4 | 6 | 10.34 | 10    | k$\Omega$  | Braun, Schwarz, Orange, Gold |
| 3.5 | 1 | 14.15 | 15    | k$\Omega$  | Braun, Grün, Orange, Gold |
| 3.5 | 2 | 50.00 | 51    | k$\Omega$  | Grün, Braun, Orange, Gold |
| 3.5 | 3 | 24.75 | 24    | k$\Omega$  | Rot, Gelb, Orange, Gold |
| 3.5 | 4 | 22.53 | 22    | k$\Omega$  | Rot, Rot, Orange, Gold |
| 3.5 | 5 | 19.74 | 20    | k$\Omega$  | Rot, Schwarz, Orange, Gold |
| 3.5 | x | 45.55 | 47    | k$\Omega$  | Gelb, Violett, Orange, Gold |
| 3.7 | 1 | 2.87  | 2.7   | k$\Omega$  | Rot, Violett, Rot, Gold |
Table: E24 normierte Widerstandswerte

|||||||
|---|---|---|---|---|---|
|1.0|1.5|2.2|3.3|4.7|6.8|
Table: E6 Norm-Reihe Wertefolge

$C$ ... personenbezogener Kondensatorwert

$C_E$ ... personenbezogener Kondensatorwert entsprechend E6 normiert

|Aufgabe|$C$|$C_{E}$|Einheit|
|:--|--:|:--|:--|
|3.7|8.77|6.8|µF|
Table: E6 normierte Kondensatorwerte

### 3.2 Innenwiderstand der Messgeräte

#### 3.2.1 Amperemeter

Vor der Innenwiderstandsmessung kümmern wir uns darum, innerhalb akzeptierbarer Leistungsgrenzen zu bleiben. Dafür benötigen wir einen Vorwiderstand $R_V$ für welchen gilt:

$$ P_{total} < \frac{1}{4} W $$

$$ P_{total} = V_{total} I_{total} = \frac{V_{total}^2}{R_V} $$

Hier wurde das Ohm'sche Gesetz angewendet. Um herauszufinden, welche Widerstandsgröße wir benutzen sollen, lösen wir die konkrete Ungleichung für eine anliegende Spannung $V_{total}$ in Höhe von 1.5V:

$$ \frac{1}{4} > \frac{1.5^2}{R_V} \implies x < 4 \cdot 1.5V^2 \implies R_V > 9 \Omega $$

Für einen passenden Vorwiderstand benötigen wir also mindestens 9 $\Omega$ Widerstand. Für Einfachheit wurden hier 15 $\Omega$ für $R_V$ gewählt.

Mit diesem Vorwiderstand ist ein Strom in Höhe von $\frac{1.5}{15} = 0.1A = 100 mA$ zu erwarten. Unsere Messwerte werden sich daher im Messbereich `0 bis 400mA` (mA) befinden.

![schematics](./res/3-2-amperemeter-schema.png)
![measurements](./res/3-2-amperemeter-measurements.png)

Mit dem illustrierten Schaltkreis ergeben sich folgende Messwerte:

$$ \frac{315.79 mV}{78.95 mA} = 4 \Omega $$

Wir inferieren also mithilfe des Ohm\'schen Gesetz\' ($R = \frac{V}{A}$), dass unser Amperemeterinnenwiderstand 4 $\Omega$ betragen muss.

#### 3.2.2 Voltmeter

Analog zur Amperemetermessung wählen wir ein Amperemeter in einem akzeptablen Messbereich, diesmalig ein Amperemeter des Messbereiches `0 bis 10mA` ($\mu$A). Hier ist wichtig, dass Voltmeter immer parallel zu einer Schaltung geschalten werden, weil sonst aufgrund des normalerweise sehr hohen Innenwiderstandes der Schaltkreis beeinflusst wird. 

![schematics](./res/3-2-voltmeter-schema.png)
![measurements](./res/3-2-voltmeter-measurements.png)

Mit dem illustrierten Schaltkreis ergeben sich folgende Messwerte:

$$ \frac{1.5 V}{149.84 \cdot 10^{-6} A} = R_V = 10^7 \Omega $$

Das erste was uns auffällt, ist dass unser Voltmeter kein ideales Voltmeter ist, da ansonsten der interne Widerstand $\inf$ wäre und sich kein Stromteiler ergeben würde. Wir können dieses Ergebnis auch verifizieren, indem wir einen Ersatzwiderstand $R_{ers} = \frac{R_2 \cdot R_V}{R_2 + R_V}$ definieren. Am Knoten `a1` beträgt der Strom $1.65 \mu A$ und dessen Spannung zu Knoten `a2` (welches der Masse entspricht) $1.498 V$. Es folgen dann die Berechnungen:

$$ R_{ers} = \frac{1.498 V}{1.65 \cdot 10^{-6} A} = 909090.91 \Omega $$
$$ R_{ers} = \frac{10^6 \cdot R_V}{10^6 + R_V} \implies R_V = \frac{10^6 \cdot R_{ers}}{10^6 - R_{ers}} = 10^7 \Omega = 10 M \Omega $$

#### 3.2.3 Auswertung

Durch Variation der individuellen Werte und Amperemeter ergibt sich die folgende Auswertung:

Messgerät|Innenwiderstand
:--|--:
Voltmeter|$10 M \Omega$
Amperemeter|$0.1 \Omega$
Milliamperemeter|$4 \Omega$
Microamperemeter|$1 k \Omega$
Table: Innenwiderstandsauswertung

### 3.3 Belastungsfehler

##### 3.3.1 Schema

Die für die Erledigung verwendeten Schaltkreise für eine spannungsrichtige (links) oder stromrichtige (rechts) Messung werden hier angeführt.

![spannungsrichtig](./res/3-3-spannungsrichtig.png)
![stromrichtig](./res/3-3-stromrichtig.png)

##### 3.3.2 Berechnungen

Für die Richtigstellung der Messwerte sind Korrekturen notwendig. Abhängig davon, ob eine *spannungsrichtige* oder eine *stromrichtige* Schaltung vorliegt, wird entweder der Strom oder die Spannung korrigiert. Es ergeben sich folgende Formeln:

$$ R_{xU (korrigiert)} = \frac{U_v}{I_A - \frac{U_V}{R_V}} $$
$$ R_{xI (korrigiert)} = \frac{U_v - I_A R_A}{I_A} $$

Der relative Fehler wird analog wie folgt berechnet:

$$ F_r = \frac{R_{mY} - R_x}{R_x}, Y \in \{ U, I \} $$

##### 3.3.3 Auswertung

Legende und Tabellen

$R_n$ ... personenbezogener Widerstand entsprechend E24 normiert

$X_{mY}$ ... Wert X in Messung aus $Y$-richtigem Schaltkreis

$X_{xY (korrigiert)}$ ... Wert X in Messung aus $Y$-richtigem Schaltkreis korrigiert

|$R_n$|$I_{mU}$|$U_{mU}$|$I_{mI}$|$U_{mI}$|$U_{total}$|Messbereich
|:-:|:-:|:-:|:-:|:-:|:-:|--:|
|$\Omega$|$A$|$V$|$A$|$V$|$V$|
| $18$   | $1.473 m$     | $26.522 m$ | $82.873 m$    | $1.5$ | 1.5 | `0 bis 10A` (A)       |
| $18$   | $68.182 m$    | $1.227$    | $68.182 m$    | $1.5$ | 1.5 | `0 bis 400mA` (mA)    |
| $18$   | $82.873 m$    | $1.492$    | $1.473 m$     | $1.5$ | 1.5 | `0 bis 10mA` ($\mu$A) |
| $27 k$ | $334.232 \mu$ | $9$        | $333.332 \mu$ | $9$   | 9   | `0 bis 10A` (A)       |
| $27 k$ | $334.184 \mu$ | $8.998$    | $333.284 \mu$ | $9$   | 9   | `0 bis 400mA` (mA)    |
| $27 k$ | $322.265 \mu$ | $8.678$    | $321.429 \mu$ | $9$   | 9   | `0 bis 10mA` ($\mu$A) |
| $2  M$ | $5.4 \mu$     | $9$        | $4.5 \mu$     | $9$   | 9   | `0 bis 10A` (A)       |
| $2  M$ | $5.4 \mu$     | $9$        | $4.5 \mu$     | $9$   | 9   | `0 bis 400mA` (mA)    |
| $2  M$ | $5.397 \mu$   | $8.995$    | $4.498 \mu$   | $9$   | 9   | `0 bis 10mA` ($\mu$A) |
Table: Messwerte

Wobei für die jeweiligen personenbezogenen Widerstände $R_n$ folgender Messbereich verwendet wurde:

$R_n$|Messbereich
:--|--:
18 $\Omega$  | `0 bis 400mA` (mA)
27 k$\Omega$ | `0 bis 10mA` ($\mu$A)
2  M$\Omega$ | `0 bis 10mA` ($\mu$A)
Table: verwendete Messbereiche

|$R_n$|$R_A$|$R_V$|$R_{mU}$|$R_{mI}$|$R_{xU (korrigiert)}$|$R_{xI (korrigiert)}$|$F_{rU}$|$F_{rI}$|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|$\Omega$|$\Omega$|$\Omega$|$\Omega$|$\Omega$|$\Omega$|$\Omega$|%|%|
| $18$   | $0.1$ | $10 M$ | $18.005$   | $18.1$    | $18.005$   | $18$    | $0.028$  | $0.03$    |
| $18$   | $4$   | $10 M$ | $17.996$   | $22$      | $17.996$   | $18$    | $-0.022$ | $22.22$   |
| $18$   | $1 k$ | $10 M$ | $18.003$   | $1018.33$ | $18.003$   | $18.33$ | $0.017$  | $5557.39$ |
| $27 k$ | $0.1$ | $10 M$ | $26.927 k$ | $27$      | $27 k$     | $27 k$  | $-0.27$  | $0$       |
| $27 k$ | $4$   | $10 M$ | $26.925 k$ | $27.004$  | $26.998 k$ | $27 k$  | $-0.28$  | $0.01$    |
| $27 k$ | $1 k$ | $10 M$ | $26.928 k$ | $28$      | $27.001 k$ | $27 k$  | $-0.27$  | $3.7$     |
| $2 M$  | $0.1$ | $10 M$ | $1.667$    | $2 M$     | $2 M$      | $2 M$   | $-16.65$ | $0$       |
| $2 M$  | $4$   | $10 M$ | $1.667$    | $2 M$     | $2 M$      | $2 M$   | $-16.65$ | $0$       |
| $2 M$  | $1 k$ | $10 M$ | $1.667$    | $2.001 M$ | $2 M$      | $2 M$   | $-16.65$ | $0.05$    |
Table: Auswertung

##### 3.3.4 Diskussion

Bei beiden Szenarien verwenden wir reale Messgeräte bei denen ein gewisser Innenwiderstand in solch einer Messung nicht vernachlässigt werden kann. Durch die Innenwiderstände leisten unsere Messgeräte Arbeit und beeinflussen den Schaltkreis, den sie messen sollten. Amperemeter werden in Serie geschalten um den Strom zu messen, daher wird üblicherweise der Innenwiderstand so gering wie möglich gehalten. Voltmeter hingegen werden parallel geschalten um die Spannung zu messen, daher wird üblicherweise der Innenwiderstand so hoch wie möglich gewählt.

Bei einer spannungsrichtigen Schaltung wird die Spannung des Widerstandes korrekt gemessen, aber der Strom fällt durch den Innenwiderstand des Voltmeters ab und gehört somit rektifiziert. Das Voltmeter bildet nämlich einen Stromteiler.

Bei einer stromrichtigen Schaltung hingegen wird der Strom durch den Widerstand korrekt gemessen. Durch das Amperemeter aber, welches in Serie mit dem zu messenden Widerstand geschalten ist, fällt die Spannung ab und gehört somit rektifiziert.

Wie sich anhand der Ergebnisse observieren lässt, war in keinem der drei Experimente die Wahl des Amperemeters im Messbereich `0 bis 10A` (A) günstig. In diesem Fall beeinflusst der hohe interne Widerstand unsere Messungen. Da die gemessenen Ströme bereits durch andere Messbereiche abgedeckt werden und diese geringerwertige Innenwiderstände besitzen, wären die kleineren Messbereiche vorteilshafter.

Eine Besonderheit ergibt sich bei dem Widerstand in Höhe $18 \Omega$. Hätten wir hier unsere übliche Spannung in Höhe von $9 V$ gewählt, wären wir weit über die maximale Leistung des Widerstandes gekommen, daher haben wurde im Zuge des Experimentes eine Spannung in Höhe von $1.5 V$ gewählt.

##### Hinweis

Zur Veranschaulichung der Fehler wurde für diese Aufgabe eine Rundung auf 3 Dezimalstellen gewählt. In jedem Zwischenschritt wurde mit gerundeten Werten gearbeitet.

### 3.4 Spannungs- und Stromteiler

##### Widerstandswahl

Für diese Übung werden personenbezogene Widerstandswerte aus *Table 2* entnommen. Da hier mit einer Spannung in Höhe von $10 V$ zu arbeiten ist, werden die respektiven Widerstandswerte $47 \Omega$, $11 \Omega$ und $10 \Omega$ aufgrund thermischer Belastung nicht ausreichen, sehr wohl aber $47 k \Omega$, $11 k \Omega$ und $10 k \Omega$. Ein Beispiel dieser zu hohen thermischen Belastung wäre $P_{R_1} = \frac{U_{R_1}^2}{R_1} = \frac{10^2}{47} 2.13 W > \frac{1}{4} W$. Alle Widerstände der zweiten Variante hingegen überschreiten ihre maximale Leistung nicht:

$$ P_{R_1} = \frac{U_{R_1}^2}{R_1} = \frac{10^2}{47 \cdot 10^3} = 2.13 mW < \frac{1}{4} W $$
$$ P_{R_2} = \frac{U_{R_2}^2}{R_2} = \frac{10^2}{11 \cdot 10^3} = 9.1 mW < \frac{1}{4} W $$
$$ P_{R_3} = \frac{U_{R_3}^2}{R_3} = \frac{10^2}{10 \cdot 10^3} = 10 mW < \frac{1}{4} W $$

Wobei $R_2$ und $R_3$ durch den Spannungsabfall an $R_1$ den $10 V$ nicht ausgesetzt sein werden, aber hiermit sind wir auf der sicheren Seite. Den Aufbau der Schaltung betrachte man im Abschnitt *Simulation*.

##### Simulation

Das verwendete Schaltbild sowie die simulierten Werte für diese Aufgabe werden hier abgebildet:

![](./res/3-4-divider.png)
![](./res/3-4-measurements.png)

##### Auswertung

Für die Berechnung individueller Spannungen bietet sich in diesem Beispiel besonders die Spannungsteilerregel an. Analog zur Berechnung der Ströme die Stromteilerregel.

Um die Berechnungen durchzuführen, fassen wir alle Widerstände in einen Ersatzwiderstand $R_{ers}$ zusammen. $R_2$ und $R_3$ werden zueinander parallel geschalten, $R_1$ aber zu diesen beiden Widerständen in Serie. Daraus ergibt sich folgender Ersatzwiderstand:

$$ R_{ers} = R_1 + R_{2,3} = R_1 + \frac{R_2 \cdot R_3}{R_2 + R_3} $$ 

Da über nur über $R_1$ und $R_{2,3}$ jeweils ein Spannungsabfall stattfindet, müssen wir nur die Spannung an Knoten `u1` berechnen. Mithilfe der Spannungsteilerregel (Verhältnis der Spannungen entspricht dem Verhältnis der Widerstände) wird folgende Rechung aufgestellt:

$$ \frac{U_1}{U_{total}} = \frac{R_1}{R_{ers}} \implies U_1 = U_{total} \cdot \frac{R_1}{R_{ers}} = 8.997 V $$

Es ergibt sich ein Spannungsabfall in Höhe von $8.997 V$. Die Spannung die also an Knoten `u1` anliegt, ergibt sich mit einem Weg von `u1` zur Masse entgegen der Stromrichtung durch die Gleichung $u_1 = -U_1 + 10 V = 1.003 V$. Die Spannungen über $R_2$ und $R_3$ zur Masse sind dann trivialerweise $U_2 = U_3 = 1.003 V$.

Für die Berrechnung der Ströme wird die Stromteilerregel (Verhältnis der Ströme entspricht invers dem Verhältnis der Spannungen) herangezogen und folgende Rechnungen aufgestellt:

$$ I_1 = I_{total} = \frac{U_{total}}{R_{ers}} = 191.43 \mu \Omega $$
$$ \frac{I_2}{I_{total}} = \frac{R_{ers}}{R_2} \implies I_2 = I_{total} \cdot \frac{R_{ers}}{R_2} = 91 \mu A $$
$$ \frac{I_3}{I_{total}} = \frac{R_{ers}}{R_3} \implies I_3 = I_{total} \cdot \frac{R_{ers}}{R_3} = 100 \mu A $$

Der Strom $I_1$ der durch Widerstand $R_1$ fließt ist nicht Teil eines Stromteilers und es gilt somit $I_1 = I_{total} = 191.43 \mu \Omega$.

Die Simulationsergebnisse entsprechen den theoretischen Erwartungen, weil idealisierte Messungen stattfinden.

### 3.5 Superpositionsprinzip

##### Schema

der Simulation

![schema](./res/3-5-schema.png)
![measurements](./res/3-5-measurements.png)

##### Auswertung

der Parameter, Messungen und Ergebnisse

$R_1$ | $R_2$ | $R_3$ | $R_4$ | $R_5$ | $R_x$ | $U_1$ | $U_2$
:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:
k$\Omega$ | k$\Omega$ | k$\Omega$ | k$\Omega$ | k$\Omega$ | k$\Omega$ | V | V
15 | 51 | 24 | 22 | 20 | 47 | 10 | 10
Table: Simulationsparameter

$R_x$ | $I_x$ | $U_x$ | $I_{x |_{U_1 = 0}}$ | $U_{x |_{U_1 = 0}}$ | $I_{x |_{U_2 = 0}}$ | $U_{x |_{U_2 = 0}}$
:-:|:-:|:-:|:-:|:-:|:-:|:-:
k$\Omega$ | $\mu$A | V | $\mu$A | V | $\mu$A | V
47 | 105.77 | 4.95 | 73.74 | 3.45 | 32.02 | 1.5
Table: Messungen

Durch Superposition ergeben sich folgende Berechnungen:

$$ U_{x |_{U_1 = 0}} + U_{x |_{U_2 = 0}} = U_x $$
$$ I_{x |_{U_1 = 0}} + I_{x |_{U_2 = 0}} = I_x $$
$$ 3.45 V + 1.5 V = 4.95 V $$
$$ 73.74 \mu A + 32.02 \mu A = 105.76 \mu A $$

Die durch Superposition zusammenaddierten Messwerte stimmen mit der ursprünglichen Messung überein!

### 3.6 Erste Messungen mit dem Oszilloskop

##### Parameter und Schema

der Simulation

Parameter|Wert|Einheit
:--|--:|:--
Frequenz        |  339000 | Hz
$U_{peak}-Peak$ |  1.50   | V
$U_{offset}$    | -1.40   | V
Phase           |  10     | deg
Duty Cycle 1    |  55     | %
Duty Cycle 2    |  10     | %
Periode         |  1100   | ms
Impulsbreite    |  650    | ms
Table: Simulationsparameter

![schema](./res/3-6-schema.png)\

##### Simulationsergebnisse

der jeweiligen Eingangssignale

###### (a) Sinus

![sine](./res/3-6-sine.png)
![sine-cursor](./res/3-6-sine-cursor.png)

Wie sich ablesen lässt, beträgt die Periodendauer $T = \frac{1}{339 kHz} = 2.95 \mu s$. Die höchsten Spannungswerte kommen auch sehr nah an die gewünschte Amplitude, $1.5 V$.

###### (b) Rampe

![ramp](./res/3-6-ramp.png)
![ramp-cursor](./res/3-6-ramp-cursor.png)

Hier liegt der Duty Cycle bei 55%, d.h., das Eingangssignal hält nur für 55% der Periodendauer $T = 2.95 \mu s$.

###### (c) Rechteck

![square](./res/3-6-square.png)
![square-cursor](./res/3-6-square-cursor.png)


Hier liegt der Duty Cycle bei 10%, d.h., das Eingangssignal hält nur für 10% der Periodendauer $T = 2.95 \mu s$. Damit ein Rechteck entsteht, wurde eine besonders geringe Sprungdauer $t_{rise} = t_{fall} = 1 p S$ gewählt.

###### (d) Pulse

![pulse](./res/3-6-pulse.png)
![pulse-cursor](./res/3-6-pulse-cursor.png)

Ein einfaches Pulse Signal. $t_{on}$ beträgt hier 650 ms, die Periode 1100 ms und die Amplitude $U_{peak} = 1.5 V$, wobei aber das Signal eine Offset-Spannung in Höhe von $U_{offset} = -1.4 V$ hat.

##### Oszilloskopimpedanz

ist eine wichtige Kenngröße die beeinflusst, wie Signale am Oszilloskop angezeigt werden. Nämlich kommen bei hochfrequentigen Signalen mit einer hohen Impedanzeinstellung ($\hat{=}$ *HighZ* Modus) Artefakte, sogenannte Reflektionen vor. Dies kann mitigiert werden, indem auf die 50 $\Omega$ Impedanzeinstellung geschalten wird.

Ein Video das diese Artefakte veranschaulicht, findet sich hier: *[High Impedance vs. 50 Ohm Impedance](https://youtube.com/watch?v=Fg63gIrmQ2k) (link)*.

### 3.7 X/Y-Betrieb

##### Parameter und Schema

der Simulation

$C_1$ | $R_1$
:-:|:-:
6.8 $\mu$F | 2.7 k$\Omega$
Table: Simulationsparameter

![schema](./res/3-7-schema.png)\

##### X/Y Figuren

der Simulation

Für ein sinusoidales Eingangssignal mit einer respektiven Frequenz $f_1 = 30 Hz, f_2 = 300 Hz, f_3 = 3 kHz$ ergeben sich folgende Figuren:

![30Hz](./res/3-7-xy-30hz.png)\

![300Hz](./res/3-7-xy-300hz.png)\

![3kHz](./res/3-7-xy-3khz.png)\

Dass die *traces* der Spannungen sich erst später im Verlauf synchronisieren und überlagern, deutet darauf, dass zu Beginn die Schwingungen nicht gekoppelt sind. Im Verlauf aber schwingt das System ein, die Spannungen synchronisieren ihre Amplituden und die *traces* der Spannungen zeichnen übereinander. Eine Verzehnfachung der Frequenz bezweckt also ein beschleunigtes Einschwingen der Spannungen. 

Äußerst interessant ist, welche Figuren sich ergeben, wenn für das sinusoidale Eingangssignal ein Phasenwinkel in Höhe von 10° eingestellt wird (mit Frequenz $f_3 = 3 kHz$).

![3kHz shifted](./res/3-7-xy-3khz-shifted.png)\

### 3.8 Diodenkennlinie

##### Schema

und die dafür gemessenen Spannungswerte sind folgende.

![schema](./res/3-8-schema.png)\

![voltages](./res/3-8-voltages.png)\

Der Widerstand beträgt 1 k$\Omega$. Es ergibt sich für die Wechselspannungsfrequenz $f = 200 Hz$ eine Periodendauer $T = \frac{1}{200 Hz} = 5 ms$. Das anliegende Signal hat eine Peak Spannung $U_{peak}$ in Höhe von 5 V.

Wie sich aus der Schaltung ablesen lässt, ist die zu testende Diode die Gleichrichterdiode `1N4148` (Kennlinie *pink*). Für einen Vergleich wurden die Leuchtdiode `LED_RED` (Kennlinie *violett*) und die Z-Diode `1N5228` (Kennlinie *magenta*) herangezogen. Es werden im Verlauf die beiden Kennlinien für einen Vergleich dargestellt, wobei Spannung auf der Abszisse, Strom auf der Ordinate abgebildet sind. Gekennzeichnet sind jeweils die Punkte (0 V, 0 A)$^T$, sowie die respektiven Schwellenspannungswerte. Für alle Spannungswerte $U_x < 0$ gilt in Folge $I_x < 0$, analog für alle Spannungswerte $U_x > 0$ gilt $I_x > 0$. Das ergibt Sinn, weil die Kennlinie dann in jenen Quadranten liegt, die Stromverbraucher charakterisieren. Außerdem unterscheidet sich die Schwellenspannung, welche bei der `1N4148` Diode bei ca. 500 mV und bei der `LED_RED` Diode bei ca. 1.5 V liegt. Bei der Z-Diode zeigt sich ein kurioseres Phänomen, nämlich gibt es neben der positiven Schwellenspannung, welche bei ungefähr 700 mV liegt, auch eine negative Schwellenspannung, welche bei ungefähr -2.7 V liegt.

`1N4148`

![1n4148](./res/3-8-1n4148-characteristic.png)\

`LED_RED`

![led-red](./res/3-8-led-red-characteristic.png)\

`1N5223`

![1n5223](./res/3-8-1n5223-characteristic.png)\
