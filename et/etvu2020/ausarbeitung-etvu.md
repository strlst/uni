% Ausarbeitung Übungsbeispiele ETVU
%
% August, 2020

# 1. komplexe Zahlen

|
:-:|:-:|:-:|:-:
$z_1 = 1 - j$|$z_2 = -1 + j$|$z_3 = -1 - j$|$z_4 = \frac{1}{j}$

|
|:--|
|$z_1 \cdot z_2 = -1 + j + j - j^2 = 2j$|
|$\dfrac{z_1}{z_2} = \dfrac{-1 - j + j + j^2}{1 + j - j - j^2} = -\dfrac{2}{2} = -1$|
|$z_1 \cdot z_3 = -1 - j + j + j^2 = -2$|
|$\dfrac{z_1}{z_3} = \dfrac{-1 + j + j - j^2}{1 - j + j - j^2} = \dfrac{2j}{2} = j$|
|$z_1 \cdot z_4 = \dfrac{1 - j}{j} = \dfrac{j - j^2}{j^2} = -1 - j$|
|$\dfrac{z_1}{z_4} = (1 - j)j = j - j^2 = 1 + j$|
|$\dfrac{z_3}{z_4} = (-1 - j)j = -j - j^2 = 1 - j$|

|
|:--|
|$z_1 = 1 - j = \sqrt{2} \left (cos(-\frac{\pi}{4}) + j sin(-\frac{\pi}{4}) \right )$|
|$z_2 = -1 + j = \sqrt{2} \left (cos(\frac{3 \pi}{4}) + j sin(\frac{3 \pi}{4}) \right )$|
|$z_3 = -1 - j = \sqrt{2} \left (cos(\frac{5 \pi}{4}) + j sin(-\frac{5 \pi}{4}) \right )$|
|$z_4 = \frac{1}{j} = cos(\frac{3 \pi}{2}) + j sin(\frac{3 \pi}{2})$|

In Fig.1 sind die komplexen Zahlen auf der Gauß'schen Zahlenebene dargestellt:

![komplexe Zahlen](./komplexe-zahlen.png)

# 2. LTI - System

Ein System ist ein ***linear, time-invariant** system* (***LTI** System*), wenn die beiden Eigenschaften erfüllt sind:

- Linearität
- Zeitinvarianz

Dass ein System linear sein muss können wir nicht definitiv zeigen, aber dass ein System keines ist schon. Stellt man sich ein System vor als eine Funktion einer Eingangsspannung, so ist Linearität gegeben durch:

- Additivität: $f(u_1 + u_2) = f(u_1) + f(u_2)$
- Homogenität: $f(c \cdot u) = c \cdot f(u)$

Dasselbe gilt für Zeitinvarianz. Betrachten wir ein System als eine Ausgangsspannung zu einer Eingangsspannung als eine Funktion der Zeit, dann muss jedes zeitinvariantes System folgende Eigenschaft erfüllen:

- wenn $f(t) = h(t)$, dann $f(t + \tau) = h(t + \tau)$

Daher, egal wann ein Eingangssignal angewandt wird, das System produziert dieselbe Antwort.

Wir können also um Linearität zu testen folgendes machen: wir erzeugen mehrere Signale mit unseren Sinusgeneratoren und zeichnen deren individuellen Systemantworten auf, dann überlagern wir die Signale und zeichnen die gesamte Systemantwort auf. Wenn due individuellen Systemantworten überlagert die gesamte Systemantwort ergeben, ist Additivität gegeben. Um Homogenität zu testen, können wir ein Signal entsprechend skalieren und das Ergebnis betrachten.

Um Zeitinvarianz zu testen können wir in irregulären Abständen ein Experiment wiederholen. Dabei muss jedes Mal dieselbe Antwort durch das System geliefert werden.

# 3. "Oszilloskop" - Bild

Im Bild lassen sich 3 volle Perioden erkennen. Insgesamt gibt es 10x10 *divisions*, daher ist die Amplitude $A = 5 \cdot 0.5 V = 2.5 V$. Die dargestellte Funktion lässt sich daher abbilden durch $2.5 \cdot cos(3t)$, wenn die Phase nicht mitbetrachtet wird.

Um die Frequenz zu berrechnen, müssen wir berrechnen, wie lange eine Periode genau dauert. In dem gesamten Zeitfenster (10 *div*), d.h. in $10 \cdot 10 \mu s$, gehen sich drei ganze Perioden aus. Daher dauert eine Periode $T = 100 \mu s / 3 = 33. \dot 3 \mu s$. Die Frequenz lässt sich dann wie folgt berrechnen:

$$
f = \dfrac{1}{T} = \dfrac{1}{33. \dot 3 \mu s} = 30 kHz
$$

# 4. Einschaltvorgang

Zur weiteren Berrechnung designieren wir die totale Spannung als $U_T = 10 V$, und die Ströme zu $R_1$, $R_2$ und $C_1$ als $i_1$, $i_2$ und $i_3$. Wir nehmen an, die Ladung des Kondensators ist zu $t_0 = 0$ gleich $Q = 0$. Weil $R_2$ und $C_1$ parallel angeschlossen sind, müssen ihre Spannungen identisch sein, also $U_{R_2} = U_{C_1}$. Aber da $Q = C \cdot U$, gilt $U_{C_1} = \frac{Q}{C} = \frac{0 C}{1 \mu F}$ und somit $U_{R_2} = U_{C_1} = 0$.

$$
t = 0 \implies Q = 0 \implies U_{R_2} = U_{C_1} = 0
$$

Wir nehmen als nächstes an, es vergeht genügend Zeit, dass der Kondensator vollständig geladen ist. Da er geladen ist, kann die Ladung $Q$ sich nicht weiter erhöhen, daher fließt durch $C_1$ kein Strom und es gilt $i_3 = 0$. Aufgrund der Maschenregel gilt:

$$
U_T = i_1 R_1 + i_2 R_2
$$

und aufgrund der Knotenregel gilt:

$$
i_1 = i_2 + i_3
$$

aber da $i_3 = 0$, gilt $i_1 = i_2$. Das bedeutet für uns, es fließt nur ein Strom durch $R_2$, aber nicht durch $C_1$. Wir können daher den durch die Maschenregel gewonnenen Ausdruck vereinfachen:

$$
U_T = i_1 (R_1 + R_2) \implies i_1 = i_2 = \dfrac{U_T}{R_1 + R_2} = \dfrac{10 V}{2k \Omega} = 5mA
$$

Und abschließend finden wir den Wert für $U_{R_2}$ durch das gute alte Ohm'sche Gesetz:

$$
U_{R_2} = i_2 R_2 = 5mA \cdot 1k \Omega = 5V
$$

*Hinweis: versucht man die Lade DGL zu lösen, so findet man, dass die Spannung an $C_1$ nach einer Sekunde ebenfalls 5V beträgt, also ist $C_1$ nach einer Sekunde bereits vollständig geladen*

# 5. diskrete Faltung

$$
g[n] = ..., 0, 0, 2, -1, 1, 0, 0, ...\ \ \ h[n] = ..., 0, 0, 1, -1, 0, 0, ...
$$

Tabellarisch könnte die Berrechnung wie folgt (Fig.2) dargestellt werden:

![Diskrete Faltung](./diskrete-faltung.png){ width=125% }

# 6. komplexes Signal

$$
\begin{aligned}
f(t) &= 4 \cdot e^{-2000 s^{-1} t} \cdot cos(6283.2 \frac{rad}{s} \cdot t + 30 ^{\circ} ) \\
\end{aligned}
$$

Nutzen wir ein paar Hilfsrelationen, so können wir diese Form erheblich vereinfachen. Unter anderem: $1 \frac{rad}{s} = \frac{1}{2 \pi} Hz$ sowie $1 \deg = \frac{\pi}{180 ^{\circ}} rad$

$$
\begin{aligned}
f(t) &= 4 \cdot e^{-2000 s^{-1} t} \cdot cos(6283.2 \frac{rad}{s} \cdot t + 30 ^{\circ} ) \\
&= 4 \cdot e^{-2000 \cdot 2 \pi \frac{rad}{s} \cdot t} \cdot cos(6283.2 \frac{rad}{s} \cdot t + \frac{\pi}{6}) \\
\underline s(t) &= 4 \cdot e^{-4000 \pi \frac{rad}{s} \cdot t} \cdot e^{j(6283.2 \frac{rad}{s} \cdot t + \frac{\pi}{6})} \\
&= 4 \cdot e^{-4000 \pi \frac{rad}{s} \cdot t} \cdot e^{j 6283.2 \frac{rad}{s} \cdot t} \cdot e^{j \frac{\pi}{6}} \\
\end{aligned}
$$

Um $f(t)$ mit komplexer Amplitude und Frequenz darzustellen, benötigen wir folgende Form:

$$
\underline s(t) = A e^{j \varphi} \cdot e^{\sigma t} \cdot e^{j \omega t}
$$

wobei:

$$
\underline X = A e^{j \varphi} = 4 e^{j \frac{\pi}{6}},\ \ \ \underline s = \sigma + j \omega = (-4000 \pi + j 6283.2) \frac{rad}{s}
$$

somit:

$$
\begin{aligned}
\underline s(t) = \underline X e^{\underline s t} = 4 e^{j \frac{\pi}{6}} \cdot e^{(-4000 \pi + j 6283.2) \frac{rad}{s} \cdot t}
\end{aligned}
$$

Auf der komplexen Zahlenebene zeigt der Zeiger wie in Fig.3:

![komplexe Frequenz in der Gauß'schen Zahlenebene](./komplexe-frequenz.png)

Die Fkt. schaut im Zeitbereich aus wie in Fig.4:

![komplexes Signal im Zeitbereich](./komplexes-signal-zeitbereich.png)

# 7. Widerstandsschaltung

Um die Spannungsteilerregel anwenden zu können müssen wir den Widerstand für das gesamte Netzwerk kennen. Dafür definieren wir einen Ersatzwiderstand:

$$
\begin{aligned}
R_{ers} &= R_1 + R_{2,3,L} \\
\dfrac{1}{R_{2,3,L}} &= \dfrac{1}{R_{2,L}} + \dfrac{1}{R_3} \\
R_{2,L} &= R_2 + R_L
\end{aligned}
$$

$$
\implies R_{2,3,L} = \dfrac{1}{\dfrac{1}{2k \Omega} + \dfrac{1}{1k \Omega}} = 0. \dot 6 k \Omega
$$

Jetzt lautet die Spannungsteilerregel:

$$
\dfrac{U_{total}}{U_{R_{2,3,L}}} = \dfrac{R_{ers}}{R_{2,3,L}}
$$

Daraus folgt:

$$
U_{R_{2,3,L}} = U_{total} \cdot \dfrac{R_{2,3,L}}{R_{ers}} = 1V \cdot \dfrac{0. \dot 6k \Omega}{1. \dot 6 k \Omega} = 0.4 V
$$

und daher:

$$
U_{R_L} = 0.2 V
$$

- Leerlaufspannung: Die Spannung $U_{R_L}$ wenn keine Last angeschlossen ist, dh. $R_L = \infty$. In diesem Fall fließt kein Strom über $R_L$ und wir haben $U_{R_L} = U_{R_3} = U_{total} \cdot \dfrac{1}{2} = 0.5 V$
- Innenwiderstand: unser "$R_{ers}$" wenn $R_L = 0$, $R_{inn} = R_1 + \dfrac{1}{\frac{1}{R_2} + \frac{1}{R_3}} = 1k \Omega + 0.5k \Omega = 1.5k \Omega$
- Kurzschlussstrom: der Strom $I_{total}$ der fließt, wenn $R_L$ einen Kurzschluss darstellt, dh. $R_L = 0$. $I_{total} = \dfrac{U_{total}}{R_{inn}} = \dfrac{1 V}{1.5k \Omega} = 0. \dot 6 mA$

# 8. Leistung in einem Widerstandsnetzwerk

$$
P = U \cdot I \\
$$
$$
\dfrac{U_2}{U} = \dfrac{R_2}{R_1 + R_2} \implies U_2 = U \cdot \dfrac{R_2}{R_1 + R_2}
$$
$$
I_2 = \dfrac{U_2}{R_2}
$$
$$
P_2 = U_2 \cdot I_2 = \dfrac{U_2^2}{R_2} = \dfrac{\left( U \cdot \dfrac{R_2}{R_1 + R_2} \right)^2}{R_2} = U^2 \cdot R_2 \cdot \left(\dfrac{1}{R_1 + R_2}\right)^2
$$

Ein Plot der die Form der resultierenden Funktion (Leistung gegen Widerstand) darstellen soll ist in Fig.5 zu sehen:

![Leistung Plot](./leistung-plot.png)

# 9. Wasserkocher

$$
P = U \cdot I = I^2  \cdot R = \dfrac{U^2}{R}
$$
$$
1500 W = 230 V \cdot I \implies I = \dfrac{1500 W}{230 V} = \dfrac{150}{23} A = 6.52 A
$$
$$
R = \dfrac{230 V}{\frac{150}{23} A} = 230 \cdot \dfrac{23}{150} \Omega = 35.2 \dot 6 \Omega
$$

# 10. Knotenregel

### (a)

$$
2A + 3A - i_a = 0 \implies i_a = 5A
$$

### (b)

$$
1A + (-3A) - 2A - i_b = 0 \implies i_b = -4A
$$

### (c)

$$
-(-1A) - 3A + 4A - i_c = 0 \implies i_c = 2A
$$

# 11. Maschenregel

Wir können drei eigene Maschen aufstellen um Gleichungen zu erhalten, aus denen wir $U_1$, $U_2$ und $U_3$ bestimmen können:

$$
\begin{aligned}
-3V - 5V + U_1 - (-2V) &= 0 \implies U_1 = 6V \\
-2V - 6V + U_3 &= 0 \implies U_3 = 8V \\
-3V - 5V + (-10V) + U_2 + U_3 &= 0 \implies -18V + 8V = U_2 \implies U_2 = 10V
\end{aligned}
$$

# 12. Widerstand in einem Fernsehgerät

$$
P = U \cdot I = I^2  \cdot R = \dfrac{U^2}{R}
$$

$$
\begin{aligned}
&\implies I = \sqrt{\dfrac{P}{R}} = \sqrt{\dfrac{0.25}{10^3}} = 0.01581 A = 15.81 mA \\
&\implies U = \sqrt{P R} = \sqrt{0.25 \cdot 10^3} = 15.81 V
\end{aligned}
$$

# 13. Ersatzwiderstände

### (a)

$$
R_{ers} = 2 \Omega + \dfrac{1}{\frac{1}{6 \Omega} + \frac{1}{3 \Omega} + \frac{1}{2 \Omega}} = 3 \Omega
$$

### (b)

$$
R_{ers} = \dfrac{1}{\frac{1}{200 \Omega} + \frac{1}{50 \Omega}} + \dfrac{1}{\frac{1}{100 \Omega} + \frac{1}{25 \Omega}} = 60 \Omega
$$

### (c)

$$
\begin{aligned}
R_{rest} &= 8 \Omega + \dfrac{1}{\frac{1}{3 \Omega} + \frac{1}{6 \Omega}} = 10 \Omega \\
\dfrac{1}{R_{ers}} &= \dfrac{1}{10 \Omega} + \dfrac{1}{R_{rest}} = \dfrac{1}{10 \Omega} + \dfrac{1}{10 \Omega} \\
\implies R_{ers} &= 5 \Omega
\end{aligned}
$$

### (d)

$$
\begin{aligned}
\dfrac{1}{R_{ers}} &= \dfrac{1}{6k \Omega} + \dfrac{1}{6k \Omega} + \dfrac{1}{1k \Omega + 2k \Omega}\\
\implies R_{ers} &= 1.5k \Omega
\end{aligned}
$$

# 14. RLC - Filter

Der Filter besteht aus 2 Tiefpassfiltergliedern (1 RC Tiefpassfilter 1. Ordnung, 1 LC Tiefpassfilter 2. Ordnung), ist daher ein Tiefpassfilter 3. Ordnung.

Um die Übertragungsfkt. $\underline H(\underline s)$ aufzustellen, können wir das Knotenpotentialverfahren verwenden. Dafür definieren wir die beiden Knoten $U_a$ und $U_b$, wie abgebildet in Fig.6. Außerdem legen wir die Stromrichtungen fest:

![RLC-Filter](./rlc-filter.png)

Folgende Gleichungen stellen wir auf für unsere Knoten:

$$
\begin{aligned}
i_R + i_L + i_{C1} &= 0 \\
-i_L + i_{C2} &= 0
\end{aligned}
$$

Jetzt setzen wir ein für die Ströme:

$$
\begin{aligned}
\rm I: \dfrac{U_1 - U_a}{R} + \dfrac{U_a - U_b}{sL} + sC_1U_a &= 0 \\
\rm II: -\dfrac{U_a - U_b}{sL} + sC_2U_b &= 0
\end{aligned}
$$

$$
\begin{aligned}
\rm II: -\dfrac{U_a}{sL} + \dfrac{U_b}{sL} + sC_2U_b = 0 &\implies U_b \left(\dfrac{1}{sL} + sC_2 \right) = \dfrac{U_a}{sL} \\
&\implies U_a = U_b(1 + s^2LC_2)
\end{aligned}
$$

$$
\begin{aligned}
\rm I: \dfrac{U_1}{R} - \dfrac{U_a}{R} + \dfrac{U_a}{sL} - \dfrac{U_b}{sL} + sC_1U_a &= 0 \\
\rm I: U_a \left(\dfrac{1}{sL} - \dfrac{1}{R} + sC_1 \right) + U_1 \left(\dfrac{1}{R} \right) + U_b \left(-\dfrac{1}{sL} \right) &= 0 \\
\rm I: U_b(1 + s^2LC_2) \left(\dfrac{1}{sL} - \dfrac{1}{R} + sC_1 \right) + U_1 \left(\dfrac{1}{R} \right) + U_b \left(-\dfrac{1}{sL} \right) &= 0 \\
\rm I: U_b \left((1 + s^2LC_2) \left(\dfrac{1}{sL} - \dfrac{1}{R} + sC_1 \right) - \dfrac{1}{sL} \right) + U_1 \left(\dfrac{1}{R} \right) &= 0 \\
\end{aligned}
$$

und da $U_b = U_2$, können wir die gewünschte Relation aufstellen:

$$
\begin{aligned}
\dfrac{U_b}{U_1} = \dfrac{U_2}{U_2} = H(s) &= -\dfrac{1}{R} \cdot \dfrac{1}{\frac{1}{sL} - \frac{1}{R} + sC_1 + \frac{s^2LC_2}{sL} - \frac{s^2LC_2}{R} + s^3LC_1C_2} \\
H(s) &= -\dfrac{1}{\frac{R}{sL} - 1 + sRC_1 + sRC_2 - s^2LC_2 + s^3RLC_1C_2} \\
H(s) &= \dfrac{1}{1 - \frac{R}{sL} - sRC_1 - sRC_2 + s^2LC_2 - s^3RLC_1C_2}
\end{aligned}
$$

Wir können den Frequenzganz erhalten indem wir den Spezialfall $s \rightarrow s = j \omega$ betrachten:

$$
\def \jw {j \omega}
\begin{aligned}
H(s) &= \dfrac{1}{1 - \frac{R}{\jw L} - \jw RC_1 - \jw RC_2 + (\jw)^2LC_2 - (\jw)^3RLC_1C_2} \\
 &= \dfrac{1}{1 + \frac{jR}{\omega L} - \jw RC_1 - \jw RC_2 - \omega^2LC_2 + j \omega^3RLC_1C_2} \\
 &= \dfrac{1}{1 - \omega^2LC_2 + j(\frac{R}{\omega L} - \omega RC_1 - \omega^2 RC_2 + \omega^3RLC_1C_2)} \\
\end{aligned}
$$

in Folge:

$$
\begin{aligned}
|H(s)| &= \dfrac{1}{\sqrt{(1 - \omega^2LC_2)^2 + (\frac{R}{\omega L} - \omega RC_1 - \omega^2 RC_2 + \omega^3RLC_1C_2)^2}} \\
\end{aligned}
$$

# 15. Integrator mit Operationsverstärker

Es handelt sich um eine (invertierende) Operationsverstärkerschaltung. Unter Anwendung der Spannungsteilerregel:

$$
\begin{aligned}
\dfrac{U_a}{U_e} = H(s) = -\dfrac{C \parallel R_2}{R_1} = -\dfrac{\frac{1}{\frac{1}{R_2}+sC}}{R_1} = -\dfrac{1}{\frac{R_1}{R_2} + sCR_1}
\end{aligned}
$$

Betrachten wir wieder den Fall $s \rightarrow s = j \omega$:

$$
\begin{aligned}
H(s) = -\dfrac{1}{\frac{R_1}{R_2} + j \omega CR_1}
\end{aligned}
$$

dann gilt:

$$
\begin{aligned}
|H(s)| = \sqrt{\dfrac{1}{\left( \frac{R_1}{R_2} \right)^2 + \omega^2 C^2}} = \dfrac{1}{R_1} \cdot \sqrt{\dfrac{1}{\frac{1}{R_2^2} + \omega^2 C^2}}
\end{aligned}
$$

# 16. Passives Filter

Der Filter besteht aus 2 Hochpassfiltergliedern (2 RL Hochpassfilter 1. Ordnung) und ist daher ein Hochpassfilter 2. Ordnung.

Bei diesem Schaltkreis lassen sich intuitiv folgende Gleichungen aufstellen:

$$
\begin{aligned}
\rm I   &: U_e - U_{R_1} - U_{L_1} = 0 \\
\rm II  &: U_{L_1} - U_{R_2} - U_{L_2} = 0 \\
\rm III &: U_{L_2} - U_a = 0
\end{aligned}
$$

Außerdem können wir beobachten:

$$
\begin{aligned}
\rm IV &: i_{R_1} = i_1 \\
\rm V  &: i_{R_2} = i_{L_2} = i_2 \\
\rm VI &: i_{L_1} = (i_1 - i_2)
\end{aligned}
$$

Jetzt stellen wir unser Gleichungssystem auf:

$$
\begin{aligned}
\rm I   &: U_e = R_1i_1 + sL_1(i_1 - i_2) \\
\rm II  &: sL_1(i_1 - i_2) = R_2i_2 +sL_2i_2 = i_2(R_2 + sL_2) \\
\rm III &: U_a = sL_2i_2 \\
\hline
\rm I   &: U_e = i_1(R-1 + sL_1) + i_2 (-sL_1) \\
\rm II  &: 0 = i_1(-sL_1) + i_2(sL_1 + R_2 + sL_2) \\
\rm III &: U_a = i_2sL_2 \\
\hline
\rm III &: i_2 = \dfrac{U_a}{sL_2} \\
\rm II  &: 0 = i_1(-sL1) + \dfrac{U_a}{sL_2} (sL_1 + R_2 + sL_2) \\
\rm II  &: i_1 = \dfrac{U_a}{s^2L_1L_2} (sL_1 + R_2 + sL_2) \\
\rm II  &: i_1 = U_a \left(\dfrac{1}{sL_2} + \dfrac{1}{s^2L_1L_2} + \dfrac{1}{sL_1}\right) \\
\rm I   &: U_e = U_a \left(\dfrac{1}{sL_2} + \dfrac{1}{s^2L_1L_2} + \dfrac{1}{sL_1}\right)(R_1 + sL_1) + U_a \left(-\dfrac{sL_1}{sL_2}\right) \\
\rm I   &: \dfrac{U_e}{U_a} = \dfrac{R_1}{sL_1} + \dfrac{R_1R_2}{s^2L_1L_2} + \dfrac{R_1}{sL_2} + 1 + \dfrac{R_2}{sL_2} + \dfrac{sL_1}{sL_2} - \dfrac{sL_1}{sL_2} \\
\rm I   &: \dfrac{U_a}{U_e} = \dfrac{1}{\dfrac{R_1}{sL_1} + \dfrac{R_1R_2}{s^2L_1L_2} + \dfrac{R_1}{sL_2} + 1 + \dfrac{R_2}{sL_2}} \\
\rm I   &: \dfrac{U_a}{U_e} = \dfrac{1 \cdot s^2L_1L_2}{\left(\dfrac{R_1}{sL_1} + \dfrac{R_1R_2}{s^2L_1L_2} + \dfrac{R_1}{sL_2} + 1 + \dfrac{R_2}{sL_2}\right) \cdot s^2L_1L_2} \\
\rm I   &: \dfrac{U_a}{U_e} = \dfrac{s^2L_1L_2}{sR_1L_2 + sR_1L_1 + sR_2L_1 + R_1R_2 + s^2L_1L_2} \\
\end{aligned}
$$

somit:

$$
H(s) = \dfrac{s^2L_1L_2}{sR_1L_2 + sR_1L_1 + sR_2L_1 + R_1R_2 + s^2L_1L_2}
$$

# 17. Sallen-Key-Tiefpassfilter

![Sallen-Key-Tiefpassfilter](./sallen-key-circuit.png)

Die wichtigste Observation um dieses Beispiel lösen zu können ist an dem Plan der Schaltkreises (Fig.7) eingetragen: die Spannungsdifferenz der beiden $V_+$ und $V_-$ Kontakte ist idealerweise $0V$!

Wir stellen die Knotengleichungen auf:

$$
\begin{aligned}
\rm  I &: i_1 - i_2 - i_3 = 0 \\
\rm II &: i_2 - i_4 = 0
\end{aligned}
$$

Außerdem unsere Gleichungen für die Ströme:

$$
\begin{aligned}
\rm III &: i_1 = \dfrac{U_e - U_1}{R_1} \\
\rm  IV &: i_2 = \dfrac{U_1 - U_2}{R_2} \\
\rm   V &: i_3 = (U_1 - U_a)sC_2 = (U_1 - U_2)sC_2 \\
\rm  VI &: i_4 = (U_2 - 0V)sC_1 = U_2sC_1 \\
\end{aligned}
$$

und lösen schließlich unser Gleichungssystem bzw. stellen die Übertragungsfkt. auf:

$$
\begin{aligned}
\rm  I &: \dfrac{U_e - U_1}{R_1} = \dfrac{U_1 - U_2}{R_2} + (U_1 - U_2)sC_2 \\
\rm  I &: \dfrac{U_e}{R_1} - \dfrac{U_1}{R_1} = \dfrac{U_1}{R_2} - \dfrac{U_2}{R_2} + U_1sC_2 - U_2sC_2 \\
\rm  I &: 0 = U_e \left(-\dfrac{1}{R_1}\right) + U_1 \left(\dfrac{1}{R_1} + \dfrac{1}{R_2}  + sC_2 \right) + U_2 \left(-\dfrac{1}{R_2} - sC_2 \right) \\
\rm II &: \dfrac{U_1 - U_2}{R_2} = U_2sC_1 \\
\rm II &: \dfrac{U_1}{R_2} - \dfrac{U_2}{R_2} = U_2sC_1 \\
\rm II &: 0 = U_1 \left(-\dfrac{1}{R_2}\right) + U_2 \left(\dfrac{1}{R_2} + sC_1 \right) \\
\rm II &: \implies U_1 = U_2(1+sR_2C_1) \\
\rm  I &: U_e \dfrac{1}{R_1} = U_2 \left( \dfrac{1}{R_1} + \dfrac{1}{R_2} + sC_2 + \dfrac{sR_2C_1}{R_1} + \dfrac{sC_1R_2}{R_2} + s^2R_2C_1C_2 - \dfrac{1}{R_2} - sC_2 \right) \\
\rm  I &: U_e = U_2 \left( 1 + \dfrac{R_1}{R_2} + sR_1C_2 + sR_2C_1 + sR_1C_1 + s^2R_1R_2C_1C_2 - \dfrac{R_1}{R_2} - sR_1C_2 \right) \\
\rm  I &: U_e = U_2 \left( 1 + sR_2C_1 + sR_1C_1 + s^2R_1R_2C_1C_2 \right) \\
\rm  I &: \implies \dfrac{U_2}{U_e} = \dfrac{U_a}{U_e} = \dfrac{1}{1 + sR_2C_1 + sR_1C_1 + s^2R_1R_2C_1C_2} \\
\end{aligned}
$$

und somit:

$$
H(s) = \dfrac{1}{1 + sR_2C_1 + sR_1C_1 + s^2R_1R_2C_1C_2}
$$

# 18. Matlab

Die drei Plots/Zeichnungen sind in Fig.8, Fig.9 und Fig.10 abgebildet.

![Octave Filter Charakteristik](./octave-filter.png)

![Octave Filter PN Diagramm](./octave-zplane.png)

![~~Octave~~ Filter Sprungantwort](./octave-step-response.png){ width=100% }
