close all;
clear;
clc;

% 1
z1 = 1 + 1j;
z2 = -1 + 1j;
z3 = -1 - 1j;
z4 = 1 + 1/1j;

r = [ abs(z1) abs(z2) abs(z3) abs(z4) ];
[ sqrt(2) sqrt(2) sqrt(2) sqrt(2) ];
a = [ angle(z1) angle(z2) angle(z3)+2*pi angle(z4)+2*pi ];
[ pi/4 3*pi/4 5*pi/4 7*pi/4 ];

%compass([z1, z2,  z3, z4])

z1 + z2;
z2 + z4;
[z1 * z2, z1 / z2];
[z1 * z3, z1 / z3];
[z1 * z4, z1 / z4];

% 6
A = 2;
phi = pi/6;
sigma = -4000*pi;
omega = 8000*pi;
% initial position
%compass(A*exp(1j*phi)*exp((sigma+1j*omega)*0))

t = 0:1/50*1/(omega):15*1/(omega);
ft = real(A*exp(1j*phi)*exp((sigma+1j*omega)*(t)));
% function plot
%plot(t, ft, '--or')
%xlabel('t [s]')
%ylabel('Re\{s(t)\} [V]')

% 14
s = tf('s');
R = 8;
C1 = 1;
C2 = 2;
L = 4;
H = C2/(C1*(1 + C1*C2*L*R*s^3 + L*(C1 + C2)*s^2 + C2*R*s));
%bode(H)

% 16
H2 = (s^2*L^2+s*R*L)/(s^2*L^2+3*s*R*L+R^2)
%bode(H2)

% 18
[bz, bp, bk] = besselap(5);
[bnum, bden] = zp2tf(bz, bp, bk);
[cz, cp, ck] = ellipap(5, 1, 50);
[cnum, cden] = zp2tf(cz, cp, ck);
btf = tf(bnum, bden)
ctf = tf(cnum, cden)

opts = bodeoptions('cstprefs');
opts.PhaseVisible = 'off';
opts.YLim = {[-60 0]};
opts.XLim = {[.01 100]};

pzopt_b = pzoptions('cstprefs');
pzopt_b.XLim = {[-1.5, 1.5]};
pzopt_b.YLim = {[-1.5, 1.5]};

pzopt_c = pzoptions('cstprefs');
pzopt_c.XLim = {[-1, 1]};
pzopt_c.YLim = {[-2.5, 2.5]};

figure 
bode(btf, opts)
title('Bode Plot (Bessel)')
grid on

figure 
bode(ctf, opts)
title('Bode Plot (Cauer)')
grid on

figure
pzmap(btf, pzopt_b)
title('Pol-/Nullstellendiagramm (Bessel)')

figure
pzmap(ctf, pzopt_c)
title('Pol-/Nullstellendiagramm (Cauer)')

figure
step(btf)
title('Sprungantwort (Bessel)')

figure
step(ctf)
title('Sprungantwort (Cauer)')