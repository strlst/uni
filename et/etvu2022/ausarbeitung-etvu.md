% Ausarbeitung Übungsbeispiele ETVU
%
% Mai, 2022

# 1. komplexe Zahlen

Gegeben sind $z_1 = 1 + 1j$, $z_2 = -1 + 1j$, $z_3 = -1 - 1j$, $z_4 = 1 + 1/j$.

In Polarform:

$|z_1| = |z_2| = |z_3| = |z_4| = \sqrt{2}$

$\angle{z_1} = \frac{\pi}{4}$, $\angle{z_2} = \frac{3 \pi}{4}$, $\angle{z_3} = \frac{5 \pi}{4}$, $\angle{z_4} = \frac{7 \pi}{4}$

![Zeiger $z_i, i \in [1, 4]$ in der komplexen Ebene](res/1.png){ width=50% }

|Berechnungen|
|:-:|
|$z_1 + z_2 = 1 - 1 + 1j + 1j = 2j$|
|$z_2 + z_4 = -1 + 1 + 1j - 1j = 0$|
|$z_1 \cdot z_2 = \sqrt{2} \sqrt{2} \angle{(\frac{\pi}{4} + \frac{3\pi}{4})} = 2 \angle(\pi) = -2$|
|$z_1 \cdot z_3 = \sqrt{2} \sqrt{2} \angle{(\frac{\pi}{4} + \frac{5\pi}{4})} = 2 \angle(\frac{3\pi}{2}) = -2j$|
|$z_1 \cdot z_4 = \sqrt{2} \sqrt{2} \angle{(\frac{\pi}{4} + \frac{7\pi}{4})} = 2 \angle(2\pi) = 2$|
|$z_1 \div z_2 = \frac{\sqrt{2}}{\sqrt{2}} \angle{(\frac{\pi}{4} - \frac{3\pi}{4})} = 1 \angle(-\frac{\pi}{2}) = -1j$|
|$z_1 \div z_3 = \frac{\sqrt{2}}{\sqrt{2}} \angle{(\frac{\pi}{4} - \frac{5\pi}{4})} = 1 \angle(-\pi) = -1$|
|$z_1 \div z_4 = \frac{\sqrt{2}}{\sqrt{2}} \angle{(\frac{\pi}{4} - \frac{7\pi}{4})} = 1 \angle(-\frac{3\pi}{2}) = 1j$|

\pagebreak

# 2. LTI - System

Ein System ist ein ***linear, time-invariant** system* (***LTI** System*), wenn die beiden Eigenschaften erfüllt sind:

- Linearität
- Zeitinvarianz

Dass ein System linear sein muss können wir nicht definitiv zeigen, aber dass ein System keines ist schon. Stellt man sich ein System vor als eine Funktion einer Eingangsspannung, so ist Linearität gegeben durch:

- Additivität: $f(u_1 + u_2) = f(u_1) + f(u_2)$
- Homogenität: $f(V \cdot u) = V \cdot f(u)$

Dasselbe gilt für Zeitinvarianz. Betrachten wir ein System als eine Ausgangsspannung zu einer Eingangsspannung als eine Funktion der Zeit, dann muss jedes zeitinvariantes System folgende Eigenschaft erfüllen:

- wenn $f(t) \mapsto h(t)$, dann $f(t + \tau) \mapsto h(t + \tau)$

Daher, egal wann ein Eingangssignal angewandt wird, das System produziert dieselbe Antwort (also auch wenn das Eingangssignal zeitverzögert oder bereits früher angewandt wird).

Wir können also um Linearität zu testen folgendes machen: wir erzeugen mehrere Signale mit unseren Sinusgeneratoren und zeichnen deren individuellen Systemantworten auf, dann überlagern wir die Signale und zeichnen die gesamte Systemantwort auf. Wenn due individuellen Systemantworten überlagert die gesamte Systemantwort ergeben, ist Additivität gegeben. Um Homogenität zu testen, können wir ein Signal entsprechend skalieren und das Ergebnis betrachten.

Um Zeitinvarianz zu testen können wir in irregulären Abständen ein Experiment wiederholen, immer nach abklingen des letzten Experimentes. Dabei muss jedes Mal dieselbe Antwort durch das System geliefert werden.

\pagebreak

# 3. "Oszilloskop" - Bild

Wir können jede harmonische (Kosinus-)schwingung mithilfe von drei Werten parametrieren, der Amplitude $A$, der Frequenz $\omega = 2 \pi f$ und der Phasenverschiebung $\varphi$.

Die Amplitude errechnet sich zu $A = 4 \text{div} \cdot 25 \text{mV} / \text{div} = 100 \text{mV}$, da das Signal sich über 4 Divisionen erstreckt mit $25 \text{mV}$ pro Division.

Die Periode $T$ errechnet sich zu $T = \frac{1}{2} \cdot 10 \text{div} \cdot 10 \mu \text{s} / \text{div} = 50 \mu \text{s}$, da das Signal zwei volle Perioden über 10 Divisionen schwingt mit $10 \mu \text{s}$ pro Division. Die Frequenz $f$ lautet dann $f = \frac{1}{T} = 20 \text{kHz}$. Dabei ist $\omega$ die Kreisfrequenz $\omega = 2 \pi f$.

Die Phasenverschiebung lässt sich nicht genau erkennen, aber da das Signal ungefähr $\frac{2}{3}$ einer Division später erst wieder bei seinem Initialwert $A$ (zu $t = 0$ wenn $\varphi = 0$) beginnt und eine volle Schwingung innerhalb von $5$ Divisionen geschieht errechnet sie sich ungefähr zu $\varphi \approx \frac{2}{3} \cdot \frac{1}{5} \cdot 2 \pi = \frac{4 \pi}{15}$.

Somit lautet die Schwingung $x(t) = A \cos(\omega t + \varphi) = 100 \cos(20 \cdot 10^3 \cdot 2 \pi \cdot t + \frac{4 \pi}{15}) \text{mV}$.

\pagebreak

# 4. Einschaltvorgang

Zunächst beginnen wir mit der Konfiguration beim Kurzschließen des Schalters. In dieser Konfiguration wirken sich beide Widerstände $R_1$ und $R_2$ auf die Schaltung aus. Da diese parallel geschalten sind, betrachtet vom Knoten der die Widerstände und den Kondensator verbindet, erwarten wir eine Zeitkonstante wie folgt:

$$
\tau_1 = (R_1 || R_2) C_1 = \frac{R_1 R_2}{R_1 + R_2} C_1 = \frac{1 \text{k}\Omega \cdot 3 \text{k}\Omega}{1 \text{k}\Omega + 3 \text{k}\Omega} 1 \mu \text{F} = 750 \Omega \cdot 1 \mu \text{F} = 750 \mu \text{s}
$$

Nachdem der Schalter wieder geöffnet wird, nach ausreichender Zeit damit der Kondensator sich aufladen konnte und alle transienten Prozesse beendet sind, sieht die Situation anders aus. Nun hängt der Widerstand $R_1$ ohne Verbindung und kann daher ignoriert werden. Lediglich über den Widerstand $R_2$ entläd sich nun der Kondensator. Die neue Zeitkonstante ergibt sich daher wie folgt:

$$
\tau_2 = R_2 C_1 = 1 \text{k}\Omega \cdot 1 \mu \text{F} = 1 \text{ms}
$$

In Fig. 2 wird dieser Lade- und Entladeprozess dargestellt (mithilfe einer Simulation zur Überprüfung der Ergebnisse in LTSpice). Dabei aufgetragen sind die Punkte von Interesse, nämlich jene wo der Kondensator aus seinem vorherigen Ruhezustand $1 - 1/e = 63.2\%$ seines Endwertes nach abklingen des transienten Prozesses erreicht.

Das bedeutet für unsere Situation, einmal Punkt $(0.75 \text{ms}, 1.896 \text{V})$ bei der Transition $0 \text{V} \to 3 \text{V}$ und $(7 \text{ms}, 1.104 \text{V})$ bei der Transition $3 \text{V} \to 0 \text{V}$.

($63.2\% \cdot 3 \text{V} = 1.896 \text{V}$, $(100\% - 63.2\%) \cdot 3 \text{V} = 1.104 \text{V}$)

![Auflade- und Entladekurve](res/4.png)

\pagebreak

# 5. diskrete Faltung

In Fig. 3 lässt sich die Durchführung der diskreten Faltung in tabellarischer Form visualisiert betrachten. Da $h[n]$ nur zwei von $0$ verschiedene Werte beinhält kann man vereinfacht für die Zwecke der Berechnung von $f[n]$ die gelbe und grüne Zone betrachten.

![Durchführung diskreter Faltung visualisiert](res/5.png){ width=100% }

\pagebreak

# 6. komplexes Signal

$$
\begin{aligned}
f(t) &= 2 \cdot e^{-2000 \text{s}^{-1} t} \cdot cos(4000 \text{Hz} \cdot t + 30 ^{\circ} ) \\
\end{aligned}
$$

Nutzen wir ein paar Hilfsrelationen, so können wir diese Form auf gemainsame Einheiten bringen. Unter anderem gilt: $1 \text{Hz} = 1 \text{s}^{-1} = 2 \pi \frac{\text{rad}}{\text{s}}$ sowie $1 ^\circ = \frac{\pi}{180 ^{\circ}} \text{rad}$

$$
\begin{aligned}
f(t) &= 2 e^{-2000 \text{s}^{-1} t} \cdot cos(4000 \text{Hz} \cdot t + 30 ^{\circ} ) \\
&= 2 e^{-4000 \pi \frac{\text{rad}}{\text{s}} t} \cdot cos\left(8000 \pi \frac{\text{rad}}{\text{s}} t + \frac{\pi}{6}\right) \\
\underline s(t) &= 2 e^{-4000 \pi \frac{\text{rad}}{\text{s}} \cdot t} \cdot e^{j(8000 \pi \frac{\text{rad}}{\text{s}} t + \frac{\pi}{6})} \\
&= 2 e^{-4000 \pi \frac{\text{rad}}{\text{s}} t} \cdot e^{j 8000 \pi \frac{\text{rad}}{\text{s}} t} \cdot e^{j \frac{\pi}{6}} \\
\end{aligned}
$$

Um $f(t)$ mit komplexer Amplitude und Frequenz darzustellen, benötigen wir folgende Form:

$$
\underline s(t) = A e^{j \varphi} \cdot e^{\sigma t} \cdot e^{j \omega t}
$$

wobei:

$$
\underline X = A e^{j \varphi} = 2 e^{j \frac{\pi}{6}},\ \ \ \underline s = \sigma + j \omega = (-4000 \pi + j 8000 \pi) \frac{\text{rad}}{\text{s}}
$$

somit:

$$
\begin{aligned}
\underline s(t) = \underline X e^{\underline s t} = 2 e^{j \frac{\pi}{6}} \cdot e^{(-4000 \pi + j 8000 \pi) \frac{\text{rad}}{\text{s}} t}
\end{aligned}
$$

Auf der komplexen Zahlenebene zeigt der Zeiger wie in Fig. 4 und die (reelle) Funktion schaut im Zeitbereich aus wie in Fig. 5.

![komplexe Frequenz in der Gauß'schen Zahlenebene, $\underline s(t) \big|_{t=0}$](res/6-1.png){ width=85% }

![komplexes Signal im Zeitbereich](res/6-2.png)

\pagebreak

# 7. Widerstandsschaltung

Der Spannungsabfall am Lastwiderstand $R_L$ lässt sich beispielsweise durch Berechnen des Thevenin Äquivalents und doppelter Anwendung der Spannungsteilerregel durchführen. Der Vorteil dieser Methode ist dass gleich ein Ersatzwiderstand der den Innenwiderstand als ganzes repräsentiert berechnet werden kann.

Zunächst betrachten wir die Schaltung im Leerlaufzustand, wie in Fig. 6.

![Schaltung im Leerlaufzustand, $R_L$ nicht verbunden](res/7-1.png)

Dabei fließt über $R_2$ kein Strom, somit lässt sich mithilfe der Spannungsteilerregel in diesem Fall $U_{TH} = U_{R_3, \text{Leerlauf}}$ berechnen:

$$
\frac{U_{TH}}{U} = \frac{R_3}{R_1 + R_3} \implies U_{TH} = U \frac{R_3}{R_1 + R_3}
$$

$$
U_{TH} = 12 \text{V} \frac{2\text{k}\Omega}{2 \cdot 2\text{k}\Omega} = 12 \text{V} \cdot 0.5 = 6 \text{V}
$$

Mit folgendem Innenwiderstand:

$$
R_{TH} = (R_1 || R_3) + R_2 = \frac{1}{\frac{1}{2\text{k}\Omega} + \frac{1}{2\text{k}\Omega}} + 1\text{k}\Omega = 1\text{k}\Omega + 1\text{k}\Omega = 2\text{k}\Omega
$$

Mit diesen zwei Parametern können wir nun die Ersatzschaltung welche nun von $U_{TH}$ und $R_{TH}$ abhängt (Fig. 7) mithilfe erneuter Anwendung der Spannungsteilerregel analysieren.

![Schaltung mit Ersatzwiderstand $R_{TH}$ als Innenwiderstand](res/7-2.png)

Nämlich ergibt sich:

$$
\frac{U_{RL}}{U_{TH}} = \frac{R_L}{R_{TH} + R_L} \implies U_{RL} = U_{TH} \frac{R_L}{R_{TH} + R_L}
$$

$$
U_{RL} = 6 \text{V} \frac{2\text{k}\Omega}{2\cdot2\text{k}\Omega} = 6 \text{V} \cdot 0.5 = 3 \text{V}
$$

- Leerlaufspannung: die Spannung $U_{RL}$ wenn keine Last angeschlossen ist, dh. $R_L = \infty$. In diesem Fall fließt kein Strom über $R_L$ und $R_2$. Die Leerlaufspannung wäre hier unser $R_{R_3, \text{Leerlauf}} = 6 \text{V}$

- Innenwiderstand: unser $R_{TH} = 2\text{k}\Omega$

- Kurzschlussstrom: der Strom $I_{TH}$ welcher fließt, wenn $R_L = 0$. Daher, $I_{TH} = \frac{U_{TH}}{R_{TH}} = \frac{6 \text{V}}{2\text{k}\Omega} = 3 \text{mA}$

\pagebreak

# 8. Leistung in einem Widerstandsnetzwerk

Für dieses Beispiel berechnen wir zunächst die Leistung die im Widerstand $R_2$ umgesetzt wird. Die Ströme sind überall gleich, aber der allgemeine Spannungsabfall am Widerstand $R_2$ nicht. Um das Problem auf eine sinnvolle Art und Weise lösen zu können berechnen wir also zuerst den Spannungsabfall mithilfe der Spannungsteilerregel.

$$
\frac{U_2}{U} = \frac{R_2}{R_1 + R_2} \implies U_2 = U \frac{R_2}{R_1 + R_2}
$$

Die Leistung berechnet sich dann mit Hilfe des Ohm'schen Gesetzes und einsetzen unserer zuvor berechneten Spannung zu folgender Gleichung.

$$
\begin{aligned}
P_2 &= U_2 I \\
&= \frac{U_2^2}{R_2} \\
&= \left(U \frac{R_2}{R_1 + R_2}\right)^2 \frac{1}{R_2} \\
&= U^2 \frac{R_2^2}{(R_1 + R_2)^2} \frac{1}{R_2} \\
&= U^2 \frac{R_2}{(R_1 + R_2)^2} \\
\end{aligned}
$$

Mit diesem gewonnen Ausdruck können wir uns nun der Extremwertaufgabe widmen. Es gilt, $P_2$ zu maximieren mit $R_2$ als anpassbaren Parameter, also leiten wir den Ausdruck $P_2$ nach $R_2$ ab und berechnen mithilfe der Quotientenregel das Ergebnis. Für unsere Berechnungen treffen wir zunächst folgende Annahmen

$$
\{ R_2 = 150 \Omega, U \not = 0, R_2 > 0 \}
$$

$$
\begin{aligned}
\frac{dP}{dR_2} = 0 &= \frac{d}{dR_2} \left( U^2 \frac{R_2}{(R_1 + R_2)^2} \right ) \\
&= U^2 \frac{(R_1 + R_2)^2 - 2 R_2 (R_1 + R_2)}{(R_1 + R_2)^4} \\
&= U^2 \frac{(R_1 + R_2) - 2 R_2}{(R_1 + R_2)^3} \\
0 &= R_1 - R_2 \\
R_2 &= R_1 \\
\end{aligned}
$$

Der Widerstand $R_2$ muss also gleich groß wie $R_1$ dimensioniert werden. Damit ergibt sich folgende Leistung.

$$
P_2 = U^2 \frac{R_2}{(R_1 + R_2)^2} = U^2 \frac{1}{4R_1} = U^2 \frac{1}{600 \Omega}
$$

\pagebreak

# 9. Heizlüfter

Zunächst berechnen wir den Lastwiderstand $R_L$, der in der ersten Schaltung zwei mal parallel geschalten vorkommt. Dafür benutzen wir wieder die Formel für die Leistung.

$$
R_{total} = \frac{1}{\frac{1}{R_L} + \frac{1}{R_L}} = \frac{R_L}{2}
$$

$$
P = \frac{U^2}{R_{total}} = \frac{2 U^2}{R_L}
$$

Der Lastwiderstand kommt auf:

$$
3.5 \text{kW} = \frac{2\cdot230^2 \text{V}^2}{R_L} \implies R_L = \frac{2\cdot230^2 \text{V}^2}{3.5 \text{kW}} \approx 30.22857 \Omega
$$

Da die zwei Lastwiderstände parallel geschalten sind, teilt sich der Strom (in einem gleichmäßigen Verhältnis) auf die zwei Zweige auf:

$$
I_{R_L} = \frac{I}{2} = \frac{U}{2 R_{total}} = \frac{2 U}{2 R_L} = \frac{230 \text{V}}{R_L} \approx 7.6087 \text{A}
$$

Der Gesamtstrom in der parallelen Schaltung kommt auf:

$$
I = 2 I_{R_L} = \frac{U}{R_{total}} = \frac{2 U}{R_L} = \frac{2 \cdot 230 \text{V}}{R_L} \approx 15.21739 \text{A}
$$

Jetzt betrachten wir die serielle Schaltung. Der Widerstand bleibt $R_L$, ist aber nun in Serie geschalten. Der Gesamtwiderstand berechnet sich nun also zu:

$$
R_{total, Serie} = 2 R_L
$$

Die neue Leistung zu:

$$
P_{Serie} = \frac{U^2}{R_{total, Serie}} = \frac{U^2}{2 R_L} = \frac{2 \cdot 230^2 \text{V}^2}{2 R_L} = 875 \text{W}
$$

Der Gesamtstrom der seriellen Schaltung berechnet sich dann zu:

$$
I_{Serie} = \frac{U}{R_{total, Serie}} = \frac{U}{2 R_L} = \frac{230 \text{V}}{2 R_L} \approx 3.80435 \text{A}
$$

Hier ist schön ersichtlich dass der Gesamtstrom der parallelen Schaltung das vierfache des Gesamtstromes der seriellen Schaltung ergibt und nach weiterer Untersuchung bestätigt sich auch dass die Gesamtleistung in der seriellen Schaltung das vierfache der Gesamtleistung der parallelen Schaltung ergibt ($4\cdot 875 \text{W} = 3.5 \text{kW}$).

\pagebreak

# 10. Knotenregel

Durch Anwendung der Knotenregel erhält man die gewünschten Ströme.

### (a)

$$
\begin{aligned}
\Sigma i &= 0 \\
\implies 0 &= 2 \text{A} + 3 \text{A} - 1 \text{A} - i_a \\
\implies i_a &= 2 \text{A} + 3 \text{A} - 1 \text{A} \\
i_a &= 4 \text{A}
\end{aligned}
$$

### (b)

$$
\begin{aligned}
\Sigma i &= 0 \\
\implies 0 &= 1 \text{A} - 2 \text{A} + 3 \text{A} + 2 \text{A} - 1 \text{A} + i_b \\
\implies i_b &= -1 \text{A} + 2 \text{A} - 3 \text{A} - 2 \text{A} + 1 \text{A} \\
i_b &= -3 \text{A}
\end{aligned}
$$

### (c)

$$
\begin{aligned}
\Sigma i &= 0 \\
\implies 0 &= 1 \text{A} - 3 \text{A} + 4 \text{A} - i_c \\
\implies i_c &= 1 \text{A} - 3 \text{A} + 4 \text{A} \\
i_c &= 2 \text{A}
\end{aligned}
$$

\pagebreak

# 11. Spannungsteilerregel, Maschenregel

Die Maschenregel lautet $\Sigma u = 0$. Die Spannungsteilerregel nutzt das Verhältnis in einer Spannungsteilerschaltung aus, z.B. $\frac{U_{R_i}}{U_{total}} = \frac{R_i}{R_{total}}$, wobei $R_i$ in $R_{total}$ in Serie vorkommen muss.

Um nach $5$ Unbekannten aufzulösen, benötigen wir ein Gleichungssystem in min. $5$ Gleichungen. Eine Beispielkonfiguration bestehend aus Maschengleichungen $M_k$, $k \in [1, 3]$ und $S_l$, $l \in [1, 2]$ wäre wie folgt:

$$
\begin{aligned}
M_1&: &0 &= U_4 + U_{OUT} - U_0 \\
M_2&: &0 &= U4 + U_{CE} + U_3 - U_0 \\
M_3&: &0 &= U_{BE} + U_3 - U_1 \\
S_1&: &U_1 &= U_0 \frac{R_1}{R_1 + R_2} \\
S_2&: &U_2 &= U_0 \frac{R_2}{R_1 + R_2} \\
\end{aligned}
$$

Die Auflösung lässt sich dann wie folgt durchführen:

$$
\begin{aligned}
S_1&: &U_1 &= 11 \text{V} \frac{10 \text{k}\Omega}{10 \text{k}\Omega + 100 \text{k}\Omega} &=& 11 \text{V} \frac{1}{11} &= 1 \text{V} \\
S_2&: &U_2 &= 11 \text{V} \frac{100 \text{k}\Omega}{10 \text{k}\Omega + 100 \text{k}\Omega} &=& 11 \text{V} \frac{10}{11} &= 10 \text{V} \\
M_3&: &U_3 &= U_1 - U_{BE} &=& 1 \text{V} - 0.6 \text{V} &= 0.4 \text{V} \\
M_2&: &U4 &= U_0 - U_{CE} - U_3 &=& 11 \text{V} - 5 \text{V} - 0.4 \text{V} &= 5.6 \text{V} \\
M_1&: &U_{OUT} &= U_0 - U_4 &=& 11 \text{V} - 5.6 \text{V} &= 5.4 \text{V} \\
\end{aligned}
$$

\pagebreak

# 12. Widerstand in einem Fernsehgerät

In diesem Problem betrachten wir wieder eine Schaltung mit nur einem (Last-)widerstand und einer Leistung von $P_{MAX} = 0.25 \text{W}$.

$$
P = U \cdot I = \frac{U^2}{R_L} = \frac{I^2}{R_L}
$$

Somit ergeben sich die Spannung und der Strom:

$$
|U| = \left|\sqrt{P_{MAX} R_L}\right| \approx 7.4162 \text{V}
$$

$$
|I| = \left|\sqrt{\frac{P_{MAX}}{R_L}}\right| \approx 33.71 \text{mA}
$$

Diese Werte ergeben zusammenmultipliziert tatsächlich die Belastungsgrenze mit $0.25 \text{W}$.

\pagebreak

# 13. Ersatzwiderstände

In diesem Beispiel wendet man die üblichen Regeln zur Berechnung der Ersatzwiderstände an.

### (a)

$$
R_{a,ers} = 3 \Omega + \cfrac{1}{\cfrac{1}{12 \Omega} + \cfrac{1}{6 \Omega} + \cfrac{1}{4 \Omega}} = 5 \Omega
$$

### (b)

$$
R_{b,ers} = \frac{200 \Omega \cdot 50 \Omega}{200 \Omega + 50 \Omega} + \frac{120 \Omega \cdot 60 \Omega}{120 \Omega + 60 \Omega} = 60 \Omega
$$

### (c)

$$
R_{3,3,6} = 3 \Omega + \frac{3 \Omega \cdot 6 \Omega}{3 \Omega + 6 \Omega} = 5 \Omega
$$

$$
R_{c,ers} = \frac{20 \Omega \cdot R_{3,3,6}}{20 \Omega + R_{3,3,6}} = \frac{20 \Omega \cdot 5 \Omega}{20 \Omega + 5 \Omega} = 4 \Omega
$$

### (d)

$$
R_{d,ers} = \cfrac{1}{\cfrac{1}{18 \text{k}\Omega} + \cfrac{1}{6 \text{k}\Omega} + \cfrac{1}{1.5 \text{k}\Omega + 1.5 \text{k}\Omega}} = 1.8 \text{k}\Omega
$$

\pagebreak

# 14. RLC - Filter

### (a)

Wie sich später beim Ermitteln der Übertragungsfunktion noch zeigen wird, handelt es sich um einen (passiven) Tiefpass Filter 3. Ordnung.

### (b)

Zunächst betrachten wir, um das Maschenstromverfahren anzuwenden, wie viele Zweige und Knoten es gibt. Da $U_2 = U_{C_2}$, können wir diesen Zweig zunächst vernachlässigen. Weiters zu beachten ist, dass aufgrund von der Äquipotenzialfläche auf der unteren Seite der Schaltung es nur $2$ Knoten gibt (die Anschlüsse von Kondensator $C_1$).  Zweige sind es aber $3$. Daher benötigen wir $Z - (K - 1) = 2$ Gleichungen.

Wir legen also zwei Maschen in unser Netzwerk, nämlich durch Kondensator $C_1$ getrennt die linke und rechte Hälfte der Schaltung. Wir erhalten folgende Maschengleichungen $\{ M_1, M_2 \}$:

$$
\begin{aligned}
M_1&: &0 &= U_L + U_{C_1} - U_1 &=& U_1 = s L I_1 + \frac{1}{s C_1}(I_1 - I_2) \\
M_2&: &0 &= U_R - U_{C_2} - U_{C_1} &=& 0 =  \\
\end{aligned}
$$

Eingesetzt mit den entsprechenden Maschenströmen $\{ I_1, I_2 \}$ folgt:

$$
\begin{aligned}
M_1&: &U_1 &= s L I_1 + \frac{1}{s C_1}(I_1 - I_2) \\
M_2&: &0 &= R I_2 + \frac{1}{s C_2} I_2 - \frac{1}{s C_1} (I_1 - I_2) \\
\end{aligned}
$$

Nach Gruppierung nach den Maschenströmen:

$$
\begin{aligned}
M_1&: &U_1 &= I_1 \left ( s L + \frac{1}{s C_1} \right ) + I_2 \left ( -\frac{1}{s C_1} \right ) \\
M_2&: &0 &= I_1 \left ( -\frac{1}{s C_1} \right) + I_2 \left ( R + \frac{1}{s C_1} + \frac{1}{s C_2} \right ) \\
\end{aligned}
$$

In Matrixform erhalten:

$$
\left (
\begin{aligned}
& s L + \frac{1}{s C_1} & -\frac{1}{s C_1} &\\
& -\frac{1}{s C_1} & R + \frac{1}{s C_1} + \frac{1}{s C_2} &\\
\end{aligned}
\right ) \left (
\begin{aligned}
I_1& \\
I_2&
\end{aligned}
\right ) = \left (
\begin{aligned}
U_1& \\
0&
\end{aligned}
\right )
$$

Löst man nun dieses System nach dem Strom $I_2$ und setzt man sie in folgende Gleichung ein:

$$
U_2 = U_{C_2} = \frac{1}{s C_1} I_2
$$

So erhält man einen Term der unmittelbar in die Form $\underline{H}(s) = \frac{U_2}{U_1}$ gebracht werden kann:

$$
\underline{H}(s) = \frac{U_2}{U_1} = \frac{C_2}{C_1} \frac{1}{R L C_1 C_2 s^3 + L (C_1 + C_2) s^2 + C_2 R s + 1}
$$

### (c)

Für die Berechnung des Frequenzgangs $|\underline{H}(j \omega)|$ wenden wir die Transformation $s \to j \omega$ an und nehmen den Betrag davon. Angenommen alle Bauteilwerte sind positiv, so erhalten wir vereinfacht:

$$
|\underline{H}(j \omega)| = \frac{C_2}{C_1}\frac{1}{\sqrt{(L (C_1 + C_2) \omega^2 + 1)^2 + (C_1 C_2 L R \omega^3 - C_2 R \omega)^2}}
$$

\pagebreak

# 15. Integrator mit Operationsverstärker

Für diese (invertierende) Integrator Operationsverstärkerschaltung kann für einen idealen Operationsverstärker zunächst ein Eingangsruhestrom von $i_B = 0$ sowie eine Spannungsdifferenz der beiden Eingansanschlüsse des Operationsverstärkers von $u_d = 0$ angenommen werden.

Das Beispiel lässt sich nun sehr leicht lösen, nämlich entstehen zwei einfache Schleifen wenn man Kondensator $C$ und Widerstand $R_2$ zusammenfasst. Die Ersatzimpedanz $Z_{C,R_2}$ lautet aufgrund paralleler Zusammenschaltung:

$$
Z_{C,R_2} = \cfrac{\frac{1}{s C} R_2}{\frac{1}{s C} + R_2} = \frac{R_2}{s C R_2 + 1}
$$

Rechnet man nun beide $u_e$ sowie $u_a$ über beide Maschen die durch $u_d = 0$ verlaufen, da der Operationsverstärker den Knoten in eine virtuelle Masse verwandelt, so ergibt sich:

$$
\underline{V}(s) = \frac{U_a}{U_e} = -\frac{Z_{C,R_2}}{R_1} = -\frac{R_2}{s C R_1 R_2 + R_1} = -\frac{R_2}{R_1} \frac{1}{s C R_2 + 1}
$$

Rechnet man nun den Frequenzgang, unter Annahme positiver Bauteilwerte, so ergibt sich:

$$
\underline{V}(j \omega) = -\frac{R_2}{R_1} \frac{1}{|j \omega C R_2 + 1|} = -\frac{R_2}{R_1} \frac{1}{\sqrt{1 - (\omega C R_2)^2}}
$$

Dabei ist interessant was bei einer angelegten Gleichspannung $\omega = 0$ passiert:

$$
\underline{V}(0) = -\frac{R_2}{R_1} \frac{1}{\sqrt{1}} = -\frac{R_2}{R_1}
$$

\pagebreak

# 16. Passives Filter

Bei diesem Filter handelt es sich um eine zweifach kaskadierte Zusammenschaltung eines $RL$ Hochpassfilters. Normalerweise wäre hier zu erwarten, dass die beiden Übertragungsfunktionen der kaskadierten Filter sich einfach multiplizieren würde, aber dafür müsste um die Spannung im mittleren Knoten zwischen den beiden Widerständen effektiv zu übertragen ein Impedanzwandlers eingesetzt werden.

Zur Berechnung mithilfe der Spannungsteilerregel stellen wir zunächst eine Ersatzimpedanz $Z_{L,L,R}$ auf, welcher in die beiden Spulen und den Widerstand dazischen stehenden Widerstand zusammenfassen.

$$
Z_{L,L,R} = \frac{sL (s L + R)}{2 s L + R}
$$

Mithilfe der Spannungsteilerregel lässt sich nun direkt die Übertragungsfunktion $\underline{H}(s)$ bestimmen:

$$
\begin{aligned}
\underline{H}(s) &= \frac{Z_{L,L,R}}{R + Z_{L,L,R}} = \frac{\frac{sL (s L + R)}{2 s L + R}}{R + \frac{sL (s L + R)}{2 s L + R}} = \frac{s L (s L + R)}{2 s R L + R^2 + s L (s L + R)} \\
&= \frac{s^2 L^2 + s R L}{s^2 L^2 + 3 s R L + R^2}
\end{aligned}
$$

Wie zu erkennen ist unterscheidet sich die Übertragungsfunktion von derselben aber dafür rückwirkungsfreien Schaltung, welche nämlich wäre:

$$
\underline{H}_2(s) = \underline{H}_{RL} \cdot \underline{H}_{RL} = \left ( \frac{s L}{s L + R} \right )^2 = \frac{s^2 L^2}{s^2 L^2 + 2 s R L + R^2}
$$

Es handelt sich um einen *passiven kaskadierten Hochpassfilter 2. Ordnung*.

\pagebreak

# 17. Sallen-Key-Tiefpassfilter

Bei der Knotenpotenzialanalyse für diese Schaltung sind bereits Ströme und Knoten vorgegeben. Zwar ist kein Knoten für den Ausgang des Operationsverstärkers denotiert, aber wir rufen die zwei entscheidenden Merkmale des Operationsverstärkers in Erinnerung:

- Eingangsruhestrom $I_B = 0$
- Spannungsdifferenz $U_d = 0$

Mithilfe dieser Eigenschaften können wir nun auf die entscheidene Beziehung $U_2 = U_a$ schließen und für die beiden Knoten die Analyse durchführen. Wir stellen folgende Gleichungen auf:

$$
\begin{aligned}
K_1: I_1 &= I_2 + I_3 \\
K_2: I_2 &= I_4
\end{aligned}
$$

Setzen wir nun die Spannungen anhand der Knotenspannungen $\{ U_e, U_1, U_a \}$ ein erhalten wir folgende Gleichungen:

$$
\begin{aligned}
K_1: \frac{U_e - U_1}{R_1} &= \frac{U_1 - U_a}{R_2} + s C_2 (U_1 - U_a) \\
K_2: \frac{U_1 - U_a}{R_2} &= s C_1 (U_a - 0)
\end{aligned}
$$

Gleichung $K_2$ können wir leicht nach $U_1$ lösen:

$$
U_1 = U_a (s R_2 C_1 + 1)
$$

Setzen wir nun die Lösung von $U_1$ in Gleichung $K_1$ ein und gruppieren die Terme, so erhalten wir nach einigen Rechenschritten die Übertragungsfunktion:

$$
U_e = U_a \left ( (s R_2 C_1 + 1) \left ( \frac{R_1}{R_2} + s R_1 C_2 + 1 \right ) - \frac{R_1}{R_2} - s R_1 C_2 \right)
$$

$$
\underline{H}(s) = \frac{U_a}{U_e} = \frac{1}{s^2 R_1 R_2 C_1 C_2 + s C_1 (R_1 + R_2) + 1}
$$

\pagebreak

# 18. Matlab

Mit Matlab können um zu einem Filterdesign für einen Besselfilter und einen Cauerfilter zu kommen jeweils `besselap(5)` (Besselfilter 5. Ordnung) und `ellipap(5, 1, 50)` (Cauerfilter 5. Ordnung, 1 dB Ripple, 40 dB Sperrdämpfung) verwendet werden. Dabei leiten die von Matlab berechneten Systemfunktionen wie folgt:

$$
H_{Bessel}(s) = \frac{1}{s^5 + 3.811 s^4 + 6.777 s^3 + 6.886 s^2 + 3.936 s + 1}
$$
 
$$
H_{Cauer}(s) = \frac{0.01881 s^4 + 0.1271 s^2 + 0.1852}{s^5 + 0.9274 s^4 + 1.789 s^3 + 1.072 s^2 + 0.713 s + 0.1852}
$$

In Fig. 8 sowie Fig. 9 sind die jeweiligen Bode Diagramme des Bessel- und Cauerfilter ersichtlich.

In Fig. 10 sowie Fig. 11 befinden sich die Pol-/Nullstellendiagramme des Bessel- und Cauerfilter.

In Fig. 12 sowie Fig. 13 findet man die Sprungantwort des Bessel- und Cauerfilter.

\pagebreak

![Besselfilter Bode Diagramm](res/18-1.png){ width=100% }

![Cauerfilter Bode Diagramm](res/18-2.png){ width=100% }

![Besselfilter Pol-/Nullstellendiagramm](res/18-3.png){ width=100% }

![Cauerfilter Pol-/Nullstellendiagramm](res/18-4.png){ width=100% }

![Besselfilter Sprungantwort](res/18-5.png){ width=100% }

![Cauerfilter Sprungantwort](res/18-6.png){ width=100% }
