# Protokoll Spektren

## 2. Simulationen

### 2.0. Parameter der Simulationen

Aufgabe|Parameter|Wert|Einheit
:--|:--|--:|:--|--:
2.1 | $U$             | 1.2              | $V_{eff}$
2.1 | $f$             | 1                | $kHz$
2.1 | Fensterfunktion | Blackman-Fenster |    
2.2 | $U$             | 3.4              | $V_{ss}$
2.2 | $f$             | 400              | $Hz$
2.3 | $U_{Signal}$    | 0.8              | $V$
2.3 | $f_{Signal}$    | 500.0            | $Hz$
2.3 | $U_{Träger}$    | 1.8              | $V$
2.3 | $f_{Träger}$    | 25               | $kHz$
2.4 | $U_{e2}$        | 8.0              | $V$
Table: personenbezogene Simulationsparameter

### 2.1. Simulation eines Sinussignals im Zeitbereich und Transformation in den Spektralbereich mittels FFT

#### Aufgabenstellung

*In diesem Teil der Laborübung sollen sie die Funktionsweise der FFT für die Darstellung im Frequenzbereich anhand eines einfachen Sinussignals kennen lernen. Dabei werden sie erkennen, dass die Einstellungen der "Messung" auf das Signal abgestimmt werden müssen, um korrekte Aussagen bezüglich des Signals treffen zu können. Weiters soll der Zusammenhang zwischen dem Signal im Zeit- und Frequenzbereich grafisch dargestellt werden.*

**Hinweis**: *Amplituden werden in der Spektraldarstellung oft in dB angegeben, also in Dezibelbezogen auf eine Spannung von einem Volt (Effektivwert).* $A_{dB} = 20 \cdot log(\frac{U_V}{1V_{eff}})$. *Wenn sie eine Amplitude von 1V (Spitzenwert) für ihr Sinus-Signal wählen, ergibt das also in der dB-Skala für diese Spektrallinie einen Fourierkoeffizienten von -3dB.*

#### Parameter

Es werden die Werte aus Tab.1 für Aufgabe 2.1 übernommen.

#### Messwerte

Wir legen ein Sinussignal an, welches in (Fig.1) eingesehen werden kann. Dabei achten wir darauf, dass innerhalb des Zeitfensters $\Delta t = 3 ms$ exakt drei Perioden abgebildet werden können.

![Ausgangssignal](./res/2-1-sine.png)

Bei der Durchführung einer FFT, zunächst einmal mit einer Rechtecksfensterfunktion, ergibt sich (Fig.2). Dabei werden 4096 samples gewählt und das gesamte Zeitfenster $\Delta t$ analysiert.

![Ausgangssignal FFT (Rechtecksfenster)](./res/2-1-fft-square.png)

Zwar ist die dB-Skala im FFT gerechtfertigt, aber wir betrachten dennoch zunächst eine lineare Darstellung derselben FFT Ergebnisse (Fig.3).

![Ausgangssignal FFT (Rechtecksfenster) linear](./res/2-1-fft-square-linear.png)

Es sieht stark danach aus, als hätte unser Signal hauptsächlich Anteile in der Umgebung von einer Frequenz. Die Frequenzen strecken sich von $1.6666667 Hz$ bis $1.3333333 Hz$, wir errechnen daher ein Fenster von $\Delta \omega = \frac{2}{3} kHz$. Weiters ermittlen wir die Werte an der Spitze des Ergebnisses, mit einer Amplitude von $848.41 mV$ bei einer Frequenz von $1 kHz$.

Als nächstes führen wir eine FFT mit denselben Parametern durch, bis auf das Zeitfenster, welches wir als $2.5 \Delta t$ wählen (Fig.4).

![Ausgangssignal FFT (Rechtecksfenster) linear Zeitfenster $2.5 \Delta t$](./res/2-1-fft-square-linear-elongated.png)

Ändern wir nun die Fensterfunktion auf ein Blackman-Fenster, so ergibt sich (Fig.5).

![Ausgangssignal FFT (Blackman-Fenster) linear Zeitfenster $2.5 \Delta t$](./res/2-1-fft-blackman-linear-elongated.png)

Als nächstes betrachten wir die FFT mit 4096 Punkten und einer Rechtecksfensterfunktion eines 20 Perioden Sinussignals (Fig.6).

![Ausgangssignal FFT (Rechtecksfenster) linear, $20 T$](./res/2-1-fft-long-square-linear.png)

Und vergleichen es mit der FFT mit denselben Einstellungen aber einem Sinussignal mit 19.5 Perioden (Fig.7).

![Ausgangssignal FFT (Rechtecksfenster) linear, $19.5 T$](./res/2-1-fft-long-square-linear-distorted.png)

Schließlich führen wir noch eine FFT mit denselben Einstellungen durch, aber mit einer Blackman Fensterfunktion, auch für ein Sinussignal mit 19.5 Perioden (Fig.8).

![Ausgangssignal FFT (Blackman-Fenster) linear, $19.5 T$](./res/2-1-fft-long-blackman-linear-distorted.png)

#### Ergebnisse

Interessant ist es auch, sich Überlegungen über mögliche Abtastfrequenzen zu machen. Wir stellen den Zeitabstand zwischen individueller Messungen fest, indem wir die Abtastmenge und das Zeitfenster in Relation stellen:

$$ T_S = \frac{T_0}{N} = \frac{3 ms}{4096} \approx 732.42 ns $$

Mit Abtastungen im Abstand von unter $1 \mu s$ voneinander stehen wir auf der sicheren Seite. Überlegen wir uns nämlich, welche Abtastrate wir mindestens benötigen, um nach dem Shannon Sampling Theorem unser Signal im Zeitbereich exakt rekonstruiren zu können, so brauchen wir zumindest eine Abtastrate $> 2 f_{max}$.

$$ \frac{1}{T_S} = \frac{1}{732.42 ns} = 1.365 MHz > 2 f_{max} = 2 \cdot 1 kHz $$

Es ist also sichergestellt, dass wir genügend Abtastungen haben.

Aus unseren Messungen für den Peak stellen wir fest: den größten Beitrag zu unserem Signal hat eine Sinusschwingung mit einer Frequenz von $1 kHz$. Das deutet schon darauf, dass unser Signal ursprünglich eine Sinusschwingung war. Hätten wir statt einer FFT eine normale Fourier Analyse des kontinuierlichen Signals gemacht, dann wäre das Ergebniss ein Impuls genau bei $1 kHz$, mit einer Amplitude von 1. Aber diese idealisierte Umgebung haben wir wie aus dem theoretischen Hintergrund für diese Übung hervorgeht leider nicht, daher haben wir auch genügend andere Frequenzanteile.

In (Fig.4) ergibt sich etwas ähnliches wie bereits im theoretischen Hintergrund für diese Übung gezeigt wird: unsere Daten gibt es nur im Intervall $[0, 3 ms]$, aber wir betrachten ein Signal welches sich über $2.5 \Delta t$ streckt. Bei einer perioden Fortsetzung wäre also im Zeitbereich gar keine Amplitude, nämlich $2.5 \Delta t - \Delta t = 4.5 ms$ (im Vergleich zu $3 ms$!). Das führt dazu, dass das Signal stark an niedrigfrequenten Anteilen gewinnt, weil nämlich in Abständen von $4.5 ms$ die Amplitude sich erst wieder ändert.

Wie aus (Fig.5) hervorgeht, haben mit einem Blackman-Fenster die niedrigen Frequenzen keine so hohe Amplitude mehr. Außerdem werden die Frequenzen in der Umgebung von $1 kHz$ (unser eigentliches Signal) hervorgehoben in einem kleinen lokalen Maximum. Die Blackman-Fenster Funktion liefert also eine Darstellung mit der viel leichter die Ausgangssituation ermittelt werden kann. Eine schnelle Recherche ergibt, dass das Blackman-Fenster als Fensterfunktion für die "sidelobes" (äußeren Segmente des Spektrums) einen relativ starken Abfall (18 dB/oct) beherbergt. Dafür gibt es überall sonst (inneren Segmente des Spektrums) einen etwas schwächeren Abfall (6 dB/oct).

In (Fig.6) lässt sich erkennen, dass unsere Genauigkeit steigt, je mehr Perioden wir unter Betracht ziehen. Wichtig aber ist, dass unser Signal exakt periodisch fortgesetzt werden kann. Ist das nicht mehr der Fall, wie für die FFT in (Fig.7) und (Fig.8), so bekommen wir Artefakte in unseren Ergebnissen. Mit einer Rechtecksfunktion erkennen wir für das Sinussignal mit 19.5 Perioden signifikante Anteile für Frequenzen $< 1 kHz$, während bei einer Blackmanfensterfunktion diese fast zur Gänze reduziert werden.

Kurz rekapituliert, die implizite Annahme, es würde ein periodisches Signal vorliegen, welches wir für die FFT annehmen müssen, führt unerwünschte Effekte mit sich. Unter anderem ist Leaking ein Begriff, welches beschreibt, wie sehr eigentlich Frequenzanteile an Nachbarn verteilt werden. Aus (Fig.7) und (Fig.8) erkennt man keine eindeutige Spitze bei $1 kHz$ mehr, und die Amplitude insgesamt ist niedriger, genau wegen dem Leaking Effekt. Fensterfunktionen helfen uns, Leaking zu bekämpfen. So kann der relativ dramatische Effekt in (Fig.7), wo nur mehr ungefähr 550 mV an $1 kHz$ vorliegen (!) reduziert werden auf ein engeres Frequenzband mit insgesamt höherer Amplitude (ungefähr 750 mV) in (Fig.8).

### 2.2. Untersuchung eines Rechtecksignals

#### Aufgabenstellung

*Im zweiten Teil dieser Übung soll ein Rechtecksignal im Frequenzbereich untersucht und vermessen werden.*

#### Parameter

Es werden die Werte aus Tab.1 für Aufgabe 2.2 übernommen.

#### Messwerte

Zu aller erst legen wir ein Rechtecksignal mit Duty Cycle 50% und einer Frequenz $f = 400 Hz$ an. $V_{SS}$ beträgt 3.4 V. Dafür berechnen wir die Periode $T = 0.0025 s$ sowie andere Parameter. Nun brauchen wir ein Zeitfenster, in welchem das Signal sich oft genug wiederholen kann, aber wir mit 4096 Abtastpunkten immer noch zurechtkommen. Schließlich kommen wir auf ein Zeitfenster $\Delta t = 2 \cdot 10^2 \cdot T = 0.5 s$. Führen wir die Berrechnungen welche wir in 2.1 bereits durchgeführt haben noch einmal durch, so kommen wir auf:

$$ T_S = \frac{\Delta t}{N} = \frac{0.5 s}{4096} = 122.07 ms $$

$$ \frac{1}{T_S} = \frac{1}{122.07 ms} = 8192 Hz > 2 \cdot f_{max} = 800 Hz $$

Außerdem verwenden wir ein ganzzaliges Vielfaches der Periodendauer um Leaking zu vermeiden. Das sich daraus ergebende FFT mit Parametern N = 4096 (Rechtecksfensterfunktion) findet sich in (Fig.9).

![Ausgangssignal FFT (Rechtecksfenster) linear, $10^3 T$](./res/2-2-fft-square.png)

Wird die x-Achse linear dargestellt, so ergibt sich (Fig.10).

![Ausgangssignal FFT (Rechtecksfenster) linear, $10^3 T$, lineare x-Achse](./res/2-2-fft-square-x-linear.png)

Es ergeben sich folgende Messwerte:

||f||U||
|:--|--:|:--|--:|:--|
|Grundfrequenz    |400|Hz|1.48|V|
|1. Oberschwingung|1.2|kHz|398.58|mV|
|2. Oberschwingung|2|kHz|169.82|mV|
|3. Oberschwingung|2.8|kHz|79.33|mV|
Table: Messwerte

#### Ergebnisse

Wie aus (Fig.10) sowie Tab.2 ablesbar ist, hat unsere ursprüngliche Frequenz (Grundfrequenz) die höchste Amplitude mit 1.48 V. An bestimmten Vielfachen der Grundfrequenz kommt es zu einem Oberschwingen. Es wurden in Tab.2 die ersten drei signifikanten Oberschwingungen herausgehoben. Spannenderweise lässt sich ein Muster erkennen: $1.2 kHz / 400 Hz = 3$, $2 kHz / 400 Hz = 5$, $2.8 kHz / 400 Hz = 7$. Genau an ungeraden Vielfachen kommt es zu bedeutsamen Oberschwingungen.

Aufgrund der Unstetigkeit eines Rechtecksignals, kann es keine einzelne Spektrallinie für das gesamte Signal geben. Daher ist eine ganze Bandbreite an Spektrallinien notwendig, um ein derartiges Rechtecksignal abzubilden. Bei einer Fourierreihenbildung würde das bedeuten, dass wir unbeschränkt viele Koeffizienten benötigen würden, um unser Signal zu repräsentieren. Ein theoretisches Rechtecksignal ist ideal und ändert von einem Zustand in den anderen in keiner Zeit. Ein derartiges Signal im Frequenzbereich darzustellen ist unmöglich, weil wir eine unendliche Bandbreite benötigen würden. Dem zufolge würden die Amplituden an den jeweiligen ungeraden Vielfachen der Grundfrequenz nicht abnehmen, weil wir sonst keine unendliche Bandbreite benötigen würden. Weil es sich um eine Simulation handelt und wir kein ideales Signal vorliegen haben, nimmt die Amplitude bei unserer Simulation stetig ab, bis sie schließlich insignifikant wird.

### 2.3. Amplitudenmodulation

#### Aufgabenstellung

*Bei der Amplitudenmodulation wird die Amplitude eines hochfrequenten Trägersignal* $U_{carrier}$ *abhängig von einem niederfrequenten Nutzsignal* $U_{info}$ *verändert. Dabei wird das Nutzsignal zusammen mit einem passenden Gleichanteil* $U_0$ *mit dem Trägersignal multipliziert. Die Aufgabe besteht darin ein solches Signal zu messen und zu berechnen.*

$\rightarrow$ *Bei fehlendem Gleichanteil* $U_0$ *wird das Trägersignal unterdrückt, d.h. die Spektrallinie des Trägers verschwindet im Frequenzbereich ... sie können dieses Faktum durch Berechnung mit Hilfe der Summensätze leicht überprüfen!*

$$ U_{info}(t) = \hat U_{info} \cdot cos(\omega _i t) $$
$$ U_{carrier}(t) = \hat U_{carrier} \cdot cos(\omega _c t) $$
$$ U_{AM}(t) = [ U_0 + U_{info}(t) ] \cdot U_{carrier}(t) $$

#### Parameter

Es werden die Werte aus Tab.1 für Aufgabe 2.3 übernommen.

#### Schaltung

Folgende Schaltung wird für die Simulation verwendet (Fig.11).

![Signale](./res/2-3-circuit.png)

#### Messwerte

Es wurde ein Zeitfenster $\Delta t = 4 T = 4 \cdot \frac{1}{500 Hz} = 8 ms$ gewählt. Interessant ist natürlich den Effekt der Modulation zu observieren (Fig.12).

![Signale im Zeitbereich](./res/2-3-am-modulation.png)

Jetzt führen wir für das AM Signal eine FFT durch (4096 Punkte, Rechtecksfensterfunktion), wie ersichtlich in (Fig.13).

![AM Signal im Frequenzbereich](./res/2-3-am-fft-linear.png)

Die interessante Stelle mit den höchsten Amplituden wollen wir uns in (Fig.14) etwas genauer ansehen.

![AM Signal im Frequenzbereich](./res/2-3-am-fft-linear-zoom.png)

Anschließend ermitteln wir noch die Amplituden der drei Spitzen in Tab.3.

|Frequenz||Amplitude||
|--:|:--|--:|:--|
|24.5|kHz|489.56|mV|
|25|kHz|1.21|V|
|25.5|kHz|487.04|mV|
Table: Messwerte

Dieselben Simulationsexperimente können wir ebenfalls für ein Rechteckssignal mit identischen Parametern durchführen, wie in (Fig.15) und (Fig.16) abgebildet.

![Signale im Zeitbereich (Rechteck)](./res/2-3-am-modulation-square.png)

![AM Signal im Frequenzbereich (Rechteck)](./res/2-3-am-fft-linear-square.png)

#### Fourier-Analyse

des Nutzsignals. Wir modellieren das Nutzsignal mit $f(t) = 1 + 0.8 \cdot cos(500t)$, wobei $f(t)$ $T = 2 \pi$ periodisch ist. Dann lässt sich $f(t)$ durch eine Fourierreihe abbilden:

$$ f(t) = \frac{a_0}{2} + \sum_{n = 1}^{\infty} a_n cos(n \omega t) + b_n sin(n \omega t) $$

Die Fourierkoeffizienten $a_n, b_n$ sind dann gegeben durch die Formeln von Euler-Fourier:

$$ a_n = \frac{2}{T} \int_{0}^{T} f(t) \cdot cos(n \omega t) dt $$

$$ b_n = \frac{2}{T} \int_{0}^{T} f(t) \cdot sin(n \omega t) dt $$

Da $f(t)$ gerade ist, d.h. $f(t) = f(-t)$, gilt $\forall n > 0 : b_n = 0$. Uns bleibt nur noch die Berechnung von den Fourierkoeffizienten $a_n$:

$$
\begin{aligned}
 a_n &= \frac{2}{2 \pi} \int_{0}^{2 \pi} (1 + 0.8 \cdot cos(500t)) \cdot cos(n \omega t) dt\\
     &= \frac{1}{\pi} \left( \int_{0}^{2 \pi} cos(n \omega t) dt + 0.8 \cdot \int_{0}^{2 \pi} cos(500t) \cdot cos(n \omega t) dt \right)
\end{aligned}
$$

Unter Anwendung des trigonometrischen Summensatzes

$$ cos x \cdot cos y = \frac{1}{2}(cos(x + y) + cos(x - y)) $$

kommen wir auf folgende Berechnung:

$$
\begin{aligned}
 a_n &= \frac{1}{\pi} \left( \int_{0}^{2 \pi} cos(n \omega t) dt + 0.8 \cdot \int_{0}^{2 \pi} cos(500t) \cdot cos(n \omega t) dt \right)\\
     &= \frac{1}{\pi} \left( \left[ sin(n \omega t) \frac{1}{n \omega} \right]_0^{2 \pi} + 0.8 \left( \cdot \int_{0}^{2 \pi} cos(t(n \omega - 500)) dt + \int_{0}^{2 \pi} cos(t(n \omega + 500)) dt \right) \right)\\
     &= \frac{0.8}{\pi} \left( \int_{0}^{2 \pi} cos(t(n \omega - 500)) dt + \int_{0}^{2 \pi} cos(t(n \omega + 500)) dt \right)\\
     &= \frac{0.8}{\pi} \left( \left[ \frac{sin(t(n \omega - 500))}{n \omega - 500} \right]_0^{2 \pi} + \left[ \frac{sin(t(n \omega + 500))}{n \omega + 500} \right]_0^{2 \pi} \right)\\
     &= \frac{0.8}{\pi} \left( \frac{sin(2 \pi(n \omega - 500))}{n \omega - 500} + \frac{sin(2 \pi(n \omega + 500))}{n \omega + 500} \right)
\end{aligned}
$$

Insbesondere gilt $a_0 = 0$.

Um diese Fourierreihe zu analysieren, betrachten wir die Koeffizienten $a_n$. Wir definieren eine Funktion die uns bei dieser Analyse helfen soll, $A(n, \omega) = a_n$. Wir bemerken, dass $\forall n > 1 : A(n, \omega) = 0$, $\forall \omega \not = 500 : A(n, \omega) = 0$, $A(1, 500) = undefiniert$ bzw. $A(1, 500) = \infty$. Das entspricht einem Dirac-Impuls genau an der Frequenz $\omega = 500$, was nicht überraschend ist, weil unser Nutzsignal eine Kosinusschwingung mit genau dieser Frequenz ist.

#### Ergebnisse

In (Fig.14) erkennen wir die Anatomie unseres AM modulierten Signals. Dabei gibt es einen Peak (bei der Trägerfrequenz) und zwei "Gipfel" nebenan. Um das zu erklären betrachten wir die mathematische Seite des Problems. Nämlich multiplizieren wir im Zeitbereich unser Nutzsignal mit dem Trägersignal, was im Frequenzbereich dazu führt, dass die Spektren gefaltet werden. Dabei ist das Trägersignal, eine perfekte Sinusschwingung, im Frequenzbereich ein Impuls an seiner Grundfrequenz. In realen Schaltungen schaut der Impuls nicht wie ein Impuls aus, sondern ein Dreieck, aus Gründen die bei früheren Aufgaben bereits festgestellt worden sind. Jetzt ist das Nutzsignal im Frequenzbereich irgendeine Funktion in der Gegend von der Frequenz 0, wir sagen der Beginn der Bandbreite des Nutzsignals ist $\omega _d$ von der Frequenz 0 entfernt. Aufgrund der Faltung im Frequenzbereich scheint die Bandbreite des Nutzsignals um diese Distanz $\omega _d$ vom Peak des Trägersignals auf. Das bedeutet, rechts von unserem Trägersignal im Frequenzbereich erscheint die gesamte Bandbreite des Nutzsignals.

Betrachten wir nun kurz negative Frequenzen. Wird die Frequenzachse ins Negative fortgeführt, so sehen wir ein Spiegelbild (gespiegelt um die y-Achse) der Bandbreite. Das bedeutet, $\omega _d$ weit von der Frequenz 0 im Bereich der negativen Frequenzen erscheint die gesamte Bandbreite gespiegelt. Für normale Zwecke ist die Bandbreite im negativen Bereich der Frequenzen imaginär, aber genau dieses Spiegelbild erscheint bei unserer Faltung. Wir haben bereits etabliert, dass rechts in einer Distanz $\omega _d$ vom Peak des Trägersignals unsere Bandbreite aufscheint, aber links erscheint in derselben Distanz das Spiegelbild dieser Bandbreite. Was davor abstrakt war wird plötzlich real.

Nun schauen wir auf die Amplitudenwerte, insbesondere ziehen wir die Messwerte aus Tab.3 heran. Wie wir erkennen können, sind die Amplituden der Frequenzanteile unseres Nutzsignals bei jeweils ungefähr 500 mV und die Amplitude des Trägersignals ungefähr bei 1.2 V. Aufgrund der großen Verteilung an benachbarte Frequenzen haben wir relativ hohe Verluste. Zwar hat das Trägersignal im Zeitbereich eine Amplitude von 1.8 V aufgewiesen, das Nutzsignal an der Spitze ebenfalls 1.8 V.

Für einen guten Vergleich zwischen Theorie und Simulation, betrachten wir auch das Ergebnis der Fourier-Analyse. Idealerweise kommt für das Nutzsignal ein Spektrum zustande, in welchem $\forall \omega \not = 500 : f(\omega) = 0$. Wird aber für das Nutzsignal eine FFT durchgeführt, so ist das Ergebnis eine Verteilung auf etliche Frequenzen in der Gegend von $\omega = 500$, wobei aber die Amplitude für $\omega = 500$ immer noch am höchsten ist. Speziell an (Fig.14) lässt sich das Spektrum des Nutzsignals observieren, an den beiden "Gipfel" links und rechts vom "Peak".

Ein Gleichspannungsanteil von $U_0 \geq 1$ wird beim Nutzsignal angelegt, damit das Amplitudenmodulierte Signal an jeder Stelle im Zeitbereich zumindest die Amplitude des Trägersignals besitzt. Wird nicht zumindest ein Gleichspannungsanteil von 1 gewählt, so gehen im modulierten AM Signal Anteile des Nutzsignals verloren, was nicht mehr bei der Demodulation rückgängig gemacht werden kann. Eine derartige Verzerrung des Nutzsignals wollen wir verhindern.

Wie sich erkennen lässt ist im Frequenzbereich für das AM modulierte Rechtecksignal eine sehr weite Bandbreite notwendig, weil genügend Amplitudenanteile auf benachbarten Frequenzen aufgeteilt werden. Das bedeutet, wir benötigen niedrigere und höhere Frequenzen für unser Signal. Ein AM moduliertes Rechteckssignal mit unterschiedlicher Trägerfrequenz wird trotzdem eine vergleichsweise weite Bandbreite benötigen. Jetzt kann es passieren, dass die Bandbreiten der beiden Signale überlappen, was zu einer Interferenz führt. Die Amplitudenanteile für eine bestimmte Frequenz könnte beispielsweise durch ein anderes Nutzsignal hervorgerufen werden, sodass beim Empfangen des AM modulierten Signals Amplitudenanteile aufkommen, die ursprünglich gar nicht dabei sein hätten sollen. In der Literatur wird dies als *Übersprechen* (*Crosstalk*) bezeichnet.

Weil unser Signal mit einem hochfrequenten Sinussignal (Trägersignal) moduliert wird, hat das resultierende AM modulierte Signal natürlicherweise eine höhere Amplitude für die Frequenzen des hochfrequenten Sinussignals. Weil es sich aber um ein Sinussignal handelt, sind die Anteile des Trägersignals nur auf eine sehr enge Bandbreite verteilt. Aus diesem Zweck können wir bei der Demodulation, um diese Anteile zu entfernen, einen Tiefpassfilter verwenden, da die Nutzsignale wenige derartig hochfrequente Anteile haben. Dabei filtern wir das AM modulierte Signal.

### 2.4. Brückengleichrichter

#### Aufgabenstellung

*Ein Brückengleichrichter dient der Erzeugung von Gleichspannung aus Wechselspannung. Im Gegensatz zur Gleichrichtung mit einer einzelnen Diode können mit dem Brückengleichrichter sowohl die positive, als auch die negative Sinus-Halbwelle genutzt werden. Die Ausgangsspannung entspricht dabei* - ***sofern ideale Bauteile (Dioden) angenommen werden*** - *dem Betrag der Eingangsspannung (negative Halbwellen nach "oben geklappt"). Um das Spektrum des Ausgangssignals zu berechnen, müssen sie also die Fourierreihe von* $|sin(\omega t)|$ *berechnen. Hinweise zur Berechnung:*

- $|sin(\omega t)|$ *ist periodisch mit Periodendauer* $T=\frac{1}{2f}$ ; $f=\omega 2 \pi$
- $|sin(\omega t)|$ *ist gleich* $sin(\omega t)$ $\mid$ *für* $t \in [0, T]$
- $cos x = \frac{1}{2}(e^{jx} + e^{-jx})$
- $sin x = \frac{1}{2j}(e^{jx} - e^{-jx})$

#### Parameter

Es werden die Werte aus Tab.1 für Aufgabe 2.4 übernommen.

#### Schaltung

Dabei ergibt sich folgende für die Simulation zu verwendende Schaltung (Fig.17). Soll zum Messen ein Voltmeter benutzt werden, so können wir die Schaltung ergänzen, wie abgebildet in (Fig.18).

![Brückengleichrichter](./res/2-4-circuit.png)

![Brückengleichrichter mit Voltmeter](./res/2-4-circuit-2.png)

#### Messwerte

Wir simulieren initiell im Zeitbereich für eine Frequenz $f = 100 Hz$, mit der Versorgungsspannung $\hat U_{e1} = 1 V$. Wir wollen 4 volle Perioden simulieren, daher wählen wir $\Delta t = 4 \cdot \frac{1}{100} = 40 ms$. Die resultierende Spannung $U_a$ lässt sich dann aus (Fig.19) ablesen. Führen wir die Simulation ein zweites Mal mit dem personenbezogenen Eingangsspannungswert $\hat U_{e2} = 8 V$ durch, so erhalten wir das Ergebnis aus (Fig.20).

![Systemantwort im Zeitbereich, $\hat U_{e1} = 1 V$](./res/2-4-1v-100hz.png)

![Systemantwort im Zeitbereich, $\hat U_{e2} = 8 V$](./res/2-4-8v-100hz.png)

Wird für die zweite Simulation zusätzlich eine FFT durchgeführt, so ergibt sich die Antwort im Frequenzbereich aus (Fig.21). Dabei werden die 5 prominentesten Frequenzen und ihre Amplituden in Tab.4 festgehalten. Die Einstellungen sind dabei 4 volle Perioden, 4096 Punkte und ein Rechtecksfenster.

![Systemantwort im Frequenzbereich, $\hat U_{e2} = 8 V$](./res/2-4-fft-ua.png)

|Frequenz||Amplitude||
|--:|:--|--:|:--|
|200|Hz|2.24|V|
|400|Hz|372.26|mV|
|600|Hz|116.49|mV|
|800|Hz|41.32|mV|
|1.4|kHz|10.88|mV|
Table: Messwerte

#### Fourier-Analyse

des Gleichrichtungssignals. Das Signal ist gegeben durch $f(t) = |sin(\omega t)|$, wobei $f(t)$ $T = \frac{1}{2f} = \frac{\pi}{\omega}$ periodisch ist, da $f = \frac{\omega}{2 \pi}$. Dann lässt sich $f(t)$ durch eine Fourierreihe abbilden:

$$ f(t) = \frac{a_0}{2} + \sum_{n = 1}^{\infty} a_n cos(n \omega t) + b_n sin(n \omega t) $$

Die Fourierkoeffizienten $a_n, b_n$ sind dann gegeben durch die Formeln von Euler-Fourier:

$$ a_n = \frac{2}{T} \int_{0}^{T} f(t) \cdot cos(n \omega t) dt $$

$$ b_n = \frac{2}{T} \int_{0}^{T} f(t) \cdot sin(n \omega t) dt $$

Da $f(t)$ gerade ist, d.h. $f(t) = f(-t)$, gilt $\forall n > 0 : b_n = 0$. Uns bleibt nur noch die Berechnung von den Fourierkoeffizienten $a_n$:

*Hinweis: um* $\omega$ *der ursprünglichen Funktion und der Fourierreihe nicht zu verwechseln, wird stattdessen* $\nu$ *verwendet.*

\pagebreak

$$
\begin{aligned}
 a_n &= \frac{\omega}{\pi} \int_{0}^{T} |sin(\omega t)| \cdot cos(n \nu t) dt\\
     &= \frac{\omega}{\pi} \int_{0}^{T} sin(\omega t) \cdot cos(n \nu t) dt
\end{aligned}
$$

da $\forall t \in [0, T] : |sin(\omega t)| = sin(\omega t)$. Unter Anwendung der trigonometrischen Produktidentität:

$$ sin x \cdot cos y = \frac{1}{2}(sin(x + y) + sin(x - y)) $$

kommen wir auf folgende Berechnung:

$$
\begin{aligned}
 a_n &= \frac{\omega}{\pi} \int_{0}^{T} sin(\omega t) \cdot cos(n \nu t) dt\\
     &= \frac{\omega}{2 \pi} \left( \int_{0}^{T} sin(\omega t + n \nu t) dt + \int_{0}^{T} sin(\omega t - n \nu t) dt \right)\\
     &= - \frac{\omega}{2 \pi} \left( \left[ \frac{cos(\omega t + n \nu t)}{\omega + n \nu} \right]_0^T + \left[ \frac{cos(\omega t - n \nu t)}{\omega - n \nu} \right]_0^T \right)\\
     &= - \frac{\omega}{2 \pi} \left( \frac{1}{\omega + n \nu} \left( cos \left(\frac{\omega}{\omega} \pi + n \frac{\nu}{\omega} \pi \right) - cos \left( 0 \right) \right) + \frac{1}{\omega - n \nu} \left( cos \left(\frac{\omega}{\omega} \pi + n \frac{\nu}{\omega} \pi \right) - cos \left( 0 \right) \right) \right)\\
     &= - \frac{\omega}{2 \pi} \left( \frac{1}{\omega + n \nu} \left( cos \left(\pi + n \frac{\nu}{\omega} \pi \right) - 1 \right) + \frac{1}{\omega - n \nu} \left( cos \left(\pi + n \frac{\nu}{\omega} \pi \right) - 1 \right) \right)\\
     &= - \frac{1}{2 \pi} \left( \frac{1}{1 + n \frac{\nu}{\omega}} \left( cos \left(\pi + n \frac{\nu}{\omega} \pi \right) - 1 \right) + \frac{1}{1 - n \frac{\nu}{\omega}} \left( cos \left(\pi + n \frac{\nu}{\omega} \pi \right) - 1 \right) \right)\\
\end{aligned}
$$

Insbesondere gilt $\forall \omega \not = 0, \omega \in \mathbb{R}, \nu \in \mathbb{R}: a_0 \approx 0.637$.

Um das Ergebnis wirklich nachvollziehen zu können, müssen wir die Dimensionalität einschränken. Gerade liegen 3 Freiheitsgrade vor, weil es zu jedem $n$, $\omega$ und $\nu$ Koeffizienten $a_n$ gibt. Wir schaffen zunächst einen Zusammenhang zu unseren Messwerten aus Tab.4, in dem wir zunächst alle Frequenzen $\omega \in \{ 200, 400, 600, 800, 1000 \}$ betrachten. Dafür parametrisieren wir unsere Koeffizienten als $a(n, \omega, \nu) = a_n$. Bilden wir Funktionen $f_m(x) = a(m, \omega, x)$ sodass $m \in [1, 5] \subset \mathbb{N}$, $\omega \in \{ 200, 400, 600, 800, 1000 \}$, dann bekommen wir die Funktionen aus (Fig.22).

![Funktionen $f_m(x)$](./res/2-4-an-distribution.png)

Die Funktionswerte liegen spannenderweise konstant bei ca. $0.638$, für alle Funktionen $f_m(x)$.

#### Ergebnisse

Wie sich aus (Fig.19) und (Fig.20) erkennen lässt, sieht die Antwort bei niedrigeren Eingangsspannungen $U_e$ problematisch aus. Nämlich ist das Ziel, eine Näherung der Funktion $f(t) = |sin(n \omega)|$ zu erreichen, jedoch weichen wir von diesem Ziel stark ab. Für die Eingangsspannung $\hat U_{e2}$ sieht die Antwort schon viel eher unserem Ideal entsprechend aus. Dennoch finden sich Imperfektionen, die Amplitude steigt und fällt im Allgemeinen schneller als gewünscht. 

Da unsere Dioden nicht ideal sind, sperrt unsere Brückengleichrichterschaltung nicht ganz wie gewünscht, es werden minimalst Ströme durchgelassen. Bei niedrigen Eingangsspannungen fällt das natürlich auf, weil die Verluste prozentuell einen großen Teil der gesamten Leistung ausmachen. Bei höheren Eingangsspannungen fällt das weniger auf, weil die Verluste prozentuell einen kleineren Teil der gesamten Leistung ausmachen.

Interessant ist auch das Verhalten an den Viertelperioden, die "Täler" der Schwingung. In einem idealen Szenario haben wir eine exakte Sinusgviertelschwingung, bis die Ausgangsspannung dann 0 erreicht und wieder sofort ansteigt. Nimmt man die erste Ableitung von $f(t) = |sin(\omega t)|$, so merkt man, dass die Ableitung $f'(t)$ an den Viertelperioden $t = n \omega \pi$ nicht stetig ist. Insofern müsste nach einer Viertelperiode die Ausgangsspannung "sofort" schalten. Wir observieren aber insbesondere in (Fig.19) und (Fig.20) breitere "Täler", weil die Dioden nicht "sofort" schalten können. Haben Dioden vorher gesperrt, so sind die Sperrbereiche größer als sie wären, wenn kein Sperrstrom geflossen wäre. Diese Zeit die eine Diode zum schalten vom Sperrstrom in den leitenden Berreich seiner I/U Kennlinie benötigt könnte mitunter beeinflussen, wie sehr unser reales Ausgangssignal im Zeitbereich der Funktion $f(t)$ ähnelt.

Im Frequenzbereich lassen sich nämlich kuriose Schlüsse ziehen. Zwar sollten die Koeffizienten an Vielfachen von 200 Hz konstant sein ($\hat U_{e2} \cdot f_m(t) \approx 8 V \cdot 0.637 V = 5.096 V$), aber wie in (Fig.21) ersichtlich, nimmt die Amplitude im Frequenzbereich mit höherer Frequenz stetig ab. Außerdem sind die Amplituden keine Impulse an einzelnen Frequenzen, sondern verteilt auf einem Band um die relevanten Frequenzen, wie schon bei Beispiel 2.3. Das passt ins Konzept der Erklärung vorhin, nämlich können unsere realen Dioden nicht "sofort" schalten. Somit *müssen* höherfrequentige Amplituden im Frequenzbereich abnehmen, sonst wären sie ja konstant und somit entsprechend dem idealen Szenario.