# Protokoll OPV

## 2. Simulationen

### 2.0. Parameter der Simulationen

Aufgabe|Parameter|Wert|Einheit|runden auf|gerundet|
:--|:--|--:|:--|--:|--:|:--
2.1| V  | 45     | Verstärkung |     |        |
2.1| R1 | 4.87   | k$\Omega$   | E24 | 4.7    | k$\Omega$
2.2| V  | -30    | Verstärkung |     |        |
2.2| R1 | 3.37   | k$\Omega$   | E24 | 3.3    | k$\Omega$
2.3| C  | 1242   | nF          | E6  | 1000   | nF
2.3| R1 | 2.41   | k$\Omega$   | E24 | 2.4    | k$\Omega$
2.3| Rs | 103.73 | k$\Omega$   | E24 | 100    | k$\Omega$
2.4| R1 | 5.29   | k$\Omega$   | E24 | 5.1    | k$\Omega$
2.4| R2 | 2.15   | k$\Omega$   | E24 | 2.2    | k$\Omega$
2.4| R3 | 8.85   | k$\Omega$   | E24 | 9.1    | k$\Omega$
Table: personenbezogene Simulationsparameter

### 2.1. nichtinvertierender Verstärker

#### Aufgabenstellung

*In dieser Übung soll eine einfache nicht-invertierende Operationsverstärkerschaltung aufgebautund das Verhalten des OPV nachvollzogen werden. Zeichnen sie dazu die Schaltung aus Abbildung 7. Verwenden sie ±15V als Versorgungsspannung für den OPV. Simulieren sie das Verhaltendes Systems mit LTSpice.*

#### Parameter

Aus Tab.1 zu entnehmen ist eine für diesen Versuch zu simulierende Verstärkung vom Faktor *45*. Um die Verstärkung zu erreichen, berechnen wir welchen Widerstandswert $R_2$ annehmen muss:

$$ V = 45 = 1 + \frac{R_2}{R_1} \implies 44 \cdot R_1 = R_2 \implies 44 \cdot 4.7 k \Omega = R_2 \implies R_2 = 206.8 k \Omega $$

Da es keinen derartigen Widerstand gibt, definieren wir zwei Widerstände $R_{21}, R_{22}$ welche in Serie den gewünschten Widerstandswert annehmen.

$$ 206.8 k \Omega = R_{21} + R_{22} = 200 k \Omega + 6.8 k \Omega $$

#### Schaltung

Dabei ergibt sich folgende für die Simulation zu verwendende Schaltung (Fig.1):

![nichtinvertierender Verstärker](./res/2-1-circuit.png)

#### Messwerte

Eine `transient` Simulation mit $\Delta t = 1s$ ergibt folgende Werte von Interesse.

Die Verstärkung wird in (Fig.2) illustriert mit gegenübergestellten Spannungswerten von $U_e$ und $U_a$. Zusätzlich ersichtlich sind auch die anliegenden Spannungen an den Eingängen der OPV, denotiert durch $U_e$ und $U_m$.

![$U_e$, $U_m$ und $U_a$](./res/2-1-dc-values.png)

Die Ströme die sich ergeben werden in (Fig.3) illustriert, wobei die obere Hälfte die Ströme an den beiden eingehenden Klemmen der OPV anzeigen und die untere Hälfte die Ströme über die drei Widerstände.

![$I(U_1:In+), I(U_1:In-)$ oben, $I(R_1), I(R_{21}), I(R_{22})$ unten](./res/2-1-current-values.png)

Als nächstes wird eine Rechtecksspannung angelegt, mit Parametern: $(V_{initial}=-0.1,V_{on}=0.1,T_{delay}=5ms,T_{rise}= 0.1 \mu s,T_{fall}=0.1 \mu s,T_{on}=5ms,T_{period}=10ms)$. Dabei ergeben sich Spannungswerte wie abgebildet in (Fig.4). Simulationsdauer $\Delta t = 10ms$.

![$U_e$, $U_m$ und $U_a$](./res/2-1-square-1.png)

Weitergehend wird eine höherfrequentige Rechtecksspannung angelegt, mit Parametern: $(V_{initial}=-0.1,V_{on}=0.1,T_{delay}=50 \mu s,T_{rise}= 0.1 \mu s,T_{fall}=0.1 \mu s,T_{on}=50 \mu s,T_{period}=100 \mu s)$. Dabei ergeben sich Spannungswerte wie abgebildet in (Fig.5). Simulationsdauer $\Delta t = 1ms$.

![$U_e$, $U_m$ und $U_a$](./res/2-1-square-2.png)

#### Ergebnisse

Die OPV verstärkt klarerweise um einen Faktor von 45, wie etliche Messwerte bestätigen. In (Fig.2) fällt auf, dass die Spannungsdifferenz der anliegenden Spannungen der OPV nicht *0* beträgt, sondern leicht abweicht. Diese Abweichung sind geschätzt in der Größenordnung von mehreren hunderten $\mu V$. Analog fällt in (Fig.3) auf, dass die Ströme an den anliegenden Kontakten der OPV auch nicht $0 A$ betragen. Unsere OPV ist in diesem Fall keine ideale OPV und der Eingangswiderstand daher nicht $\infty$.

Weiters fällt bei einem Vergleich von (Fig.4) und (Fig.5) auf, dass die Ausgangsspannung $U_a$ der Eingangsspannung $U_e$ nur leicht zeitverzögert folgt. In (Fig.4) ist dieser Effekt vernachlässigbar, aber in (Fig.5) ist gar keine ausgehende Rechtecksspannung mehr erkennbar. In dieser Hinsicht stellt unsere OPV eine Art Tiefpassfilter dar. Für genügend hochfrequentige Signale ließe sich unsere OPV gleichzeitig als nichtinvertierender Verstärker sowie als Tiefpassfilter verwenden.

### 2.2. invertierender Verstärker

#### Aufgabenstellung

*In dieser Übung soll eine einfache invertierende Operationsverstärkerschaltung aufgebaut werden. Außerdem sollen die Grenzen des OPV bezüglich seines Frequenzbereichs (Bode-Diagramm)untersucht werden. Zeichnen sie dafür die Schaltung aus Abbildung 8 und simulieren sie dasVerhalten des Systems mit LTSpice.*

#### Parameter

Aus Tab.1 zu entnehmen ist eine für diesen Versuch zu simulierende Verstärkung vom Faktor *-30*. Um die Verstärkung zu erreichen, berechnen wir welchen Widerstandswert $R_2$ annehmen muss:

$$ V = -30 = -\frac{R_2}{R_1} \implies -30 \cdot -R_1 = R_2 \implies -30 \cdot -3.3 k \Omega = R_2 \implies R_2 = 99 k \Omega $$

Da es keinen derartigen Widerstand gibt, definieren wir drei Widerstände $R_{21}, R_{22}, R_{23}$ welche in Serie den gewünschten Widerstandswert annehmen.

$$ 99 k \Omega \approx R_{21} + R_{22} + R_{22} = 91 k \Omega + 7.5 k \Omega + 510 \Omega $$

##### Schaltung

Dabei ergibt sich folgende für die Simulation zu verwendende Schaltung (Fig.6):

![invertierender Verstärker](./res/2-2-circuit.png)

#### Messwerte

Eine `transient` Simulation mit $\Delta t = 1ms$ ergibt folgende Werte von Interesse.

Die Verstärkung wird in (Fig.7) illustriert mit gegenübergestellten Spannungswerten von $U_e$ und $U_a$. Zusätzlich ersichtlich sind auch die anliegenden Spannungen an den Eingängen der OPV, denotiert durch $U_m$. Am positiven Eingang liegt die Masse an.

![$U_e$, $U_m$ und $U_a$](./res/2-2-dc-values.png)

Die Ströme die sich ergeben werden in (Fig.8) illustriert, wobei die obere Hälfte die Ströme an den beiden eingehenden Klemmen der OPV anzeigen und die untere Hälfte die Ströme über die drei Widerstände.

![$I(U_1:In+), I(U_1:In-)$ oben, $I(R_1), I(R_{21}), I(R_{22}), I(R_{23})$ unten](./res/2-2-current-values.png)

Als nächstes wird eine Dreiecksspannung angelegt, mit Parametern: $(V_{initial}=-0.1,V_{on}=0.1,T_{rise}=5 ms,T_{fall}=5 ms,T_{period}=10ms, N_{cycles}=10)$. Dabei ergeben sich Spannungswerte wie abgebildet in (Fig.9). Simulationsdauer $\Delta t = 100ms$.

![$U_e$, $U_m$ und $U_a$](./res/2-2-triangle-1.png)

Jetzt wird eine neue Dreiecksspannung angelegt, mit Parametern: $(V_{initial}=-0.1,V_{on}=0.1,T_{rise}=50 \mu s,T_{fall}=50 \mu s,T_{period}=100 \mu s, N_{cycles}=10)$. Dabei ergeben sich Spannungswerte wie abgebildet in (Fig.10). Simulationsdauer $\Delta t = 1ms$.

![$U_e$, $U_m$ und $U_a$](./res/2-2-triangle-2.png)

Auch von Interesse ist neben der Antwort im Zeitbereich die Antwort im Frequenz- bzw. Phasenbereich. Wir führen eine `AC analysis` Simulation mit Frequenzen $f \in [1 Hz, 10 MHz]$ durch. In (Fig.11) ersichtlich ist dann das dazugehörige Bode-Diagramm.

![Bode-Diagramm](./res/2-2-bode.png)

Als nächstes wollen wir Messwerte für einen Vergleich der Frequenz- bzw. Phasenantwort für unterschiedliche Verstärkungsparameter $V$ sammeln. Wir erzielen jetzt ein $\frac{1}{10}$ der ursprünglichen Verstärkung $V_1 = -30$: $V_2 = -3$. Dafür müssen wir wieder die Widerstandswerte für $R_2$ anpassen:

$$ V = -3 = -\frac{R_2}{R_1} \implies -3 \cdot R_1 = R_2 \implies -3 \cdot 3.3 k \Omega = R_2 \implies R_2 = 9.9 k \Omega $$

Da es auch hier keinen Widerstand gibt, der exakt $9.9 k \Omega$ beträgt, müssen wir einen Ersatzwiderstand definieren.

$$ 9.9 k \Omega \approx R_{21} + R_{22} + R_{22} = 9.1 k \Omega + 750 \Omega + 51 \Omega $$

Das neue sich ergebende Bode-Diagramm ist abgebildet in (Fig.12).

![Bode-Diagramm](./res/2-2-bode-2.png)

Im Sinne der einfacheren Vergleichbarkeit finden sich in (Fig.13) beide Bode-Diagramme zusammengesetzt. Dabei bezeichnet $V(ua1)$ die Schaltung mit einer Verstärkung $V_1 = -30$ und $V(ua2)$ die mit einer Verstärkung $V_2 = -3$.

![Bode-Diagramm](./res/2-2-bode-comparison.png)

Ein paar interessante Größen sind:

i|$V_i$|$A_i$|$f_{i,g,cl}$
:--|:--|:--|:--
1|$-30$|$29.54 dB$|$33.15 kHz$
2|$-3$ |$9.54$    |$325.05 kHz$
Table: Bode-Diagramm Vergleichswerte

Wobei $V_i$ die jeweilige Verstärkung, $A_i$ die jeweilige flache Amplitudenantwort für niedrige Frequenzen und $f_{i,g,cl}$ die jeweilige *closed-loop* Grenzfrequenz.

#### Ergebnisse

In (Fig.9) ist die Antwort des Systems im Zeitbereich ersichtlich, wenn eine Dreiecksspannung mit den entsprechenden Parametern angelegt wird. Wird jetzt im Vergleich dazu (Fig.10) betrachtet, in dessen Simulation nur die Periodendauer um einen Faktor $10^2$ kleiner geworden ist, wird schnell ersichtlich, dass die Verstärkung im Allgemeinen nicht mehr $V=-30$ beträgt, sondern die OPV plötzlich Schwierigkeiten hat, die Amplitude des Ausgangssignals auf die gewünschte Verstärkung zu bringen.

In (Fig.11) ist ein Bode-Diagramm für die Simulation der Antwort des Systems im Frequenz- bzw. Phasenbereich ersichtlich. Eingezeichnet ist die 3dB-Grenzfrequenz, welche in diesem Fall ungefähr $33.15KHz$ beträgt. Sofort ersichtlich ist natürlich, dass das System mit geringeren Amplituden auf höhere Frequenzen antwortet. Es handelt sich somit um einen Tiefpassfilter. Des weiteren verhält sich die Phase ziemlich nicht-linear. Gründe dafür könnten eine interne Kapazität sein, mit höherer Frequenz steigt somit die Impedanz bzw. sinkt die Reaktanz. Beschrieben kann dieser Effekt werden mit Parametern wie der *slew rate* einer OPV, welche angibt, wie schnell ein tatsächliches Ausgangssignal sich dem gewünschten Ausgangssignal anpassen kann. Es gibt auch OPVs welche eine Frequenzkompensationsstufe eingebaut haben, dieser OPV scheint aber kein solcher zu sein.

Für einen Vergleich ist in (Fig.12) das Bode-Diagramm ersichtlich, welches sich ergibt, wenn die Verstärkung von $V_1 = -30$ auf $V_2 = -3$ angepasst wird. Eingezeichnet sind die ursprüngliche 3dB-Grenzfrequenz in Höhe von $f_1 = 33.15 KHz$ (linker cursor) sowie die neue 3dB-Grenzfrequenz in Höhe von $f_2 = 325.05 KHz$. Sehen lässt sich dieser Vergleich noch leichter in (Fig.13), wo beide Bode-Diagramme zusammen ersichtlich sind. Eingezeichnet ist hier dasselbe. Wie sich erkennen lässt, setzt der Effekt den wir bereits observiert haben für eine Schaltung mit kleiner Verstärkung später an, wir gewinnen bei $V_2 = -3$ durch die verminderte Verstärkung die Frequenzen im Bereich $f \in [f_1, f_2]$, welche bei $V_1 = -30$ bereits stetig abgenommen haben.

Werden die Werte aus Tab.2 betrachtet, so lässt sich die jeweilige Transitfrequenz $f_{i,t}$ wie folgt berechnen:

$$ f_{1,t} = A_1 \cdot f_{1,g,cl} \approx 1 MHz; f_{2,t} = A_2 \cdot f_{2,g,cl} \approx 3 MHz $$

Die Transitfrequenz gibt uns an, bei welcher Frequenz die Verstärkung auf genau -1 sinken würde. Die Bedeutung des Verstärkungs-Bandbreiten-Produkt ist im Grunde, dass wenn wir die Amplitude $A_i$ niedriger ansetzen, dann wird entsprechend die Grenzfrequenz $f_{i,g,cl}$ größer. Das bewirkt, dass die Rechnung die wir vorhin durchgeführt haben immer auf dieselbe Transitfrequenz kommt.

### 2.3. Integrierer

#### Aufgabenstellung

*In dieser Übung soll eine einfache integrierende Operationsverstärkerschaltung aufgebaut, unddessen Verhalten nachvollzogen werden. Zeichnen sie hierfür die Schaltung aus Abbildung 9 undsimulieren sie das Verhalten des Systems mit LTSpice. Für die Widerstände R1 & RS (zur Stabilisierung), sowie den Kondensator C nehmen sie wieder ihre personalisierten Werte aus der Tabelle.*

#### Parameter

Es werden die Werte aus Tab.1 für Aufgabe 2.3 übernommen.

#### Schaltung

Folgende Schaltung wird für die Simulation verwendet (Fig.14).

![Integrierer](./res/2-3-circuit.png)

#### Messwerte

Es wird eine Rechtecksspannung angelegt, mit Parametern: $(V_{initial}=-0.1,V_{on}=0.1,T_{delay}=100ms,T_{rise}= 1 \mu s,T_{fall}=1 \mu s,T_{on}=100ms,T_{period}=200ms)$. Dabei ergeben sich Spannungswerte $U_e, U_a$ wie abgebildet in (Fig.15), sowie Spannungs- und Stromwerte $U_d, I(U_1:In+), I(U_1:In-), I_C$ wie abgebildet in (Fig.16). Simulationsdauer $\Delta t = 2s$.

![$U_e$ und $U_a$](./res/2-3-dc-values.png)

![$U_d, I(U_1:In+), I(U_1:In-), I(C1)$](./res/2-3-current-values.png)

Betrachten wir nun die Systemantwort im Frequenz- sowei Phasenbereich. Wir führen eine `AC analysis` Simulation mit Frequenzen $f \in [0.01 Hz, 10 MHz]$ durch. In (Fig.17) ersichtlich ist dann das dazugehörige Bode-Diagramm.

![Bode-Diagramm](./res/2-3-bode.png)

#### Ergebnisse

Die Schaltung wirkt, wie bereits vom Namen angedeutend, *integrierend*. Das bedeutet, das Ausgangssignal entspricht einem Integral des Eingangssignals. Dieser Prozess ist aber nicht ideal, wie sich in (Fig.15) erkennen lässt, benötigt die Schaltung für ein Rechteckssignal ungefähr eine halbe Sekunde um effektiv eingeschwungen zu sein. Das lässt auf den Kondensator $C$ schließen, der an der integrierenden Funktion beteiligt ist, daher investigieren wir auch die Ströme. Wie sich erkennen lässt in (Fig.16) gibt es keinen derartigen Einschwingvorgang für die Ströme an den Eingangsklemmen der OPV, sehr wohl aber für den Strom durch Kondensator $C$. Dieser wird nämlich erst geladen beginnend mit $t = 0$.

Wie sich am Bode-Diagramm erkennen lässt, sinkt die Antwort im Frequenzbereich schon ziemlich früh, mit einer 3dB-Grenzfrequenz bei bereits $f_1 = 1.5 Hz$. Diese sinkt stetig weiter, bis schließlich bei ungefähr $f_2 = 47 kHz$ ein Minimum erreicht wurde. Anschließend steigt sie wieder bis zu einem lokalen Maximum bei ungefähr $f_3 = 1.4 MHz$, woraufhin sie wieder sinkt und sich für Frequenzen darüber bei $-30 dB$ stabilisiert. Insofern stellt die Schaltung eine Art Bandstoppfilter dar.

Zur Erklärung der Funktionsweise des Integrierers stellen wir uns eine ideale OPV Schaltung vor. Durch unendlich hohen Innenwiderstand kann durch die Eingangsklemmen der OPV kein Strom fließen, daher muss jeder Strom über den Kondensator fließen. Da der Widerstand $R_s$ nur für Stabilisierungszwecke eingebaut wurde, vernachlässigen wir ihn für unsere Überlegungen.

Eine mögliche Anwendung wäre eine analoge Schaltung zur Berechnung eines mathematischen Integrals, wobei die zu integrierende Funktion einfach als Eingangssignal angelegt wird. Wir können somit also "rechnen". Ein zweiter möglicher Nutzen wäre ein Bandstoppfilter, wobei die Schaltung aber für diesen besonderen Zweck nicht besonders effektiv ist.

### 2.4. invertierender Schmitt-Trigger

#### Aufgabenstellung

*In dieser Übung soll ein invertierender Schmitt-Trigger simuliert und dessen Funktionsweisedokumentiert werden. Zeichnen sie dafür die Schaltung aus Abbildung 10 und simulieren siedas Verhalten des Systems mit LTSpice. Beachten sie dabei die im Vergleich zu den vorigenSimulationen geänderte Versorgungsspannung Vcc = +5V. Die Widerstandswerte erhalten siewieder aus der Tabelle.*

#### Parameter

Es werden die Werte aus Tab.1 für Aufgabe 2.4 übernommen. $V_cc$ beträgt der Angabe zu entnehmen ist 5V.

#### Schaltung

Dabei ergibt sich folgende für die Simulation zu verwendende Schaltung (Fig.18).

![invertierender Schmitt-Trigger](./res/2-4-circuit.png)

#### Messwerte

Als Eingangssignal speisen wir ein an $U_e$ anliegendes sinusoidales Signal mit folgenden Parametern: $(DC_{offset}=2.5V,V_{PP}=5V,f=50 Hz)$. Simulationsdauer $\Delta t = 100 ms$. Die sich ergebende Eingangsspannung sowie Ausgangsspannung ist in (Fig.19) abgebildet.

![$U_e$ und $U_a$](./res/2-4-dc-values.png)

Da der Schmitt-Trigger durch Spannungsquelle $V_{cc}$ betrieben wird, wären für einen idealen Schmitt-Trigger die Schwellwerte $U_{low} = 0 V$ und $U_{high} = 5 V$. Da es sich aber nicht um einen idealen Schmitt-Trigger handelt, stellen wir als nächstes die Schwellwerte mithile von Messungen fest.

Um durch Messung herauszufinden, bei welcher Eingangsspannung der Schmitt-Trigger transitioniert, messen wir $U_e$ ungefähr an jenen Stellen, an jenen $U_a$ schaltet. Somit finden wir heraus, dass wenn $U_a$ gerade die logische 1 (41 mV) annimmt (es ist ein *invertierender* Schmitt-Trigger), ab einem Schwellenwert von $U_{low} = 75 mV$ in die logische 0 (4.44 V) transitioniert. Analog finden wir heraus, dass wenn $U_a$ gerade die logische 0 (4.44 V) annimmt, ab einem Schwellenwert von $U_{high} = 3.35 V$ in die logische 1 (41 mV) transitioniert.

*Hinweis: die Netzwerkanalyse zur manuellen Bestimmung der Schwellwerte mithilfe von Superposition ist nicht gelungen, daher findet sich hier oder in den Ergebnissen kein Vergleich :(*

Als nächstes legen wir statt eines sinusoidalen Eingangssignals eine Rechtecksspannung (degeneriert zu einer Dreiecksspannung) an mit den Parametern: $(V_{initial}=0 V,V_{on}=5V,T_{delay}=0 \mu s,T_{rise}= 0.1 \mu s,T_{fall}=0.1 \mu s,T_{on}=0 \mu s,T_{period}=0.2 \mu s)$. Die Simulationsdauer wird in Folge dessen angepasst auf $\Delta t = 1 \mu s$. Die sich neu ergebenden Spannungswerte $U_e$ und $U_a$ finden sich in (Fig.20).

![$U_e$ und $U_a$ angepasst](./res/2-4-dc-values-2.png)

Um zu verstehen, wie sich eine Veränderung der Frequenz auf die Schaltung auswirkt, betrachten wir nun XY-Diagramme, in welchen die Hystere-Kennlinie (x-Achse: $U_e$, y-Achse: $U_a$) dargestellt wird. Unterschiedliche Frequenzen sowie auch unsere vorherige Rechtecksspannung sind abgebildet in (Fig.21) bis (Fig.26).

![XY-Diagramm $5 Hz$](./res/2-4-xy-sine-5hz.png)

![XY-Diagramm $50 Hz$](./res/2-4-xy-sine-50hz.png)

![XY-Diagramm $500 Hz$](./res/2-4-xy-sine-500hz.png)

![XY-Diagramm $5 k Hz$](./res/2-4-xy-sine-5khz.png)

![XY-Diagramm $50 k Hz$](./res/2-4-xy-sine-50khz.png)

![XY-Diagramm Rechtecksspannung](./res/2-4-xy-square.png)

#### Ergebnisse

Der Vorteil des Schmitt-Triggers ist, dass sobald transitioniert wird, die Ausgangsspannung stabil bleibt, bis der nächste Schwellwert wieder über-/unterschritten wird. Somit kann ein Schmitt-Trigger sehr effektiv mit beispielsweise Störsignalen umgehen. Um das zu emulieren, wurde die Schaltung leicht angepasst, d.h. mit einer Störfunktion erweitert (Fig.27). Die Antwort im Zeitbereich ist in (Fig.28) zu finden.

![invertierender Schmitt-Trigger angepasst mit Störfkt.](./res/2-4-circuit-2.png)

![$U_e$ und $U_a$](./res/2-4-stabilizer.png)

Wie in (Fig.20) ersichtlich ist, transitioniert der invertierende Schmitt-Trigger bei einem angelegten Dreieckssignal mit der kurzen Periodendauer gar nicht, sondern verbleibt ständig im Zustand der logischen 0 (4.44 V). Gründe dafür könnten sein, dass die Schaltung einfach nicht genug Zeit hat, zu transitionieren nachdem $U_{low}$ erreicht wurde, bevor wieder $U_{high}$ erreicht wird. Wie sich in (Fig.19) vielleicht herauslesen lässt, transitioniert $U_a$ nicht sofort, sondern innerhalb einer kurzen Zeitspanne.

Nun betrachten wir unsere Ergebnisse aus (Fig.21) bis (Fig.26). Wie sich erkennen lässt, ist unsere Hystere-Kennline ziemlich quadratförmig in (Fig.21), und somit der Abstand zwischen $U_{low}$ und $U_{high}$ angemessen groß. Mit sich erhöhender Frequenz gehen diese zwei Werte auch immer mehr auseinander. Interessant ist hier aber, dass die Hystere-Kennlinie sich ungleichmäßig entwickelt, d.h. bei hohen $U_a$ schneller transitioniert als bei niedrigen $U_a$. Das hätte in der Tat auch schon bei (Fig.19) auffallen können, wo erkenntlich ist, dass die logische 0 (4.44 V) im allgemeinen länger aktiv bleibt, als die logische 1 (41 mV). Bei hohen Frequenzen ($\leq 50 kHz$) deformiert die rechteckige Hystere-Kennline in etwas weniger quadratförmiges. Dieser Effekt ist interessant, deswegen investigieren wir noch die Antwort im Zeitbereich für die Frequenz $f = 50 kHz$ (Fig.29).

![invertierender Schmitt-Trigger Breakdown](./res/2-4-breakdown.png)

Wie sich aus der Hysterese-Kennlinie bereits ablesen ließe, bleibt der invertierende Schmitt-Trigger praktisch gar nicht bei der logischen 0 (4.44 V). Nicht nur das, der tatsächliche Spannungswert $U_a$ kommt nicht einmal nah ran. Offensichtlich ist der invertierende Schmitt-Trigger nicht mehr besonders zuverlässig für genug hohe Frequenzen.

In (Fig.26) findet sich auch das XY-Diagramm für die Rechtecksspannung/degenerierte Dreiecksspannung die wir bereits simuliert haben. Anders als wir festgestellt haben, bleibt der Schmitt-Trigger nicht immer im selben Zustand.

Spannend ist auch, wieso wir eine unipolare Versorgungsspannung einsetzt. Es stellt sich heraus, Schmitt-Trigger werden oft mit bipolaren Transistoren realisiert. Die gesamte Schaltung dafür ist Teil einer *Transistor-Transistor Logik* (TTL). TTL ist dabei eine *stromziehende Schaltungstechnik* und muss daher von den Eingängen einen Strom ziehen können. Würden wir eine negative Spannung anlegen, wäre das offensichtlich nicht möglich. Oftmals kann eine solche TTL nicht mit zu hohen Spannungen getrieben werden. [(Wikipedia Quelle (link))](https://en.wikipedia.org/wiki/Transistor%E2%80%93transistor_logic)

Wie aus (Fig.28) entnommen werden kann, sind Schmitt-Trigger sehr zuverlässig für die Umwandlung eines rauschenden sinusoidalen Signals in eine Rechtecksschwingung. Ein Komparator hat vergleichbare Eigenschaften, hat aber keine solche Hysteresekurve und besitzt somit keine zwei Schwellwerte, sondern nur einen. Das führt dazu, dass bei rauschenden Signalen der Komparator unnötig oft seinen Zustand transitionieren kann, während das beim Schmitt-Trigger in vielen Fällen verhindert werden kann.

Weiters kann dieser Effekt genutzt werden, um "Prellen" bei mechanischen Schaltern zu kontern, ebenfalls mithilfe dem Hysterese-Effekt.
