% Beispiel 2.1
% name lastname (11907086)
% 23. März 2021

Aufgabenstellung: Beweisen Sie für jeder der durch Eingangs/Ausgangsbeziehungen gegebenen Systeme die Gültigkeit oder Ungültigkeit der Systemeigenschaften *Linearität*, *Zeitinvarianz*, *Kausalität*, *Stabilität*. Wo es möglich ist, geben Sie die Impulsantwort $h[n]$ des Systems an.

### Linearität

Ein gegebenes System $y[n]$ ist *linear* wenn:

$$
y[n] = \mathcal{T}\{a x_1[n] + b x_2[n]\} = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\}
$$ {#eq:linearity1}

$$
x[n] \equiv 0 \implies y[n] \equiv 0
$$ {#eq:linearity2}

### Zeitinvarianz

Ein gegebenes System $y[n]$ ist *zeitinvariant* wenn:

$$
y[n] = \mathcal{T}\{\delta [n-k]\} = h[n-k]
$$ {#eq:time-invariance}

### Kausalität

Ein gegebenes System $y[n]$ ist *kausal* wenn:

$$
h[n] = 0, n < 0
$$ {#eq:causality}

### Stabilität

Eine hinreichende Bedingung für die *Stabilität* eines gegebenen Systems $y[n]$ ist *BIBO Stabilität*:

$$
\sum_{k = -\infty}^\infty | h_{1,2}[k] | < \infty
$$ {#eq:bibo}

## (a) $y[n] = x[n] - x[n - 1]$

Linearität:

$$
\begin{aligned}
y_{tot}[n] &= a y_1[n] + b y_2[n] \\
&= a (x_1[n] - x_1[n-1]) + b (x_2[n] - x_2[n-1]) \\
&= a x_1[n] - a x_1[n-1] + b x_2[n] - b x_2[n-1]
\end{aligned}
$$ {#eq:a-linearity1}

$$
\forall n : x[n] = 0 \implies \forall n : y[n] = x[n] - x[n-1] = 0 - 0 = 0
$$ {#eq:a-linearity2}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= x[n - k] - x[n - k - 1] \\
= \mathcal{T}\{x[n - k]\} &= x[n - k] - x[n - k - 1]
\end{aligned}
$$ {#eq:a-time-invariance}

Stabilität:

Das *BIBO Stabilitätskriterium* sagt, dass für eine endliche Eingabegröße $x[n]$ eine endliche Ausgabegröße $y[n]$ resultieren muss. Dies ist offensichtlich gegeben und lässt sich leicht überprüfen indem man z.B. $x[n] = \delta[n]$ anlegt:

$$
y[n] = \delta[n] - \delta[n - 1]
$$ {#eq:a-bibo}

Kausalität:

Das System ist offensichtlich kausal, da die Ausgabe des Systems $y[n]$ nur von den letzen zwei Systemeingangsgrößen $x[n]$ und $x[n - 1]$ abhängt. Legt man wieder $x[n] = \delta[n]$ an, so lässt sich diese Eigenschaft von Gleichung [@eq:a-bibo] ablesen.

Das System $y[n]$ ist also *linear*, *zeitinvariant*, *stabil* und *kausal*.

Wir bekommen die Impulsantwort $h[n]$ indem wir $x[n] = \delta[n]$ anlegen:

$$
h[n] = \delta[n] - \delta[n - 1]
$$ {#eq:a-impulse-response}

\newpage

## (b) $y[n] = x[n] x[n-1]$

Linearität:

$$
\begin{aligned}
y_{tot1}[n] &= a y_1[n] + b y_2[n] = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\} \\
&= a (x_1[n] x_1[n-1]) + b (x_2[n] x_2[n-1]) \\
&= (a + b) x_1[n] x_1[n-1] \\
y_{tot2} &= \mathcal{T}\{ax_1[n] + bx_2[n]\} \\
&= (a x_1[n] + b x_2[n]) (a x_1[n-1]) + b x_2[n-1]) \\
&= a^2 x_1[n] x_1[n-1] + a b x_1 x_2[n-1] + a b x_2[n] x_1[n-1] + b^2 x_2[n] x_2[n-1] \\
y_{tot1} & \not = y_{tot2}
\end{aligned}
$$ {#eq:b-linearity1}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= x[n - k] x[n - k - 1] \\
= \mathcal{T}\{x[n - k]\} &= x[n - k] x[n - k - 1]
\end{aligned}
$$ {#eq:b-time-invariance}

Stabilität:

Hier kann gleich vorgegangen werden wir bereits bei (a).

Kausalität:

Hier kann ebenfalls gleich vorgegangen werden wir bereits bei (a).

Das System $y[n]$ ist also *nicht-linear*, *zeitinvariant*, *stabil* und *kausal*.

Es liegt kein LTI-System vor.

\newpage

## (c) $y[n] = \sum_{k = n - 2}^{n + 4} x[k]$

Linearität:

$$
\begin{aligned}
y_{tot1}[n] &= a y_1[n] + b y_2[n] = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\} \\
&= a \sum_{k = n - 2}^{n + 4} x_1[k] + b \sum_{k = n - 2}^{n + 4} x_2[k] \\
&= \sum_{k = n - 2}^{n + 4} a x_1[k] + b x_2[k] \\
y_{tot2}[n] &= \mathcal{T}\{a x_1[n] + b x_2[n]\} \\
&= \sum_{k = n - 2}^{n + 4} a x_1[k] + b x_2[k] \\
y_{tot1} &= y_{tot2}
\end{aligned}
$$ {#eq:c-linearity1}

$$
\forall n : x[n] = 0 \implies \forall n : y[n] = \sum_{k = n - 2}^{n + 4} 0 = 0
$$ {#eq:c-linearity2}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= \sum_{l = n - k - 2}^{n - k + 4} x[l] \\
= \mathcal{T}\{x[n - k]\} &= \sum_{l = n - k - 2}^{n - k + 4} x[l]
\end{aligned}
$$ {#eq:c-time-invariance}

Stabilität:

Das *BIBO Stabilitätskriterium* sagt, dass für eine endliche Eingabegröße $x[n]$ eine endliche Ausgabegröße $y[n]$ resultieren muss. Das ist der Fall, da über eine endliche Anzahl an Eingangswerten summiert wird und lässt sich überprüfen indem man z.B. $x[n] = \delta[n]$ anlegt:

$$
y[n] = \sum_{k = n - 2}^{n + 4} \delta[k]
$$ {#eq:c-bibo}

Kausalität:

Das System ist nicht kausal, da in der Summe 4 Einganswerte aus der Zukunft benötigt werden, wie sich an der oberen Schranke $n + 4$ erkennen lässt.

Das System $y[n]$ ist also *linear*, *zeitinvariant*, *stabil* und *akausal*.

Auch hier bekommen wir die Impulsantwort $h[n]$ indem wir $x[n] = \delta[n]$ anlegen:

$$
h[n] = \sum_{k = n - 2}^{n + 4} \delta[k] = \delta[n-2] + \delta[n-1] + ... + \delta[n+4]
$$ {#eq:c-impulse-response}

\newpage

## (d) $y[n] = x[n] + x[-n]$

Linearität:

$$
\begin{aligned}
y_{tot1}[n] &= a y_1[n] + b y_2[n] = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\} \\
&= a (x_1[n] + x_1[-n]) + b (x_2[n] + x_2[-n]) \\
&= a x_1[n] + a x_1[-n] + b x_2[n] + b x_2[-n] \\
y_{tot2}[n] &= \mathcal{T}\{a x_1[n] + b x_2[n]\} \\
&= a x_1[n] + b x_2[n] + a x_1[-n] + b x_2[-n]  \\
y_{tot1} &= y_{tot2}
\end{aligned}
$$ {#eq:d-linearity1}

$$
\forall n : x[n] = 0 \implies \forall n : y[n] = x[n] + x[-n] = 0 - 0 = 0
$$ {#eq:d-linearity2}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= x[n - k] + x[-n - k] \\
\not = \mathcal{T}\{n - k\} &= x[n - k] + x[-n + k]
\end{aligned}
$$ {#eq:d-time-invariance}

Stabilität:

Das *BIBO Stabilitätskriterium* sagt, dass für eine endliche Eingabegröße $x[n]$ eine endliche Ausgabegröße $y[n]$ resultieren muss. Das ist gegeben, da nur zwei Einganswerte summiert werden und lässt sich leicht überprüfen indem man z.B. $x[n] = \delta[n]$ anlegt:

$$
y[n] = \delta[n] + \delta[-n]
$$ {#eq:d-bibo}

Kausalität:

Das System ist nicht kausal, da für jeden Wert $n$ angenommen $n < 0$ in der Vergangenheit ein entsprechender Wert $n > 0$ aus der Zukunft benötigt wird.

Das System $y[n]$ ist also *linear*, *zeitvariant*, *stabil* und *akausal*.

Es liegt kein LTI-System vor.

\newpage

## (e) $y[n] = x[2n]$

Linearität:

$$
\begin{aligned}
y_{tot1}[n] &= a y_1[n] + b y_2[n] = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\} \\
&= a x_1[2n] + b x_2[2n] \\
y_{tot2}[n] &= \mathcal{T}\{a x_1[n] + b x_2[n]\} \\
&= a x_1[2n] + b x_2[2n] \\
y_{tot1} &= y_{tot2}
\end{aligned}
$$ {#eq:e-linearity1}

$$
\forall n : x[n] = 0 \implies \forall n : y[n] = x[2n] = 0
$$ {#eq:e-linearity2}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= x[2n - k] \\
\not = \mathcal{T}\{n - k\} &= x[2(n - k)]
\end{aligned}
$$ {#eq:e-time-invariance}

Stabilität:

Das *BIBO Stabilitätskriterium* sagt, dass für eine endliche Eingabegröße $x[n]$ eine endliche Ausgabegröße $y[n]$ resultieren muss. Das ist gegeben, da ein Einganswert als Ausganswert resultiert und lässt sich leicht überprüfen indem man z.B. $x[n] = \delta[n]$ anlegt:

$$
y[n] = \delta[2n]
$$ {#eq:e-bibo}

Kausalität:

Das System ist nicht kausal, da für jeden Wert $n$ angenommen $n > 0$ ein entsprechend doppelt so weit in der Zukunft liegender Wert $2n$ benötigt wird.

Das System $y[n]$ ist also *linear*, *zeitvariant*, *stabil* und *akausal*.

Es liegt kein LTI-System vor.

\newpage

## (f) $y[n] = \frac{1}{n + 0.5} x[n]$

Linearität:

$$
\begin{aligned}
y_{tot1}[n] &= a y_1[n] + b y_2[n] = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\} \\
&= \frac{a}{n + 0.5} x_1[n] + \frac{b}{n + 0.5} x_2[n] \\
y_{tot2}[n] &= \mathcal{T}\{a x_1[n] + b x_2[n]\} \\
&= \frac{1}{n + 0.5} (a x_1[n] + b x_2[n]) \\
&= \frac{a}{n + 0.5} x_1[n] + \frac{b}{n + 0.5} x_2[n] \\
y_{tot1} &= y_{tot2}
\end{aligned}
$$ {#eq:f-linearity1}

$$
\forall n : x[n] = 0 \implies \forall n : y[n] = \frac{1}{n + 0.5} x[n] = 0
$$ {#eq:f-linearity2}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= \frac{1}{n + 0.5} x[n - k] \\
\not = \mathcal{T}\{x[n - k]\} &= \frac{1}{n - k + 0.5} x[n - k]
\end{aligned}
$$ {#eq:f-time-invariance}

Stabilität:

Das Signal hängt nur von einem Eingangswert ab, wobei dieser aber um einen Faktor $\frac{1}{n + 0.5}$ skaliert wird. Dieser Skalierungsfaktor hätte zwar eine Polstelle an $n = 0.5$, aber da wir uns für $n$ auf ganzzahlige Werte beschränken, liegen die absolut betrachtet höchstmöglichen Faktoren $a[n] = |\frac{1}{n + 0.5}|$ bei $a[0] = a[-1] = 2$. Somit muss nach einem ähnlichen Argument wie bereits bei den vorhergehenden Beispielen das System *BIBO stabil* sein.

Kausalität:

Das System ist kausal, da für jeden ausgehenden Wert $y[n]$ nur der eingehende Wert des "jetztigen" Zeitpunktes $n$ sowie $n$ selbst benötigt wird.

Das System $y[n]$ ist also *linear*, *zeitvariant*, *stabil* und *kausal*.

Es liegt kein LTI-System vor.

\newpage

## (g) $y[n] = x[n - 1] + x[n] - x[n + 1]$

Linearität:

$$
\begin{aligned}
y_{tot1}[n] &= a y_1[n] + b y_2[n] = a \mathcal{T}\{x_1[n]\} + b \mathcal{T}\{x_2[n]\} \\
&= a (x_1[n - 1] + x_1[n] - x_1[n + 1]) + b (x_2[n - 1] + x_2[n] - x_2[n + 1]) \\
&= a x_1[n - 1] + a x_1[n] - a x_1[n + 1]) + b x_2[n - 1] + b x_2[n] - b x_2[n + 1] \\
y_{tot2}[n] &= \mathcal{T}\{a x_1[n] + b x_2[n]\} \\
&= a x_1[n - 1] + b x_2[n - 1] + a x_1[n] + b x_2[n] - a x_1[n + 1] - b x_2[n + 1] \\
y_{tot1} &= y_{tot2}
\end{aligned}
$$ {#eq:g-linearity1}

$$
\forall n : x[n] = 0 \implies \forall n : y[n] = x[n - 1] + x[n] - x[n + 1] = 0 + 0 - 0 = 0
$$ {#eq:g-linearity2}

Zeitinvarianz:

$$
\begin{aligned}
y[n - k] &= x[n - k - 1] + x[n - k] - x[n - k + 1] \\
= \mathcal{T}\{n - k\} &= x[n - k - 1] + x[n - k] - x[n - k + 1]
\end{aligned}
$$ {#eq:g-time-invariance}

Stabilität:

Auch hier muss das System anhand eines ähnlichen Arguments vorhergehender Teilaufgaben *BIBO stabil* sein, da für jeden Ausgangswert einfach drei Eingangswerte summiert werden.

Kausalität:

Das System kann nicht kausal sein. Stellt $n$ den "jetztigen" Zeitpunkt dar, so braucht der ausgehende Wert $y[n]$ jeweils den vorhergehenden Wert $x[n-1]$, den "jetztigen" Wert $x[n]$ und den nachhergehenden Wert $x[n+1]$, wobei der nachhergehende Wert in der Zukunft liegt!

Das System $y[n]$ ist also *linear*, *zeitinvariant*, *stabil* und *akausal*.

Auch hier bekommen wir die Impulsantwort $h[n]$ indem wir $x[n] = \delta[n]$ anlegen:

$$
h[n] = \delta[n - 1] + \delta[n] - \delta[n + 1]
$$ {#eq:c-impulse-response}

