% Beispiel 2.5
% name lastname (11907086)
% 12. April 2021

Gegeben ist ein LTI-System mit der Impulsantwort

$$
h[n] = 12 \left ( \left ( -\frac{1}{3} \right ) ^n - \left ( -\frac{1}{2} \right ) ^n \right ) \sigma [n]
$$ {#eq:system}

#### (a)

Berechnen und skizzieren Sie die Sprungantwort $a[n]$ des Systems.

#### Lösung

Die Sprungantwort $a[n]$ ist, wie der Name andeutet, die Antwort des Systems auf einen Sprung. Daher legen wir einen Sprung an indem wir die Impulsantwort mit dem Sprung falten:

$$
a[n] = \sum_{k = -\infty}^\infty h[k] \sigma[n - k] = \sum_{k = -\infty}^n h[k]
$$ {#eq:a}

Das Ergebnis aus [@eq:a] resultiert weil $\sigma [n - k] \not = 0$ nur gilt wenn $n \geq k$. Mithilfe von MATLAB lässt sich das Resultat auch skizzieren wie dargestellt in Abb. 1. Eingesetzt und weiter vereinfacht kommen wir auf folgendes Ergebnis:

$$
\begin{aligned}
a[n] = \sum_{k = -\infty}^n h[k] &= 12 \left ( \sum_{k = -\infty}^n \left ( -\frac{1}{3} \right )^k \sigma[n]  - \sum_{k = -\infty}^n \left ( -\frac{1}{2} \right )^k \sigma[n] \right ) \\
&= 12 \left ( \sum_{k = 0}^n \left ( -\frac{1}{3} \right ) ^k - \sum_{k = 0}^n \left ( -\frac{1}{3} \right ) ^k \right ) \\
&= 12 \left ( \frac{1 - \left ( -\frac{1}{3} \right ) ^{n + 1}}{1 - \left ( -\frac{1}{3} \right )} - \frac{1 - \left ( -\frac{1}{2} \right ) ^{n + 1}}{1 - \left ( -\frac{1}{2} \right )} \right ),&& n \geq 0 \\
&= 9 \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n + 1} \right ) - 8 \left ( 1 - \left ( -\frac{1}{2} \right ) ^{n + 1} \right ),&& n \geq 0 \\
&= 9 - \left ( -\frac{1}{3} \right ) ^{n - 1} - 8 - \left ( -\frac{1}{2} \right ) ^{n - 2},&& n \geq 0
\end{aligned}
$$ {#eq:a2}

$$
a[n] = \sum_{k = -\infty}^n h[k] 
= \sigma[n] \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 1} - \left ( -\frac{1}{2} \right ) ^{n - 2} \right )
$$ {#eq:a3}

[@eq:a2] folgt durch Anwendung der Summenformel für geometrische Reihen. Zu beachten ist dass in diesem Schritt der Wertebereich von $n$ auf $n \geq 0$ eingeschränkt wird. [@eq:a3] ist das vereinfachte Ergebnis.

![Sprungantwort](./res/2-4-a.png)

Das System schwingt und stabilisiert sich bereits nach kurzer Zeit auf einen konstanten Wert von 1.

#### (b)

Zur Vermeidung von Überschwingen der Sprungantwort $a[n]$ wird nun zum gegebenen System ein System in Kette geschaltet. Dieses Entzerrsystem wird durch die folgende Eingangs/Ausgangsbeziehung beschrieben:

$$
y[n] = a x[n] + b x[n - 1] + c x[n - 2]
$$

Wie sind die Koeffizienten $a, b, c$ zu wählen, so dass das Gesamtsystem kein Überschwingen zeigt, d.h. die Sprungantwort des entzerrten Gesamtsystems ist $\tilde a[n] = \sigma [n - n_0]$?

#### Lösung

Wir stellen die Gleichung wie in der Aufgabe auf um zu sehen wie die gewünschte Eigenschaft erfüllt werden könnte. Dabei beachten wir insbesondere dass der Eingang $x[n]$ der Ausgang unseres Systems aus (a) ist:

$$
\begin{aligned}
y[n] = \tilde a[n] &= \sigma[n - n_0] \\
&= 
a \cdot \sigma[n] \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 1} - \left ( -\frac{1}{2} \right ) ^{n - 2} \right ) \\
& + b \cdot \sigma[n - 1] \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 2} - \left ( -\frac{1}{2} \right ) ^{n - 3} \right ) \\
& + c \cdot \sigma[n - 2] \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 3} - \left ( -\frac{1}{2} \right ) ^{n - 4} \right )
\end{aligned}
$$ {#eq:b}

Eigentlich sind wir nur an den Koeffizienten interessiert. Wählen wir $n$ so dass all die Sprünge bereits geschehen sind, sprich $n > n_0 \land n > 2$, dann entfallen uns die Sprungfunktionen:

$$
\begin{aligned}
1 &= 
a \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 1} - \left ( -\frac{1}{2} \right ) ^{n - 2} \right ) \\
& + b \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 2} - \left ( -\frac{1}{2} \right ) ^{n - 3} \right ) \\
& + c \left ( 1 - \left ( -\frac{1}{3} \right ) ^{n - 3} - \left ( -\frac{1}{2} \right ) ^{n - 4} \right )
\end{aligned}
$$ {#eq:b2}

$$
\begin{aligned}
1 &= 
a - a \left ( -\frac{1}{3} \right ) ^{n - 1} - a \left ( -\frac{1}{2} \right ) ^{n - 2} \\
& + b - b \left ( -\frac{1}{3} \right ) ^{n - 2} - b \left ( -\frac{1}{2} \right ) ^{n - 3} \\
& + c - c \left ( -\frac{1}{3} \right ) ^{n - 3} - c \left ( -\frac{1}{2} \right ) ^{n - 4} \\
\end{aligned}
$$ {#eq:b3}
$$
\begin{aligned}
1 &=
(a + b + c) - \left ( -\frac{1}{3} \right ) ^{n - 1} (a - 3b + 9c) - \left ( -\frac{1}{2} \right ) ^{n - 2} (a - 2b + 4c)
\end{aligned}
$$ {#eq:b4}

Gleichung [@eq:b4] entsteht wenn man alle Brüche auf die selbe Potenz bringt (dabei ändert sich nur das Vorzeichen von dem Term der multiplikativ mit $b$ verbunden ist!). Jetzt gilt es nur noch das resultierende Gleichungssystem aus [@eq:b4] zu lösen:

$$
\begin{aligned}
1 &= a + b + c \\
0 &= -a + 3b - 9c \\
0 &= -a + 2b - 4c \\
\end{aligned}
$$ {#eq:b2}

Es ergeben sich die Koeffizienten $a = \frac{1}{2}$, $b = \frac{5}{12}$ und $c = \frac{1}{12}$.

#### (c)

Wie groß sollte zweckmäßigerweise die Zeitverzögerung $n_0$ gewählt werden?

#### Lösung

Da unser System eine Sprungantwort modelliert, bekommen wir für einen Wert $n_0 = 0$ die normale Sprungfunktion $\delta[n - n_0] = \delta[n]$. Damit unser System kausal bleibt, beschränken wir zunächst $n_0$ auf $\mathbb{N}_0$. Wenn wir unser System (z.B. wieder mithile von MATLAB) visualisieren, dann bemerken wir dass $n_0 = 1$ sein muss, damit die in (b) geforderte Beziehung $\sigma[n - n_0] = \tilde a[n]$ gilt.