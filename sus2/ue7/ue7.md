% Beispiel 3.5
% name lastname (11907086)
% 3. Mai 2021

Das Tiefpasssignal $x[n]$ wird mit dem abgebildeten zeitdiskreten System verarbeitet, wobei das Modulationssignal ein Einspuls mit der Periode $N$ ist:

$$
p[n] = \sum_{k=-\infty}^{\infty} \delta[n - kN]
$$ {#eq:signal-p}

![System](./res/desc1.png){ width=75% }

Das Spektrum des Eingangssignals und die Übertragungsfunktion des Tiefpassfilters sind in den folgenden Abbildungen dargestellt:

![Spektren](./res/desc2.png)

a) Berechnen und skizzieren Sie die Fouriertransformationen $P(e^{j \theta})$, $Z(e^{j \theta})$, $Y(e^{j \theta})$ der Signale $p[n]$, $z[n]$, $y[n]$.

b) Bestimmen Sie das Ausgangssignal $y[n]$ des Gesamtsystems.

##### Lösung

Die Fouriertransformation für $p[n]$ finden wir sofort mithilfe der Formelsammlung:

$$
P(e^{j \theta}) = \mathcal{F}^{-1}\left\{\sum_{k=-\infty}^{\infty} \delta[n - kN]\right\} = \frac{2 \pi}{N} \sum_{k=-\infty}^{\infty} \delta \left(\theta - \frac{2 \pi k}{N} \right)
$$ {#eq:spectrum-p}

Bevor wir die Fouriertransformation für $z[n]$ versuchen zu finden betrachten wir zuerst das Signal selbst etwas näher. Das Signal ergibt sich offensichtlich als Multiplikation von $x[n]$ und $p[n]$. Dafür finden wir mit entsprechender Korrespondenz aus der Formelsammlung zuerst das entsprechende Signal für $X(e^{j \theta})$:

$$
x[n] = \mathcal{F}^{-1}\{1_{\pm \alpha}(\theta)\} = \frac{\sin \alpha n}{\pi n}, \alpha = \frac{\pi}{N}
$$ {#eq:signal-x}

Notiz: $1_{\pm \alpha}(\theta)$ beschreibe die Indikatorfunktion $1_{\pm \alpha}(\theta) = 1, 0 \leq |\theta| \leq \alpha$

Nun können wir $z[n]$ berechnen:

$$
\begin{aligned}
z[n] &= x[n] p[n] \\
&= \frac{\sin \alpha n}{\pi n} \sum_{k=\infty}^{\infty} \delta[n-kN] \\
&\text{ wobei } 0 = n - kN \implies n = kN \\
&= \underbrace{\sum_{k=\infty}^{\infty}}_{=0 \forall k \not = 0} \frac{\sin \pi k}{\pi k N} \\
&= \delta[n] \lim_{k \to 0} \frac{\sin \pi k}{\pi k N} \\
&= \delta[n] \lim_{k \to 0} \frac{\pi \overbrace{\cos \pi k}^{1}}{\pi N} \\
z[n] &= \delta[n] \frac{1}{N}
\end{aligned}
$$ {#eq:signal-z}

Nun können wir einfach das Spektrum von $z[n]$ mit entsprechender Korrespondenz finden:

$$
\begin{aligned}
Z(e^{j \theta}) &= \mathcal{F}^{-1} \left\{ \delta \frac{1}{N} \right \} \\
&= \frac{1}{N} e^{-j \theta 0} \\
&= \frac{1}{N}
\end{aligned}
$$ {#eq:spectrum-z}

Um nun das Spektrum von $y[n]$ zu finden betrachten wir einmal was aus dem System für ein Ausgangssignal entsteht:

$$
y[n] = z[n] \ast h_{TP}[n]
$$ {#eq:signal-y-1}

wobei $h_{TP}[n]$ einfach mit entsprechender Korrespondenz wie bereits bei [@eq:signal-x] gefunden werden kann:

$$
h_{TP}[n] = \frac{\sin \frac{3 \pi}{N} n}{\pi n}
$$ {#eq:signal-h}

somit ergibt sich durch die Korrespondenz der Faltung im Zeitbereich folgende Multiplikation im Frequenzbereich:

$$
\begin{aligned}
Y(e^{j \theta}) &= Z(e^{j \theta}) H_{TP}(e^{j \theta}) \\
&= \frac{1}{N} 1_{\pm \frac{3 \pi}{N}}(\theta) \\
&= \frac{1}{N} H_{TP}(e^{j \theta})
\end{aligned}
$$ {#eq:spectrum-y}

und anschließend für $y[n]$ im Zeitbereich:

$$
\begin{aligned}
y[n] &= \frac{1}{N} h_{TP}[n] \\
&= \frac{1}{N} \frac{\sin \frac{3 \pi}{N} n}{\pi n}
\end{aligned}
$$ {#eq:signal-y}

Anschließend folgen noch Skizzen für die Spektren von $p[n]$, $z[n]$ und $y[n]$.

![Spektrum $P(e^{j \theta})$](./res/spectrum-p.png)

![Spektrum $Z(e^{j \theta})$](./res/spectrum-z.png)

![Spektrum $Y(e^{j \theta})$](./res/spectrum-y.png)

Notiz: leider beherrsche ich nicht die arkanen Künste des Erstellens von Latex Graphen
