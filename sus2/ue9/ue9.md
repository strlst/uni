% Beispiel 4.5
% name lastname (11907086)
% 24. Mai 2021

Von einem digitalen Filter ist das Schaltbild gegeben:

![Schaltbild](./res/circuit.png){ width=50% }

# (a)

Berechnen Sie die Übertragungsfunktion $H(z)$ dieses Filters.

##### Lösung

Beginnen können wir indem wir die dem System entsprechenden Systemgleichungen aufstellen. Zunächst führen wir eine Knotenbezeichnung $v[n]$ ein:

![Schaltbild](./res/circuit-annotated.png){ width=50% }

Nun können wir die Gleichungen bestimmen:

$$
\begin{aligned}
v[n] &= x[n] - \frac{k}{3} v[n - 1] \\
y[n] &= v[n] - \frac{k}{4} v[n - 1]
\end{aligned}
$$ {#eq:difference-eq}

Um die Differenzengleichungen zu lösen gehen wir direkt in den Bildbereich mithilfe der Z-Transformation:

$$
\begin{aligned}
V(z) &= X(z) - \frac{k}{3z} V(z) \\
Y(z) &= V(z) - \frac{k}{4z} V(z)
\end{aligned}
$$ {#eq:z-eq}

$$
\begin{aligned}
X(z) &= \left (1 + \frac{k}{3z} \right ) V(z) \\
Y(z) &= \left (1 - \frac{k}{4z} \right ) V(z)
\end{aligned}
$$ {#eq:z-eq-2}

$$
\begin{aligned}
Y(z) &= \underbrace{\frac{1-\frac{k}{4z}}{1+\frac{k}{3z}}}_{H(z)} X(z) \\
\end{aligned}
$$ {#eq:z-eq-3}

Somit folgt die Übertragungsfunktion in Gl. [@eq:transfer]:

$$
H(z) = \frac{z-\frac{k}{4}}{z+\frac{k}{3}}
$$ {#eq:transfer}

\newpage

# (b)

Wählen Sie den Parameter $k$ so, dass das Filter stabil ist. Skizzieren Sie das Pol/Nullstellendiagramm für dieses stabile Filter.

##### Lösung

Aus der Übertragungsfunktion in Gl. [@eq:transfer] lassen sich die Pol- und Nullstelle ablesen: $z_{01} = \frac{k}{4}$ und $z_{\infty 1} = -\frac{k}{3}$

Abhängig davon, ob wir nun von einem linksseitigen oder rechtsseitigen Signal ausgehen, ändert sich der zugelassene Wertebereich von $k$.

Bei einem rechtsseitigen Signal setzen wir von $k$ vorraus: $|z| > \frac{|k|}{3}$ und $\frac{|k|}{3} < 1$, somit $k < 3$

Bei einem linksseitigen Signal setzen wir von $k$ vorraus: $|z| < \frac{|k|}{3}$ und $\frac{|k|}{3} > 1$, somit $k > 3$

![Pol/Nullstellendiagramm Skizze](./res/roots-poles-sketch.png)

\newpage

# (c)

Berechnen und skizzieren Sie die Impulsantwort $h[n]$ und die Sprungantwort $a[n]$ des stabilen Filters.

##### Lösung

Um die Impulsantwort zu berechnen, ordnen wir zunächst unser Ergebnis aus [@eq:transfer] um:

$$
\begin{aligned}
H(z) &= \frac{z-\frac{k}{4}}{z+\frac{k}{3}} \\
&= \frac{z}{z+\frac{k}{3}} - \frac{k}{4} \frac{z}{z+\frac{k}{3}} \\
&= \frac{z}{z+\frac{k}{3}} - z^{-1} \left ( \frac{k}{4} \frac{z}{z+\frac{k}{3}} \right )
\end{aligned}
$$ {#eq:transfer-new}

Nun können wir mithilfe der Formelsammlung in den Zeitbereich rücktransformieren:

$$
h[n] = \sigma[n] \left ( - \frac{k}{3} \right )^n - \sigma[n - 1] \frac{k}{4} \left ( - \frac{k}{3} \right )^{n - 1}
$$ {#eq:impulse-basic}

Die Impulsantwort lässt sich weiter vereinfachen:

$$
\begin{aligned}
h[n] &= \sigma[n] \left ( - \frac{k}{3} \right )^n - \sigma[n - 1] \frac{k}{4} \left ( - \frac{k}{3} \right )^{n - 1} \\
&= \sigma[n] \left ( - \frac{k}{3} \right )^n - \sigma[n - 1] \frac{3}{4} \frac{k}{3} \left ( - \frac{k}{3} \right )^{n - 1} + \delta[n] \frac{3}{4} \overbrace{\left ( -\frac{k}{3} \right )^0}^{1} - \delta[n] \frac{3}{4} \overbrace{\left ( -\frac{k}{3} \right )^0}^{1} \\
&= \sigma[n] \left ( - \frac{k}{3} \right )^n + \sigma[n - 1] \frac{3}{4} \left ( - \frac{k}{3} \right )^{n} + \delta[n] \frac{3}{4} - \delta[n] \frac{3}{4} \\
&= \sigma[n] \left ( - \frac{k}{3} \right )^n + \sigma[n] \frac{3}{4} \left ( - \frac{k}{3} \right )^{n} - \delta[n] \frac{3}{4} \\
&= \sigma[n] \left ( - \frac{k}{3} \right )^n \left ( 1 + \frac{3}{4} \right ) - \delta[n] \frac{3}{4} \\
h[n] &= \sigma[n] \frac{7}{4} \left ( - \frac{k}{3} \right )^n - \delta[n] \frac{3}{4}
\end{aligned}
$$ {#eq:impulse-advanced}

Somit ergibt sich die in Gl. [@eq:impulse] stehende Impulsantwort. In Fig. 4 wird die Impulsantwort auch anhand einer Skizze veranschaulicht, wobei der konkrete Fall $k=1$ abgebildet ist.

$$
h[n] = \sigma[n] \frac{7}{4} \left ( - \frac{k}{3} \right )^n - \delta[n] \frac{3}{4}
$$ {#eq:impulse}

![Impulsantwort (an dem Beispiel $k=1$)](./res/impulse-response.png){ width=80% }

Als zweiten Teil der Aufgabe gilt es die Sprungantwort zu berechnen. Hier gehen wir ähnlich vor, nur dass wir eben einen Sprung anlegen. Das bedeutet folgendes für die Systemgleichung im Bildbereich:

$$
A(z) = H(z) \frac{z}{z - 1}
$$ {#eq:transfer-step}

Mit etwas Umformung motivieren wir eine Form für die wir schließlich eine Partialbruchzerlegung durchführen können:

$$
\begin{aligned}
A(z) &= H(z) \frac{z}{z - 1} \\
&= \frac{z - \frac{k}{4}}{z + \frac{k}{3}} \frac{z}{z - 1} \\
&= \frac{z^2 - z \frac{k}{4}}{(z + \frac{k}{3})(z - 1)} \\
A(z) &= z \frac{z - \frac{k}{4}}{(z + \frac{k}{3})(z - 1)} \\
\end{aligned}
$$ {#eq:transfer-step-new}

Nun setzen wir folgenden Ansatz für eine Partialbruchzerlegung:

$$
\hat A(z) = z^{-1} A(z) = \frac{z - \frac{k}{4}}{(z + \frac{k}{3})(z - 1)} = \frac{A}{z + \frac{k}{3}} + \frac{B}{z - 1}
$$ {#eq:pfd}

Multiplizieren wir aus, erhalten wir folgende Gleichung:

$$
\begin{aligned}
z - \frac{k}{4} &= A(z - 1) + B(z + \frac{k}{3}) \\
&= Az - A + Bz + B\frac{k}{3}
\end{aligned}
$$ {#eq:system-eq}

Mithilfe eines Koeffizientenvergleiches entsteht dann folgendes Gleichungssystem:

$$
\left \{
\begin{aligned}
1 &= A + B \\
-k/4 &= -A + B k/3
\end{aligned}
\right .
$$ {#eq:system}

Wobei $A = \frac{7k}{4(3+k)}$ und $B = \frac{3(4 - k)}{4(3+k)}$, woraus dann gilt:

$$
A(z) = \frac{7k}{4(3+k)} \frac{z}{z + \frac{k}{3}} + \frac{3(4-k)}{4(3+k)} \frac{z}{z - 1}
$$ {#eq:transfer-step-final}

Mit Gl. [@eq:transfer-step-final] resultiert dann auch in Folge die Sprungantwort in Gl. [@eq:step-response]. Skizziert ist auch diese wieder, konkret mit $k=1$ in Fig. 5. Im allgemeinen lässt sich sagen, dass die Sprungantwort auf einen von $k$ abhängigen Gleichanteil konvergiert.

$$
\begin{aligned}
a[n] &= \sigma[n] \frac{7k}{4(3+k)} \left ( -\frac{k}{3} \right )^n + \sigma[n] \frac{3(4 - k)}{4(3 + k)} \\
&= \sigma[n] \left ( \frac{7k}{4(3+k)} \left ( -\frac{k}{3} \right )^n + \frac{3(4 - k)}{4(3 + k)} \right ) 
\end{aligned}
$$ {#eq:step-response}

![Sprungantwort (an dem Beispiel $k=1$)](./res/step-response.png){ width=80% }
