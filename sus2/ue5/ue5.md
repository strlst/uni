% Beispiel 2.9
% name lastname (11907086)
% 20. April 2021

Sie sollen ein zeitdiskretes System untersuchen, von dem die Beziehung zwischen Eingangssignal $x[n]$ und Ausgangssignal $y[n]$ gegeben ist:

$$
y[n] = \sum_{k = n-1}^{n+1} (x[k + 1] - x[k] + x[k - 1])
$$ {#eq:system}

Zunächst lässt sich auf eine einfachere Darstellung umformen:

$$
\begin{aligned}
y[n] &= x[n] - x[n - 1] + x[n - 2] \\
&+ x[n + 1] - x[n] + x[n - 1] \\
&+ x[n + 2] - x[n + 1] + x[n] \\
\end{aligned}
$$ {#eq:system-simple-transform}

$$
y[n] = x[n + 2] + x[n] + x[n - 2]
$$ {#eq:system-simple}

Da das System nur aus drei additiven Termen besteht die alle den Eingang benutzen, lässt sich auch die Impulsantwort raten:

$$
y[n] = \sum_{k = -\infty}^\infty x[k] h[n - k] \implies h[n] = \delta[n + 2] + \delta[n] + \delta[n - 2]
$$ {#eq:impulse-response}

Diese wird in Folge nützlich sein.

#### (a)

Prüfen Sie (mit Beweis), ob das System folgende Eigenschaften besitzt:

##### (I)
Linearität

$$
\begin{aligned}
y_{tot1} &= \mathcal{T}\{ ax_1[n] + bx_2[n] \} \\
&= ax_1[n + 2] + bx_2[n + 2] + ax_1[n] + bx_2[n] + ax_1[n - 2] + bx_2[n - 2] \\
y_{tot2} &= a\mathcal{T}\{x_1[n]\} + b\mathcal{T}\{x_2[n]\} \\
&= a(x_1[n + 2] + x_1[n] + x_1[n - 2]) + b(x_2[n + 2] + x_2[n] + x_2[n - 2])
\end{aligned}
$$ {#eq:linearity1-argument}

$$
y_{tot1} = y_{tot2}
$$ {#eq:linearity1}

$$
\forall n : x[n] = 0 \implies y[n] = 0
$$ {#eq:linearity2}

Linearität ist gegeben.

##### (II)
Kausalität

Anhand der Impulsantwort [@eq:impulse-response] lässt sich sehr einfach bemerken dass das System akausal sein muss, da Werte aus der Zukunft benötigt werden.

##### (III)
Stabilität

Anhand der Impulsantwort [@eq:impulse-response] ist offensichtlich, dass das System BIBO-stabil sein muss, da die Impulsantwort endlich ist.

##### (IV)
Zeitinvarianz

$$
y[n - k] = x[n - k + 2] + x[n - k] + x[n - k - 2] = \mathcal{T}\{x[n - k]\}
$$ {#eq:time-invariance}

Zeitinvarianz ist gegeben.

Das System ist also linear, akausal, stabil und zeitinvariant.

#### (b)

Berechnen und skizzieren Sie die Impulsantwort und die Übertragungsfunktion des Systems.

Die Impulsantwort wurde bereits berechnet in [@eq:impulse-response]. Die Übertragungsfunktion lässt sich mithilfe der Formelsammlung berechnen:

$$
\begin{aligned}
h[n] \iff H(e^{j \theta}) &= e^{j 2 \theta} + e^{-j 0 \theta} + e^{-j 2 \theta} \\
&= 1 + \frac{2}{2} (e^{j 2 \theta} + e^{-j 2 \theta}) \\
&= 1 + 2cos(2 \theta)
\end{aligned}
$$ {#eq:transfer-function}

![Impulsantwort](./res/impulse-response.png)

![Übertragungsfunktion](./res/transfer-function.png)

In Figur (1) und (2) werden die Impulsantwort [@eq:impulse-response] und die Übertragungsfunktion [@eq:transfer-function] jeweils dargestellt.

\newpage

#### (c)

Welches Ausgangssignal liefert das System für $x[n] = (-1)^n\ \forall n$?

$$
\forall n : x[n] = (-1)^n \implies \forall n : y[n] = \sum_{k = -\infty}^\infty (-1)^k h[n - l] = (-1)^{n + 2} + (-1)^n + (-1)^{n - 2}
$$ {#eq:alternating-reasoning}

aufgrund der duplizierenden Funktion der Impulsfunktion in einer Faltung. Wobei aus $(-1)^{n + 2} = (-1)^n = (-1)^{n - 2}$ folgt, dass:

$$
y[n] = 3 (-1)^n
$$ {#eq:alternating}

#### (d)

Nun wird das System mit dem Signal $x[n] = \lambda ^n$ angeregt. Wie ist $\lambda$ zu wählen, damit $y[n] \equiv 0$ ist?
$$
\forall n : x[n] = \lambda ^n \implies \forall n : y[n] = \sum_{k = -\infty}^\infty \lambda ^k h[n - l] = \lambda ^{n + 2} + \lambda ^n + \lambda ^{n - 2}
$$ {#eq:alternating-reasoning}

$$
y[n] = \lambda ^n (\lambda ^{-2} + 1 + \lambda ^2)
$$ {#eq:alternating}

Damit nun $y[n] \equiv 0$ gelten kann, muss folgendes gelten:

$$
\begin{aligned}
0 &= \lambda ^n (\lambda ^{-2} + 1 + \lambda ^2) \implies \\
\lambda = 0 \lor 0 &= \lambda (\lambda ^{-2} + 1 + \lambda ^2), \lambda \not = 0 \implies \\
0 &= \lambda ^{-2} + 1 + \lambda ^2
\end{aligned}
$$ {#eq:lambda-part-1}

nun substituieren wir:

$$
\begin{aligned}
\alpha ^n = \lambda ^{2n} \implies 0 &= \alpha ^{-1} + 1 + \alpha ^{1} \\
0 &= 1 + \alpha + \alpha ^2
\end{aligned}
$$ {#eq:lambda-part-2}

und verwenden die kleine Lösungsformel:

$$
\begin{aligned}
x_{1,2} &= -\frac{p}{2} \pm \sqrt{\left (\frac{p}{2} \right)^2 - q} \\
\alpha_{1,2} &= -\frac{1}{2} \pm \sqrt{-\frac{3}{4}} \\
\alpha_{1,2} &= \frac{1}{2} \pm j\frac{3}{4}
\end{aligned}
$$ {#eq:lambda-part-3}

anschließend substituieren wir züruck:

$$
\begin{aligned}
\alpha_{1,2} &= \frac{1}{2} \pm j\frac{3}{4} \\
\lambda_{1,...,4} &= \pm \sqrt{\frac{1}{2} \pm j\frac{3}{4}} \\
\end{aligned}
$$ {#eq:lambda-part-4}

und wir landen beim gewünschten Ergebnis.

#### (e)

Geben Sie eine Realisierung des Systems an.

Das System ist offensichtlich in dieser Form unmöglich zu realisieren, aufgrund seiner Akausalität. Aber verschieben wie die Antwort um jeweils $2T$ ($T$ beschreibe den Abstand zweier diskreter Werte), ist es möglich, wie abgebildet in Figur 3.

![Systemrealisierung](./res/system.png)
