% Beispiel 3.2
% name lastname (11907086)
% 28. April 2021

Berechnen Sie das Zeitsignal $x[n]$ für folgende Spektren:

#### (a)

$$
X(e^{j \theta}) = cos^2 \theta
$$ {#eq:spectrum-a}

##### Lösung

Anhand einer trig. Identität können wir unser Spektrum umordnen:

$$
cos 2 \theta = 2 cos^2 \theta - 1 \implies cos^2 = \frac{1}{2} (cos 2\theta + 1) = X(e^{j \theta})
$$ {#eq:spectrum-a-2}

Mithilfe der Linearität der Fouriertransformation können wir nun die zwei additiven Terme separat behandeln:

$$
x[n] = \frac{1}{2} \left ( \mathcal{F}^{-1}\{ 1 \} + \mathcal{F}^{-1}\{cos 2\theta\} \right ) = \frac{1}{2} \left ( \delta[n] + \frac{1}{2} \delta[n - 2] + \frac{1}{2} \delta[n - 2] \right )
$$ {#eq:signal-a}

#### (b)

![Spektrum](./res/spectrum-b.png)

##### Lösung

Um dieses Spektrum handhabbar zu machen, es notwendig das Spektrum in eine andere Form zu bringen. Eine besonders hierfür geeignete Form wäre, das Dreiecksspektrum als eine Faltung eines Rechtsspektrums mit sich selbst zu gestalten. Insbesondere aufgrund der Korrespondenz der Faltung im Zeitbereich. Damit dabei die Werte zusammenpassen wird das Signal wie folgt gewählt:

$$
Y(e^{j \theta}) = 
\begin{cases}
\frac{1}{\sqrt{\theta_g}}, & 0 \leq \theta \leq \frac{\theta_g}{2} \\
0, & 0 < \theta < \pi
\end{cases}
$$ {#eq:spectrum-b-y}

![Spektrum des Zwischensignals](./res/spectrum-b-y.png)

mit der Eigenschaft:

$$
X(e^{j \theta}) = Y(e^{j \theta}) \ast Y(e^{j \theta})
$$ {#eq:spectrum-b-as-y}

nun brauchen wir noch das Signal $y[n]$:

$$
y[n] = \frac{1}{\sqrt{\theta_g}} \frac{sin \frac{\theta_g}{2}}{\pi n}, 0 < \frac{\theta_g}{2}
$$ {#eq:signal-b-y}

Das Signal $x[n]$ ergibt sich dann mithilfe der Faltungseigenschaft:

$$
\mathcal{F}^{-1}\{Y(e^{j \theta}) \ast Y(e^{j \theta})\} = 2 \pi \mathcal{F}^{-1}\{ Y(e^{j \theta}) \}^2
$$ {#eq:property-transform}

Somit ergibt sich für uns das Signal $x[n]$ wie folgt:

$$
x[n] = \frac{2 \pi sin^2 \frac{\theta_g}{2}}{\sqrt{\theta_g}^2 \pi^2 n^2} = \frac{2 sin^2 \frac{\theta_g}{2}}{\theta_g \pi n^2}
$$ {#eq:signal-b-x-calculation}

#### (c)

##### Lösung

keine Lösung

#### (d)

$$
X(e^{j \theta}) = \frac{e^{-j \theta}}{1 + \frac{1}{6} e^{-j \theta} - \frac{1}{6} e^{-j 2 \theta}}
$$ {#eq::spectrum-d}

##### Lösung

Um eine Chance zu haben versuchen wir zunächst den Term zu vereinfachen, dafür führen wir eine Variablentransformation $t = e^{-j \theta}$ ein:

$$
X(e^{j \theta}) = \frac{e^{-j \theta}}{1 + \frac{1}{6} e^{-j \theta} - \frac{1}{6} e^{-j 2 \theta}} = \frac{t}{1 + \frac{1}{6} (t - t^2)}
$$ {#eq::spectrum-d}

Wir suchen zunächst einmal die Nullstellen des Nennerterms mithilfe der quadratischen Lösungsformel $x_{1,2} = -\frac{p}{2} \pm \sqrt{(\frac{p}{2})^2 - q}$ und stellen mithilfe der Nullstellen das Ergebnis als Multiplikation darstellen:

$$
\begin{aligned}
0 = 1 + \frac{1}{6} (t - t^2) \implies -6 &= t - t^2 \implies t^2 - t - 6 = 0 \\
t_{1,2} &= \frac{1}{2} \pm \sqrt{(-\frac{1}{2}^2 - (-6))} = \frac{1}{2} \pm \frac{5}{6}
\end{aligned}
$$ {#eq:roots}

$$
X(t) = \frac{t}{(t-3)(t+2)}
$$ {#eq:new-form}

Dann führen wir eine Partialbruchzerlegung durch:

$$
\begin{aligned}
\frac{t}{(t-3)(t+2)} &= \frac{A}{t - 3} + \frac{B}{t + 2} \\
\implies t &= At + 2A + Bt - 3B \\
I: 0 &= 2A - 3B \\
II: 1 &= A + B
\end{aligned}
$$ {#eq:break}

Mit $A = \frac{3}{5}$ und $B = \frac{2}{5}$. Nun können wir unser Spektrum umschreiben:

$$
X(t) = \frac{3}{5(t - 3)} + \frac{2}{5(t + 2)}
$$ {#eq:new-spectrum}

$$
\begin{aligned}
X(e^{-j \theta}) &= \frac{3 \cdot -\frac{1}{3}}{5(e^{-j \theta} - 3) \cdot -\frac{1}{3}} + \frac{2 \cdot \frac{1}{2}}{5(t + 2) \cdot \frac{1}{2}} \\
&= -\frac{1}{5(1 - \frac{1}{3} e^{-j \theta})} + \frac{1}{5(1 + \frac{1}{2} e^{-j \theta})}
\end{aligned}
$$ {#eq:new-spectrum-2}

Mithilfe der dazu entsprechenden Korrespondenz aus der Formelsammlung sowie die Linearität der Fouriertransformation gilt:

$$
\begin{aligned}
x[n] &= -\frac{1}{5} \left (-\frac{1}{3} \right )^n \sigma[n] + \frac{1}{5} \left ( \frac{1}{2} \right )^n \sigma[n] \\
&= \frac{1}{5} \sigma[n] \left ( \left ( \frac{1}{2} \right )^n - \left ( \frac{1}{3} \right )^n \right )
\end{aligned}
$$

Notiz: es scheint ein Faktor 6 verloren gegangen zu sein und zwei Koeffizienten gehören vertauscht, aber die ansonsten ist die Lösung richtig
