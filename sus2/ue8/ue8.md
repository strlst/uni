% Beispiel 4.3
% name lastname (11907086)
% 18. Mai 2021

Führen Sie eine Partialbruchzerlegung durch und bestimmen Sie Pole und Nullstellen. Wählen Sie den Konvergenzbereich derart, dass Sie ein rechtsseitiges Signal erhalten und berechnen Sie dieses!

# (a)

$$
H(z) = \frac{z}{(z - \frac{1}{2})(z - \frac{1}{4})}
$$ {#eq:a-transfer}

##### Lösung

Die Pole und Nullstellen lassen Sich unmittelbar aus der Übertragungsfunktion ablesen: $z_{\infty 1} = 1/4$, $z_{\infty 2} = 1/2$ und $z_{01} = 0$

Wir legen die Partialbruchzerlegung wie folgt an. Insbesondere bringen wir zuerst das $z$ im Nenner auf die andere Seite um nachher weiterverwenden zu können:

$$
\hat H(z) = z^{-1} H(z) = \frac{1}{(z - \frac{1}{2})(z - \frac{1}{4})} = \frac{A}{z - \frac{1}{2}} + \frac{B}{z - \frac{1}{4}}
$$ {#eq:a-pfd}

Multiplizieren wir aus erhalten wir eine Gleichung [@eq:a-eq]:

$$
1 = Az - A/4 + Bz - B/2
$$ {#eq:a-eq}

aus der wir mithilfe eines Koeffizientenvergleichs ein Gleichungssystem [@eq:a-sys] aufstellen und lösen können:

$$
\left \{
\begin{aligned}
0 &= A + B \\
1 &= -A/4 + -B/2
\end{aligned}
\right .
$$ {#eq:a-sys}

Schließlich erhalten wir mit $A=4$ und $B=-4$:

$$
H(z) = z \hat H(z) = 4\frac{z}{z - \frac{1}{2}} - 4\frac{z}{z - \frac{1}{4}}
$$ {#eq:a-transfer-decomposed}

Wir gehen aufgrund der Angabe von einem rechtsseitigen Signal aus, welches dann durch $|z| > R_{max} = 1/2$ stabil ist, da es den Einheitskreis enthält. Nun können wir mithilfe der Formelsammlung rücktransformieren:

$$
h[n] = 4 \sigma[n] \left ( \left( \frac{1}{2} \right)^n - \left( \frac{1}{4} \right)^n \right )
$$ {#eq:a-signal}

# (b)

$$
H(z) = \frac{1}{(z - \frac{1}{3})(z + \frac{1}{4})}
$$ {#eq:b-transfer}

##### Lösung

Die Lösung verlauft analog zu (a). Wieso unser Pol unendlich ist offenbart sich im Verlauf der Berechnung. $z_{\infty 1} = -1/4$, $z_{\infty 2} = 1/3$ und $z_{01} = \infty$

Wir legen die Partialbruchzerlegung wie folgt an:

$$
H(z) = \frac{1}{(z - \frac{1}{3})(z + \frac{1}{4})} = \frac{A}{z - \frac{1}{3}} + \frac{B}{z + \frac{1}{4}}
$$ {#eq:b-pfd}

Multiplizieren wir aus erhalten wir eine Gleichung [@eq:b-eq]:

$$
1 = Az + A/4 + Bz - B/3
$$ {#eq:b-eq}

aus der wir mithilfe eines Koeffizientenvergleichs ein Gleichungssystem [@eq:b-sys] aufstellen und lösen können:

$$
\left \{
\begin{aligned}
0 &= A + B \\
1 &= A/4 + -B/3
\end{aligned}
\right .
$$ {#eq:b-sys}

Schließlich erhalten wir mit $A=\frac{12}{7}$ und $B=-\frac{12}{7}$:

$$
H(z) = \frac{12}{7} \frac{1}{z - \frac{1}{3}} - \frac{12}{7} \frac{1}{z + \frac{1}{4}} = z^{-1} \left (\frac{12}{7} \frac{z}{z - \frac{1}{3}} - \frac{12}{7} \frac{z}{z + \frac{1}{4}} \right)
$$ {#eq:b-transfer-decomposed}

Hier erweitern wir um $z$ und machen dann anschließend in der Rücktransformation Gebrauch von der Rechenregel aus der Formelsammlung: $x[n + n_0] \Leftrightarrow z^{n_0} X(z)$. Wir gehen wieder aufgrund der Angabe von einem rechtsseitigen Signal aus, welches dann durch $|z| > R_{max} = 1/3$ stabil ist, da es den Einheitskreis enthält. Nun können wir mithilfe der Formelsammlung rücktransformieren:

$$
h[n] = \frac{12}{7} \sigma[n - 1] \left ( \left( \frac{1}{3} \right)^{n-1} + \left( \frac{1}{4} \right)^{n-1} \right )
$$ {#eq:b-signal}

# (c)

$$
H(z) = \frac{1}{(3z - 1)(6z - 1)(2z - 1)}
$$ {#eq:c-transfer}

##### Lösung

Die Lösung verlauft analog zu (b). $z_{\infty 1} = 1/6$, $z_{\infty 2} = 1/3$, $z_{\infty 3} = 1/2$ und $z_{01} = \infty$

Wir legen die Partialbruchzerlegung wie folgt an:

$$
H(z) = \frac{1}{(3z - 1)(6z - 1)(2z - 1)} = \frac{A}{3z - 1} + \frac{B}{6z - 1} + \frac{C}{2z - 1}
$$ {#eq:c-pfd}

Multiplizieren wir aus erhalten wir eine Gleichung [@eq:c-eq]:

$$
\begin{aligned}
1 &= A(6z - 1)(2z - 1) + B(3z - 1)(2z -1) + C(3z - 1)(6z - 1) \\
&= 12Az^2 - 2Az - 6Az + A + 6Bz^2 - 2Bz - 3Bz + B + 18Cz^2 - 3Cz - 6Cz + C \\
&= z^2 (12A + 6B + 18C) + z (-8A - 5B - 9C) + (A + B + C) \\
\end{aligned}
$$ {#eq:c-eq}

aus der wir mithilfe eines Koeffizientenvergleichs ein Gleichungssystem [@eq:c-sys] aufstellen und lösen können:

$$
\left \{
\begin{aligned}
0 &= 12A + 6B + 18C \\
0 &= 8A + 5B + 9C \\
1 &= A + B + C
\end{aligned}
\right .
$$ {#eq:c-sys}

Schließlich erhalten wir mit $A=-3$, $B=3$ und $C=1$:

$$
H(z) = -3 \frac{1}{3z - 1} + 3 \frac{1}{6z - 1} + \frac{1}{2z - 1} = z^{-1} \left ( -\frac{z}{z - \frac{1}{3}} + \frac{z}{2(z - \frac{1}{6})} + \frac{z}{2(z - \frac{1}{2})} \right )
$$ {#eq:b-transfer-decomposed}

Analog wie bei (b). Wir gehen wieder aufgrund der Angabe von einem rechtsseitigen Signal aus, welches dann durch $|z| > R_{max} = 1/2$ stabil ist, da es den Einheitskreis enthält. Nun können wir mithilfe der Formelsammlung rücktransformieren:

$$
h[n] = \sigma[n - 1] \left ( - \left ( \frac{1}{3} \right ) ^{n-1} + \frac{1}{2} \left ( \frac{1}{6} \right ) ^{n-1} + \frac{1}{2} \left ( \frac{1}{2} \right ) ^{n-1}\right )
$$ {#eq:e-signal}

\pagebreak

# nur Partialbruchzerlegung (d)

$$
H(z) = \frac{z}{(z - \frac{1}{2})^2}
$$ {#eq:d-transfer}

##### Lösung

Für diese Aufgabe führen wir nur die Partialbruchzerlegung durch. Das tun wir, indem wir speziell ansetzen:

$$
H(z) = \frac{z}{(z - \frac{1}{2})^2} = \frac{A}{(z - \frac{1}{2})^2} + \frac{B}{z - \frac{1}{2}}
$$ {#eq:d-pfd}

Multiplizieren wir aus erhalten wir eine Gleichung [@eq:d-eq]:

$$
z = A + Bz - B/2
$$ {#eq:d-eq}

aus der wir mithilfe eines Koeffizientenvergleichs ein Gleichungssystem [@eq:d-sys] aufstellen und lösen können:

$$
\left \{
\begin{aligned}
1 &= B \\
0 &= A - B/2
\end{aligned}
\right .
$$ {#eq:d-sys}

Schließlich erhalten wir mit $A=\frac{1}{2}$ und $B=1$:

$$
H(z) = \frac{1}{2(z - \frac{1}{2})^2} + \frac{1}{z - \frac{1}{2}}
$$ {#eq:d-transfer-decomposed}
