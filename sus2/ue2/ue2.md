% Beispiel 1.5
% name lastname (11907086)
% 15. März 2021

Aufgabenstellung: Das Signal $x[n]$ sei periodisch mit Periode $N$. Prüfen Sie, ob das Signal $y[n] = x[M n]$ ($M$ ganzzahlig) ebenfalls periodisch ist. Bestimmen Sie gegebenenfalls die Periode von $y[n]$.

Betrachten wir zunächst für unsere Überlegungen einen Spezialfall diskreter periodischer Signale: Ein Kosinus welcher $N$ periodisch ist kann in allgemeiner Form $x[n] = cos(\theta_0 n)$ dargestellt werden, wobei die Frequenzvariable $\theta_0 = \frac{2 \pi k}{N}$ ist. Betrachten wir nun das Signal $y[n] = x[M n]$, müssen wir in $x[n]$ entsprechend unseren Index transformieren: $y[n] = cos(\theta_0 M n)$.

Die Vorraussetzung für Periodizität ist nämlich, das $\frac{k}{N}$ eine rationale Zahl ist, da:

$$
x[n + N] = cos \left(2 \pi \frac{k}{N} n + 2 \pi \frac{k}{N} N \right) = cos \left(2 \pi \frac{k}{N} n \right) = x[n]
$$ {#eq:periodicity}

Für $y[n]$ ist diese Bedingung trivialerweise erfüllt, da die Transformation unserer Indexvariable $n$ nicht unsere hinreichende Bedingung beeinflusst:

$$
x[M n + N] = cos \left(2 \pi \frac{k}{N} M n + 2 \pi \frac{k}{N} N \right) = cos \left(2 \pi \frac{k}{N} M n \right) = x[M n]
$$ {#eq:y_periodicity}

Wir beobachten außerdem in dem spezifischen Beispiel eines Kosinussignals, dass für entsprechende $M$ die kleinstmögliche Periode sich verändert. Für ein diskretes Signal setzen wir allgemein voraus, dass die Indexvariable des Signals ganzzahlig ist. Für $M \not = 0$ betrachten wir die Definition der Eigenschaft der Perioditizität mit $\forall n, N \in \mathbb{N}, m \in \mathbb{Z}$:

$$
x[M n + m N] = x[M n] = x \left[n + m \frac{N}{M} \right]
$$ {#eq:periodicity_def}

Für den Fall das $\frac{N}{M} = N_M$ ganzzahlig ist ergibt sich für $y[n] = x[M n]$ die neue Periode $N_M$ mit $N_M \leq N$. Für den fall das $N_M$ nicht ganzzahlig ist folgt aus (2) das N die Periode von $y[n]$ sein muss. Wir können also argumentieren, dass die tatsächliche Periode $N_y$ von $y[n] = kgV(N_M, M)$ sein muss, wobei immer $N_y \leq N$. Für $M = 0$ haben wir einen Sonderfall:

$$
x[M n + m N] = x[0 + m N] = x[0]
$$

Welcher auch in einem offensichtlich periodischen Signal resultiert.

Damit unsere Observierungen auch für den allgemeinen Fall gelten muss noch gezeigt werden, $y[n] = x[M n]$ periodisch sein muss für jedes periodische Signal $x[n]$. Für die Indexvariable $o = M n$ muss gelten, dass $o \in \mathbb{Z}$, da $n \in \mathbb{N}, M \in \mathbb{Z}$. Mit der Definition der Eigenschaft der Periodizität gilt also:

$$
x[n + m N] = x[n]
$$ {#eq:periodicity_def_x}

$$
x[M n + m N] = x[o + m N] = x[o] = x[M n] = y[n]
$$ {#eq:periodicity_def_y}

Da $o \in \mathbb{Z}$ muss auch $y[n]$ periodisch sein.
