% Beispiel 1.1
% name lastname (11907086)
% 8. März 2021

Wir gehen von folgendem zeitbegrenzten Signal aus:

$$ x[n] =  \left\{
\begin{array}{ll}
      0 & n < 0 \\
      n & 0 \leq n \leq 10 \\
      0 & n > 0 \\
\end{array} 
\right.
$$

## (a)

Aufgabenstellung: zeichnen Sie $x[n + 5]$

![MATLAB 1.1 a](./res/ue1-a.png)
\ 
\pagebreak

## (b)

Aufgabenstellung: zeichnen Sie $x[-n + 5]$

![MATLAB 1.1 b](./res/ue1-b.png)
\ 
\pagebreak

## (c)

Aufgabenstellung: zeichnen Sie $x[2n]$

![MATLAB 1.1 c](./res/ue1-c.png)
\ 
\pagebreak

## (d)

Aufgabenstellung: zeichnen Sie das gerade und ungerade Teilsignal von $x[n]$

![MATLAB 1.1 d](./res/ue1-d.png)
\ 
\pagebreak

## (e)

Aufgabenstellung: zeichnen Sie $x[n + 10] + x[-n + 10] - 10 \delta [n]$

![MATLAB 1.1 e](./res/ue1-e.png)
\ 
\pagebreak

## MATLAB source code

```matlab
n = 0:10;

delta = @(t, shift) 1 * (t == shift);

figure;
x = n;
hold on;
stem(n, x);
stem(n - 5, x);
legend('x[n]', 'x[n + 5]');

figure;
hold on;
stem(n, x);
stem(-n + 5, x);
legend('x[n]', 'x(- n + 5)');

figure;
hold on;
stem(n, x);
stem(2 * n, x);
legend('x[n]', 'x[2n]');

figure;
hold on;
stem(n, x);
stem(n, 1/2*(x + x(end:-1:1)));
stem(n, 1/2*(x - x(end:-1:1)));
legend('x[n]', 'x_u[n]', 'x_g[n]');

figure;
hold on;
stem(n, x);
stem(n - 10, x + x(end:-1:1) + 10 * delta(n, 10));
legend('x[n]', 'x[n + 10] + x[-n + 10] + 10\delta[n]');
```
