---
title: High-Level Synthesis
author: Norbert Tremurici (\texttt{e11907086@student.tuwien.ac.at})
header-includes:
  - '`\usepackage{pdflscape}`{=latex}'
---

# Assignment 1

## Exercise 1

### task 1 a, b

Description: Give the three address code for the function. (2)

Description: Optimize the three address code as much as possible. (3)

First we convert the code into three address form. Do note that in our conversion, we have already considered the upcoming optimization steps by avoid multiplications with negative powers of 2 wherever possible for easier transformation steps.

The following code on the left side of the table implements the function `calc_homework`, where the block beginning at B1 represents the taken branch of the if statement in the C-program.
This means, starting from line 4, we are calculating the not taken branch up until the return statement.

On the right we have already annotated possible optimization steps and the code on the left side of the table represents the transformed version.
(SR = strength reduction, THR = tree height reduction)

```
 1     t1 = a * b           |        |
 2     t2 = t1 * c          |        |
 3     if t2 > 0 goto B1    |        |
 4     t3 = 2 * a           | SR     |        t3 = a << 1
 5     t4 = 4 * c           | SR     |        t4 = c << 2
 6     t5 = t4 * c          |        |
 7     t6 = t5 - t3         |        |
 8     t7 = 8 * b           | THR SR |        t7 = b << 3
 9     t8 = t7 * b          | THR    |        t8 = b * b
10     t9 = t8 * b          | THR    |        t9 = t7 * t8
11     d = t6 - t9          |        |
12     return d             |        |
13 B1: t10 = 2 * a          | SR     |    B1: t10 = a << 1
14     t11 = 4 * c          | SR     |        t11 = c << 2
15     t12 = t11 * c        |        |
16     t13 = t10 - t12      |        |
17     t14 = 8 * b          | THR SR |        t14 = b << 3
18     t15 = t14 * b        | THR    |        t15 = b * b
19     t16 = t15 * b        | THR    |        t16 = t14 * t15
20     d = t13 + t16        |        |
21     return d             |        |
```

Noticing that what we have optimized essentially has the same structure, we can eliminate the common subexpressions.
Since both branches are executing the same subexpressions, we can also move the common subexpression before the branch.
We have called this optimization "code motion + common subexpression elimination".
(CM + CSE = code motion + common subexpression elimination)

```
 1     t1 = a * b           |          |        t1 = a * b
 2     t2 = t1 * c          |          |        t2 = t1 * c
 3     if t2 > 0 goto B1    |          |        t3 = a << 1
 4     t3 = a << 1          | CM + CSE |        t4 = c << 2
 5     t4 = c << 2          | CM + CSE |        t5 = t4 * c
 6     t5 = t4 * c          | CM + CSE |        t6 = b << 3
 7     t6 = t5 - t3         |          |        t7 = b * b
 8     t7 = b << 3          | CM + CSE |        t8 = t6 * t7
 9     t8 = b * b           | CM + CSE |        if t2 > 0 goto B1
10     t9 = t7 * t8         | CM + CSE |        t9 = t5 - t3
11     d = t6 - t9          |          |        d = t9 - t8
12     return d             |          |        return d
13 B1: t10 = a << 1         | CM + CSE |    B1: t10 = t3 - t5
14     t11 = c << 2         | CM + CSE |        d = t10 + t8
15     t12 = t11 * c        | CM + CSE |        return d
16     t13 = t10 - t12      |          |
17     t14 = b << 3         | CM + CSE |
18     t15 = b * b          | CM + CSE |
19     t16 = t14 * t15      | CM + CSE |
20     d = t13 + t16        |          |
21     return d             |          |
```

With this complex optimization, we can see the structure of the code has been profoundly changed.
The remaining operations in the branch could not be eliminated since they have a different order of operands relative to each other.
We can also notice something interesting, in essence, both branches compute the same quantity, only that one of the branches negates the result.
This might allow the following transformations:

```
 9     if t2 > 0 goto B1 |      if t2 > 0 goto B1 |      t9 = t3 - t5
10     t9 = t5 - t3      >      t9 = t3 - t5      >      d = t9 + t8
11     d = t9 - t8       |      d = t9 + t8       |  B1: if t2 > 0 goto RET
12     return d          |      d = 0 - d         |      d = 0 - d
13 B1: t10 = t3 - t5     |      return d          | RET: return d
14     d = t10 + t8      | B1:  t10 = t3 - t5     |
15     return d          >      d = t10 + t8      >
16                       |      return d          |
```

By expanding the code we have reduced the code even further.
However, since we now have unbalanced branches and an additional operation on the critical path, which will not actually make a hardware implementation faster, we will not use this last optimization step.
It is just interesting to note that this optimization might be interesting for a software solution.

So our final code for the solution is the following.

```
 1      t1 = a * b
 2      t2 = t1 * c
 3      t3 = a << 1
 4      t4 = c << 2
 5      t5 = t4 * c
 6      t6 = b << 3
 7      t7 = b * b
 8      t8 = t6 * t7
 9      if t2 > 0 goto B1
10      t9 = t5 - t3
11      d = t9 - t8
12      return d
13  B1: t10 = t3 - t5
14      d = t10 + t8
15      return d
```

One last note, since the value of d is composed of factors which are progressive powers of 2 each, we could have broken down the computation into the following formula.

$$
\text{sign} * 2 * (a - 2 * (c * c - 2 * (b * b * b)))
$$

However, since this solution would likely yield a longer critical path, blocking tree height optimizations for instance, this line was not further pursued. 

### task 1 c

Description: Draw the sequencing graph for the optimized three address code (all following tasks should use this optimized code). (2)

Figure \ref{fig:1c} shows the sequencing graph for the optimized code of the previous task.
Do note that we have already annotated the priorities we are going to use in the next task.

![\label{fig:1c}Sequencing graph (with priority annotations)](graphics/1-c-seq-graph.pdf){ width=100% }

\pagebreak

### task 1 d

Description: Compute the start times for all operations using the list schedule method for a resource constraint of one ALU that can execute shifts, comparisons, additions and substractions and two pipelined multipliers that can execute multiplications. The delay of the ALU is one clock cycle and the delay of the pipelined multipliers is three clock cycles with initialization interval of one clock cycle. (4)

So for this task, we are performing the list scheduling algorithm.
In our case we have a branch unit (non-functional unit) of which we will compute a separate schedule and use its resulting latency $\Lambda_{\text{BR}}$ as the delay of the unit.
Later we will have to make sure that no operations using resources which are also used in the branch unit are scheduled while the branch unit is active.

Table \ref{tab:schedule-br} gives us the start times just for the branch unit (non-functional unit).
We have two ALUs, because we can schedule operations on the same ALU on different branches.
In our case, the different ALUs are denoted by `ALU,br` and `ALU,nbr` for the branch and no branch cases respectively.

Something interesting to note is, the decision of the branch is actually needed not at the beginning of the branch unit, but at the end when deciding which result to use.
This means the branch unit could be scheduled one cycle earlier.
In general the question is at what granularity to look at dataflow dependencies crossing hierarchical units.
For the purpose of this exercise, we will assume hierarchical components form clean barriers and can only start executing their function once all required inputs are available, so in this case we are waiting even for the branch decision, even though we would only need it later.

Table \ref{tab:schedule-all} gives the schedule according to the list scheduling for the entire function.
For this schedule, there is no $T_{\text{act,MUL}}$, since we have pipelined units with initiation interval 1.
In effect this means that the multiplier cannot be blocked by ongoing operations as we are able to schedule new multiplications every cycle.
Even though our unit is pipelined, sequential data dependencies do not allow us to keep the pipeline filled for optimal throughput.
In general, we have omitted the sets of blocked operations for all units which cannot be blocked due to having an initiation interval of 1 cycle, or a delay of 1 cycle.

\pagebreak

\begin{landscape}

\begin{table}
\centering
\begin{tabular}{cccccccc}
$t_{\text{act}}$ & $U_{\text{act,ALU,nbr}}$ & $S_{\text{act,ALU,nbr}}$ & $U_{\text{act,ALU,br}}$ & $S_{\text{act,ALU,br}}$ & start times \\
1 & $\{v_{13}\}$ & $\{v_{13}\}$ & $\{v_{14}\}$ & $\{v_{14}\}$ & $t_{11} = t_{12} = t_{13} = t_{14} = 1$ \\
2 & $\{v_{15}\}$ & $\{v_{15}\}$ & $\{v_{16}\}$ & $\{v_{16}\}$ & $t_{15} = t_{16} = 2$ \\
3 & $\emptyset$ & $\emptyset$ & $\emptyset$ & $\emptyset$ & $t_{17} = t_{18} = 3$
\end{tabular}
\caption{\label{tab:schedule-br}Schedule for the branch unit with latency $\Lambda_{\text{BR}} = max\{t_{17}, t_{18}\} - 1 = 2$}
\end{table}

\begin{table}
\centering
\begin{tabular}{cccccccccccccc}
$t_{\text{act}}$ &
$U_{\text{act,ALU}}$ & $S_{\text{act,ALU}}$ &
$U_{\text{act,MUL}}$ & $S_{\text{act,MUL}}$ &
$U_{\text{act,BR}}$ & $S_{\text{act,BR}}$ & $T_{\text{act,BR}}$ &
$U_{\text{act,ret}}$ & $S_{\text{act,ret}}$ &
start times \\
1 &
$\{v_{3}, v_{4}, v_{6}\}$ & $\{v_{6}\}$ &
$\{v_{1}, v_{7}\}$ & $\{v_{1}, v_{7}\}$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{0} = t_{1} = t_{6} = t_{7} = 1$ \\
2 &
$\{v_{3}, v_{4}\}$ & $\{v_{4}\}$ &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{4} = 2$ \\
3 &
$\{v_{3}\}$ & $\{v_{3}\}$ &
$\{v_{5}\}$ & $\{v_{5}\}$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{3} = t_{5} = 3$ \\
4 &
$\emptyset$ & $\emptyset$ &
$\{v_{2}, v_{8}\}$ & $\{v_{2}, v_{8}\}$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{2} = t_{8} = 4$ \\
5 &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
\\
6 &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
\\
7 &
$\{v_{9}\}$ & $\{v_{9}\}$ &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{9} = 7$ \\
8 &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$\{v_{10}\}$ & $\{v_{10}\}$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{10} = 8$ \\
9 &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ & $\{v_{10}\}$ &
$\{v_{19}\}$ & $\{v_{19}\}$ &
$t_{19} = 9$ \\
10 &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ & $\emptyset$ &
$\emptyset$ & $\emptyset$ &
$t_{20} = 10$ \\
\end{tabular}
\caption{\label{tab:schedule-all}Schedule for the entire function}
\end{table}

\end{landscape}

\pagebreak

### task 1 e

Description: Find a valid binding of the multiplications to the multipliers using the left-edge algorithm. (3)

Usually, we would perform the left-edge algorithm for each functional unit type separately, however we will skip this for units on which there can never be any conflict, like the branch or return units on which only one operation is ever scheduled, or the ALU of which there is only one unit.

This leaves us with just the multipliers for this task.
All intervals have length 1, since we have two pipelined multipliers with initiation intervals of 1.
The intervals are as follows.

$$
I(v_{1}) = I(v_{7}) = [1\ 1], I(v_{5}) = [3\ 3], I(v_{2}) = I(v_{8}) = [4\ 4]
$$

Thus we can construct the table in Table \ref{tab:bind-op}. What we end up with are colors 1 and 2, where color 1 is assigned to $\{v_{1}, v_{5}, v_{2}\}$ and color 2 is assigned to $\{v_{7}, v_{8}\}$.
We will be able to confirm the correctness of this schedule and operation binding visually later by drawing the dataflow graph with the schedule and bindings combined.

\begin{table}
\centering
\begin{tabular}{ccccc}
$c$ & $r_{\text{act}}$ & $L$ & $S$ & $I_{s}$ \\
$1$ & $0$ & $[I(v_{1}), I(v_{7}), I(v_{5}), I(v_{2}), I(v_{8})]$ & $\emptyset$ & $I(v_{1})$ \\
$1$ & $1$ & $[I(v_{7}), I(v_{5}), I(v_{2}), I(v_{8})]$ & $\{I(v_{1})\}$ & $I(v_{5})$ \\
$1$ & $3$ & $[I(v_{7}), I(v_{2}), I(v_{8})]$ & $\{I(v_{1}), I(v_{5})\}$ & $I(v_{2})$ \\
$1$ & $4$ & $[I(v_{7}), I(v_{8})]$ & $\{I(v_{1}), I(v_{5}), I(v_{2})\}$ & $/$ \\
$2$ & $0$ & $[I(v_{7}), I(v_{8})]$ & $\emptyset$ & $I(v_{7})$ \\
$2$ & $1$ & $[I(v_{8})]$ & $\{I(v_{7})\}$ & $I(v_{8})$ \\
$2$ & $4$ & $[]$ & $\{I(v_{7}), I(v_{8})\}$ & $/$ \\
\end{tabular}
\caption{\label{tab:bind-op}Operation binding for multiplier units}
\end{table}

### task 1 f

Description: Give the lifetime of all compiler temporary variables `tx`. Draw the register conflict graph for these variables `tx`. (3)

For this exercise, we are going to use our actual start times and read off the lifetimes of variables that need to be stored.
This is easier done visually, so Figure \ref{fig:lifetimes} gives a visualization of all variable lifetimes, from the moment they become available, denoted `AV`, to the moment they are consumed (used and not needed any more), denoted `use`.
Black lines serve as visual guidelines.
Red lines indicate the actual lifetimes.
Do note that the operation binding of the last exercise has also been included by using various colors.

![\label{fig:lifetimes}Lifetimes of variables `tx` visualized](graphics/1-f-lifetimes.pdf)

From this visualization, we can infer the following lifetimes.
To avoid naming conflicts (with the code), we have named the output of node $v_{9}$ as $t_{B}$.

We can also draw the conflict graph.
Figure \ref{fig:conflict} shows the basic, uncolored conflict graph for variables `tx`.
Do note that since we have mutually exclusive paths for the branches, where in each branch the ALU is used in a different way, the nodes $v_{13}$ and $v_{14}$, or the nodes $v_{15}$ and $v_{16}$ are not truly in conflict with each other.
This is visualized on the figure using dashed lines.

![\label{fig:conflict}Uncolored conflict graph for variables `tx`](graphics/1-f-conflict.pdf){ width=50% }

$$
\begin{aligned}
I(t_{1}) &= [4\ 4], &I(t_{2}) &= [7\ 7], &I(t_{3}) &= [4\ 8], &I(t_{4}) &= [3\ 3], &I(t_{5}) &= [6\ 8], \\
I(t_{6}) &= [2\ 4], &I(t_{7}) &= [4\ 4], &I(t_{8}) &= [7\ 9], &I(t_{10}) &= [10\ 10], &I(t_{B}) &= [8\ 9], \\
I(t_{13}) &= [9\ 9], &I(t_{14}) &= [9\ 9], &I(t_{15}) &= [10\ 10], &I(t_{16}) &= [10\ 10]& \\
\end{aligned}
$$

### task 1 g

Description: Find a valid binding of the compiler temporary variables to registers using the left edge algorithm. How many registers are required? Color the register conflict graph accordingly. (2)

Similar to the operation binding, we construct a table in Table \ref{tab:bind-reg}.
The only difference is that we introduce the notation $I_{n}$ to represent $I(t_{n})$ for brevity.
The first row of the table shows the entire list of intervals sorted by their start times.

As a small note, we will allow ourselves as a slight modification of the left-edge algorithm to take the branch temporaries which are not actually in conflict with each other at the same time for the same register.
One way to do this in the algorithm is to merge nodes which are virtually the same, representing $I_{13}$ and $I_{14}$ as $I_{13/14}$.
At this stage we are not only merging $I_{15}$ and $I_{16}$, but also $I_{11}$.
This is because both $v_{15}$ and $v_{16}$ are final outputs of the branch unit, have the same type and represent the return value of the branch unit, which is also the output of $v_{11}$.

\begin{table}
\centering
\begin{tabular}{ccccc}
$c$ & $r_{\text{act}}$ & $L$ & $S$ & $I_{s}$ \\
$1$ & $0$ & $[I_{6}, I_{4}, I_{1}, I_{3}, I_{7}, I_{5}, I_{2}, I_{8}, I_{B}, I_{13/14}, I_{15/16/10}]$ & $\emptyset$ & $I(t_{6})$ \\
$1$ & $4$ & $[I_{4}, I_{1}, I_{3}, I_{7}, I_{5}, I_{2}, I_{8}, I_{B}, I_{13/14}, I_{15/16/10}]$ & $\{I_{6}\}$ & $I(t_{5})$ \\
$1$ & $8$ & $[I_{4}, I_{1}, I_{3}, I_{7}, I_{2}, I_{8}, I_{B}, I_{13/14}, I_{15/16/10}]$ & $\{I_{6}, I_{5}\}$ & $I(t_{13/14})$ \\
$1$ & $9$ & $[I_{4}, I_{1}, I_{3}, I_{7}, I_{2}, I_{8}, I_{B}, I_{15/16/10}]$ & $\{I_{6}, I_{5}, I_{13/14}\}$ & $I(t_{15/16/10})$ \\
$1$ & $10$ & $[I_{4}, I_{1}, I_{3}, I_{7}, I_{2}, I_{8}, I_{B}]$ & $\{I_{6}, I_{5}, I_{13/14}, I_{15/16/10}\}$ & $/$ \\
$2$ & $0$ & $[I_{4}, I_{1}, I_{3}, I_{7}, I_{2}, I_{8}, I_{B}]$ & $\emptyset$ & $I_{4}$ \\
$2$ & $3$ & $[I_{1}, I_{3}, I_{7}, I_{2}, I_{8}, I_{B}]$ & $\{I_{4}\}$ & $I_{1}$ \\
$2$ & $4$ & $[I_{3}, I_{7}, I_{2}, I_{8}, I_{B}]$ & $\{I_{4}, I_{1}\}$ & $I_{2}$ \\
$2$ & $8$ & $[I_{3}, I_{7}, I_{8}, I_{B}]$ & $\{I_{4}, I_{1}, I_{2}\}$ & $I_{B}$ \\
$2$ & $10$ & $[I_{3}, I_{7}, I_{8}]$ & $\{I_{4}, I_{1}, I_{2}, I_{B}\}$ & $/$ \\
$3$ & $0$ & $[I_{3}, I_{7}, I_{8}]$ & $\emptyset$ & $I_{3}$ \\
$3$ & $8$ & $[I_{7}, I_{8}]$ & $\{I_{3}\}$ & $/$ \\
$4$ & $0$ & $[I_{7}, I_{8}]$ & $\emptyset$ & $I_{7}$ \\
$4$ & $4$ & $[I_{8}]$ & $\{I_{7}\}$ & $I_{8}$ \\
$4$ & $9$ & $[]$ & $\{I_{7}, I_{8}\}$ & $/$ \\
\end{tabular}
\caption{\label{tab:bind-reg}Register binding}
\end{table}

As can be seen, we need 5 registers in total, as the algorithm has yielded 5 necessary colors.
Thus we can say: we color variables $t_{6}, t_{5}, t_{13/14}, t_{15/16/10}$ with color 1, variables $t_{4}, t_{1}, t_{2}, t_{B}$ with color 2, variable $t_{3}$ with color 3 and variables $t_{7}, t_{8}$ with color 4.

We can also draw the conflict graph.
Figure \ref{fig:conflict-color} shows the colored conflict graph for variables `tx`.

![\label{fig:conflict-color}Colored conflict graph for variables `tx`](graphics/1-g-conflict.pdf){ width=50% }

### task 1 h

Description: Draw the data flow graph with schedule and binding. (3)

We now have all the ingredients necessary to make an actual hardware pipeline, as of yet without resource sharing but including our concrete schedule and binding of operations as well as registers.
Figure \ref{fig:flow-graph} contains the data flow graph with all this information.

### task 1 i

Description: Draw a structural view of the data path. (2)

For this task, we are sharing the resources according to the binding.
In a sense we are merging nodes of the data flow graph.

In order to do this, we need to keep track which inputs each unit requires to build our multiplexers for the unit inputs.
Table \ref{tab:unit-inputs} shows which inputs take which variables (and which signals represent those variables).

\begin{table}
\centering
\begin{tabular}{lll}
Input & Variable & Signal \\
\hline
ALU.A & b, c, a, t2, t5, t3, t13/t14 & b, c, a, R2.Q, R1.Q, R3.Q \\
ALU.B & 3, 2, 1, 0, t3, t5, t8 & 3, 2, 1, 0, R3.Q, R1.Q, R4.Q \\
MUL1.A & a, c & a, c \\
MUL1.B & b, t4, t1 & b, R2.Q \\
MUL2.A & b, c & b, c \\
MUL2.B & b, t7 & b, R4.Q \\
R1.D & t6, t5, t13/14, t15/16/10 & R1.Q, ALU.R, MUL1.R \\
R2.D & t4, t1, t2, tB & R2.Q, ALU.R, MUL1.R \\
R3.D & t3 & R3.Q, ALU.R \\
R4.D & t7, t8 & R4.Q, MUL2.R \\
\end{tabular}
\caption{\label{tab:unit-inputs}Inputs to each unit}
\end{table}

Figure \ref{fig:structure} shows the structural view of the data path.

\pagebreak

\begin{landscape}

\begin{figure}
\centering
\includegraphics[width=1.6\textwidth]{graphics/1-h-flow-graph.pdf}
\caption{\label{fig:flow-graph}Data flow graph containing schedule and binding of operations and registers}
\end{figure}

\end{landscape}

\pagebreak


![\label{fig:structure}Structural view of the data path](graphics/1-i-structure.pdf)

### task 1 j, k

Description: Give the activation signals for all operations. (4)

Description: Give the hold signals for all variables, for which they are required. (2)

Table \ref{tab:activation} gives all activation signals for all operations, including a register binding (which register to save the temporary result in) and an operation binding.
We have multiplexer inputs S1 to S8, an ALU opcode OpC and register enables En1 to En4.

Similarly Table \ref{tab:hold} gives all hold signals for all values.

Since we include a control signal for the ALU operation, which is the opcode, Table \ref{tab:opcodes} gives opcodes for all available operations.

\pagebreak

\begin{landscape}

\begin{table}
\centering
\begin{tabular}{llcll}
Node & Operation & Reg. Binding & Op. Binding & Control Signals \\
\hline
$v_{1}$ & \texttt{t1 = a * b} & R2 & MUL1 & $\text{S1} = \text{S2} = 0, S8 = 0, \text{En2} = 1$ \\
$v_{2}$ & \texttt{t2 = t1 * c} & R2 & MUL1 & $\text{S1} = \text{S2} = 1, S8 = 0, \text{En2} = 1$ \\
$v_{3}$ & \texttt{t3 = a << 1} & R3 & ALU & $\text{S3} = \text{S4} = 010, \text{OpC} = 101, \text{En3} = 1$ \\
$v_{4}$ & \texttt{t4 = c << 2} & R2 & ALU & $\text{S3} = \text{S4} = 001, S8 = 1, \text{OpC} = 101, \text{En2} = 1$ \\
$v_{5}$ & \texttt{t5 = t4 * c} & R1 & MUL1 & $\text{S1} = \text{S2} = 1, \text{S7} = 0, \text{En1} = 1$ \\
$v_{6}$ & \texttt{t6 = b << 3} & R1 & ALU & $\text{S3} = \text{S4} = 000, \text{S7} = 1, \text{OpC} = 101, \text{En1} = 1$ \\
$v_{7}$ & \texttt{t7 = b * b} & R4 & MUL2 & $S5 = S6 = 0, \text{En4} = 1$ \\
$v_{8}$ & \texttt{t8 = t6 * t7} & R4 & MUL2 & $S5 = S6 = 1, \text{En4} = 1$ \\
$v_{9}$ & \texttt{branch on t2 > 0} & R2 & ALU & $\text{S3} = \text{S4} = 011, S8 = 1, \text{OpC} = 010, \text{En2} = 1$ \\
$v_{13/14}$ & \texttt{t9 = t5 - t3} & R1 & ALU & $\text{S3} = \text{S4} = 100, \text{S7} = 1, \text{OpC} = 001, \text{En1} = 1$ if $t_{B} = 0$ \\
& \texttt{t10 = t3 - t5} &&& $\text{S3} = \text{S4} = 101, \text{S7} = 1, \text{OpC} = 001, \text{En1} = 1$ if $t_{B} = 1$ \\
$v_{15/16/10}$ & \texttt{d = t9 - t8} & R1 & ALU & $\text{S3} = 100, \text{S4} = 110, \text{S7} = 1, \text{OpC} = 001, \text{En1} = 1$ if $t_{B} = 0$  \\
& \texttt{d = t10 + t8} &&& $\text{S3} = 100, \text{S4} = 110, \text{S7} = 1, \text{OpC} = 000, \text{En1} = 1$ if $t_{B} = 1$  \\
\end{tabular}
\caption{\label{tab:activation}Activation signals for all operations}
\end{table}

\begin{table}[!htb]
    \begin{minipage}{.45\linewidth}
\centering
\begin{tabular}{lcc}
Value & Reg. Binding & Control Signals \\
\hline
hold t3 & R3 & $\text{En3} = 0$ \\
hold t5 & R1 & $\text{En1} = 0$ \\
hold t6 & R1 & $\text{En1} = 0$ \\
hold t8 & R4 & $\text{En4} = 0$ \\
hold tB & R2 & $\text{En2} = 0$ \\
hold d & R1 & $\text{En1} = 0$ \\
\end{tabular}
\caption{\label{tab:hold}Hold signals for all values}
    \end{minipage}
    \begin{minipage}{.2\linewidth}
\centering
\begin{tabular}{lc}
ALU operation & Opcode \\
\hline
\texttt{+} & 000 \\
\texttt{-} & 001 \\
\texttt{>} & 010 \\
\texttt{<=} & 011 \\
\texttt{==} & 100 \\
\texttt{sll} & 101 \\
\texttt{srl} & 110 \\
\texttt{sra} & 111 \\
\end{tabular}
\caption{\label{tab:opcodes}Opcodes for all ALU operations}
    \end{minipage} 
    \begin{minipage}{.35\linewidth}
\centering
\begin{tabular}{lclc}
State & Code & State & Code\\
\hline
$x_{1}$ & b"0000" & $x_{7}$ & b"0110" \\
$x_{2}$ & b"0001" & $x_{8}$ & b"0111" \\
$x_{3}$ & b"0010" & $x_{9}$ & b"1000" \\
$x_{4}$ & b"0011" & $x_{10}$ & b"1001" \\
$x_{5}$ & b"0100" & $x_{11}$ & b"1010" \\
$x_{6}$ & b"0101" & $x_{12}$ & b"1011" \\
\end{tabular}
\caption{\label{tab:coding}FSM state assignment}
    \end{minipage} 
\end{table}

\end{landscape}

\pagebreak

### task 1 l

Description: Draw the FSM with data specification to control the data path. Apply a start, ready, acknowledge control scheme. The value of d should be kept at its output port for readout until the acknowledge signal is high. (4)

Figure \ref{fig:fsm} shows a FSM with a data specification.
One important detail is to consider that each multiplication operation really has two different points in time in which the control path must act.
The first is the dispatch of the operation, the second is the storing/sampling of the output of the multiplier pipeline.
Operation dispatches are denoted using $v_{i}$, whereas multiplication pipeline stores (which basically affect the register enables and the multiplexer control signals at the multiplier outputs) are denoted using $s_{i}$.

![\label{fig:fsm}FSM with data specification used to control the data path](graphics/1-l-fsm.pdf){ width=75% }

As can be seen, the branch unit is implemented using a branch in the state machine.
The states are similar in that they have the same data specification, however the actual outputs of the control path are different for the respective states.

### task 1 m

Description: Conduct a state assignment for your FSM applying binary encoding. (1)

To assign states of the FSM using binary encoding, we simply count up in binary.
The binary number has $\lceil \log_{2} 10 \rceil = 4$ bits.

Table \ref{tab:coding} includes all binary code values for our FSM states.

### task 1 n

Description: Give the truth table for the output logic of your FSM. (2)

Table \ref{tab:fsm-output} gives the output truth table.
Like previously mentioned, we are ensuring that we treat dispatch of multiplications and storing of multiplications accordingly.

### task 1 o

Description: Give the truth table for the next state logic of your FSM. Include a reset signal that brings the FSM from any state to its initial state. (2)

Table \ref{tab:fsm-next} gives the next state logic truth table.
Guiding lines were inserted to split the rows which are semantically different.

In the first section, we wait in state $x_{1}$ until start goes high.
Once start goes high, we begin processing, going all the way to state $x_{7}$.
In state $x_{7}$ we are at a branching point, having to decide whether to go into one or the other FSM branch, we base this decision on $t_{B}$ (which will be available in R2).
Then we have the parallel processing of both branches, until we enter the final state $x_{12}$.
In this state, we have computed the final result and we keep this in our register until the result becomes acknowledged.
Finally there is a line to handle when reset goes high, in which case we go to the initial state.

\begin{table}
\centering
\begin{tabular}{ll|cccc|ll}
State    &    Code & Start & Ack & Reset & $t_{B}$ & Next State & Next Code \\
\hline
 $x_{1}$ & b"0000" &     0 &   - &     0 &       - &    $x_{1}$ & b"0000" \\
 $x_{1}$ & b"0000" &     1 &   - &     0 &       - &    $x_{2}$ & b"0001" \\
\hline
 $x_{2}$ & b"0001" &     - &   - &     0 &       - &    $x_{3}$ & b"0010" \\
 $x_{3}$ & b"0010" &     - &   - &     0 &       - &    $x_{4}$ & b"0011" \\
 $x_{4}$ & b"0011" &     - &   - &     0 &       - &    $x_{5}$ & b"0100" \\
 $x_{5}$ & b"0100" &     - &   - &     0 &       - &    $x_{6}$ & b"0101" \\
 $x_{6}$ & b"0101" &     - &   - &     0 &       - &    $x_{7}$ & b"0110" \\
\hline
 $x_{7}$ & b"0110" &     - &   - &     0 &       0 &    $x_{8}$ & b"0111" \\
 $x_{7}$ & b"0110" &     - &   - &     0 &       1 &    $x_{9}$ & b"1000" \\
\hline
 $x_{8}$ & b"0111" &     - &   - &     0 &       - &    $x_{10}$ & b"1001" \\
 $x_{9}$ & b"1000" &     - &   - &     0 &       - &    $x_{11}$ & b"1010" \\
$x_{10}$ & b"1001" &     - &   - &     0 &       - &    $x_{12}$ & b"1011" \\
$x_{11}$ & b"1010" &     - &   - &     0 &       - &    $x_{12}$ & b"1011" \\
\hline
$x_{12}$ & b"1011" &     - &   0 &     0 &       - &    $x_{12}$ & b"1011" \\
$x_{12}$ & b"1011" &     - &   1 &     0 &       - &    $x_{1}$ & b"0000" \\
\hline
     any & b"\texttt{--------}" &     - &   - &     1 &       - &    $x_{1}$ & b"0000" \\
\end{tabular}
\caption{\label{tab:fsm-next}FSM next state logic truth table (don't care given as -)}
\end{table}

\pagebreak

\begin{landscape}

\begin{table}
\centering
\begin{tabular}{ll|c|cccccccc|c|cccc|l}
State    &    Code & Ready & S1 & S2 &  S3 &  S4 & S5 & S6 & S7 & S8 & OpC & En1 & En2 & En3 & En4 & data specification \\
\hline
 $x_{1}$ & b"0000" &     0 &  0 &  0 & 000 & 000 &  0 &  0 &  - &  - & 101 &   1 &   - &   - &   - & v6, v1, v7 \\
 $x_{2}$ & b"0001" &     0 &  - &  - & 001 & 001 &  - &  - &  - &  1 & 101 &   0 &   1 &   - &   - & v4, hold t6 \\
 $x_{3}$ & b"0010" &     0 &  1 &  1 & 010 & 010 &  - &  - &  - &  0 & 101 &   0 &   1 &   1 &   1 & s1, s7, v3, v5, hold t6 \\
 $x_{4}$ & b"0011" &     0 &  1 &  1 &   - &   - &  1 &  1 &  - &  - &   - &   - &   - &   0 &   - & v2, v8, hold t3 \\
 $x_{5}$ & b"0100" &     0 &  - &  - &   - &   - &  - &  - &  0 &  - &   - &   1 &   - &   0 &   - & s5, hold t3 \\
 $x_{6}$ & b"0101" &     0 &  - &  - &   - &   - &  - &  - &  - &  0 &   - &   0 &   1 &   0 &   1 & s2, s8, hold t5, hold t3 \\
 $x_{7}$ & b"0110" &     0 &  - &  - & 011 & 011 &  - &  - &  - &  1 & 010 &   0 &   1 &   0 &   0 & v9, hold t8, hold t5, hold t3 \\
 $x_{8}$ & b"0111" &     0 &  - &  - & 100 & 100 &  - &  - &  1 &  - & 001 &   1 &   0 &   - &   0 & v13/14, hold t8, hold tB \\
 $x_{9}$ & b"1000" &     0 &  - &  - & 101 & 101 &  - &  - &  1 &  - & 001 &   1 &   0 &   - &   0 & v13/14, hold t8, hold tB \\
$x_{10}$ & b"1001" &     0 &  - &  - & 100 & 110 &  - &  - &  1 &  - & 001 &   1 &   - &   - &   - & v15/16/10 \\
$x_{11}$ & b"1010" &     0 &  - &  - & 100 & 110 &  - &  - &  1 &  - & 000 &   1 &   - &   - &   - & v15/16/10 \\
$x_{12}$ & b"1011" &     1 &  - &  - &   - &   - &  - &  - &  - &  - &   - &   - &   - &   - &   - & \\
\end{tabular}
\caption{\label{tab:fsm-output}FSM output logic truth table (don't care given as -)}
\end{table}

\begin{table}
\centering
\begin{tabular}{ccccccc}
$t_{act}$& $U_{\text{act,add}}$ & slacks  & $S_{\text{act,add}}$ & $R_{\text{act,add}}$ & $a_{\text{add}}$ & $t_{i}$ \\
\hline
1 & $\{ v_{1}, v_{3}, v_{5}, v_{6} \}$ & $s_{1} = 0, s_{3} = 1, s_{5} = s_{6} = 2$ & $\{ v_{1} \}$ & $\emptyset$ & 1 & $t_{1} = 1$ \\
2 & $\{ v_{2}, v_{3}, v_{5}, v_{6} \}$ & $s_{2} = s_{3} = 0, s_{5} = s_{6} = 1$ & $\{ v_{2}, v_{3} \}$ & $\emptyset$ & 2 & $t_{2} = t_{3} = 2$ \\
3 & $\{ v_{4}, v_{5}, v_{6} \}$ & $s_{4} = s_{5} = s_{6} = 0$ & $\{ v_{4}, v_{5}, v_{6} \}$ & $\emptyset$ & 3 & $t_{4} = t_{5} = t_{6} = 3$ \\
4 & $\{ v_{7}, v_{8} \}$ & $s_{7} = s_{8} = 0$ & $\{ v_{7}, v_{8} \}$ & $\emptyset$ & 3 & $t_{7} = t_{8} = 4$ \\
5 & $\{ v_{9} \}$ & $s_{9} = 0$ & $\{ v_{9} \}$ & $\emptyset$ & 3 & $t_{9} = 5$ \\
\end{tabular}
\caption{\label{tab:list-sched-timing}List scheduling with timing constraint}
\end{table}

\end{landscape}

## Exercise 2

### task 2 a

Description: Explain why we need to restart List Scheduling with Timing Constraints after we increase the number of available resources. Give an example of a sequencing graph and the delays and deadline, for which this restart would give an improved result over the base version. Use an example, which is substantially different from the ones used in the lecture and exercises, and add an explanation to justify your choice. (5)

Figure \ref{fig:seq-unopt} provides an example as to why we restart list scheduling *after* increading resources once.
There are only additions with 1 cycle delay.
It is constructed in a way that it could be made arbitrarily long, in our case we have a critical path with five additions before we have to merge with the other trees of additions.
We could insert additional nodes on the critical path and bigger trees to extend this example further.
What the example is supposed to illustrate is that even when it is possible to schedule operations within a given timing constraint and with at most $k$ resources (in our case at most 2 adders in 5 at least cycles), without restarting the algorithm, it will yield a higher resource usage (in our case 3 adders).

![\label{fig:seq-unopt}Sequencing graph example with ALAP schedule to illustrate optimization improvement usefulness](graphics/2-a-seq-graph.pdf){ width=80% }

If we look at the ALAP schedule, then we will find that $t^L_{1} = 1$, $t^L_{2} = t^L_{3} = 2$, $t^L_{4} = t^L_{5} = t^L_{6} = 3$, $t^L_{7} = t^L_{8} = 4$ and $t^L_{9} = 5$.
Thus we have a minimal latency of $\Lambda_{\text{min}} = 5$.
Now we want to use list scheduling with the smallest possible timing constraint, $\Lambda_{\text{max}} = \Lambda_{\text{min}} = 5$.

If we now look at Table \ref{tab:list-sched-timing}, we can see that as long as nodes 3, 5, 6 and 8 have non-zero slacks, their dispatch will be further delayed until the latest possible point, at which they must all be scheduled at the same time.
Thus our resource usage jumps from 1 adder, to 2 adders and finally to 3 adders.
If we restarted right after adding the first adder, we would have kept a constant resource usage of 2 adders, since we could schedule $v_{1}$ with $v_{3}$, $v_{2}$ with $v_{5}$, $v_{4}$ with $v_{6}$ and $v_{7}$ with $v_{8}$.

### task 2 b

Description: Explain what is the advantage of Force-directed Scheduling over List Scheduling with Timing Constraints. (3)

Force-directed scheduling is a sophisticated technique which will try to balance out operations according to their mobility.
This is because we split an operation over an interval uniformly (uniform probability to be scheduled at a given time) and calculate something called the mean demand for each type of operation.
With the demands we can calculate self forces, successor forces and predecessor forces which will emulate a kind of physical, spring-based system where the equilibrium state represents an optimally balanced placement of operations within the schedule (although we are calculating the result only heuristically).

For nodes on the critical path, there won't be any difference.
However, for all other nodes which have some mobility, there is a difference in their resulting schedules.
For list scheduling, we will simply schedule everything that can be scheduled on all available resources for each type of operation.
The final number of available resources for each type of operation will depend, on how many operations must run concurrently to meet the timing constraint.
Once we are on the final run of our list scheduling with timing constraints (after we have rerun the algorithm enough times), the algorithm will simply bunch together as many operations as possible, with no regard for whether they will end up idle later on anyways.

In contrast, force-directed scheduling will balance out operations more nicely, which might lead to a tangible reduction in necessary registers.
In a way, the advantage of force-directed scheduling is to reduce the idle time of resources, or as we might also say, improve utilization.

If the critical path allows for it, we could make our latency deadline smaller and compact the schedule, or make it bigger and widen the schedule.
We would still maintain good utilization or resources so widening the schedule, for instance, might make sense to improve resource sharing.

### task 2 c

Description: What happens if we want to synthesize a pipelined data path design without resource sharing but with timing constraint (deadline on latency). Would we still need an algorithm such as List Scheduling with Timing Constraints or Force-directed scheduling. (3)

We assume we have infinite available resource as well as a deadline on latency that can be met considering the critical path and the delay of operations.
In this case we could schedule all operations simply as early as possible (ASAP scheduling) and instantiate the pipeline accordingly, placing a pipelined unit for each individual operation.
Since we have assumed that the critical path can be scheduled and that we have enough (infinite available) resources, the ASAP schedule will suffice for an actual implementation that meets the latency.

Since we are not sharing any resources at all, list scheduling (with timing constraints) will not give us any benefit and neither will force-directed scheduling over the simple ASAP schedule.
