#include <stdio.h>
#include <stdint.h>

int32_t calc_homework(int32_t a, int32_t b, int32_t c)
{
	int32_t d;
	if ((a * b * c) > 0) {
		d = 2 * a - 4 * c * c + 8 * b * b * b;
	} else {
		d = -2 * a + 4 * c * c - 8 * b * b * b;
	}
	return d;
}

int main(int argc, char *argv[]) {
	int32_t d = calc_homework((int32_t)argv[0][0], (int32_t)argv[0][1], (int32_t)argv[0][2]);
	printf("value is %d\n", d);
	return 0;
}
