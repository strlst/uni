calc_homework(int, int, int):
        mulw    a5,a0,a1
        mulw    a4,a5,a2
        mulw    a5,a1,a1
        mulw    a2,a2,a2
        mulw    a5,a5,a1
        ble     a4,zero,.L2
        slliw   a2,a2,1
        subw    a0,a0,a2
        slliw   a5,a5,2
        addw    a0,a5,a0
        slliw   a0,a0,1
        ret
.L2:
        negw    a2,a2
        slliw   a2,a2,1
        negw    a5,a5
        addw    a2,a2,a0
        slliw   a5,a5,2
        subw    a0,a5,a2
        slliw   a0,a0,1
        ret
.LC0:
        .string "value is %d\n"
main:
        ld      a5,0(a1)
        addi    sp,sp,-16
        sd      ra,8(sp)
        lbu     a1,1(a5)
        lbu     a2,2(a5)
        lbu     a0,0(a5)
        call    calc_homework(int, int, int)
        mv      a1,a0
        lui     a0,%hi(.LC0)
        addi    a0,a0,%lo(.LC0)
        call    printf
        ld      ra,8(sp)
        li      a0,0
        addi    sp,sp,16
        jr      ra
