#!/usr/bin/env python3
import sys
import os
import re

def add(regfile: dict, regs: str):
	pass

def create_regfile() -> dict:
	regfile = {'zero': 0, 'ra': 0, 'sp': 0, 'gp': 0, 'tp': 0}
	for i in range(7):
		regfile[f't{i}'] = 0
	for i in range(12):
		regfile[f's{i}'] = 0
	for i in range(8):
		regfile[f'a{i}'] = 0
	return regfile

def parse(file=sys.stdin) -> (list, dict):
	program = []
	labels = {}
	for address, line in enumerate(file):
		_, label, instr, operands = re.match(r'((\w+):)?\s*(\w+)\s(.*)', line.strip()).groups()
		program.append((instr, [op.strip() for op in operands.split(',')]))
		if label:
			labels[label] = address
	return program, labels

def as_hex_pc(pc: int):
	return hex(pc * 4)

def execute(pc: int, program: list, regfile: dict, bp: int, btb: dict, table=sys.stdout) -> (int, int, int):
	bp_state = {0: 'PSNT', 1: 'PWNT', 2: 'PWT', 3: 'PST'}
	instruction = program[pc]
	npc, opcode, operands = pc + 1, instruction[0], instruction[1]
	delay = 1
	print(as_hex_pc(pc), instruction)
	match opcode:
		case 'add':
			r0, r1, r2 = operands
			if r0 != 'zero':
				regfile[r0] = regfile[r1] + regfile[r2]
		case 'addi':
			r0, r1, imm = operands
			if r0 != 'zero':
				regfile[r0] = regfile[r1] + int(imm)
		case 'lw':
			if npc < len(program) and operands[0] in program[npc][1][1:]:
				print(f'{opcode} stall')
				delay += 1
		case 'beq' | 'bne':
			r0, r1, label = operands
			branch_result = (opcode == 'beq' and regfile[r0] == regfile[r1]) or (opcode == 'bne' and regfile[r0] != regfile[r1])
			branch_prediction = bp > 1
			table.write(f'{pc + 1} & {hex(pc * 4)} & {bp_state[bp]} & {str(branch_prediction)[0]} & {str(branch_result)[0]} & {str(branch_result == branch_prediction)[0]} \\\\\n')
			if branch_result:
				npc = labels[label]
				print(f'{opcode} to {label}')
				# update bp
				bp = min(3, bp + 1)
			else:
				# update bp
				bp = max(0, bp - 1)
			if pc not in btb and branch_prediction:
				# penalty for not knowing branch target is two cycles for our example
				delay = 3
				btb[pc] = labels[label]
			elif branch_result != branch_prediction:
				delay = 3
	filtered = {k: regfile[k] for k in regfile if regfile[k] != 0}
	print(f'{as_hex_pc(npc)} next pc, regs {filtered}, delay {delay}\n')
	return npc, bp, delay

def run(program: list, labels: dict):
	regfile = create_regfile()
	# btb implemented as a dictionairy and bp as an int between 0 and 3
	btb = {}
	pc, cycles, retired, bp = 0, 0, 0, 1
	with open('1-a-table', 'w') as table:
		table.write(f'line & pc & bp state & prediction & result & correct \\\\\n')
		while pc < len(program):
			pc, bp, delay = execute(pc, program, regfile, bp, btb, table=table)
			retired += 1
			cycles += delay
	print(f'program took {cycles} cycles for {retired} instructions (avg CPI of {round(cycles/retired, 3)})')

if __name__ == '__main__':
	program, labels = parse()
	run(program, labels)