---
title: High-Level Synthesis
author: Norbert Tremurici (\texttt{e11907086@student.tuwien.ac.at})
header-includes:
  - '`\usepackage{pdflscape}`{=latex}'
  - '`\usepackage{graphicx}`{=latex}'
---

# Assignment 2

## Exercise 1

### task 1 a

For this task, we would be well-advised to analyze the program first.
Also, we will first consider the branch behavior and then look at the average CPI.

The program first sets some constants, then it enters a loop where a register `a6` accumulates certain values.
Another register `a2` serves to control the loop, counting down from its initial value 4 until the it equals a constant zero.

Trying to decompile the code snippet, our program would probably look something like this:

```c
for (i = 4; i > 0; i--) {
    acc += *memlocation;
    if (i != 2)
        acc += param1
}
acc += param2
```

The program has two branches, one of them a relative branch back, which indicates a loop (just like the label says), the other to skiop an instruction, indicating conditional execution.

\begin{table}
\centering
\begin{tabular}{cccccc}
line & pc & bp state & prediction & result & correct \\
6 & 0x14 & PWNT & F & F & T \\
9 & 0x20 & PSNT & F & T & F \\
6 & 0x14 & PWNT & F & F & T \\
9 & 0x20 & PSNT & F & T & F \\
6 & 0x14 & PWNT & F & T & F \\
9 & 0x20 & PWT & T & T & T \\
6 & 0x14 & PST & T & F & F \\
9 & 0x20 & PWT & T & F & F \\
\end{tabular}
\caption{\label{tab:bp}BP results}
\end{table}

Table \ref{tab:bp} shows the BP results, indicating which line number/PC the branch has occurred at, the state of the 2-bit global predictor, the prediction, the actual result of the branch and whether the prediction was correct.
As can be seen, the loop branch is taken 3 times, just like we would expect.
Similarly for the conditional execution, line 7 is only skipped once.

As we can see, out of 8 predictions only 3 are correct, yielding a misprediction rate of 0.625.

As for the CPI, the program took 43 cycles for 27 instructions yielding an average CPI of 1.296.
We can add the preamble, then the loop body times four and subtract one for the skipped instruction and finally add one for the postamble for $3 + 4 * 6 - 1 + 1 = 27$ instructions in total.
Because of the RAW hazard for the target of the load instruction, our loads which is executed four times in total will stall one cycle each time, adding an extra 4 cycles.
Assuming a misprediction penalty of two cycles, we get an additional 10 cycles.
Finally, for each correct prediction of a taken branch, where the branch target is not in the BTB, we get an extra penalty of 2 cycles.
Thus in total we have $27 + 4 + 10 + 2 = 43$.

Note: For full disclosure, the results were verified using an ad-hoc written Python script that simulates the code snippet, which will also be supplied in the submission.

### task 1 b

#### architectural considerations

For this task, we assume the out-of-order pipeline from the lecture, but with the latencies and parameters given in the exercise.
The important detail to observe right off the bat is that instructions with true dependencies (RAW) will not be issued in the pipeline from the IB until we have successfully written back the result of the dependent instruction (logically preceeding instruction of a dependency) to the register file, because we require all operands to be available.

The simplest solution would be execute out-of-order, but commit in-order.
We want to consider an approach which only buffers results in case of WAR or WAW dependencies.
Since instructions enter the IB in-order, we will be able to deduce whether a depending instruction is in the IB for a dependent instruction in a functional unit.
We will need a way to deduce the actual program order, so the first component we would like to have is a counter of issued instructions for logical timestamps.
This begs the question what happens when the counter overflows, but we can handle this special case by considering the last four (due to IB size) possible counter values to "occur before" the first possible counter value (value 0).
Now that we have a counter, we will annotate all instructions that enter the IB with a logically increasing timestamp, which is simply the counter value.
Since we issue at most one instruction from the IB each cycle, we can be sure that a logical order of operations can be constructed this way.

Next, we want some way to keep outputs of functional units in-case a result needs to wait before it is committed.
For this, we can use an output buffer (OB) with one slot for each functional unit write port (size 4).
The idea is to use this buffer only in case we cannot directly commit results to the register file and to make the logistics simpler, we will reserve a slot in the buffer for each functional unit.
This output buffer is neither FIFO nor LIFO, since we are executing and possibly retiring instructions out-of-order.

So what happens in the write-back stage?

Since we have introduced a new buffer, we need to address what happens if the buffer is full.
More specifically, the buffer is "full" if a functional unit wants to write to its reserved slot in the buffer but there is already an element in its slot that needs to be buffered.
In this case, which should be rare, we simply stall the functional unit in the execute stage (without blocking the other functional units).
After at most 3 cycles, the OB slot should become empty again.

Now we need to consider when to put the result of a functional unit into the OB.
For an instruction with target register `rdA` and a logically preceding instruction with target register `rdB` and operands `rs1` and `rs2`:
If either `rdB`, `rs1` or `rs2` is equal to `rd`, then the result needs to be buffered.
Conversely, if there is no such logically preceeding instruction for a result stored in the OB, then the result can be committed safely.

This way we will have handled both WAR and WAW dependencies.
Now we can finally define an operation to be complete, if it has actually been committed.
That is, the result has either been written directly to the register file or it has been written from the OB.

Notice that WAR dependencies are handled by comparing `rdA` against `rs1` or `rs2` of the logically preceeding instruction, while WAW dependencies are handled by comparing `rdA` against `rdB`.
We could also have said, WAR depending instructions (instructions depending on a logically preceeding instruction) are not allowed to enter the IB, in which case we would only need to compare `rdA` against `rdB`.

One weakness of this approach is the susceptibility to stalls.
If we increased the amount of slots each functional units can buffer from only one slot to some number high enough, we could completely avoid stalls, since the amount of instructions that can be in-flight has an upper bound.
However this would incur a lot of additional complexity that might not be worth it.
We have also opted against employing a buffer shared between multiple functional units, since we would have to deal with the problems of multiple write-back values entering the buffer at once and what to do if the buffer is full or almost full.

Another possible solution could have been to employ a re-order buffer, which can also solve this problem.

#### code dependencies

In this section we want to look at which dependencies exist in the given code snippet.
We denote dependencies by $(x, y)$ where $x$ and $y$ are the line numbers of the dependent (logically preceeding) and depending (logically succeeding) instructions.

RAW: $(1, 3)$, $(1, 4)$, $(1, 5)$, $(2, 3)$, $(2, 7)$, $(3, 5)$, $(4, 5)$, $(6, 7)$, $(7, 8)$, $(9, 10)$

WAR: $(8, 9)$

WAW: $(2, 10)$, $(6, 7)$, $(7, 9)$

\pagebreak

\begin{landscape}

\begin{figure}[hbtp]
\centering
\includegraphics[width=1.6\textwidth]{graphics/1-b.jpg}
\end{figure}

\end{landscape}

\pagebreak

\begin{figure}[hbtp]
\centering
\includegraphics[width=1\textwidth]{graphics/2-a.jpg}
\end{figure}

\pagebreak

\begin{figure}[hbtp]
\centering
\includegraphics[width=1\textwidth]{graphics/2-b.jpg}
\end{figure}

\pagebreak

\begin{figure}[hbtp]
\centering
\includegraphics[width=1\textwidth]{graphics/2-c-1.jpg}
\end{figure}

\pagebreak

\begin{figure}[hbtp]
\centering
\includegraphics[width=1\textwidth]{graphics/2-c-2.jpg}
\end{figure}