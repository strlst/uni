% Memory technology comparison
% Name Surname

## DRAM

DRAM technology has found widespread use even though it could be seen as "painful to use".
To effectively use DRAM for reading and writing, one has to adhere to complicated protocols that adhere to all timing parameters and its peculiar requirement to refresh memory.
Reads for example, which are often batched as row reads for performance reasons, are destructive and the capacitors that keep a charge to store a value have to be recharged anew.
Another important aspect is the fact that DRAM technology is volatile.

They have very low densities however and this drives lower costs and larger storage capacity.
But their need for refreshing and higher power consumption are detractors.

## SRAM

SRAM technology compares favorably to other memory technologies in a couple of dimensions.
Its main benefits are fast access times, a low power consumption and unlike DRAM, no need for refreshing.
Access times in particular are so favorable compared to other technologies, that SRAM finds widespread use for use cases such as processor caches.
Wherever they are used however, their higher cost and limited storage capacity make them costly and small.

## ROM/PROM

ROM/PROM technology has simple use case, where a memory that is either preprogrammed and read-only or reprogrammable (with the expectation that it wouldn't be reprogrammed very often, compared to how other memory technology might be used).
They have low power consumption, are non-volatile and highly reliable.
Their trade-off are their read-only property and thus non-reprogrammability or long write times.

## FLASH

Flash technology has been very competitive for non-volatile use cases because of their very high densities.
They are also low in their power consumption, so they are also attractive for mobile use cases.
But they have their own disadvantages:
they have limited write-cycles and write speeds are generally lower.
Memory must also be erased before being overwritten, which gives them their name.

### NAND FLASH vs. NOR FLASH

Comparing NAND and NOR technology, we can see that NAND flash has a higher density than NOR flash and is more suitable for large non-volatile memories.
However, the sequential write and read accesses are slower in NAND technology compared to NOR technology.
Reads to NAND flash must also be sequential and often entire pages are read, these reads are slow to start up (making them less useful for random access) but manage to deliver the data quite fast after this initial latency.
