/**
 * compressor header
 *
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-11-05
 * @brief contains compress function that does all the work
 * @details none
 */

#ifndef COMPRESS_H
#define COMPRESS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

/**
 * @brief takes an input file and output file, compresses the contents of the input file and writes the results to the output file
 * @details reads from infilepath and writes (not appends!) to outfilepath
 * @param infilepath filepath as string to read from
 * @param outfilepath filepath as string to write to
 * @return status code, EXIT_SUCCESS on success, EXIT_FAILURE on failure
 */
int compress(char*, char*);

#endif
