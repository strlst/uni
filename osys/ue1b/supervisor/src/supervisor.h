/**
 * supervisor header
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-11-06
 * @brief contains methods for setup of shared memory and semaphores, which are utilized to read data from a shared memory circular_buffer
 */

#ifndef SUPERVISOR_H
#define SUPERVISOR_H

#include <limits.h>
#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stddef.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>

#include "def.h"

/**
 * @brief scans shared memory buffer for solutions submitted by other processes
 * @details assumes a successfully initialized environment, with shared memory /circular_buffer and semaphores /sem_access, /sem_used and /sem_free already set up correctly, uses global variable exit_requested
 * @param buffer shared memory circular_buffer
 * @param s_access binary mutex semaphore s_access
 * @param s_used counting semaphore for unavailable buffer elements
 * @param s_free counting semaphore for available buffer elements
 */
void collect_results(struct circular_buffer*, sem_t*, sem_t*, sem_t*);

/**
 * @brief initializes semaphores in preparation to set up collect_results(...)
 * @details assumes a successfully initialized partial environment, with shared memory /circular_buffer already set up correctly
 * @param buffer shared memory circular_buffer
 * @return status code, OS_SUCCESS on success, defined in "def.h"
 */
int supervise_init(struct circular_buffer*);

/**
 * @brief initializes shared memory in preparation to set up supervise_init(...) and sets up signal handler handle_signal() for SIGINT, SIGTERM
 * @details none
 * @return status code, OS_SUCCESS on success, defined in "def.h"
 */
int supervise();

/**
 * @brief requests exit
 * @details uses global variable handle_signal
 */
void handle_signal(int);

#endif
