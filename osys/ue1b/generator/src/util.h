/**
 * util header
 *
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-11-10
 * @brief contains auxiliary definitions and implementations of methods used to manipulate a graph structure as it is defined in "def.h"
 * @details stores a color_names array of strings that is used to output color information when debug is enabled
 */

#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "def.h"

/**
 * @brief initializes a graph g
 * @details assumes an uninitialized graph g that was freshly allocated
 * @param g graph to initialize
 */
void init_graph(struct graph*);

/**
 * @brief prints a graph to stdout
 * @details assumes an initialized graph
 * @param g graph to print
 */
void print_graph(struct graph*);

/**
 * @brief extracts edges from argument vector and stores them in a specified graph g
 * @details assumes a graph g freshly initialized with init_graph(...), assumes all arguments after and beginning with index optin to be in the format of "%u-%u"
 * @param g graph to fill with edges
 * @param optint index representing the start of edge arguments in argument vector argv
 * @param argc argument count
 * @param argv argument vector
 * @return status code, OS_SUCCESS on success, INVALID_INPUT on invalid input, defined in "def.h"
 */
int extract_edges(struct graph*, int, int, char**);

/**
 * @brief extracts nodes from edges within a graph
 * @details assumes an initialized graph with edges set and nodes unset
 * @param g graph to fill with nodes
 */
void extract_nodes(struct graph*);

/**
 * @brief assigns random colors to nodes
 * @details assumes an initialized graph with edges set, nodes set but colors unset
 * @param g graph to assign random coloring to
 */
void assign_random_coloring(struct graph*);

/**
 * @brief invalidates the edge withing a graph at index i
 * @details assumes an initialized graph with edges set, invalidates at index i and i + 1
 * @param g graph to invalidate edge in
 * @param i index to invalidate edge at
 */
void remove_edge(struct graph*, size_t);

/**
 * @brief searches for invalid edges within a graph g
 * @details assumes an initialized graph with edges, nodes and colors set, returns INVALID_EDGE_INDEX when none is found, defined in "def.h"
 * @param g graph to find invalid edge in
 * @return index of an invalid edge
 */
size_t find_invalid_edge(struct graph*);

#endif
