/**
 * generator header
 *
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-11-10
 * @brief contains methods for setup of shared memory and semaphores, which are utilized to write data to a shared memory circular_buffer
 * @details none
 */

#ifndef GENERATOR_H
#define GENERATOR_H

#include <string.h>
#include <limits.h>
#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stddef.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>

#include "def.h"
#include "util.h"

/**
 * @brief takes a graph g and generates a solution to the 3coloring problem
 * @details assumes an initialized graph with edges, nodes and colors set
 * @param g graph g for which to generate a solution for
 */
void generate_solution(struct graph*);

/**
 * @brief takes a graph g and generates solutions to the 3coloring problem continually, until terminated via shared memory communication
 * @details assumes a successfully initialized environment, with shared memory /circular_buffer and semaphores /sem_access, /sem_used and /sem_free already set up correctly
 * @param g graph g for which to generate a solution for
 * @param buffer buffer to write solutions into
 * @param s_access binary mutex semaphore s_access
 * @param s_used counting semaphore for unavailable buffer elements
 * @param s_free counting semaphore for available buffer elements
 */
void generate_solutions(struct graph*, struct circular_buffer*, sem_t*, sem_t*, sem_t*);

/**
 * @brief initializes shared memory and semaphores in preparation to set up generate_solutions(...)
 * @details none
 * @param g graph g for which to generate solutions for, later passed on to generate_solutions(...)
 * @return status code, OS_SUCCESS on success, defined in "def.h"
 */
int generate_solutions_setup(struct graph*);

#endif
