/**
 * main header
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-12-01
 * @brief contains macro definitions and data structures used throughout the module
 */

#ifndef MAIN_H
#define MAIN_H

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief kills program while printing terminating message
 * @details prints usage string before exiting using exit(...)
 */
void usage();

/**
 * @brief main entry point to program
 * @details does practically all the heavy lifting
 * @param argc argument count
 * @param argv argument vector
 * @return EXIT_FAILURE on failure, EXIT_SUCCESS on success
 */
int main(int, char**);

#endif
