# ue3

solves an exercise in which both an http server and client have to be written. admittedly got quite disorganized and bloated.

bonus exercises solved:

```
[X] Content-Type for .html, .htm, .css and .js
[X] Binary data
[X] Compression (gzip)
```

![task description 1](./res/http-1.png)

![task description 2](./res/http-2.png)

![task description 3](./res/http-3.png)

![task description 4](./res/http-4.png)

![task description 5](./res/http-5.png)

![task description 6](./res/http-6.png)

![task description 7](./res/http-7.png)

![task description 8](./res/http-8.png)

![task description 9](./res/http-9.png)
