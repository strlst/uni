/**
 * zutil header
 * 
 * @author inspired by zlib team @ https://zlib.net/zlib_how.html
 * @date 2020-01-17
 * @brief contains definitions of compression related I/O
 */

#ifndef ZUTIL_H
#define ZUTIL_H

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <zlib.h>

#define SET_BINARY_MODE(file)
#define CHUNK 16384

int def(FILE *source, FILE *dest, int level);
int inf(FILE *source, FILE *dest);
void zerr(char *program, int ret);

#endif