/**
 * url header
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-01-01
 * @brief contains common definitions and structures
 */

#ifndef URL_H
#define URL_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief url structure to store components of a url
 * @details urls in the form of http://webserver/path, additionally storing
 * file as the last part of path
 */
typedef struct {
    char *webserver; /**< webserver component */
    char *path; /**< path component */
    char *file; /**< file component */
} url;

/**
 * @brief method enum to store possible http methods
 * @details in it's current form not strictly necessary, but useful if further
 * methods are to be implemented
 */
typedef enum {
    OPTIONS, /**< unsupported */
    GET,     /**< supported */
    HEAD,    /**< unsupported */
    POST,    /**< unsupported */
    PUT,     /**< unsupported */
    DELETE,  /**< unsupported */
    TRACE,   /**< unsupported */
    CONNECT, /**< unsupported */
    OTHER    /**< unsupported */
} method;

/**
 * @brief request structure to store components of an http request
 */
typedef struct {
    char *http_version; /**< version component, denotes the version string */
    uint16_t req_port; /**< port component, denotes at which port to request */
    method req_method; /**< method component, denotes which method to use */
    url req_url; /**< url component, denotes which url to request at */
    int compressed; /**< denotes wether to advertise compression */
} request;

/**
 * @brief auxiliary method used to print components of a url
 * @details naively prints all parts, can me misinitialized
 * @param url url which to print
 */
void print_url(url);

/**
 * @brief auxiliary method used to free components of a url
 * @details naively frees all parts, does not handle uninitialized memory gracefully
 * @param url url which to free components of
 */
void destroy_url(url);

/**
 * @brief auxiliary method used to print components of a request
 * @details naively prints all parts, can be misinitialized
 * @param request request which to print
 */
void print_request(request);

/**
 * @brief auxiliary method used to free components of a request
 * @details naively frees all parts, does not handle uninitialized memory gracefully
 * @param request request which to free components of
 */
void destroy_request(request);

#endif