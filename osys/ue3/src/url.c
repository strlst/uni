/**
 * url module
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-01-01
 * @brief contains common methods operating on structures defined in the
 * url header
 * @details contains state
 */

#include "url.h"

void print_url(url u) {
    printf("URL { webserver: %s @ %p, path: %s @ %p, file: %s @ %p }\n", u.webserver, u.webserver, u.path, u.path, u.file, u.file);
}

void destroy_url(url u) {
    free(u.webserver);
    free(u.path);
    free(u.file);
}

static const char *method_names[] = {
    "OPTIONS",
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "TRACE",
    "CONNECT",
    "OTHER"
}; /**< stores strings associated with values in method enum for debug output purposes */

void print_request(request r) {
    printf("REQUEST { version: %s, URL { webserver: %s, path: %s, file: %s }, method: %s }\n", r.http_version, r.req_url.webserver, r.req_url.path, r.req_url.file, method_names[r.req_method]);
}

void destroy_request(request r) {
    free(r.http_version);
}