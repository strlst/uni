/**
 * def header
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-01-01
 * @brief contains common definitions and structures
 */

#ifndef DEF_H
#define DEF_H

#define DEBUG (0)
#define EXIT_PROTOCOL_ERROR (2)
#define EXIT_INVALID_RC     (3)
#define RC_PROTOCOL_ERROR   ((uint16_t) -1)
#define RC_OK               ((uint16_t) 200)
#define RQ_PROTOCOL_ERROR   ((uint16_t) -1)
#define RQ_UNPROCESSED      ((uint16_t) 0)
#define RQ_OK               ((uint16_t) 200)
#define RQ_BAD              ((uint16_t) 400)
#define RQ_NOT_FOUND        ((uint16_t) 404)
#define RQ_NOT_IMPLEMENTED  ((uint16_t) 501)
#define BUF_SIZE            ((int) 1024)

#endif