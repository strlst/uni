/**
 * http header
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-01-02
 * @brief contains definitions of extensive client and server methods used
 * to facilitate HTTP exchanges
 */

#ifndef HTTP_H
#define HTTP_H

#include <dirent.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>        
#include <unistd.h>

#include "def.h"
#include "url.h"
#include "client.h"
#include "parser.h"
#include "zutil.h"

/**
 * @brief takes a program name, request and file pointer to send a request
 * and process the results
 * @details it is assumed that `request` and `file` already been initialized
 * successfully and are ready to be used subsequently.
 * 
 * will exit abnormally in the case of critical errors.
 *
 * supports only http requests with:
 * - http_version == "HTTP/1.1"
 * - method == GET
 *
 * is a client function.
 * @param program program name to print in the event of errors
 * @param req request with each of its fields already set
 * @param out_file FILE* that was previously opened and where output of this
 * method is expected to be written at
 */
void send_request(char*, request, FILE*);

/**
 * @brief takes a program name, port, doc_root and index to serve
 * http requests continuously, until a desire for exit is communicated via
 * signals
 * @details it is assumed that `port`, `doc_root` and `index` already been
 * initialized successfully and are ready to be used subsequently.
 * 
 * will exit abnormally in the case of critical errors, but otherwise continue
 * to continually serve http requests, until a desire for exit is communicated
 * via signals `SIGINT` and `SIGTERM`
 *
 * will always send http responses of the following form:
 * - http_version == "HTTP/1.1"
 * - "Date" header set
 * - "Content-Length" header set
 * - "Connection" header set
 *
 * is a server function.
 * @param program program name to print in the event of errors
 * @param port port to attempt to listen at, must have permission to open
 * @param doc_root directory containing files to be served, is assumed to be
 * a valid and existing location
 * @param index filename that is understood to be the index when requested
 * @param compress whether the server should try to compress
 * files end with '/'
 */
void receive_requests(char*, uint16_t, char*, char*, int);

#endif