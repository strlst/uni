/**
 * server header
 * 
 * @author strlst <e11907086@student.tuwien.ac.at>
 * @date 2020-01-07
 * @brief contains main entry point and auxiliary methods of the server part
 * of the project
 */

#ifndef SERVER_H
#define SERVER_H

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "http.h"

/**
 * @brief kills program while printing terminating message, returning a
 * specified exit code in the process
 * @details prints a specified error message before exiting with a specified
 * exit code
 * @param message message, typically in the form of "%s: error text\n", can
 * optionally be given as "%s: error text: %s\n" to include the output of
 * `strerror(errno)`
 * @param program string which to put at the first format specified of the
 * message parameter
 * @param exit_code integer which to `exit(...)` the program with
 */
void die(char*, char*, int);

/**
 * @brief kills program while printing terminating message
 * @details prints usage string before exiting using exit(...)
 */
void usage();

/**
 * @brief main entry point to program
 * @details does practically all the heavy lifting
 * @param argc argument count
 * @param argv argument vector
 * @return EXIT_FAILURE on failure, EXIT_SUCCESS on success
 */
int main(int, char**);

#endif