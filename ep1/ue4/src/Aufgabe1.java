/*
    Aufgabe 1) Code Analyse - Eindimensionale Arrays
*/
public class Aufgabe1 {

    private static void genArray(int[] filledArray){
        int value = 2;
        for (int i = 0; i < filledArray.length; i++) {
            filledArray[i] = value;
            value += 2;
        }
    }

    private static void printFilteredArrayContent(int[] workArray){
        int[] copiedArray = workArray;
        for (int i = 0; i < copiedArray.length; i++) {
            if(copiedArray[i] % 3 == 0) {
                copiedArray[i] = 0;
            }
        }
        printArray(copiedArray);
    }
    
    private static void genNewArrayContent(int[] workArray){
        int[] helpArray = new int[20];
        int value = 4;
        for (int i = 0; i < helpArray.length; i++) {
            helpArray[i] = value;
            value += 4;
        }
        workArray = helpArray;
        printArray(workArray);
    }
    
    private static void printArray(int[] workArray){
        for (int i = workArray.length - 1; i > -1; i--) {
            System.out.print(workArray[i] + " ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        int[] filledArray = new int[20];
        genArray(filledArray);
        printArray(filledArray);
        
        printFilteredArrayContent(filledArray);
        printArray(filledArray);
        
        filledArray[0] = 1000;
        printArray(filledArray);
        
        genNewArrayContent(filledArray);
        printArray(filledArray);
    }
    
    // **************************************************************************
    // **** Notizen und Fragebeantwortungen bitte hier unterhalb durchführen! ***
    // **************************************************************************
    // Frage 1:
    //
    //  off by one error
    //for (int i = workArray.length; i > 0; i--) {
    //  should be
    //for (int i = workArray.length - 1; i > -1; i--) {
    //
    // Frage 2:
    //
    //  genArray takes a reference (int[]) and operates on it
    //  specifically, generates values for it
    //
    // Frage 3:
    //
    //  printFilteredArrayContent takes a reference (int[]) and operates on it
    //  specifically, all elements divisible by 3 are set to 0 and finally printed
    //  the elements of the array passed by reference are MODIFIED
    //
    // Frage 4:
    //
    //  the following lines taken from genNewArrayContent:
    //int[] helpArray = new int[20];
    //workArray = helpArray;
    //  imply the reference to helpArray is copied to workArray, such that
    //  workArray points to the same object as helpArray
    //
    //  the content is changed for helpArray exlusively, and while workArray
    //  is changed to point helpArray, the **reference** workArray is only
    //  persistent in **the local scope**, so while workArray does copy the
    //  reference to helpArray, outside the function scope filledArray remains
    //  the same
    //
    // Zusatzfrage 1: Welchen Datentyp muss der Indexausdruck haben, mit dem
    //                die Position in einem Array bestimmt wird?
    //
    //x i; var array[i] = null;
    //
    //  x is required to be a non-negative integer (nichtnegativ, ganzzahlig)
    //  thus, every x that lets i be promoted to int is legal
    //
    //  this limits us to short, byte, char and int
    //
    // Zusatzfrage 2: Müssen Sie ein Array initialisieren?
    //
    //  an option is to reference null for initialisation, but not initialising
    //  at all in the form of:
    //
    //var array[i]
    //
    // Zusatzfrage 3: Wie kann die Länge eines Arrays verändert werden?
    //
    //  this can only be done by making a copy, as an instantiated array cannot
    //  dynamically change its length (for this purpose, more dynamic data
    //  types exist)
    //
    // Zusatzfrage 4: Wie gehen Sie vor, wenn Sie ein int-Array kopieren müssen?
    //
    //Arrays.copyOf(...)
    //  or, because int is primitive:
    //int[] copy = new int[I.length];
    //for (int i = 0; i < I.length; i++) copy[i] = I[i];
    //
    // Zusatzfrage 5: Beginnt die Indexzählung eines Arrays immer bei 0:
    //
    //  yes
    //
    // Zusatzfrage 6: Ist es sinnvoll, zwei Arrays mit "==" zu vergleichen?
    //                Was passiert im Detail, bei einem Vergleich mit "=="?
    //
    //  "==" is strict equality and compares REFERENCES for objects that are
    //  not primitive. If the text of a and b is theoretically equal, but
    //  a and b were changed at runtime and have different references,
    //  then "a == b" is still false;
    //
}

