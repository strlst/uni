/*
    Aufgabe 2) Eindimensionale Arrays und File I/O
*/
import java.awt.*;
import java.util.Arrays;

public class Aufgabe2 {
    
    private static String[] readFileData(String fileName, int lineStart, int lineEnd) {
        // check for boundary conditions
        if (fileName == null || lineStart > lineEnd || lineStart < 0)
            return null;

        // init file reader
        In fileReader = new In(fileName);

        // init array plus auxiliary vars
        int line = 0;
        String[] dataArray = new String[lineEnd - lineStart - 1];

        // parse until file has no more lines
        // OR lineEnd is reached
        while (fileReader.hasNextLine() && line < lineEnd - 1) {
            // read line into lineText
            String lineText = fileReader.readLine();
            // only extract lines once lineStart is reached
            if (line >= lineStart)
                // copy extracted line into dataArray
                dataArray[line - lineStart] = lineText;
            line++;
        }

        // return reference to newly populated daraArray
        return dataArray;
    }
    
    private static void extractData(String[] dataArray, int[] resultArray, int numColumn, int entriesPerYear) {
        // check for boundary conditions
        if (dataArray == null || resultArray == null || numColumn < 0 || entriesPerYear < 0)
            return;

        // init auxiliary vars
        int sum = 0;
        int relYear = 0;
        int quantity;

        for (int i = 0; i < dataArray.length; i++) {
            // take the quantity of our wished column
            // implicitly int, as dictated by resultArray
            quantity = Integer.parseInt(dataArray[i].split(";")[numColumn]);
            sum += quantity;

            // every iteration i that is a multiple of entriesPerYear
            // signifies a year to sum up
            if ((i + 1) % entriesPerYear == 0) {
                System.out.println("transferring sum " + sum + " into index " + relYear);
                // the sum thus far is copied into resultArray
                resultArray[relYear] = sum;
                // our relative year counter is incremented, sum is reset
                relYear++;
                sum = 0;
            }
        }
        resultArray[relYear] = sum;
    }
    
    
    private static void drawChart(int[] frostDays, int[] iceDays, int[] summerDays, int[] heatDays) {
        if (frostDays.length != iceDays.length || summerDays.length != heatDays.length ||
            frostDays.length != summerDays.length || iceDays.length != heatDays.length)
            return;

        int width = 1246;
        int height = 500;
        StdDraw.setCanvasSize(width, height);
        StdDraw.setXscale(0, width);
        StdDraw.setYscale(-height / 2, height / 2);

        // init parameters for both axes
        StdDraw.setPenColor(Color.BLACK);
        StdDraw.setFont(new Font("Times", Font.PLAIN, 10));

        // init y axis
        // 25days * 2px = 25days per 2px
        int yAxisStep = 25 * 2, yAxisText = yAxisStep;
        while (yAxisText < height / 2) {
            StdDraw.textLeft(8,           yAxisText, Integer.toString(yAxisText / 2));
            StdDraw.textLeft(8,          -yAxisText, Integer.toString(yAxisText / 2));
            StdDraw.textRight(width - 8,  yAxisText, Integer.toString(yAxisText / 2));
            StdDraw.textRight(width - 8, -yAxisText, Integer.toString(yAxisText / 2));
            yAxisText += yAxisStep;
        }

        // draw bars
        StdDraw.setPenRadius(0.005d);
        int xAxisText = 1979;
        double barStep;
        for (int i = 0; i < frostDays.length; i++) {
            barStep = 8d + 20d + 20d / 2 + i * 30;

            // draw summery days
            StdDraw.setPenColor(Color.RED);
            StdDraw.rectangle(barStep, summerDays[i], 10d, summerDays[i]);
            StdDraw.filledRectangle(barStep, heatDays[i], 10d / 2, heatDays[i]);

            // draw wintry days
            StdDraw.setPenColor(Color.BLUE);
            StdDraw.rectangle(barStep, -frostDays[i], 10d, frostDays[i]);
            StdDraw.filledRectangle(barStep, -iceDays[i], 10d / 2, iceDays[i]);

            // draw x axis labels
            StdDraw.setPenColor(Color.BLACK);
            StdDraw.text(barStep, -(height / 2) + 8, Integer.toString(xAxisText++).substring(2));
        }

        // init middle line
        StdDraw.setPenRadius(0.0055d);
        StdDraw.setPenColor(Color.GREEN);
        StdDraw.line(8, 0, width - 8, 0);
    }
    
    public static void main(String[] args) {
        int curYear = 2019;
        int years = 40;
        int yearLines = 12;
        int lineStart = 289;
        int lineEnd   = lineStart + years * yearLines;

        String[] data = readFileData("./weather_data.csv", lineStart, lineEnd);
        System.out.println(Arrays.toString(data));
        int[] frostDays  = new int[years];
        int[] iceDays    = new int[years];
        int[] summerDays = new int[years];
        int[] heatDays   = new int[years];
        extractData(data, frostDays, 9, yearLines);
        extractData(data, iceDays, 10, yearLines);
        extractData(data, summerDays, 11, yearLines);
        extractData(data, heatDays, 12, yearLines);

        System.out.println(Arrays.toString(frostDays));
        System.out.println(Arrays.toString(iceDays));
        System.out.println(Arrays.toString(summerDays));
        System.out.println(Arrays.toString(heatDays));

        drawChart(frostDays, iceDays, summerDays, heatDays);
    }
}


