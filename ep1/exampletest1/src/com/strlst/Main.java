package com.strlst;

public class Main {

    private static int countCharInString(String sequence, char c) {
        int counter = 0;
        for (char s : sequence.toCharArray()) {
            if (s == c) counter++;
        }
        return counter;
    }

    private static int compareSequences(String seq1, String seq2, char marker) {
        int seq1occurences = countCharInString(seq1, marker);
        int seq2occurences = countCharInString(seq2, marker);
        if (seq1occurences == seq2occurences) return 0;
        return (seq1occurences > seq2occurences) ? 1 : -1;
    }

    private static boolean isPresentNTimes(String seq, char marker, int count) {
        if (seq.length() == 0) return false;

        boolean self = seq.charAt(0) == marker;

        if (seq.length() == 1 && count == 0 && !self) return true;
        if (seq.length() == 1 && count == 1 && self)  return true;

        return isPresentNTimes(seq.substring(1), marker, self ? count - 1 : count);
    }

    public static void main(String[] args) {
        String seq1 = "ABBAACBA";
        String seq2 = "DECDB";
        System.out.println(countCharInString(seq1, 'A'));
        System.out.println(countCharInString(seq1, 'D'));
        System.out.println(compareSequences(seq1, seq2, 'D'));
        System.out.println(compareSequences(seq1, seq2, 'C'));
        System.out.println(compareSequences(seq1, seq2, 'B'));
        System.out.println(compareSequences("", "", 'D'));
        System.out.println(compareSequences("", "D", 'D'));
        System.out.println(compareSequences("D", "", 'D'));
        System.out.println(isPresentNTimes(seq1, 'A', 4));
        System.out.println(isPresentNTimes(seq1, 'A', 3));
        System.out.println(isPresentNTimes(seq1, 'A', 5));
        System.out.println(isPresentNTimes(seq1, 'Z', 0));
    }
}
