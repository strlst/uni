/*
    Aufgabe 1) Codeanalyse, Codingstyle und Methoden
*/
public class Aufgabe1 {

    /*
    sortString nimmt einen String str entgegen und liefert diesen sortiert als Rückgabewert
    */
    public static String sortString(String str) {
        
        for (int i = 1; i < str.length(); i++) {
            char currentChar = str.charAt(i);
            int j = i - 1;
            while (j >= 0 && str.charAt(j) >= currentChar) {
                j -= 1;
            }
            j += 1;
            str = str.substring(0, j)
                + currentChar
                + str.substring(j, i)
                + str.substring(i + 1);
        }
        return str;
    }

    /*
    stringLength nimmt einen String str entgegen und returned die länge davon
     */
    public static int stringLength(String str) {
        return str.length();
    }

    /*
    increment nimmt einen primitive integer n entgegen und inkrementiert diesen
     */
    public static int increment(int n) {
        return n + 1;
    }

    /*
    decrement nimmt einen primitive integer n entgegen und dekrementiert diesen
     */
    public static int decrement(int n) {
        return n - 1;
    }

    /*
    isIntGreaterOrEqual nimmt zwei primitive integer entgegen und liefert als wahrheitswert
    ob n1 größer/gleich n2 ist
     */
    public static boolean isIntGreaterOrEqual(int n_1, int n_2) {
        return n_1 >= n_2;
    }

    /*
    isCharGreaterOrEqual nimmt zwei primitive characters c_1, c_2 entgegen und liefert als wahrheitswert
    ob c_1 einen höheren/denselben ASCII wert wie c_2 hat

    von der funktionsweise mit isIntGreaterOrEqual ident
     */
    public static boolean isCharGreaterOrEqual(char c_1, char c_2) {
        return c_1 >= c_2;
    }

    /*
    substring nimmt einen string, zwei primitive integer und liefert einen ausschnitt des strings s,
    wobei n1 ein inklusiver startindex ist und n2 ein exklusiver endindex
     */
    public static String substring(String fullString, int n_1, int n_2) {
        return fullString.substring(n_1, n_2);
    }
    
    public static void main(String args[]) {
        System.out.println(sortString("ab"));
        System.out.println(sortString("ba"));
        System.out.println(sortString("aa"));
        System.out.println(sortString("cba"));
        System.out.println(sortString("abababab"));
        System.out.println(sortString("abcfghed"));
        System.out.println(sortString("abnasnasab"));
        System.out.println(sortString("najskaghkkjsfvjhbavbdfsan"));
        System.out.println(sortString("jgbgdsjabkjdbvbdjabkjsavbkjbdsvkjbagfgafjdbv"));
        
        assert (sortString("ab").equals("ab"));
        assert (sortString("ba").equals("ab"));
        assert (sortString("aa").equals("aa"));
        assert (sortString("cba").equals("abc"));
        assert (sortString("abababab").equals("aaaabbbb"));
        assert (sortString("abcfghed").equals("abcdefgh"));
        assert (sortString("abnasnasab").equals("aaaabbnnss"));
        assert (sortString("najskaghkkjsfvjhbavbdfsan").equals("aaaabbdffghhjjjkkknnsssvv"));
        assert (sortString("jgbgdsjabkjdbvbdjabkjsavbkjbdsvkjbagfgafjdbv").equals("aaaaabbbbbbbbbdddddffggggjjjjjjjjkkkksssvvvv"));
    }
}



