/*
    Aufgabe 2) Überladen von Methoden
*/
public class Aufgabe2 {

    /*
    general method to add variable tags
     */
    public static void addTags(String text, char tag1, char tag2, char tag3) {
        String mod = "";

        mod += tag1;
        int middle = (int) Math.ceil(text.length() / 2.);

        mod += text.substring(0, middle);
        mod += tag2;

        mod += text.substring(middle);
        mod += tag3;

        System.out.println(mod);
    }

    /*
    overload of addTags, handles identical tags
     */
    public static void addTags(String text, char tag) {
        addTags(text, tag, tag, tag);
    }

    /*
    overload of addTags, handles integers instead of strings
     */
    public static void addTags(int number, char tag) {
        String mod = "";
        int digits = 0, copy = number, middle;

        do {
            copy /= 10;
            digits++;
        } while (copy > 0);

        // calculate correct position for inserted tag
        middle = (int) Math.ceil(digits / 2.) - 1;
        mod += tag;
        // loop all digits
        for (int i = 0; i < digits; i++) {
            // take single digits by making use of truncation that happens
            // when an integer has a remainder
            // by dividing x * 10^y by 10^y, letting only x remain
            mod += number / (int)Math.pow(10, (digits - i) - 1);
            // remove x * 10^y by taking the remainder when divided by 10^y
            number %= Math.pow(10, (digits - i) - 1);
            // be sure to insert a middle tag character
            if (i == middle) mod += tag;
        }
        mod += tag;

        // print result
        System.out.println(mod);
    }

    /*
    overload of addTags, takes a tag string and prints all permutations
     */
    public static void addTags(String text, String tags) {
        for (int i = 0; i < tags.length(); i++) {
            addTags(text, tags.charAt(i));
        }
    }

    /*
    extra question: default value via parameter overloading
     */
    public static void addTags(String text) {
        char defSeparator = 'X';
        addTags(text, defSeparator);
    }
    
    public static void main(String[] args) {
        String text0 = "A";
        String text1 = "AB";
        String text2 = "Hello World!";//12
        String text3 = "Java-Programmierung";//19
        String text4 = "Das ist ein Test";//16
        
        addTags(text0, '?');
        addTags(text1, ',');
        addTags(text2, ':');
        addTags(text3, '+');
        addTags(text4, '-');
        
        addTags(1, '$');
        addTags(35, '*');
        addTags(2048, '#');
        addTags(657, ':');
        addTags(26348, '+');
        
        addTags(text2, "+#$");
        addTags(text3, ":*&!");

        addTags("Hello World!");
    }
}
