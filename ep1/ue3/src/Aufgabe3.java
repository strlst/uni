import java.awt.*;

/*
    Aufgabe 3) Zeichnen mit Schleifen und Methoden
*/
public class Aufgabe3 {
    
    private static void drawTriangle(double centerX, double centerY, double height, int direction) {
        int x_mod1, y_mod1, x_mod2, y_mod2;
        // determine orientation coefficients
        // direction divisible by 2 implies identical x_mods
        if (direction % 2 == 0) {
            // direction = 0 => x_mod1 = x_mod2 =  1
            // direction = 2 => x_mod1 = x_mod2 = -1
            x_mod1 = x_mod2 = 1 - direction;
            // y_mods are invariant
            y_mod1 =  1;
            y_mod2 = -1;
        } else {
            // direction = 1 => y_mod1 = y_mod2 = -1
            // direction = 3 => y_mod1 = y_mod2 =  1
            y_mod1 = y_mod2 = direction - 2;
            // x_mods are invariant
            x_mod1 =  1;
            x_mod2 = -1;
        }

        // draw 3 independent lines, while letting the orientation coefficients
        // handle correct orientation
        StdDraw.line(centerX, centerY,
                centerX + height * x_mod1,
                centerY + height * y_mod1);
        StdDraw.line(centerX, centerY,
                centerX + height * x_mod2,
                centerY + height * y_mod2);
        StdDraw.line(centerX + height * x_mod1,
                centerY + height * y_mod1,
                centerX + height * x_mod2,
                centerY + height * y_mod2);
    }
    
    private static void drawTrianglePattern(double centerX, double centerY, int height, int centerShift) {
        int x_mod, y_mod, direction;

        // generate a triangle by drawing all 4 possible directions
        for (int i = 0; i < 4; i++) {
            direction = i;
            // orientation is determined identically to drawTriangle
            if (direction % 2 == 0) {
                x_mod =   1 - direction;
                y_mod = 0;
            } else {
                x_mod = 0;
                y_mod = direction - 2;
            }
            drawTriangle(centerX + centerShift * x_mod,
                    centerY + centerShift * y_mod,
                    height,
                    direction);
            //System.out.println(x_mod + " " + y_mod + " " + direction);
        }
    }
    
    private static void drawTrianglePatternLine(double centerX, double centerY, int height, int growth, int distance) {
        int sum = 0;
        int total_px = 0;
        int i = 0;
        while (i++ < 7) {
            drawTrianglePattern(centerX + total_px,
                    centerY,
                    height,
                    sum);
            sum += growth;
            // height added only once, as total_px already determines the
            // center of the last triangle construct!
            // sum added twice to pad left and right from center point
            total_px += height + 2 * sum + distance;
        }
    }
    
    public static void main(String[] args) {
        
        int pixelWidth = 200;
        int pixelHeight = 200;
        StdDraw.setCanvasSize(pixelWidth, pixelHeight);
        StdDraw.setXscale(0, pixelWidth);
        StdDraw.setYscale(0, pixelHeight);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.005);

        StdDraw.clear(StdDraw.BLACK);
        StdDraw.setPenColor(StdDraw.BOOK_BLUE);
        drawTriangle(100, 100, 20, 0);
        StdDraw.setPenColor(StdDraw.BOOK_RED);
        drawTriangle(100, 100, 40, 1);
        StdDraw.setPenColor(StdDraw.MAGENTA);
        drawTriangle(100, 100, 30, 2);
        StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
        drawTriangle(100, 100, 50, 3);
        StdDraw.pause(5000); //Wartezeit 5 Sekunden

        StdDraw.clear(StdDraw.BLACK);
        StdDraw.setPenColor(StdDraw.BOOK_BLUE);
        drawTrianglePattern(100, 100, 50, 0);
        StdDraw.setPenColor(StdDraw.BOOK_RED);
        drawTrianglePattern(100, 100, 40, 10);
        StdDraw.setPenColor(StdDraw.MAGENTA);
        drawTrianglePattern(100, 100, 20, 40);
        StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
        drawTrianglePattern(100, 100, 10, 20);
        StdDraw.pause(5000);//Wartezeit 5 Sekunden
        StdDraw.clear();
        
        pixelWidth = 700;
        StdDraw.setCanvasSize(pixelWidth, pixelHeight);
        StdDraw.setXscale(0, pixelWidth);
        StdDraw.setYscale(0, pixelHeight);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.005);
        
        //drawTrianglePatternLine(50, 100, 20, 5, 20);
        //drawTrianglePatternLine(50, 100, 10, 3, 10);
        drawTrianglePatternLine(50, 100, 10, 10, 15);
    }
}
