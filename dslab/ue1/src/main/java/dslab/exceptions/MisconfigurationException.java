package dslab.exceptions;

/**
 * exception used to signal misconfiguration problems
 */
public class MisconfigurationException extends Exception {
    public MisconfigurationException() { super(); }
}