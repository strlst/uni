package dslab.exceptions;

/**
 * exception used to signal protocol errors when dealing with DMTP
 */
public class DMTPProtocolException extends Exception {
    public DMTPProtocolException() { super(); }
}
