package dslab.core;

/**
 * interface that forms the counterpart to Runnable, used whenever shutdown methods should be enforced
 */
public interface Shutdownable {
    /**
     * after this method, all resources should be closed, and the application should terminate;
     * typically used to add as a CLI command to shut down the server
     */
    public void shutdown();
}
