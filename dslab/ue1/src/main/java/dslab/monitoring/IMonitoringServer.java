package dslab.monitoring;

import dslab.core.Shutdownable;

/**
 * the monitoring service accepts incoming monitoring packets via UDP. It provides CLI commands to access the
 * information.
 *
 * do not change the existing method signatures!
 */
public interface IMonitoringServer extends Runnable, Shutdownable {

    /**
     * starts the server
     */
    @Override
    void run();

    /**
     * CLI command to report usage statistics for transfer servers
     */
    void servers();

    /**
     * CLI command to report usage statistics for individual senders
     */
    void addresses();
}
