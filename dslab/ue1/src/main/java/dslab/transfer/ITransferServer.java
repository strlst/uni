package dslab.transfer;

import dslab.core.Shutdownable;

/**
 * The transfer server is responsible for accepting mails sent by users, and forward them to mailbox servers via DMTP.
 * It also reports usage statistics to the monitoring server.
 *
 * Do not change the existing method signatures!
 */
public interface ITransferServer extends Runnable, Shutdownable {

    /**
     * Starts the server.
     */
    @Override
    void run();
}
