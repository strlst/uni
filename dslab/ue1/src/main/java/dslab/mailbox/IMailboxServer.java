package dslab.mailbox;

import dslab.core.Shutdownable;

/**
 * the mailbox server receives mails via DMTP from transfer servers, and makes them available to users via the DMAP
 * protocol
 *
 * do not change the existing method signatures!
 */
public interface IMailboxServer extends Runnable, Shutdownable {

    /**
     * starts the server
     */
    @Override
    void run();
}
