package dslab.mailbox;

import dslab.util.AddMessageTask;
import dslab.util.Message;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * mailbox specific utility class that is runnable and should run in its own thread;
 * the purpose is to process outbound write tasks in an orderly fashion and therefore
 * adhere to the single writer principle for write tasks
 */
public class WriteQueueHandler implements Runnable {
    private final String componentId;
    private final BlockingQueue<AddMessageTask> writeTasks;
    private final Map<String, List<Message>> mails;

    /**
     * creates a new WriteQueueHandler
     * @param componentId string representation of component
     * @param writeTasks queue where write tasks will be put to
     * @param mails reference to map with mapping of users to lists of messages
     */
    public WriteQueueHandler(String componentId, BlockingQueue<AddMessageTask> writeTasks, Map<String, List<Message>> mails) {
        if (writeTasks == null)
            throw new IllegalArgumentException();

        this.componentId = componentId;
        this.writeTasks = writeTasks;
        this.mails = mails;
    }

    @Override
    public void run() {
        System.out.printf("@%s: begin run%n", componentId);
        try {
            while (true) {
                // BlockingQueue is battle-tested and thread-safe
                AddMessageTask task = writeTasks.take();
                // get and announce users
                List<String> users = task.getUsers();
                System.out.printf("@%s: incoming add message task with message%s for users %s%n", componentId, task.getMessage(), Arrays.toString(users.toArray()));
                // TODO: abstract delete operation into another write tasks so all writes happen here
                synchronized (mails) {
                    // for all users that are recipients of the message
                    // (users are already verified to exist, by the DMAPProtocol)
                    for (String user : users) {
                        List<Message> userMails = mails.get(user);
                        // add that message
                        userMails.add(new Message(task.getMessage()));
                    }
                }
                System.out.printf("@%s: done writing message%n", componentId);
            }
        } catch (InterruptedException exception) {
            System.out.printf("@%s: stopped execution due to interruption%n", componentId);
        }
        System.out.printf("@%s: end run%n", componentId);
    }
}
