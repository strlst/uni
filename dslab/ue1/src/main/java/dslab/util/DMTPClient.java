package dslab.util;

import dslab.exceptions.DMTPProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

/**
 * DMTPClient that is used for outgoing TransferServer transfers
 * (usually in the form of a task for a SendQueueHandler)
 */
public class DMTPClient implements Runnable {
    // 15 second timeout
    private static final int TIMEOUT = 15*1000;

    private final String componentId;
    private final String host;
    private final int port;
    private final Message message;
    private final Config domains;
    private final Pair<InetAddress, Integer> monitoring;
    private final boolean notifySenderOnError;

    /**
     * creates a new DMTPClient
     * @param componentId string representation of component
     * @param host host string (ip or domain)
     * @param port port number
     * @param message message to transmit
     * @param domains domain configuration for look-ups
     * @param notifySenderOnError whether to loop-back transmission errors to the sender
     */
    public DMTPClient(String componentId, String host, int port, Message message, Config domains,
                      Pair<InetAddress, Integer> monitoring, boolean notifySenderOnError) {
        this.componentId = componentId;
        this.host = host;
        this.port = port;
        this.message = message;
        this.domains = domains;
        this.monitoring = monitoring;
        this.notifySenderOnError = notifySenderOnError;
    }

    /**
     * TODO: refactor and split method to be more readable and cleanly solved
     *       maybe separate duties also
     */
    @Override
    public void run() {
        // notify user of what is happening
        System.out.printf(
            "@%s: processing message %s%n",
            componentId,
            message.toString()
        );

        // send a UDP packet into the void in hopes of reaching a monitoring server
        try (DatagramSocket client = new DatagramSocket()) {
            // bookkeeping
            String payload = String.format("%s:%d %s%n", host, port, message.sender);
            byte[] buffer = payload.getBytes(StandardCharsets.UTF_8);
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, monitoring.key(), monitoring.value());
            client.send(packet);
            System.out.printf(
                "@%s: monitoring \"%s:%d %s\" at %s:%d%n",
                componentId,
                host,
                port,
                message.sender,
                monitoring.key(),
                monitoring.value()
            );
        } catch (SocketException exception) {
            System.err.printf(
                "@%s: unable to open UDP socket for sending: %s:%d%n",
                componentId,
                monitoring.key(),
                monitoring.value()
            );
        } catch (IOException exception) {
            System.err.printf(
                "@%s: an error has occurred: i/o error for host %s:%d%n",
                componentId,
                monitoring.key(),
                monitoring.value()
            );
            exception.printStackTrace();
        }

        // construct arrays describing DMTP exchange
        // from the perspective of a client
        String[] requests =  {
                "ok DMTP",
                "ok",
                //String.format("ok %d", groupedRecipients.get(domain).size()),
                "ok",
                "ok",
                "ok",
                "ok",
                "ok",
                "ok bye"
        };
        String[] responses = {
                "begin",
                String.format("to %s", message.recipients),
                String.format("from %s", message.sender),
                String.format("subject %s", message.subject),
                String.format("data %s", message.data),
                "send",
                "quit"
        };
        String localAddress = null;
        // try-with-resources block for resource objects
        try (
                Socket client = new Socket(host, port);
                PrintWriter clientOut = new PrintWriter(client.getOutputStream(), true);
                BufferedReader clientIn = new BufferedReader(new InputStreamReader(client.getInputStream()))
        ) {
            localAddress = client.getLocalAddress().toString();
            // register timeout
            client.setSoTimeout(TIMEOUT);
            // follow specified protocol exchange
            for (int rounds = 0; rounds < responses.length; rounds++) {
                String line = clientIn.readLine();
                if (!line.contains(requests[rounds]))
                    throw new DMTPProtocolException();
                clientOut.println(responses[rounds]);
            }

            // notify user of successful transfer
            System.out.printf("@%s: message delivered successfully%n", componentId);
        } catch (DMTPProtocolException | IOException exception) {
            // deal with various exception cases
            if (exception instanceof DMTPProtocolException)
                System.err.printf("@%s: an error has occurred: server has responded unexpectedly %s:%d%n", componentId, host, port);
            else if (exception instanceof SocketTimeoutException)
                System.err.printf("@%s: an error has occurred: socket timed out %s:%d%n", componentId, host, port);
            else if (exception instanceof UnknownHostException)
                System.err.printf("@%s: an error has occurred: cannot reach the host %s:%d%n", componentId, host, port);
            else {
                // anything that falls under this category is an IOException
                System.err.printf("@%s: an error has occurred: i/o error for host %s:%d%n", componentId, host, port);
                exception.printStackTrace();
            }

            // failure retransmission will be attempted with this flag
            if (notifySenderOnError) {
                // TODO: very ugly
                Message failureMessage = new Message();
                String address = localAddress == null ?
                        DMTPClient.getFirstNonLoopbackAddress(true, false) :
                        localAddress;

                // reformat message into failure message
                failureMessage.sender = String.format("mailer@%s", address);
                failureMessage.recipients = message.sender;
                failureMessage.subject = String.format("failed delivery: %s", message.subject);
                failureMessage.data = String.format(
                    "for at least one recipient the following message has not been delivered: %s",
                    message.data
                );

                // various transformations to find new host for failure message transmission
                String newDomain = message.sender.split("@")[1];
                String newHostWithPort = domains.getString(newDomain);
                String host = newHostWithPort.split(":")[0];

                // attempt extracting the port
                int port = 0;
                try {
                    port = Integer.parseInt(newHostWithPort.split(":")[1]);
                } catch (NumberFormatException ignored) {}

                // attempt failure message transmission
                DMTPClient dmtpClient = new DMTPClient(
                        String.format("%s-failure-sender", componentId),
                        host,
                        port,
                        failureMessage,
                        domains,
                        monitoring,
                        // prevent infinite recursion
                        false
                );

                // run the new failure message sender
                dmtpClient.run();
            }
        }
    }

    /**
     * auxiliary method to go through interfaces and find first (non-loopback) address
     * snippet sourced from:
     * https://stackoverflow.com/questions/901755/how-to-get-the-ip-of-the-computer-on-linux-through-java/901943#901943
     * (slightly modified from source)
     * @param preferIpv4 whether ipv4 is preferred
     * @param preferIPv6 whether ipv6 is preferred
     * @return first non-loopback ipv4, ipv6 address (or first address in general if both are preferred)
     */
    private static String getFirstNonLoopbackAddress(boolean preferIpv4, boolean preferIPv6) {
        Enumeration<NetworkInterface> en;
        try {
            en = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException exception) {
            return "127.0.0.1";
        }

        while (en.hasMoreElements()) {
            NetworkInterface i = (NetworkInterface) en.nextElement();
            for (Enumeration<InetAddress> en2 = i.getInetAddresses(); en2.hasMoreElements();) {
                InetAddress address = en2.nextElement();
                if (!address.isLoopbackAddress()) {
                    if (address instanceof Inet4Address) {
                        if (preferIPv6) {
                            continue;
                        }
                        return address.getHostAddress();
                    }
                    if (address instanceof Inet6Address) {
                        if (preferIpv4) {
                            continue;
                        }
                        return address.getHostAddress();
                    }
                }
            }
        }

        return "127.0.0.1";
    }
}
