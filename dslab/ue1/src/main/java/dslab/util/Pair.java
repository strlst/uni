package dslab.util;

/**
 * utility class that might be used as a generic container for read-only key-value pairs
 * @param <T> type of container for value 1
 * @param <S> type of container for value 2
 */
public class Pair<T, S> {
    private final T key;
    private final S value;

    public Pair(T key, S value) {
        this.key = key;
        this.value = value;
    }

    public T key() { return key; }
    public S value() { return value; }
}
