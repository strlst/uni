package dslab.util;

import java.util.ArrayList;
import java.util.List;

/**
 * container class for a task that stores the message to be added, as well as all
 * recipients this message is intended to be stored for
 */
public class AddMessageTask {
    private final Message message;
    private final List<String> users;

    /**
     * constructs an AddMessageTask object
     * @param message message to be stored, will take a shallow reference and make a deep copy itself
     * @param users list of users to store message for, will take a shallow reference and make a deep copy itself
     */
    public AddMessageTask(Message message, List<String> users) {
        // clone message and users for concurrency reasons
        this.message = new Message(message);
        this.users = new ArrayList<>(users);
    }

    public Message getMessage() { return this.message; }
    public List<String> getUsers() { return this.users; }
}
