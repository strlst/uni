package dslab.util;

/**
 * container for abstract DMTP message, wraps the data with useful utilities
 */
public class Message {
    public String sender;
    public String recipients;
    public String subject;
    public String data;

    /**
     * default constructor
     */
    public Message() {}

    /**
     * copy constructor
     * @param message Message object to copy
     */
    public Message(Message message) {
        this.sender = message.sender;
        this.recipients = message.recipients;
        this.subject = message.subject;
        this.data = message.data;
    }

    /**
     * query completion of message
     * @return whether all fields are set
     */
    public boolean isMessageIncomplete() {
        return (recipients != null) && (sender != null) && (subject != null) && (data != null);
    }

    /**
     * gets a formatted string
     * @return formatted string containing a message that shows which fields are not set
     */
    public String getMissingFields() {
        StringBuilder stringBuilder = new StringBuilder("no");
        if (recipients == null)
            stringBuilder.append(" to");
        if (sender == null)
            stringBuilder.append(" from");
        if (subject == null)
            stringBuilder.append(" subject");
        if (data == null)
            stringBuilder.append(" data");
        return stringBuilder.toString();
    }

    /**
     * resets all fields to null
     */
    public void reset() {
        recipients = sender = subject = data = null;
    }

    /**
     * overridden toString implementation
     * @return string representation Message
     */
    @Override
    public String toString() {
        return String.format(
            "{ 'from': '%s', 'to': '%s', 'subject': '%s', 'data': '%s' }",
            sender,
            recipients,
            subject,
            data
        );
    }
}
