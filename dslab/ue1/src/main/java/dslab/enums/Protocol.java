package dslab.enums;

/**
 * enum that specifies possible protocols for the Server class
 */
public enum Protocol {
    DMTP, DMAP
}
