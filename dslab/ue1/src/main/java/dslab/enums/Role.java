package dslab.enums;

/**
 * enum that specified possible roles for the Server class
 */
public enum Role {
    Transfer, Monitoring, Mailbox
}
