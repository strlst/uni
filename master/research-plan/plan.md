# Development of a high-performance processor with memory encryption and integrity verification mechanisms

The aim of this work is to study for an existing processor platform, preferably a suitable, open source and performance oriented RISC-V core whether it is possible to implement memory encryption and integrity verification mechanisms without increasing latency of memory to unacceptable levels.
It would be optimal to design a system with no latency penalty, but since operations on memory take time and memory is usually the bottleneck, a more realistic target would be to attempt to keep the increase in latency as minimal as possible.
What it means for the performance of the processor to implement such memory features remains to be studied in more detail.

## Project members

**Master student:** Norbert Cornelius Tremurici, BSc

(possibly in cooperation with a master student at the host university)

**Local university advisor:** Univ.Prof. Dr.-Ing. Dipl.-Ing. Daniel Müller-Gritschneder

**Host university advisor:** Ph.D. Shinya Takamaeda-Yamazaki

## Objectives

Positive outcomes of this work should be:

- a study of hardware attack vectors and mechanisms to shield against them without disregard for memory latency
- a characterization of the relationship between memory subsystem features and its relationship to memory latency
- a usable prototype for an existing processor design that has newly implemented memory encryption and integrity verification
- possible open source contributions in this field

## Tentative timeline

- March 4, 2025: arrival in Japan
- April 4, 2025: review of existing literature and implementation opportunities complete
- May 9, 2025: first basic implementation complete
- July 4, 2025: testing and verification framework complete
- August 29, 2025: draft of thesis complete and possible changes to implementation and testing/verfication framework made
