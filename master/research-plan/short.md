The aim of this work is to study for an existing processor platform, preferably a suitable, open source and performance oriented RISC-V core whether it is possible to implement memory encryption and integrity verification mechanisms without increasing latency of memory to unacceptable levels.
What it means for the performance of the processor to implement such memory features remains to be studied in more detail.
Positive outcomes of this work should be a study of hardware attack vector and mechanisms to shield against them with regard for memory latency, a usable prototype and possible open source contributions in this field.

The thesis will be officially advised by Professor Daniel Müller-Gritschneder, who leads research relating to computer architecture at TU Wien.
But the primary work will be done while at UTokyo, supervised by Professor Shinya Takamaeda-Yamazaki, where I will be integrated into CASYS (Takamaeda Lab), possibly working together with a local master student.
