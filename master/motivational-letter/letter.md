# Motivational Letter

Dear Sir or Madam,

My name is Norbert Cornelius Tremurici.
I am currently in my 6th year of study at the Institute of Computer Engineering at TU Wien.
Previously I have worked towards and acquired my bachelor at this university, exploring computer architecture in detail.
The central question for that work was: what extensions to a basic 5-stage RISC-V pipeline are necessary to support the execution of an operating system?

Fast forward a year, I am now nearing completion of my master level courses and considering to write my master thesis as part of a research stay in a Japanese university.
Japan houses fascinating laboratories with respect to the field of computer architecture.
But what truly convinced me to apply was listening to prior experiences of colleagues, who have had overwhelmingly positive things to say.

There are at least three different possible topics for my master thesis after speaking to Univ.Prof. Dr.-Ing. Dipl.-Ing. Daniel Müller-Gritschneder (professor local to TU Wien) as well as Ph.D. Shinya Takamaeda-Yamazaki (professor in Japan at the University of Tokyo).
I would be glad to discuss topics in further detail if necessary.
It is not yet determined what it will be, since that choice depends on the specific laboratory.
However a research plan in accordance with suggestions by professor Takamaeda has been created.

In my view, the match between local faculty and the particular laboratory led by professor Takamaeda are perfect and I would love to be given this opportunity to base my research stay there.
Should such a stay at that laboratory not be possible, then perhaps a new topic for an alternative university can be arranged.
I would love to be given this opportunity!

Best regards,

Norbert Tremurici
