\documentclass[a4paper,oneside,11pt,DIV12,headsepline,footexclude,headexclude]{scrartcl}


%% Normal LaTeX or pdfLaTeX? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ==> The new if-Command "\ifpdf" will be used at some
%% ==> places to ensure the compatibility between
%% ==> LaTeX and pdfLaTeX.
\newif\ifpdf
\ifx\pdfoutput\undefined
	\pdffalse              %%normal LaTeX is executed
\else
	\pdfoutput=1
	\pdftrue               %%pdfLaTeX is executed
\fi

%% Packages for Graphics & Figures %%%%%%%%%%%%%%%%%%%%%%%%%%
\ifpdf %%Inclusion of graphics via \includegraphics{file}
	\usepackage[pdftex]{graphicx} %%graphics in pdfLaTeX
\else
	\usepackage[dvips]{graphicx} %%graphics and normal LaTeX
\fi
\graphicspath{{fig/}}

%% Fonts for pdfLaTeX %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ==> Only needed, if cm-super-fonts are not installed
%\ifpdf
	%\usepackage{ae}       %%Use only just one of these packages:
	%\usepackage{zefonts}  %%depends on your installation.
%\else
	%%Normal LaTeX - no special packages for fonts required
%\fi

\renewcommand{\rmdefault}{pbk} % bookman
\renewcommand{\sfdefault}{phv} % helvetica (avantgarde = pag)
\renewcommand{\ttdefault}{pcr} % courier
\renewcommand{\familydefault}{phv}

%\usepackage{cmbright}  % computer modern bright - not for pdf


\areaset{16cm}{24cm}
\addtolength{\topskip}{0.5cm}


% texttt hyphenation
\newcommand{\origttfamily}{}
\let\origttfamily=\ttfamily
\renewcommand{\ttfamily}{\origttfamily \hyphenchar\font=`\-}


\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{array}
\usepackage{float}
\usepackage{paralist}
\usepackage{color}
\usepackage{colortbl}

\usepackage{listings}
\lstset{language=Java,basicstyle=\ttfamily\small,tabsize=2}

\usepackage{minted}


%% Line Spacing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{setspace}
%\singlespacing        %% 1-spacing (default)
%\onehalfspacing       %% 1,5-spacing
%\doublespacing        %% 2-spacing

\linespread{1.05}
\addtolength{\parskip}{0.175\baselineskip}

\widowpenalty = 10000
\clubpenalty = 10000


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DOCUMENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%% File Extensions of Graphics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ==> This enables you to omit the file extension of a graphic.
%% ==> "\includegraphics{title.eps}" becomes "\includegraphics{title}".
%% ==> If you create 2 graphics with same content (but different file types)
%% ==> "title.eps" and "title.pdf", only the file processable by
%% ==> your compiler will be used.
%% ==> pdfLaTeX uses "title.pdf". LaTeX uses "title.eps".
\ifpdf
	\DeclareGraphicsExtensions{.pdf,.jpg,.png}
\else
	\DeclareGraphicsExtensions{.eps}
\fi


\pagestyle{plain} %Now display headings: headings / fancy / ...

\title{\Large Matter: Bridge the Gap to Web of Things}

\author{Norbert Tremurici (11907086), Technische Universitšt Wien}
%\date{} %%If commented, the current date is used.

\maketitle

\begin{abstract}

The Web of Things is an emerging technology for the Internet of Things.
Several protocols have been bridged to it, yet no such bridge exists for another emerging technology, the Matter Protocol.
In this Paper we want to present a simple methodology to transform individual Matter Endpoints into Web of Things Things.

\end{abstract}

\section{Introduction}

The landscape of Internet of Things (IoT) technologies is ever shifting, there are many problems to solve and thus many attempts towards a solution.
A few such problems are the lack of interoperable standards for already deployed devices \cite{matter-connected-smart-home}, the wide range of communication protocols and use of IoT devices in applications deployed on web browsers.
The problem of interoperability exists on many levels, some devices use Ethernet, some use Bluetooth, some use Wi-Fi, some use ZigBee, there is amazing diversity amongst communication and networking protocols.
Above this abstraction level, we find an even more disorganized application layer.

The complexity involved consequently makes it difficult to build robust standards.
Standards need to deal with a wide range of requirements, some of which contradict each other.

In this paper, we will take a loot at the Matter Protocol and the Web of Things (WoT) respectively.
We will consider their specifications, targeted goals and why they are suited to solve the problems they are intended to solve.
Finally, we will look at a way to "bridge the gap," we will derive a Thing Description from a Matter device and infer an appropriate protocol binding.
Thus we will have described how a Matter-to-WoT Gateway would work.

\section{Matter Protocol}

The Matter Protocol is an emerging standard that was previously known under the name "Project Connected Home over IP (CHIP)".
It is intended to improve interoperability between IoT devices and was brought into life by the Connectivity Standard Alliance (CSA), who are also known for the ZigBee standard. \cite{matter-connected-smart-home}

Matter consists of a family of specifications with the Core specification at the center. \cite{matter-spec}

One advantage of the Matter Protocol is that not all layers of the classic OSI model are defined.
Only the top three layers are defined by Matter, which leaves room for existing transport and network technologies to handle the lower layers.
In fact, Matter was designed to use IPv6 and supports Ethernet, Wi-Fi, Bluetooth Low Energy and 802.15.4 (implemented, for example, using Thread). \cite{web-matter}

This not only allows for flexible use of Matter, but allows it to interoperate with existing deployments that use the same transport and network standards.
Another challenge of interoperability is not only integration with future products, but the great wealth of already existing ones.

\subsection{Matter nodes}

There are several roles defined for nodes of a Matter network, the roles are as follows:

\begin{itemize}
	\item \textbf{Commissioner} A commissioner serves to execute the commissioning process which provisions new nodes for the network
	\item \textbf{Controller} A controller can direct other nodes of the network
	\item \textbf{Controlee} A controlee is directed by another node of the network
	\item \textbf{OTA Provider} An OTA provider can provide OTA software updates
	\item \textbf{OTA Requestor} An OTA requestor can request OTA software updates
\end{itemize}

Each node can have one or more such roles \cite{web-matter-ddm}.
Nodes themselves can enclose one or more endpoints, which exist to realize some feature set.
These feature sets are defined as part of the standard in Clusters.
These Clusters serve as well-defined interfaces for appliances and offer a semantic model of such appliances.
A common example is a light switch, which is often realized using the \textit{On/Off Cluster}.

Each Cluster can include \textit{attributes}, \textit{commands} and \textit{events}.
Attributes include the state necessary for the Cluster.
Commands represent actions that operate on a node, often changing the state of the appliance in some way.
Lastly, events exist to provide historical data of a node, like a record of past transitions that is timestamped.

As can be seen, Matter offers a unified and standardized model to realize all kinds of appliances.
It will not be considered in more detail here, but the matter protocol also affords for latency-criticality, energy-efficiency or security, making for an attractive target in this ever evolving space.

\section{Web of Things}

Even though we call the emerging industry of automation the Internet of Things (IoT), there is surprising difficulty in interoperating IoT devices with other web technologies.
This could be explained by use of different technologies than those familiar as part of the internet.
There is a case to be made for IoT devices to be more accessible using common web technologies and this case is being made by the Web of Things (WoT) family of specifications.
For instance, HTTP interoperability is part of the standards. \cite{iot-interop-wot}

The WoT family of specifications is being developed by the W3C.
They are known for having enabled many other web technologies before.
Similar to Matter, Web of Things is intended to counter fragmentation by enabling better integration of IoT devices.
The protocol does take on quite a different form however. \cite{iot-interop-wot}

\subsection{Components of the Web of Things}

There are several parts of the specification that act together to enable the WoT to realize goals of IoT devices.
Important concepts include \textit{WoT Things}, \textit{WoT Thing Descriptions} and \textit{Interaction Affordances}, though there are also other concepts (\textit{WoT Building Blocks}), as defined in the architecture document \cite{web-wot-arch}. \cite{iot-interop-wot}

A Thing is defined in the specification to be the following:

\begin{quote}
	``An abstraction of a physical or a virtual entity whose metadata and interfaces are described by a WoT Thing Description, whereas a virtual entity is the composition of one or more Things.'' \cite{web-wot-arch-terminology}
\end{quote}

The Thing is the central concept of the Web of Things and is perhaps comparable to a Matter node.
A Thing is modelled by a Thing description, which includes structured data: metadata, interaction affordances and links to related Things.
According to the specification, the format of Thing Descriptions is the central building block of the family of protocols. \cite{web-wot-arch-terminology}

Finally, Interaction Affordances is the chosen name for exposed functionality for user interaction.
It includes possible Consumer choices and suggestions for how users may interact with the Thing (thus the name affordance, which has been adopted based on the definition of Donald Norman in the field of Human-Computer Interaction).
Interaction Affordances can be further split into properties (exposed state), actions (invokable functions or process triggers) and events (sources for asynchronously pushed data). \cite{web-wot-arch-terminology}

\subsection{Thing Descriptions}

As previously mentioned, a Thing Description is at the heart of the Web of Things.
The Web of Things assumes a hypermedia structure where content is also used for navigation (also known as the \textit{Uniform Interface} constraint of REST), but even in such a structure, there needs to be an entry point.
For a website, the \texttt{index.html} page serves as this entry point, but for Things, the Thing Descriptions serves this purpose. \cite{web-wot-thing-desc}

In case a Thing has not yet been deployed or initialized, a Thing Model is provided instead of a Thing Description.
This Thing Model resembles a template for the actual Thing Description and can be used as a class definition.

Listing~\ref{listing:thing-description} shows a prototypical Thing Description for a Lamp Thing.
As we can see, the Thing Description provides miscellaneous metadata such as an identifier, a title, a type (as linked data) and security definitions.
Furthermore, the functionality of the Lamp Thing is encoded within the properties, actions and events.
Within these sections of the Thing Description, we can find several affordances with their own types, and forms.
The forms contain a \texttt{href} property, fulfilling the Uniform Interface constraint.

\begin{listing}
\begin{minted}{json}
{
    "@context": [
        "https://www.w3.org/2022/wot/td/v1.1",
        { "saref": "https://w3id.org/saref#" }
    ],
    "id": "urn:uuid:300f4c4b-ca6b-484a-88cf-fd5224a9a61d",
    "title": "MyLampThing",
    "@type": "saref:LightSwitch",
    "securityDefinitions": {
        "basic_sc": {"scheme": "basic", "in": "header"}
    },
    "security": "basic_sc",
    "properties": {
        "status": {
            "@type": "saref:OnOffState",
            "type": "string",
            "forms": [{
                "href": "https://mylamp.example.com/status"
            }]
        }
    },
    "actions": {
        "toggle": {
            "@type": "saref:ToggleCommand",
            "forms": [{
                "href": "https://mylamp.example.com/toggle"
            }]
        }
    },
    "events": {
        "overheating": {
            "data": {"type": "string"},
            "forms": [{
                "href": "https://mylamp.example.com/oh"
            }]
        }
    }
}
\end{minted}
\caption{Sample Thing Description from the specification \cite{web-wot-thing-desc}}
\label{listing:thing-description}
\end{listing}

\subsection{WoT Protocol Bindings}

A Protocol Binding is defined to be a mapping of Interaction Affordances to concrete messages of a specific protocol, thus a Protocol Binding realizes concrete functionality.
They are serialized through hypermedia controls, which are web links for purposes of navigation and web forms for ther operations. \cite{web-wot-arch-terminology}

For instance, there is an HTTP Protocol Binding, which describes interaction with a Thing (Web Thing) over HTTP using JSON.
This Binding consists of binding rules for properties, actions and events. \cite{iot-interop-wot}

Protocol bindings make all kinds of integrations possible.
They provide the freedom to map all kinds of existing interfaces to the relatively simple structure of a Thing with a Thing Description.
This can be seen in previously published work also.
The work of Zyrianoff et al. \cite{wot-rest} for instance has outlined a generic mapping from RESTful interfaces to the Web of Things.
Furthermore their work includes an open source utility (aptly) named C3PO with published guidelines on its usage \cite{wot-c3po}.
What is remarkable is that \textit{any} RESTful interface provided in the form of an OpenAPI Specification (OAS) can be converted seamlessly into Web Thing Description of a Web Thing which can then act as a proxy of the actual web application \cite{wot-rest}.

This previous work thus suggests that when mapped at the right level, controls of even a generic protocol offering basic primitives can still be fully contained in a Thing Description.
The simplest mapping is to simply expose all protocol controls as affordances within the Thing itself, resulting in a mirrored structure of the underlying protocol within the Thing Description.
In the case of the RESTful APIs, the affordances are simply HTTP methods (\texttt{GET}, \texttt{POST}, \texttt{PUT}, \texttt{PATCH} and \texttt{DELETE}).
Such a simple mapping can provide its own set of challenges however, since the WoT equivalent structure is usually not an exact match.
All resources are mapped to each supported HTTP method for that resource in a flat hierarchy, with special care being taken to support HTTP parameters.

\section{Bridging the Gap}

In this section we want to examine the appropriate level at which to map the Matter protocol to the WoT protocol and consider a systematic protocol binding.
A Thing Description consists of metadata and affordances (which serve to expose functionality of a Thing), so the question that comes up is how to best extract the metadata and wrap available functionality.

First off, one could either target Matter endpoints or Matter nodes.
Nodes contain one or more endpoints which work tegether to realize some set of desired functionality, so it could make sense to map on the level of nodes.
However, since Clusters are specified not on a node-to-node basis but rather an endpoint-to-endpoint basis, a more fitting functional mapping emerges if we map on the level of endpoints.
Thus we define a Matter enpoint to be a WoT Thing.

One last detail needs to be mentioned.
A Matter device can undergo different phases, Matter defines elaborate procedures for discovery and commissioning for the ever-shifting network topology.
We want to assume that a Matter node has undergone these stages, is a part of the Matter network and ready expose itself as a WoT Thing.

\subsection{Protocol Binding Structure}

First and foremost we must identify components of the system.
We do this using endpoint descriptions in the Matter protocol and metadata fields in WoT Thing Descriptions.
Every matter node must contain a root endpoint which is also used for discovery, as it enumerates other available endpoints within its \texttt{PartsList}.
We will use this to compose a Thing Description.
Now we will turn our attention to individual fields required in a Thing Description.

Our binding will restrict itself to mandatory fields of a Thing Description, as well as an identifier, properties, actions and events.
The most significant part of the binding can be found in the \texttt{properties}, \texttt{actions} and \texttt{events} fields.

The Matter protocol is neatly organized to support composition of functionality.
This is done with Clusters, which describe some functionality with all its related members.
Endpoints can be composed of multiple Clusters and finally nodes can be composed of multiple endpoints.
It is important to note at this point that Clusters are restricted to those which have already been specified.
If a new Cluster becomes necessary, it will have to go through the process of standardization and be added in the next version of the standard.

This is import to mention because it enables us to translate these fixed profiles into templates.
Web of Things supports a similar way to compose functionality, although this is done by using Thing Models instead.
Since Clusters are fixed in a respective version of the Matter standard, we can generate appropriate Thing Models beforehand and instantiate them in a concrete Thing Description based on the Clusters that an endpoint exposes.
Composition happens simply by defining a relation to the Thing Model within the Thing Description.
It is even possible to generate a self-contained Thing Description which includes all definitions of related Thing Models \cite{web-wot-thing-desc}.

\subsubsection{Context}

The \texttt{@context} field is mandatory and specifies which version of the Thing Description is known to the Thing.
This is important since it restricts which terms consumers can use with respect to the Thing.
We will set this to the version of the Thing Description document referenced in \cite{web-wot-thing-desc}.

\subsubsection{Identifier}

The first metadata field we need to translate is the \texttt{id} of the Thing Description.
We could compose identifiers available in the Matter protocol, like an immutable hardware identifier, but the WoT protocol advises against using such immutable identifiers as they could pose a privacy risk.
The suggested mitigation strategy against this risk is to use random UUIDs.
This is not a problem, since WoT identifiers are also (intentionally) not required to be globally unique.
Thus we will use random UUIDs for the \texttt{id} of the Thing Description. \cite{web-wot-thing-desc}

\subsubsection{Security and Security Definitions}

Web of Things provides multiple security standards that we could map onto from available security standards within the Matter protocol.
However, as this would be complicated particularly for a prototype protocol binding, one can default to the \texttt{NoSecurityScheme} scheme here.
This can be done by setting the \texttt{security} field to \texttt{nosec\_sc} and \texttt{securityDefinitions} field to \texttt{\{ "nosec\_sc": \{ "scheme": "nosec" \}\}}.

\subsubsection{Title}

The Matter protocol does not actually store a string containing a descriptive title of an endpoint.
The Device Types however are named in the standard after characteristic appliances that realize a certain set of Clusters, such as a \texttt{Speaker} Device Type or a \texttt{Video Player} Device Type.
By implementing a simple lookup, we can translate a numerical Device Type ID to a title for our WoT \texttt{title} field.

\subsection{Properties}

Properties of a Thing expose the state of said Thing.
They can be read-only or allow writes.
Semantically they can be mapped to Attributes of the Matter protocol.

We can add property fields to our Thing Description by adding each readable property in a Cluster to its respective Thing Model and then including that Thing Model.
In doing so, we need to take care to specify whether a property is read-only or not, as well as its data type.

When invoking a read or write to a Property, we need to wrap a respective read or write on the Attribute of the Matter endpoint corresponding to the Thing.

\subsection{Actions}

Different from Properties, Actions are not limited to the read/write semantic.
Sometimes it is more useful to, for instance, toggle between two possible states instead to provide a facility that more closely resembles the actual use cases of the appliance.
In Matter, this is realized as part of each Cluster within Commands.
They are analogous to verbs in the English language.

When invoking an Action of a Thing, we need to wrap a respective invocation of the Command of the Matter endpoint corresponding to the Thing.

\subsection{Events}

The term Event carries the same meaning within the Matter protocol and the Web of Things protocol.
They both refer to occurrences at some point in the past which can be collected in a stream for a chronological view \cite{matter-spec}.

For each Cluster we are creating a Thing Model for, we need to add all defined events with their respective data types.
Accessing a Thing's Events will be wrapped in an access on the Events of the Matter endpoint corresponding to the Thing.

\section{Example Binding}

We will take a look at an example Device taken out of the Matter Device Library Specification \cite{matter-device-lib}.
The example we chose was a Dimmable Light, which in essence consists of two important Clusters.
All Cluster definitions are taken out of the Matter Application Cluster Specification \cite{matter-app-clusters}.
These Clusters are the \texttt{On/Off} and \texttt{Level Control} clusters respectively.
Thus we will derive two Thing Models and build a Thing Description.
We will include only mandatory functionality of a Cluster.

\subsection{On/Off Cluster}

The first Cluster is the On/Off Cluster.
It contains a mandatory Attribute \texttt{OnOff} of type \texttt{bool} and Commands \texttt{Off}, \texttt{On} and \texttt{Toggle}.
Listing~\ref{listing:cluster-on-off} shows a Thing Model definition for this Cluster.
As can be seen, this cluster is relatively simple.

\begin{listing}
\begin{minted}{json}
{
    "@context": ["https://www.w3.org/2022/wot/td/v1.1"], 
    "@type": "tm:ThingModel",
    "id": "urn:example:{{RANDOM_ID_PATTERN}}",
    "title": "Cluster On/Off",
    "properties": {
        "onOff": {
            "type": "boolean",
	    "readOnly": true
        }
    },
    "actions": {
        "on": {
            "description": "On Command of On/Off Cluster"
        },
        "off": {
            "description": "Off Command of On/Off Cluster"
        },
        "toggle": {
            "description": "Toggle Command of On/Off Cluster"
        }
    }
}
\end{minted}
\caption{Example Thing Model Definition for On/Off Cluster}
\label{listing:cluster-on-off}
\end{listing}

Listing~\ref{listing:cluster-level-control} shows a Thing Model for the Level Control Cluster.
It contains several mandatory Attributes and Commands, though the Commands are very similar to one another.
The Commands are modelled as Actions with an input, since the Commands require mandatory parameters.
For brevity, the Action definitions have been shortened.

\begin{listing}
\begin{minted}{json}
{
    "@context": ["https://www.w3.org/2022/wot/td/v1.1"], 
    "@type": "tm:ThingModel",
    "id": "urn:example:{{RANDOM_ID_PATTERN}}",
    "title": "Cluster Level Control",
    "properties": {
        "currentLevel": {
            "type": "integer",
	    "readOnly": true
        },
        "onLevel": {
            "type": "integer"
        },
        "options": {
            "type": "integer"
        },
    },
    "actions": {
        "moveToLevel": {
            "description": "MoveToLevel Command of Level Control Cluster"
	    "input": {
	        "type": "object",
		"properties": {
		    "moveMode": {
		        "type": "string"
	            },
		    "rate": {
		        "type": "integer"
	            },
		    "optionsMask": {
		        "type": "integer"
	            },
		    "optionsOverride": {
		        "type": "integer"
	            }
		}
	    }
        },
        "move": {
            "description": "Move Command of Level Control Cluster"
	    // shortened for brevity ...
        },
	// ...
    }
}
\end{minted}
\caption{Example Thing Model Definition for Level Control Cluster}
\label{listing:cluster-level-control}
\end{listing}

Finally, we want to compose a Thing Model for the Dimmable Light Device Type from the Device Specification.
Listing~\ref{listing:dimmable-light} shows how we can bring together our two previous Cluster Thing Models.
This is the Thing Model out of which we could instantiate an actual Dimmable Light Thing with its own Thing Description.

\begin{listing}
\begin{minted}{json}
{
    "@context": ["https://www.w3.org/2022/wot/td/v1.1"], 
    "@type": "tm:ThingModel",
    "title": "Dimmable Light",
    "id": "urn:example:{{RANDOM_ID_PATTERN}}",
    "description": "Dimmable Light Matter Device",
    "links" : [{
	"rel": "extends",
	"href": "http://localhost/ClusterOnOff",
	"type": "application/tm+json"
    },
    {
	"rel": "extends",
	"href": "http://localhost/ClusterLevelControl",
	"type": "application/tm+json"
    }],
}
\end{minted}
\caption{Example Thing Model Definition for Dimmable Light, composed of two Cluster definitions}
\label{listing:dimmable-light}
\end{listing}

\section{Conclusion}

There is a lot more to consider for a more rigorous protocol binding.
One weakness of this protocol binding is that there is no exact mapping between available data types of Matter and the Web of Things.
The protocol binding should be efficient however:
As the Matter Application Clusters Specification is static, as well as the Matter Device Library Specification (and the Matter Core Specification itself), we will need to generate appropriate Thing Models just once for all available Clusters in the Matter specifications.

Not all aspects of the Matter specification has been considered, due to its size.
For instance, security features could be provided by mapping the security facilities provided by Matter and Web of Things.
However, such minute detail would have gone beyond the scope of this paper.

\bibliographystyle{IEEEtran}
\bibliography{references}

\end{document}
