# Review of Paper 16 by Jurij Cerar

## 24th May 2024

The paper is quite concise, getting into the heart of the matter relatively quickly.
A short overview of the issue at hand, as well as the intention of this paper is provided.
Afterwards, both Matter and Web of Things are introduced, sparing any words on unnecessary detail.

There is a red thread throughout the paper, it becomes increasingly apparent how the aforementioned issue is intended to be solved.
No corners are cut, as even security features are mapped from available security standards within the Matter protocol to the Web of Things protocol binding.

Perhaps one weakness of the paper is that only an example binding is provided, but no generalization or methodology by which to create a binding for any Matter device or their clusters.
It is somewhat obvious how that would be done, but some more detail on whether a generalization to all kinds of clusters is even possible using this approach would have been fitting.
Nevertheless, it is quite a sensible description of a protocol binding and does tackle the problem stated at the outset.

The derived Thing description is mentioned in brief snippets pertaining to the current section.
That is well done, although it would have helped to understand the big picture if all parts would have been brought together to make a complete Thing description like in the example in Listing 1.
The length of this concise paper would have allowed for such a listing, perhaps at the end of the paper.

Sources are well chosen, as the paper mentions relevant papers in this field to substantiate claims, and otherwise provides links to official specifications or other credible literature.
It does not seem like there are any claims left unsubstantiated, there are no dubious sources or otherwise unfitting citations.

Putting everything together, the paper has been quite impressive in providing a legitimate solution the stated problem, the detail of the paper makes that possible.
With minor changes, the paper can be presented proudly.

Lastly we want to provide some small, uncategorized suggestions for improvement of this paper:

- You have mentioned HTTP requests such as GET or POST in chapter 3 as HTTPS GET or POST, but in our view these should be delineated as HTTP requests, since security is optional.

- Spelling is not entirely consistent, with listings usually beginning with capitalized letters, whereas tables are kept uncapitalized.

- In Table 1 mandatory fields are correctly emboldened, but perhaps it needs to be mentioned that each Thing description must supply at least one interaction affordance.
In this sense, none of Properties, Actions and Events are strictly mandatory, but at least one of them must contain an interaction affordance.

- Listing 4 has an error with the closing braces on line 9, the normal and curved braces should be swapped.