# Review of Paper 1 by Nadin Weichenrieder

## 25th May 2024

Overall the paper has a nice reading flow and is also kept quite concise, though not everything in the paper is relevant to the problem statement.
There is clearly a red thread and the solution is being introduced bit by bit.

It is not entirely clear what problem the paper is intended to solve.
The title is a big hint and it is very briefly mentioned in the Abstract, but in our view the paper would have benefitted from a concrete problem statement somewhere in the Introduction, currently the problem statement seems to be how much Matter and Web of Things align, which is not actually the focus of the paper.

In sections 2 and 3, references are often placed after entire paragraphs, with over three references at once.
This is generally fine, although it makes it difficult to attribute what statement is substantiated by which reference.
With that many references, it might be worth it to delineate them more clearly by placing them more accurately.

Listing 1 is not well-placed, as it interrupts the flow of reading by appearing in-between an enumeration.
In our view the Listing should have at least followed the enumeration.

There are also some problems with the solution.
In Listing 2, a TD specific to Matter is presented, but it is not at all different from the example already provided in Listing 1.
In fact it seems like only the title has been altered and a description added.
There are several questions that arise after looking at Listing 2.
How does this Thing Description really map to a Matter device?
The Web of Things specification mentions that IDs should preferably not be fixed, why was the IPv6 address chosen for the ID field?
Is the ID provided in Listing 2 even a valid IPv6 address?
Right now it looks more like a UUID, but of this we are not certain.

Furthermore, in Listing 3, we can see a BT for Matter, but this is a copy of the BT example adapted from the Web of Things specification.
It should at least be attributed to the specification.
However, is it even accurate, does Matter support HTTP communication?
It is mentioned that a gateway will translate any requests and this might work fine.
But is there only a single gateway in the network, or does each Matter device require its own gateway?
If there is only one gateway, how will the gateway be able to infer the correct Matter device, as the endpoints depicted in Listing 3 are not specific to the Matter device.
Perhaps this could be solved by providing a more elaborate URI instead.

There are some spelling errors left unattended sadly, at least in the Abstract ("Descripitons" instead of "Descriptions") it is important to avoid spelling errors!
There are also other spelling errors that could likely have been captured by using a spellchecker, such as "this technologies" in the Introduction.
Another spelling error is "deviceâs" or "physicle" in section 4.

In summary, the paper easy to follow and fulfills the basic requirements of a scientific paper.
Nonetheless, we are not entirely convinced that an actual solution has been presented, as it is unclear how the problem is solved.
Providing more additional information would have been possible, as there are still pages available without going over the intended length, this could have clarified the previously mentioned uncertainties.